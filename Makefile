all: tex

tex:
	find . -mindepth 2 -type f -name '*.tex' -printf '%h\n' | sort -u | parallel "cd {} && latexmk"
