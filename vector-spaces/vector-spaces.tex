\documentclass[usenames,dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}

\title{Vector Spaces}
\subtitle{Math 218}

\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\text{\ding{51}}}%
\newcommand{\xmark}{\text{\ding{55}}}%


\begin{document}

\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Definitions}
\subsection{Subcollections of $\mathbb{R}^n$}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    If $V$ is a subcollection of $\mathbb{R}^n$, then we write
    $V\subset\mathbb{R}^n$ or $V\subseteq\mathbb{R}^n$.
  \end{definition}


  \pause
  \begin{example}
    Let $V=\Set*{\langle1,0\rangle, \langle3, -34\rangle,
      \langle22,-22\rangle}$. Then $V\subset\mathbb{R}^2$.
  \end{example}

  \pause
  \begin{example}
    Let $V$ consist of all $\vv{v}=\langle v_1, v_2, v_3\rangle$ where
    $v_3=11$. \pause Then $V\subset\pause\mathbb{R}^3$. \pause Note that
    $\langle2,-22,11\rangle\in V$ but $\langle-2,1,0\rangle\notin V$.
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}[Set-Builder Notation]
    The notation $V=\Set*{\vv{v}\in\mathbb{R}^n\given P}$ means ``$V$ consists
    of all $\vv{v}\in\mathbb{R}^n$ that satisfy the property $P$.''
  \end{definition}

  \pause
  \begin{block}{$V=\Set*{\langle v_1,v_2\rangle\in\mathbb{R}^2\given v_2\geq 0}$}
    Note that $\langle 1, 3\rangle\in V$ but $\langle1,-3\rangle\notin V$.
  \end{block}

  \pause
  \begin{block}{$S=\Set*{\vv{v}\in\mathbb{R}^4\given\norm{\vv{v}}=1}$}
    Note that $\langle1, 0, 0, 0\rangle\in S$ but
    $\langle1, 1, 1, 1\rangle\notin S$.
  \end{block}
\end{frame}



\subsection{Vector Subspaces of $\mathbb{R}^n$}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A subcollection $V$ of $\mathbb{R}^n$ is a \emph{vector subspace of
      $\mathbb{R}^n$} if
    \begin{description}[<+->][$V$ respects addition]
    \item[$V$ respects zero] $\vv{O}\in V$
    \item[$V$ respects addition] $\vv{v},\vv{w}\in V$ implies $\vv{v}+\vv{w}\in V$
    \item[$V$ respects scaling] $c\in\mathbb{R}$ and $\vv{v}\in V$ implies $c\cdot\vv{v}\in V$
    \end{description}
  \end{definition}

  \onslide<+->
  \begin{block}{Idea}
    Vector subspaces of $\mathbb{R}^n$ are ``closed'' under linear combinations.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is $V=\Set*{\langle v_1,v_2\rangle\in\mathbb{R}^2\given v_2=1}$ a vector
    subspace of $\mathbb{R}^2$?
  \end{example}

  \pause
  \begin{block}{Solution}
    We can check each axiom individually.
    \begin{enumerate}[<+->]
    \item $\vv{O}\notin V$ since $\vv{O}=\langle0,0\rangle$ but $0\neq
      1$ \onslide<+->\xmark
    \item $\langle0,1\rangle\in V$ and $\langle1,1\rangle\in V$
      but $\langle0,1\rangle+\langle1,1\rangle\notin V$ \onslide<+->\xmark
    \item $\langle0,1\rangle\in V$ but
      $2\cdot\langle0,1\rangle=\langle0,2\rangle\notin V$ \onslide<+->\xmark
    \end{enumerate}
  \end{block}
\end{frame}


% \begin{frame}
%   \frametitle{\secname}
%   \framesubtitle{\subsecname}

%   \begin{example}
%     Is $V=\Set*{\vv{v}\in\mathbb{R}^3\given \norm{\vv{v}}=1}$ a vector subspace
%     of $\mathbb{R}^3$?
%   \end{example}

%   \pause
%   \begin{block}{Solution}
%     We can check each axiom individually.
%     \begin{enumerate}[<+->]
%     \item $\vv{O}\notin V$ since $\norm{\vv{O}}=0\neq 1$ \onslide<+->\xmark
%     \item $\langle1, 0, 0\rangle, \langle0,1,0\rangle\in V$ but
%       $\langle1,0,0\rangle+\langle0,1,0\rangle=\langle1,1,0\rangle\notin V$
%       \onslide<+->\xmark
%     \item $\langle1,0,0\rangle\in V$ but
%       $-10\cdot\langle1,0,0\rangle=\langle-10,0,0\rangle\notin V$
%       \onslide<+->\xmark
%     \end{enumerate}
%   \end{block}
% \end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is
    $V=\Set*{\langle x,y\rangle\in\mathbb{R}^2\given x\geq0\textnormal{ and
      }y\geq0}$ a vector subspace of $\mathbb{R}^2$?
  \end{example}

  \pause
  \begin{block}{Solution}
    We can check the axioms individually.
    \begin{enumerate}[<+->]
    \item $\vv{O}\in V$ since $\vv{O}=\langle0,0\rangle$ \onslide<+->\cmark
    \item Suppose that $\langle v_1,v_2\rangle\in V$ and
      $\langle w_1,w_2\rangle\in V$. \onslide<+->This means that
      $v_1,v_2,w_1,w_2\geq\onslide<+-> 0$. \onslide<+->Then
      \[
        \langle v_1,v_2\rangle+\langle w_1,w_2\rangle=\langle v_1+w_1,v_2+w_2\rangle\in V
      \]
      since $v_1+w_1\geq 0$ and $v_2+w_2\geq 0$. \onslide<+->\cmark
    \item $\langle1,0\rangle\in V$ but
      $-22\cdot\langle1,0\rangle=\langle-22,0\rangle\notin V$ \onslide<+->\xmark
    \end{enumerate}
  \end{block}
\end{frame}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is $V=\Set*{\vv{v}\in\mathbb{R}^2\given\vv{v}=\langle x, 2\,x\rangle}$ a
    vector subspace of $\mathbb{R}^2$?
  \end{example}

  \pause
  \begin{block}{Solution}
    We can check the axioms individually.
    \begin{enumerate}[<+->]
    \item Note that
      $\vv{O}=\langle0,0\rangle=\langle0, 2\cdot0\rangle\in V$ \onslide<+->\cmark
    \item Suppose that $\langle x, 2\,x\rangle\in V$ and
      $\langle y,2\,y\rangle\in V$. \onslide<+->Then
      \[
        \left\langle x,2\,x\rangle+\langle y,2\,y\rangle
          =\langle x+y, 2\,x+2\,y\right\rangle\in V\quad \onslide<+->\cmark
      \]
    \item Suppose that $\langle x, 2\,x\rangle\in V$. \onslide<+->Then 
      \[
        c\cdot\langle x, 2\,x\rangle
        =\langle cx, 2\,cx\rangle
        \in V\quad \onslide<+->\cmark
      \]
    \end{enumerate}
  \end{block}
\end{frame}


\section{The Four Fundamental Subspaces}
\subsection{Null Spaces}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $A$ be a $m\times n$ matrix and let 
    $
    V=\Set{\vv{x}\in\mathbb{R}^n\given A\vv{x}=\vv{O}}
    $.
    Then $V$ is a vector subspace of $\mathbb{R}^n$.
  \end{theorem}

  \pause
  \begin{proof}
    We can check the axioms individually.
    \begin{enumerate}[<+->]
    \item Note that $\vv{O}\in V$ since $A\vv{O}=\vv{O}$.
    \item Suppose that $\vv{x}\in V$ and that $\vv{y}\in V$. \onslide<+->Then
      \[
        A(\vv{x}+\vv{y})
        = A\vv{x}+A\vv{y}
        = \vv{O}+\vv{O}
        = \vv{O}
      \]
      \onslide<+->Thus $\vv{x}+\vv{y}\in V$.
    \item Suppose that $\vv{x}\in V$. \onslide<+->Then
      $A(c\cdot\vv{x})=c\cdot A\vv{x}=c\cdot\vv{O}=\vv{O}$. \onslide<+->Hence
      $c\cdot\vv{x}\in V$.
    \end{enumerate}
    \onslide<+->All axioms pass!
  \end{proof}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $A$ be a $m\times n$ matrix. The vector subspace
    \[
      \Null(A)=\Set{\vv{x}\in\mathbb{R}^n\given A\vv{x}=\vv{O}}
    \]
    of $\mathbb{R}^n$ is called the \emph{null space of $A$}. 
  \end{definition}

  \pause
  \begin{block}{Note}
    Strang writes $N(A)$ instead of $\Null(A)$.
  \end{block}

  \pause
  \begin{block}{Idea}
    $\Null(A)$ consists of all solutions $\vv{x}$ to the system $A\vv{x}=\pause\vv{O}$.
  \end{block}

\end{frame}


\begin{sagesilent}
  set_random_seed(14971)
  A = random_matrix(ZZ, 3, 4, algorithm='echelonizable', rank=2)
  n1, n2 = A.right_kernel().basis()
  r1, r2 = A.row_space().basis()
  u = n1+n2
  v = n1-n2+r1
  w = 2*n1+n2-r2
  Au, Av, Aw = A*u, A*v, A*w
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $A=\sage{A}$. Which of the vectors
    \begin{align*}
      \vv{u} &= \sage{u} & \vv{v} &= \sage{v} & \vv{w} &= \sage{w}
    \end{align*}
    is in $\Null(A)$?
  \end{example}

  \pause
  \begin{block}{Solution}
    Note that
    \begin{align*}
      A\vv{u} &= \sage{Au} & A\vv{v} &= \sage{A*v} & A\vv{w} &= \sage{Aw}
    \end{align*}
    \pause Ths means that $\vv{u}\in\Null(A)$ while $\vv{v}\notin\Null(A)$ and
    $\vv{w}\notin\Null(A)$.
  \end{block}
\end{frame}



\begin{sagesilent}
  A = matrix(2, 3, [-3,-4,3,0,6,-1])
  var('x y z')
  vvx = matrix.column([x, y, z])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the subcollection of $\mathbb{R}^3$ given by
    \[
      V=
      \Set*{\langle x,y,z\rangle\in\mathbb{R}^3\given
        \begin{array}{rcrcrcr}
          -3\,x & - & 4\,y & + & 3\,z & = & 0 \\
                &   & 6\,y & - &    z & = & 0
        \end{array}}
    \]
    Is $V$ a vector subspace of $\mathbb{R}^3$?
  \end{example}

  \pause
  \begin{block}{Solution}
    Note that the equations defining $V$ are exactly the equations given by
    $A\vv{x}=\vv{O}$ where
    \begin{align*}
      A &= \sage{A} & \vv{x} &= \sage{vvx}
    \end{align*}
    \pause This means that $V=\Null(A)$, so $V$ is a vector subspace of
    $\mathbb{R}^3$.
  \end{block}
\end{frame}



\subsection{Column Spaces}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $A$ be an $m\times n$ matrix and let
    $V=\Set{\vv{v}\in\mathbb{R}^m\given A\vv{x}=\vv{v}}$. Then $V$ is a vector
    subspace of $\mathbb{R}^m$.
  \end{theorem}
  \onslide<+->
  \begin{proof}
    We can check the axioms individually.
    \begin{enumerate}[<+->]
    \item Note that $\vv{O}\in V$ since $A\vv{O}=\vv{O}$.
    \item Suppose that $\vv*{v}{1}\in V$ and $\vv*{v}{2}\in V$. This means that
      $\vv*{v}{1}=A\vv*{x}{1}$ and $\vv*{v}{2}=A\vv*{x}{2}$. Then
      \[
        \vv*{v}{1}+\vv*{v}{2}
        = A\vv*{x}{1}+A\vv*{x}{2}
        = A(\vv*{x}{1}+\vv*{x}{2})
      \]
      Thus $\vv*{v}{1}+\vv*{v}{2}\in V$.
    \item Suppose that $\vv{v}\in V$. This means that $\vv{v}=A\vv{x}$. Then
      $c\cdot\vv{v}=c\cdot A\vv{x}=A(c\cdot\vv{x})\in V$.
    \end{enumerate}
    \onslide<+->All axioms pass!
  \end{proof}

\end{frame}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $A$ be a $m\times n$ matrix. The \emph{column space} of $A$ is defined
    as 
    \[
      \Col(A)=\Span\Set*{\textnormal{columns of }A}
    \]
    Note that $\Col(A)$ is a vector subspace of $\mathbb{R}^m$.
  \end{definition}


  \pause
  \begin{block}{Note}
    Strang writes $C(A)$ instead of $\Col(A)$.
  \end{block}

  \pause
  \begin{block}{Idea}
    $\Col(A)$ collects all ``outputs'' obtained by multiplying $A$ by a
    vector. \pause These are the vectors $\vv{v}$ that make $A\vv{x}=\vv{v}$
    \pause \emph{consistent}.
  \end{block}
\end{frame}



\begin{sagesilent}
  set_random_seed(189)
  M = random_matrix(ZZ, 3, 4, algorithm='echelonizable', rank=3, upper_bound=10)
  while M.pivots() != (0, 1, 3): M = random_matrix(ZZ, 3, 4, algorithm='echelonizable', rank=3, upper_bound=10)
  a1, a2, u, v = M.columns()
  A = matrix.column([a1, a2])
  M = A.augment(matrix.column([u, v]), subdivide=True)
  u = matrix.column(u)
  v = matrix.column(v)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $A$ and the vectors $\vv{u}$ and $\vv{v}$ given by
    \begin{align*}
      A &= \sage{A} & \vv{u} &= \sage{u} & \vv{v} &= \sage{v}
    \end{align*}
    Are either of $\vv{u}$ or $\vv{v}$ in $\Col(A)$?
  \end{example}

  \pause
  \begin{block}{Solution}
    Note that
    \[
      \rref\sage{M}=\sage{M.rref()}
    \]
    \pause This means that $\vv{u}\in\Col(A)$ but $\vv{v}\notin\Col(A)$.
  \end{block}

\end{frame}



\begin{sagesilent}
  A = matrix(SR, [(-7, 27, 14, 34), (1, -4, -2, -5), (4, -16, -8, -20)])
  import gj
  g = gj.GJ(A)
  E = g.rref_inverse()
  E = E.change_ring(SR)
  var('b1 b2 b3')
  b = vector([b1, b2, b3])
  M = A.augment(b, subdivide=True)
  e1, e2, e3 = E.rows()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $A$ given by
    \[
      A = \sage{A}
    \]
    Describe all vectors in $\Col(A)$.
  \end{example}

  \pause
  \begin{block}{Solution}
    Row-reducing gives
    \[
      \sage{M}
      \rightsquigarrow\sage{E*M}
    \]
    \pause The vectors in $\Col(A)$ are the vectors orthogonal to \pause
    $\sage{e3}$. \pause Note that $\Col(A)\neq\mathbb{R}^3$.
  \end{block}

\end{frame}




\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    We define $\Span\Set{\vv*{v}{1}, \vv*{v}{2}, \dotsc, \vv*{v}{n}}$ as the
    collection of all linear combinations of
    $\Set{\vv*{v}{1}, \vv*{v}{2}, \dotsc, \vv*{v}{n}}$.
  \end{definition}

  \pause
  \begin{theorem}
    $\Span\Set{\vv*{v}{1}, \vv*{v}{2}, \dotsc, \vv*{v}{n}}=\Col\begin{bmatrix}\vv*{v}{1} & \vv*{v}{2} & \dotsb & \vv*{v}{n}\end{bmatrix}$
  \end{theorem}

\end{frame}

\begin{sagesilent}
  A = matrix([(2, 12, -14, -14), (-5, -19, 24, 24), (-1, -8, 9, 9)])
  b = vector([3,10,-1])
  M = A.augment(b, subdivide=True)
  v1, v2, v3, v4 = [matrix.column(col) for col in A.columns()]
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is $\vv{b}={\footnotesize\sage{matrix.column(b)}}$ in
    ${\footnotesize\Span\Set*{\sage{v1},\sage{v2},\sage{v3},\sage{v4}}}$?
  \end{example}
  
  \pause
  \begin{block}{Solution}
    Note that
    \[
      \rref\sage{M}=\sage{M.rref()}
    \]
    \pause This means that the equation
    \[
      c_1\cdot\vv*{v}{1}
      +c_2\cdot\vv*{v}{2}
      +c_3\cdot\vv*{v}{3}
      +c_4\cdot\vv*{v}{4}
      =\vv{b}
    \]
    is unsolvable. \pause Thus
    $\vv{b}\notin\Span\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3},\vv*{v}{4}}$.
  \end{block}
\end{frame}


\begin{sagesilent}
  A = matrix([(16, -64, 32), (5, -19, 10), (5, -20, 10)])
  b = matrix.column([-160, -48, -50])
  M = A.augment(b, subdivide=True)
  v1, v2, v3 = [matrix.column(col) for col in A.columns()]
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is $\vv{b}={\footnotesize\sage{b}}$ in $\Span{\footnotesize\Set*{\sage{v1},
        \sage{v2}, \sage{v3}}}$?
  \end{example}
  
  \pause
  \begin{block}{Solution}
    Note that
    \[
      \rref\sage{M}=\sage{M.rref()}
    \]
    \pause This means that the equation
    \[
      c_1\cdot\vv*{v}{1}
      +c_2\cdot\vv*{v}{2}
      +c_3\cdot\vv*{v}{3}
      =\vv{b}
    \]
    has infinitely many solutions. \pause Thus
    $\vv{b}\in\Span\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3}}$.
  \end{block}
\end{frame}



\subsection{Row Spaces}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $A$ be a $m\times n$ matrix. The \emph{row space} of $A$ is defined
    as 
    \[
      \Row(A)=\Span\Set*{\textnormal{rows of }A}
    \]
    \pause
    Note that $\Row(A)$ is a vector subspace of \pause $\mathbb{R}^n$. 
  \end{definition}


  \pause
  \begin{theorem}
    $\Row(A)=\Col(A^\intercal)$
  \end{theorem}
\end{frame}


\subsection{Left Null Spaces}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $A$ be a $m\times n$ matrix. The \emph{left null space} of $A$ is
    defined as
    \[
      \LNull(A)=\Set*{\vv{x}\in \mathbb{R}^{m}\given \vv{x}^\intercal A=\vv{O}}
    \]
    \pause
    Note that $\LNull(A)$ is a vector subspace of \pause $\mathbb{R}^m$. 
  \end{definition}


  \pause
  \begin{theorem}
    $\LNull(A)=\Null(A^\intercal)$
  \end{theorem}
\end{frame}





\section{The Four Fundamental Subspaces}
\subsection{The ``Big Picture''}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \begin{block}{The Four Fundamental Subspaces}
    A $m\times n$ matrix $A$ has \emph{four fundamental subspaces}.
    \begin{center}
      \begin{tabular}{rll}
        \multicolumn{1}{c}{\myotherbold{$V$}} & \multicolumn{1}{c}{\myotherbold{$V\subset\mathbb{R}^{?}$}} & \multicolumn{1}{c}{\myotherbold{$\vv{v}\in V$}} \\ 
        \hline\pause
        \mybold{Column Space} & $\Col(A)\subset\mathbb{R}^m$ & $A\vv{x}=\vv{v}$ consistent \\ \pause
        \mybold{Null Space} & $\Null(A)\subset\mathbb{R}^n$ & $A\vv{v}=\vv{O}$ \\ \pause
        \mybold{Row Space} & $\Row(A)\subset\mathbb{R}^n$ & $A^\intercal\vv{x}=\vv{v}$ consistent \\ \pause
        \mybold{Left Null Space} & $\LNull(A)\subset\mathbb{R}^m$ & $A^\intercal\vv{v}=\vv{O}$
      \end{tabular}
    \end{center}
  \end{block}

  \pause
  \begin{block}{Note}
    Strang uses the notation $C(A)=\Col(A)$ and $N(A)=\Null(A)$. 
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \[
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , scale=4/5
      ]

      \coordinate (O) at (0, 0);
      \coordinate (P) at (1, 2);
      \coordinate (Q) at (2, -1);
      \coordinate (Rn) at ($ 1.15*(P)-(Q) $);
      \coordinate (Row) at ($ .5*(P)-(Q) $);
      \coordinate (Null) at ($ .5*(Q)-.5*(P) $);

      \node at (Rn) {$\mathbb{R}^n$};

      \onslide<5->{
        \filldraw[ultra thick, fill=YellowOrange]
        (O) -- (P) -- ($ (P)-2*(Q) $) -- ($ -2*(Q) $) -- cycle;
        \node at (Row) {$\Row(A)$};
      }


      \onslide<4->{
        \filldraw[ultra thick, fill=Green!40]
        (O) -- (Q) -- ($ (Q)-(P) $) -- ($ -1*(P) $) -- cycle;
        \node at (Null) {$\Null(A)$};
      }

      \coordinate (v) at (-1, 2);
      \coordinate (w) at (-2, -1);
      \coordinate (Rm) at ($ 1.15*(v)-1*(w) $);
      \coordinate (Col) at ($ .5*(v)-1*(w) $);
      \coordinate (LNull) at ($ .5*(w)-.5*(v) $);

      \tikzset{
        c/.style={every coordinate/.try}
      }

      \begin{scope}[every coordinate/.style={shift={(5,0)}}]


        \node at ([c]Rm) {$\mathbb{R}^m$};

        \onslide<3->{
          \filldraw[ultra thick, fill=BrickRed!50]
          ([c]O) -- ([c]v) -- ([c]$ (v)-2*(w) $) -- ([c]$ -2*(w) $) -- cycle;
          \node at ([c]Col) {$\Col(A)$};
        }

        \onslide<6->{
          \filldraw[ultra thick, fill=Blue!50]
          ([c]O) -- ([c]w) -- ([c]$ (w)-(v) $) -- ([c]$ -1*(v) $) -- cycle;
          \node at ([c]LNull) {$\LNull(A)$};
        }


        \draw[ultra thick, shorten >=1.5em, shorten <=1.5em, ->]
        (Rn) -- ([c]Rm) node[midway, above] {$A$};


        \onslide<2->{
          \path[bend right, ultra thick, shorten >=1.5em,shorten <=1.5em]
          ([c]Rm) edge["$A^\intercal$"'] (Rn);

          \path[bend right, ultra thick, ->, shorten >=1.5em,shorten <=1.5em]
          ([c]Rm) edge (Rn);
        }

      \end{scope}

    \end{tikzpicture}
  \]

\end{frame}









\subsection{Working with Column Spaces}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Produce examples of $\vv{v}\in\Col(A)$}
    This problem is \emph{easy}. \pause Simply multiply $A$ by any vector $\vv{x}$.
  \end{block}

\end{frame}

\begin{sagesilent}
  A = matrix.column([(1, 3, 2), (0, 4, 1)])
  x1 = matrix.column([1, 1])
  x2 = matrix.column([1, -1])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}
  \begin{example}
    The matrix-vector products
    \begin{align*}
      \overset{A}{\sage{A}}\sage{x1} &= \sage{A*x1} & \overset{A}{\sage{A}}\sage{x2} &= \sage{A*x2}
    \end{align*}
    show that $\sage{vector(A*x1)}\in\Col(A)$ and
    $\sage{vector(A*x2)}\in\Col(A)$.
  \end{example}
\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Determine if $\vv{v}\in\Col(A)$}
    This problem is \emph{hard}. \pause Given $\vv{v}$, we must check if the system
    \[
      A\vv{x}=\vv{v}
    \]
    is \emph{consistent}.
  \end{block}

\end{frame}



\begin{sagesilent}
  set_random_seed(4792)
  A = random_matrix(ZZ, 3, 2)
  b = random_matrix(ZZ, 3, 1)
  M = A.augment(b, subdivide=True)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $A$ and the vector $\vv{v}$ given by
    \begin{align*}
      A &= \sage{A} & \vv{v} &= \sage{b}
    \end{align*}\pause
    The Gau\ss-Jordan algorithm gives
    \[
      \rref\sage{M}=\sage{M.rref()}
    \]
    \pause The system $A\vv{x}=\vv{v}$ is inconsistent, so
    $\vv{v}\notin\Col(A)$.
  \end{example}

\end{frame}


\subsection{Working with Null Spaces}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Determine if $\vv{v}\in\Null(A)$}
    This problem is \emph{easy}. \pause Simply check if $A\vv{v}=\vv{O}$.
  \end{block}

\end{frame}


\begin{sagesilent}
  A = matrix.column([(0, 1, -1, 2), (-1, 3, -2, 0), (1, -1, 0, 4)])
  v1 = matrix.column([-1, 1, -1])
  v2 = matrix.column([2, -1, -1])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The equations
    \begin{gather*}
      \begin{align*}
        \overset{A}{\sage{A}}\sage{v1} &= \sage{A*v1} & \overset{A}{\sage{A}}\sage{v2} &= \sage{A*v2}
      \end{align*}
    \end{gather*}
    show that $\sage{vector(v1)}\notin\Null(A)$ and
    $\sage{vector(v2)}\in\Null(A)$.
  \end{example}
\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Produce examples of $\vv{v}\in\Null(A)$}
    This problem is \emph{hard}. \pause We must find all $\vv{v}$ satisfying
    $A\vv{v}=\vv{O}$.
  \end{block}

\end{frame}


\begin{sagesilent}
  A = matrix.column([(1, 3, 3, 1), (4, 13, 7, -1), (-12, -38, -26, -2)])
  var('v1 v2 v3')
  v = matrix.column([v1, v2, v3])
  vs = v(v1=4*v3, v2=2*v3)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The Gau\ss-Jordan algorithm gives
    \[
      \rref\overset{A}{\sage{A}}=\sage{A.rref()}
    \]
    \pause The vectors $\vv{v}\in\Null(A)$ are then of the form
    \[
      \vv{v}
      = \sage{v}
      = \sage{vs}
      = v_3\sage{jacobian(vs, (v3))}
    \]
  \end{example}


\end{frame}


\section{Converting Between Column and Null Spaces}
\begin{frame}

  \frametitle{\secname}

  \begin{theorem}
    Let $V\subset\mathbb{R}^n$. Then there is a $n\times k$ matrix $A$ and a
    $m\times n$ matrix $B$ satisfying $V=\Col(A)=\Null(B)$.
  \end{theorem}

\end{frame}


\subsection{Converting Null Spaces to Column Spaces}
\begin{sagesilent}
  A = matrix([(-4, -28, 17, 35), (-5, -35, 21, 43)])
  var('x1 x2 x3 x4 c1 c2')
  x = matrix.column([x1, x2, x3, x4])
  xs = x(x1=-7*c1-4*c2, x2=c1, x3=-3*c2, x4=c2)
  B = jacobian(xs, (c1, c2))
  b1, b2 = map(matrix.column, B.columns())
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Consider the computation
  \[
    \rref\overset{A}{\sage{A}}=\sage{A.rref()}
  \]
  \pause This means that every $\vv{x}\in\Null(A)$ is of the form
  \[
    \overset{\vv{x}}{\sage{x}}
    = \sage{xs}
    = c_1\sage{b1}+c_2\sage{b2}
  \]
  \pause This shows that
  \[
    \Null\sage{A}=\Col\sage{B}
  \]

\end{frame}


\subsection{Converting Column Spaces to Null Spaces}
\begin{sagesilent}
  A = matrix.column(SR, [(1, 0, 0, -2), (-1, 1, -5, -2), (-1, 3, -15, -10)])
  var('b1 b2 b3 b4')
  b = matrix.column([b1, b2, b3, b4])
  M = A.augment(b, subdivide=True)
  import gj
  g = gj.GJ(A)
  E = g.rref_inverse()
  E = E.change_ring(SR)
  e1, e2, e3, e4 = E.rows()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Consider the computation
  \[
    \sage{M}
    \rightsquigarrow
    \sage{A.rref().augment(E*b, subdivide=True)}
  \]
  \pause The vectors making this system consistent are the vectors orthogonal to
  $\sage{e3}$ and $\sage{e4}$. \pause This shows that
  \[
    \Col\sage{A}
    =
    \Null\sage{matrix([e3, e4])}
  \]

\end{frame}




\end{document}
