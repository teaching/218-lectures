\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\usepackage{booktabs}
% \usepackage{tikz-3dplot}

\title{Complex Numbers}
\subtitle{Math 218}


\begin{document}

\begin{sagesilent}
  S = GaussianIntegers(names='i')
\end{sagesilent}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Complex Numbers}
\subsection{Basic Definitions}

\begin{sagesilent}
  z = 3*I-2
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{complex number} is an expression of the form
    \[
      z = {\color<4->{beamblue}\tikzmark{b}b}\cdot{\color<2->{red}\tikzmark{i}i}+{\color<3->{beamgreen}\tikzmark{a}a}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , remember picture
        , overlay
        ]

        \onslide<4->{
          \draw[<-, thick, beamblue]
          ([shift={(3pt,-2pt)}]pic cs:b) |- ([shift={(-10pt, -10pt)}]pic cs:b)
          node[anchor=east](a) {``imaginary part'' $\Im(z)\in\mathbb{R}$};
        }

        \onslide<2->{
          \draw[<-, thick, red]
          ([shift={(4pt,-2pt)}]pic cs:i) |- ([shift={(-15pt,-25pt)}]pic cs:i)
          node[anchor=east](b) {``imaginary unit'' $i^2=-1$};
        }

        \onslide<3->{
          \draw[<-, thick, beamgreen]
          ([shift={(4pt,-2pt)}]pic cs:a) |- ([shift={(15pt,-25pt)}]pic cs:a)
          node[anchor=west](c) {``real part'' $\Re(z)\in\mathbb{R}$};
        }

        \node[fit=(a)(b)(c),inner sep=0pt](f){};
        \path let \p1=($(pic cs:a)-(f.south)$) in
        \pgfextra{\xdef\myh{\y1}};
      \end{tikzpicture}
      \vspace*{\myh}
    \]
    \onslide<5->{The collection of complex numbers is denoted by $\mathbb{C}$.}
  \end{definition}

  \onslide<6->
  \begin{example}
    $\Re(\sage{z})=\onslide<7->\sage{z.real()}$ \onslide<8->and
    $\Im(\sage{z})=\onslide<9->\sage{z.imag()}$
  \end{example}

\end{frame}


\begin{sagesilent}
  z1 = 3*I-2
  z2 = -7*I+1
  z3 = -16*I
  r1 = 17
  r2 = 0
  r3 = -11
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    $\mathbb{R}\subset\mathbb{C}$ but $\mathbb{C}\not\subset\mathbb{R}$
    \[
      \tikzstyle{mybox} = [
      , draw=black
      , ultra thick
      , rectangle
      , rounded corners
      , inner sep=16cm
      , inner ysep=9cm
      ]
      \tikzstyle{fancytitle} = [
      , rounded corners
      , fill=white
      , ultra thick
      , draw=black
      ]
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        ]

        \node [mybox, scale=1/4] (complex) {};
        \node [mybox, scale=1/8] (real) {};


        \node (num) {$\sage{r1}$, $\sage{r2}$, $\sage{r3}$, $\pi$, $e$,
          \textellipsis};

        \node[above right=5pt and 10pt] at (complex.south west) {$\sage{z1}$,
          $\sage{z2}$, $\sage{z3}$,\textellipsis};

        \node[fancytitle, right=10pt] at (complex.north west)
        {Complex Numbers $\mathbb{C}$};

        \node[fancytitle, right=10pt] at (real.north west)
        {Real Numbers $\mathbb{R}$};

      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{sum} of two complex numbers is
    \[
      (b_1\cdot i+a_1)+(b_2\cdot i+a_2)
      = (b_1+b_2)\,i+(a_1+a_2)
    \]
    \pause The \emph{product} of two complex numbers is
    \[
      (b_1\cdot i+a_1)\cdot(b_2\cdot i+a_2)
      = (a_1b_2+a_2b_1)\,i+(a_1a_2-b_1b_2)
    \]
    \pause The \emph{conjugate} of a complex number is
    \[
      \overline{b\cdot i+a}=-b\cdot i+a
    \]
    \pause The \emph{absolute value} of a complex number is
    $\abs{b\cdot i+a}=\sqrt{a^2+b^2}$.
  \end{definition}

\end{frame}


\begin{sagesilent}
  z1 = 4*I-3
  z2 = -I+1
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    $(\sage{z1})+(\sage{z2})=(4-1)i+(-3+1)=\sage{z1+z2}$
  \end{example}

  \pause
  \begin{example}
    $(\sage{z1})\cdot(\sage{z2})=(-3\cdot(-1)+1\cdot4)i+(-3\cdot 1-4\cdot(-1))=\sage{z1*z2}$
  \end{example}

  \pause
  \begin{example}
    $\overline{\sage{z1}}=\sage{z1.conjugate()}$
  \end{example}

  \pause
  \begin{example}
    $\abs{\sage{z1}}=\sqrt{(\sage{z1.real_part()})^2+\sage{z1.imag_part()}^2}=\sage{z1.abs()}$
  \end{example}

\end{frame}


\subsection{Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myEQ}[3]{
    \onslide<####3>{\overset{\mybold{\textnormal{####1}}}{####2}}
  }
  \begin{theorem}
    The operation of conjugation has the following properties
    \begin{align*}
      \myEQ{additive}{\overline{z_1+z_2}=\overline{z_1}+\overline{z_2}}{2-} && \myEQ{multiplicative}{\overline{z_1\cdot z_2}=\overline{z_1}\cdot\overline{z_2}}{3-} && \myEQ{involutive}{\overline{\overline{z}}=z}{4-}
    \end{align*}
    \onslide<5->{Furthermore, we have $\abs{z}=\sqrt{z\cdot\overline{z}}$.}
  \end{theorem}

  \onslide<6->
  \begin{theorem}
    $z\in\mathbb{R}$ if and only if $\overline{z}=z$
  \end{theorem}

\end{frame}


\begin{sagesilent}
  z = 5*I-2
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{reciprocal} of $z=b\cdot i+a$ is
    \[
      \frac{1}{z}
      = \frac{\overline{z}}{\abs{z}^2}
      = \frac{-b}{a^2+b^2}\cdot i+\frac{a}{a^2+b^2}
    \]
    \pause This formula is derived from $\abs{z}^2=z\cdot\overline{z}$.
  \end{definition}

  \pause
  \begin{example}
    $\displaystyle\oldfrac{1}{\sage{z}}=\sage{1/z}$
  \end{example}

\end{frame}


\subsection{The Fundamental Theorem of Algebra}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{polynomial in the variable $t$} is an expression $f(t)$ of the form
    \[
      f(t)=c_n\cdot t^n+c_{n-1}\cdot t^{n-1}+\dotsb+c_1\cdot t+c_0
    \]
    The \emph{degree} of $f(t)$ is the largest exponent of $t$.
  \end{definition}

  \pause
  \begin{definition}
    A polynomial $f(t)$ with $\deg(f)=n$ is \emph{monic} if $c_n=1$.
  \end{definition}

\end{frame}


\begin{sagesilent}
  set_random_seed(19)
  R = PolynomialRing(ZZ, name='t')
  R.inject_variables()
  f2 = R.random_element(degree=2)
  while not f2.is_monic(): f2 = R.random_element(degree=2)
  f3 = R.random_element(degree=3)
  while f3.is_monic(): f3 = R.random_element(degree=3)
  f4 = R.random_element(degree=4)
  while f4.is_monic(): f4 = R.random_element(degree=4)
  f5 = R.random_element(degree=5)
  while not f5.is_monic(): f5 = R.random_element(degree=5)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[Quadratic, $\deg=2$]
    $f(t)=\tikzmark{m2}\sage{f2}$
  \end{example}

  \pause
  \begin{example}[Cubic, $\deg=3$]
    $f(t)=\tikzmark{m3}\sage{f3}$
  \end{example}

  \pause
  \begin{example}[Quartic, $\deg=4$]
    $f(t)=\tikzmark{m4}\sage{f4}$
  \end{example}

  \pause
  \begin{example}[Quintic, $\deg=5$]
    $f(t)=\tikzmark{m5}\sage{f5}$
  \end{example}

  \begin{tikzpicture}[
    , line join=round
    , line cap=round
    , remember picture
    , overlay
    ]

    \pause
    \draw[<-, thick, red]
    ([shift={(3pt,-2pt)}]pic cs:m2) |- ([shift={(3.5cm, -0.3cm)}]pic cs:m2)
    node[anchor=west](a) {monic $c_{\sage{f2.degree()}}=\sage{f2.leading_coefficient()}$};

    \pause
    \draw[<-, thick, red]
    ([shift={(3pt,-2pt)}]pic cs:m3) |- ([shift={(3.5cm, -0.3cm)}]pic cs:m3)
    node[anchor=west](a) {\emph{not} monic $c_{\sage{f3.degree()}}=\sage{f3.leading_coefficient()}$};

    \pause
    \draw[<-, thick, red]
    ([shift={(3pt,-2pt)}]pic cs:m4) |- ([shift={(3.5cm, -0.3cm)}]pic cs:m4)
    node[anchor=west](a) {\emph{not} monic $c_{\sage{f4.degree()}}=\sage{f4.leading_coefficient()}$};

    \pause
    \draw[<-, thick, red]
    ([shift={(3pt,-2pt)}]pic cs:m5) |- ([shift={(3.5cm, -0.3cm)}]pic cs:m5)
    node[anchor=west](a) {monic $c_{\sage{f5.degree()}}=\sage{f5.leading_coefficient()}$};

  \end{tikzpicture}

\end{frame}


\begin{sagesilent}
  f = expand((t-1)*(t-2)*(t-5))
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{root} of a polynomial $f(t)$ is a scalar $r$ satisfying $f(r)=0$.
  \end{definition}

  \pause
  \begin{example}
    Consider the cubic given by
    \[
      f(t) = \sage{f}
    \]
    \pause The equations
    \begin{align*}
      f(1) &= (1)^3-8\cdot(1)^2+17\cdot(1)-10 = \sage{f(t=1)} \\
      f(2) &= (2)^3-8\cdot(2)^2+17\cdot(2)-10 = \sage{f(t=2)} \\
      f(3) &= (3)^3-8\cdot(3)^2+17\cdot(3)-10 = \sage{f(t=3)}
    \end{align*}
    \pause Show that $r=1$ and $r=2$ are roots but $r=3$ is not a root.
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose that $r$ is a root of
    \[
      f(t)
      = c_n\cdot t^n+c_{n-1}\cdot t^{n-1}+\dotsb+c_1\cdot t+c_0
    \]
    where $c_1,c_2,\dotsc,c_0\in\mathbb{R}$. Then $\overline{r}$ is also a root.
  \end{theorem}

  \onslide<2->
  \begin{proof}\renewcommand{\qedsymbol}{}
    Plugging $\overline{r}$ into $f(t)$ gives
    \begin{align*}
      f(\overline{r})
      &=\onslide<3->{ c_n\cdot\overline{r}^n+c_{n-1}\cdot\overline{r}^{n-1}+\dotsb+c_1\cdot\overline{r}+c_0} \\
      &\onslide<3->{=} \onslide<4->{\overline{c_n}\cdot\overline{r^n}+\overline{c_{n-1}}\cdot\overline{r^{n-1}}+\dotsb+\overline{c_1}\cdot\overline{r}+\overline{c_0}} \\
      &\onslide<4->{=} \onslide<5->{\overline{c_n\cdot r^n}+\overline{c_{n-1}\cdot r^{n-1}}+\dotsb+\overline{c_1\cdot r}+\overline{c_0}} \\
      &\onslide<5->{=} \onslide<6->{\overline{c_n\cdot r^n+c_{n-1}\cdot r^{n-1}+\dotsb+c_1\cdot r+c_0}} \\
      &\onslide<6->{=} \onslide<7->{\overline{f(r)}} \\
      &\onslide<7->{=} \onslide<8->{0{\color{beamblue}\Box}}
    \end{align*}
  \end{proof}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[The Fundamental Theorem of Algebra]
    Every monic polynomial factors as
    \[
      f(t)
      = (t-r_1)^{m_1}\cdot(t-r_2)^{m_2}\dotsb(t-r_k)^{m_k}
    \]
    where $r_1,r_2,\dotsc,r_k\in\mathbb{C}$ are distinct roots and
    \[
      \deg(f)=m_1+m_2+\dotsb+m_k
    \]
    This factorization is unique, up to reordering.
  \end{theorem}

  \pause
  \begin{definition}
    We call $m_j$ the \emph{multiplicity} of $r_j$ as a root of $f$.
  \end{definition}

\end{frame}


\begin{sagesilent}
  r1 = 3*I-2
  r2 = r1.conjugate()
  r3 = -7
  r4 = 9
  m1, m2, m3, m4 = 3, 3, 1, 4
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the factorization
    \[
      f(t)
      =
      \sage{factor((t-r1)**m1)}\cdot
      \sage{factor((t-r2)**m2)}\cdot
      \sage{factor((t-r3)**m3)}\cdot
      \sage{factor((t-r4)**m4)}
    \]
    \pause The roots and multiplicities are given by
    \begin{center}
      \begin{tabular}{lr}
        \toprule
        \multicolumn{1}{c}{\color{beamblue}Root} & \multicolumn{1}{c}{\color{beamblue}Multiplicity} \\
        \midrule
        $r_1=\sage{r1}$ & $m_1=\sage{m1}$ \\
        $r_2=\sage{r2}$ & $m_2=\sage{m2}$ \\
        $r_3=\sage{r3}$ & $m_3=\sage{m3}$ \\
        $r_4=\sage{r4}$ & $m_4=\sage{m4}$ \\
        \bottomrule
      \end{tabular}
    \end{center}
    \pause The degree of $f(t)$ is
    \[
      \deg(f)
      = m_1+m_2+m_3+m_4
      = \sage{m1}+\sage{m2}+\sage{m3}+\sage{m4}
      = \sage{m1+m2+m3+m4}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    How do we find roots of polynomials?
  \end{block}

  \pause
  \begin{block}{Answer}
    This is usually hard.
  \end{block}

\end{frame}


\begin{sagesilent}
  r1 = -I+2
  r2 = r1.conjugate()
  f = expand(-2*(t-r1)*(t-r2))
  c, b, a = f.coefficients()
  a, b, c = a[0], b[0], c[0]
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[The Quadratic Formula]
    $a\cdot t^2+b\cdot t+c=a\cdot\pair*{t-\oldfrac{-b-\sqrt{b^2-4\,ac}}{2\,a}}\cdot\pair*{t-\oldfrac{-b+\sqrt{b^2-4\,ac}}{2\,a}}$
  \end{theorem}

  \pause
  \begin{example}
    The roots of $f(t)=\sage{f}$ are
    \begin{gather*}
      \begin{align*}
        r_1 &= \oldfrac{\sage{-b}-\sqrt{\sage{b**2}-\sage{4*a*c}}}{\sage{2*a}}=\sage{r1} & r_2 &= \oldfrac{\sage{-b}+\sqrt{\sage{b**2}-\sage{4*a*c}}}{\sage{2*a}}=\sage{r2}
      \end{align*}
    \end{gather*}
    \pause This gives the factorization
    \[
      \sage{f} = \sage{a}\cdot(t-(\sage{r1}))\cdot(t-(\sage{r2}))
    \]
    \pause Note that the roots are related by conjugation $\sage{r1}=\overline{\sage{r2}}$.
  \end{example}

\end{frame}


\begin{sagesilent}
  var('a b c d')
  f = a*t**3+b*t**2+c*t+d
  D = 2*b**3-9*a*b*c+27*a**2*d+sqrt((2*b**3-9*a*b*c+27*a**2*d)**2-4*(b**2-3*a*c)**3)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Cubic Formula]
    There is a cubic formula.
  \end{theorem}

  \pause
  \begin{theorem}[Quartic Formula]
    There is a quartic formula.
  \end{theorem}

  \pause
  \begin{theorem}[Abel, 1824]
    There is no quintic formula.
  \end{theorem}

\end{frame}


\subsection{Vieta's Formulas}
\begin{sagesilent}
  var('r1 r2')
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    How do the roots of a polynomial relate to the coefficients?
  \end{block}

  \onslide<2->
  \begin{example}
    Suppose $r_1$ and $r_2$ are the roots of
    \[
      f(t)=t^2+c_1\cdot t+c_0
    \]
    \onslide<3->The Fundamental Theorem of Algebra then gives
    \[
      t^2+\mybox{c_1}{8-10}\cdot t+\mybox{c_0}{13-}
      = \onslide<4->(t-r_1)\cdot(t-r_2)
      = \onslide<5->t^2\mybox{-(r_1+r_2)}{7-10}\cdot t+\mybox{r_1\cdot r_2}{12-}
    \]
    \onslide<6->Matching coefficients gives
    \begin{align*}
      \onslide<9->{r_1+r_2} &\onslide<9->{=} \onslide<10->{-c_1} & \onslide<14->{r_1\cdot r_2} &\onslide<14->{=} \onslide<15->{c_0}
    \end{align*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Vieta's Formulas]
    Consider the factorization
    \begin{gather*}
      t^n+c_{n-1}\cdot t^{n-1}+\dotsb+c_1\cdot t+c_0
      =
      (t-r_1)^{m_1}\cdot(t-r_2)^{m_2}\dotsb(t-r_k)^{m_k}
    \end{gather*}
    \pause Then the coefficient of $t^{n-1}$ is given by
    \[
      m_1\cdot r_1+m_2\cdot r_2+\dotsb+m_k\cdot r_k = -c_{n-1}
    \]
    \pause and the constant coefficient is given by
    \[
      r_1^{m_1}\cdot r_2^{m_2}\dotsb r_k^{m_k} = (-1)^n\cdot c_0
    \]
  \end{theorem}

\end{frame}


\begin{sagesilent}
  r1 = 2*I-1
  r2 = r1.conjugate()
  r3 = -7
  r4 = 2
  m1, m2, m3, m4 = 2, 2, 1, 4
  n = m1+m2+m3+m4
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the factorization
    \[
      f(t)
      =
      \sage{factor((t-r1)**m1)}\cdot
      \sage{factor((t-r2)**m2)}\cdot
      \sage{factor((t-r3)**m3)}\cdot
      \sage{factor((t-r4)**m4)}
    \]
    \pause The degree of $f(t)$ is
    \[
      \deg(f)
      = \sage{m1}+\sage{m2}+\sage{m3}+\sage{m4}
      = \sage{n}
    \]
    \pause The coefficient $c_{\sage{n-1}}$ of $t^{\sage{n-1}}$ is given by
    \[
      -c_{n-1}
      =
      \sage{m1}\cdot(\sage{r1})
      +
      \sage{m2}\cdot(\sage{r2})
      +
      \sage{m3}\cdot(\sage{r3})
      +
      \sage{m4}\cdot(\sage{r4})
      = \sage{m1*r1+m2*r2+m3*r3+m4*r4}
    \]
    \pause so $c_{\sage{n-1}}=\pause\sage{-(m1*r1+m2*r2+m3*r3+m4*r4)}$. \pause
    The constant coefficient is given by
    \[
      (-1)^{\sage{n}}\cdot c_0
      =
      (\sage{r1})^{\sage{m1}}
      \cdot
      (\sage{r2})^{\sage{m2}}
      \cdot
      (\sage{r3})^{\sage{m3}}
      \cdot
      (\sage{r4})^{\sage{m4}}
      =
      \sage{r1**m1*r2**m2*r3**m3*r4**m4}
    \]
    \pause so $c_0\pause=\sage{-r1**m1*r2**m2*r3**m3*r4**m4}$.
  \end{example}
\end{frame}


\section{Complex Vectors}
\subsection{Basic Definitions}
\begin{sagesilent}
  set_random_seed(7912)
  v = random_matrix(S, 2, 1)
  w = random_matrix(S, 3, 1)
  x = random_matrix(S, 4, 1)
  y = random_matrix(ZZ, 5, 1)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    We write $\bv{v}\in\mathbb{C}^n$ to indicate that $\bv{v}$ is a list of $n$
    complex numbers.
  \end{definition}

  \newcommand{\myV}[2]{\onslide<####1>{\sage{####2}\in\mathbb{C}^{\sage{####2.nrows()}}}}

  \onslide<2->
  \begin{example}
    Each of the following is a complex vector.
    \begin{gather*}
      \begin{align*}
        \myV{3-}{v} && \myV{4-}{w} && \myV{5-}{x} && \myV{6-}{y}
      \end{align*}
    \end{gather*}

  \end{example}
\end{frame}


\begin{sagesilent}
  v = random_vector(S, 5)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{conjugate} of $\bv{v}=\langle v_1, v_2, \dotsc, v_n\rangle$ is
    $\overline{\bv{v}}=\langle \overline{v_1}, \overline{v_2}, \dotsc,
    \overline{v_n}\rangle$.
  \end{definition}

  \pause
  \begin{example}
    $\overline{\sage{v}}=\sage{v.conjugate()}$
  \end{example}

\end{frame}


\subsection{The Inner Product}
\begin{sagesilent}
  v = vector([i, 1])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Geometry in $\mathbb{R}^n$ is defined by the dot product. \pause What about
    $\mathbb{C}^n$?
  \end{block}

  \pause
  \begin{example}
    Consider the vector $\bv{v}=\sage{v}$. \pause Note that
    \[
      \pause\bv{v}\cdot\bv{v}
      \pause= \sage{v}\cdot\sage{v}
      \pause= \sage{v[0]}^2+\sage{v[1]}^2
      \pause= \sage{v[0]**2}+\sage{v[1]**2}
      \pause= \sage{v*v}
    \]
    \pause So $\bv{v}$ is orthogonal to\textellipsis \pause itself?  \pause This
    feels\textellipsis \pause wrong.
  \end{example}

  \pause
  \begin{alertblock}{Urgent}
    We need an alternative to the dot product that works on $\mathbb{C}^n$.
  \end{alertblock}

\end{frame}


\begin{sagesilent}
  set_random_seed(481)
  v = random_vector(S, 2)
  w = random_vector(S, 2)
  w, v = v, w
  v1, v2 = v.conjugate()
  w1, w2 = w
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{inner product} of $\bv{v}, \bv{w}\in\mathbb{C}^n$ is
    $\langle\bv{v}, \bv{w}\rangle=\overline{\bv{v}}\cdot\bv{w}$.
  \end{definition}

  \onslide<2->
  \begin{example}
    The inner product $\bv{v}=\sage{v}$ and $\bv{w}=\sage{w}$ is
    \begin{align*}
      \langle\bv{v}, \bv{w}\rangle
      &= \onslide<3->{\overline{\bv{v}}\cdot\bv{w}} \\
      &\onslide<3->{=} \onslide<4->{\overline{\sage{v}}\cdot\sage{w}} \\
      &\onslide<4->{=} \onslide<5->{\sage{v.conjugate()}\cdot\sage{w}} \\
      &\onslide<5->{=} \onslide<6->{(\sage{v1})\cdot(\sage{w1})+(\sage{v2})\cdot(\sage{w2})} \\
      &\onslide<6->{=} \onslide<7->{(\sage{v1*w1})+(\sage{v2*w2})} \\
      &\onslide<7->{=} \onslide<8->{\sage{v.hermitian_inner_product(w)}}
    \end{align*}
    \onslide<9->Note that $\langle\bv{v}, \bv{w}\rangle\in\mathbb{C}$.
  \end{example}
\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myEQ}[3]{
    \onslide<####3>{\overset{\mybold{\textnormal{####1}}}{####2}}
  }
  \begin{theorem}
    The inner product on $\mathbb{C}^n$ has the following properties
    \begin{align*}
      \myEQ{complex-valued}{\langle\bv{v}, \bv{w}\rangle\in\mathbb{C}}{2-} && \myEQ{conjugate-symmetric}{\langle\bv{v}, \bv{w}\rangle=\overline{\langle\bv{w}, \bv{v}\rangle}}{3-}
    \end{align*}
    \onslide<4->{Additionally, we have scalar laws}
    \begin{align*}
      \onslide<4->{\langle c\cdot\bv{v}, \bv{w}\rangle=\overline{c}\cdot\langle\bv{v}, \bv{w}\rangle} && \onslide<5->{\langle \bv{v}, c\cdot\bv{w}\rangle=c\cdot\langle\bv{v}, \bv{w}\rangle}
    \end{align*}
    \onslide<6->{and distribution laws}
    \begin{align*}
      \onslide<6->{\langle\bv{v}, \bv{w}+\bv{x}\rangle=\langle\bv{v}, \bv{w}\rangle+\langle\bv{v}, \bv{x}\rangle} && \onslide<7->{\langle\bv{v}+\bv{w}, \bv{x}\rangle=\langle\bv{v}, \bv{x}\rangle+\langle\bv{w},\bv{x}\rangle}
    \end{align*}
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Suppose
    $\bv{v}=\langle b_1\cdot i+a_1, b_2\cdot i+a_2,\dotsc, b_n\cdot
    i+a_n\rangle$. \onslide<2->Then
    \begin{gather*}
      \begin{align*}
        \langle\bv{v}, \bv{v}\rangle
        &\onslide<2->{=} \onslide<3->{\overline{\bv{v}}\cdot\bv{v}} \\
        &\onslide<3->{=} \onslide<4->{\overline{\langle b_1\cdot i+a_1, b_2\cdot i+a_2,\dotsc, b_n\cdot i+a_n\rangle}\cdot \langle b_1\cdot i+a_1, b_2\cdot i+a_2,\dotsc, b_n\cdot i+a_n\rangle} \\
        &\onslide<4->{=} \onslide<5->{\langle \overline{b_1\cdot i+a_1}, \overline{b_2\cdot i+a_2},\dotsc, \overline{b_n\cdot i+a_n}\rangle\cdot \langle b_1\cdot i+a_1, b_2\cdot i+a_2,\dotsc, b_n\cdot i+a_n\rangle} \\
        &\onslide<5->{=} \onslide<6->{\abs{b_1\cdot i+a_1}^2+\abs{b_2\cdot i+a_2}^2+\dotsb+\abs{b_n\cdot i+a_n}^2} \\
        &\onslide<6->{=} \onslide<7->{a_1^2+b_1^2+a_2^2+b_2^2+\dotsb+a_n^2+b_n^2}
      \end{align*}
    \end{gather*}
    \onslide<8->In particular, $\langle\bv{v}, \bv{v}\rangle\geq0$.
  \end{block}

\end{frame}


\begin{sagesilent}
  set_random_seed(891)
  v = random_vector(S, 3)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{norm} of $\bv{v}\in\mathbb{C}^n$ is
    $\norm{\bv{v}}=\sqrt{\langle\bv{v}, \bv{v}\rangle}$.
  \end{definition}

  \pause
  \begin{example}
    $\norm{\sage{v}}^2
    = \sage{v[0].real()}^2+\sage{v[0].imag()}^2
    + \sage{v[1].real()}^2+(\sage{v[1].imag()})^2
    + (\sage{v[2].real()})^2+\sage{v[2].imag()}^2
    = \sage{v.norm()**2}
    $
  \end{example}

  \pause
  \begin{definition}
    The \emph{angle between} $\bv{v},\bv{w}\in\mathbb{C}^n$ is defined by the
    formula
    \[
      \Re(\langle\bv{v}, \bv{w}\rangle)=\norm{\bv{v}}\cdot\norm{\bv{w}}\cdot\cos(\theta)
    \]
    \pause Two vectors $\bv{v}, \bv{w}\in\mathbb{C}^n$ are \emph{orthogonal} if
    $\langle\bv{v}, \bv{w}\rangle=0$.
  \end{definition}

\end{frame}


\section{Complex Matrices}
\subsection{Basic Definitions}
\begin{sagesilent}
  set_random_seed(491)
  A = random_matrix(S, 3, 2)
  B = random_matrix(S, 2, 3)
  C = random_matrix(ZZ, 2)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    An $m\times n$ \emph{complex matrix} is an $m\times n$ array of complex
    numbers.
  \end{definition}

  \onslide<2->
  \begin{example}
    Each of the following is a complex matrix
    \begin{gather*}
      \begin{align*}
        \sage{A} && \onslide<3->{\sage{B}} && \onslide<4->{\sage{C}}
      \end{align*}
    \end{gather*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Consider an $m\times n$ matrix $A$ and a vector $\bv{v}\in\mathbb{C}^n$ of
    the form
    \begin{align*}
      A &= \begin{bmatrix}\bv{a}_1 & \bv{a}_2 & \cdots & \bv{a}_n\end{bmatrix} & \bv{v} &= \langle v_1, v_2,\dotsc, v_n\rangle
    \end{align*}
    \pause The \emph{matrix-vector product} $A\bv{v}$ is given by the linear
    combination
    \[
      A\bv{v}
      = v_1\cdot\bv{a}_1+v_2\cdot\bv{a}_2+\dotsb+v_n\cdot\bv{a}_n
    \]
    \pause Note that $A\bv{v}$ is a \emph{vector} in $\mathbb{C}^m$.
  \end{definition}

  \pause
  \begin{block}{Observation}
    Every $m\times n$ matrix is a machine
    $\mathbb{C}^n\xrightarrow{A}\mathbb{C}^m$.
  \end{block}

\end{frame}


\begin{sagesilent}
  A = matrix.column([(I, -I, I), (-I-1, I+1, 2*I-1)])
  v = matrix.column([I+1, I])
  a1, a2 = map(matrix.column, A.columns())
  v1, v2 = v._list()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    $
    \begin{aligned}
      \sage{A}\sage{v}
      &\onslide<1->{=} \onslide<2->{(\sage{v1})\sage{a1}+\sage{v2}\sage{a2}} \\
      &\onslide<2->{=} \onslide<3->{\sage{v1*a1}+\sage{v2*a2}} \\
      &\onslide<3->{=} \onslide<4->{\sage{A*v}}
    \end{aligned}
    $
  \end{example}

\end{frame}


\subsection{Conjugate Transposition}
\begin{sagesilent}
  set_random_seed(81)
  A = random_matrix(S, 3, 2)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{conjugate transpose} of $A$ is $A^\ast=\overline{A^\intercal}$.
  \end{definition}

  \pause
  \begin{example}
    ${\sage{A}}^\ast=\sage{A.H}$
  \end{example}

  \pause
  \begin{alertblock}{Warning}
    Some people write $A^H$ or $A^\dagger$ instead of $A^\ast$.
  \end{alertblock}

  \pause
  \begin{block}{Observation}
    Matrices over $\mathbb{R}$ satisfy $A^\ast=A^\intercal$.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \newcommand{\myEQ}[3]{
    \onslide<####3>{\overset{\mybold{\textnormal{####1}}}{####2}}
  }
  \begin{theorem}
    The conjugate transpose is an order-reversing involution
    \begin{align*}
      \onslide<2->{(A_1A_2)^\ast=A_2^\ast A_1^\ast} && \onslide<3->{(A^\ast)^\ast=A}
    \end{align*}
    \onslide<4->{The operation is conjugate-linear}
    \[
      \onslide<4->{(c_1\cdot A_1+c_2\cdot A_2)^\ast=\overline{c_1}\cdot
        A_1^\ast+\overline{c_2}\cdot A_2^\ast}
    \]
    \onslide<5->{and cooperates with $\trace$, $\rank$, $\det$, and inversion}
    \begin{gather*}
      \begin{align*}
        \onslide<6->{\tikzmark{trace}\trace(A^\ast)=\overline{\trace(A)}} && \onslide<7->{\rank(A^\ast)=\rank(A)} && \onslide<8->{\det(A^\ast)=\overline{\det(A)}} && \onslide<9->{(A^\ast)^{-1}=(A^{-1})^\ast}
      \end{align*}
    \end{gather*}
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , remember picture
      , overlay
      ]

      \newcommand{\myA}{
        \left[
          \begin{array}{cccc}
            a_{11} & \cdots & a_{1n} \\
            \vdots & \ddots & \vdots \\
            a_{n1} & \cdots & a_{nn}
          \end{array}
        \right]
      }
      \onslide<10->{
        \draw[<-, thick, red]
        ([shift={(0.5cm,-2pt)}]pic cs:trace) |- ([shift={(0.75cm, -1cm)}]pic cs:trace)
        node[anchor=west](a) {$\trace\myA=a_{11}+a_{22}+\dotsb+a_{nn}$};
      }
    \end{tikzpicture}
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The eigenvalues of $A^\ast$ are the conjugates of the eigenvalues of $A$.
  \end{theorem}

  \pause
  \begin{theorem}
    $\langle A\bv{v}, \bv{w}\rangle=\langle\bv{v}, A^\ast\bv{w}\rangle$
  \end{theorem}

  \pause
  \begin{proof}
    $
    \langle A\bv{v}, \bv{w}\rangle
    = \pause(A\bv{v})^\ast\bv{w}
    = \pause(\bv{v}^\ast A^\ast)\bv{w}
    = \pause\bv{v}^\ast(A^\ast\bv{w})
    = \pause\langle\bv{v}, A^\ast\bv{w}\rangle
    $
  \end{proof}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    The following replacements allow us to do linear algebra over $\mathbb{C}$.
    \begin{align*}
      \mathbb{R}\leftrightsquigarrow\mathbb{C} && \vv{v}\cdot\vv{w}\leftrightsquigarrow\langle\bv{v}, \bv{w}\rangle && A^\intercal\leftrightsquigarrow A^\ast
    \end{align*}
    \onslide<2->{These replacements define ``complex'' analogues of ``real''
      concepts.}\onslide<3->
    \begin{center}
      \begin{tabular}{rll}
        \toprule
        \multicolumn{1}{c}{\color{beamblue}Concept} & \multicolumn{1}{c}{\color{beamblue}Formula in $\mathbb{R}$} & \multicolumn{1}{c}{\color{beamblue}Analog in $\mathbb{C}$} \\
        \midrule
        \onslide<4->{{\color{beamgreen}The Gramian}}           & \onslide<4->{$A^\intercal A$}                              & \onslide<4->{$A^\ast A$} \\
        \onslide<5->{{\color{beamgreen}Projection Formula}}    & \onslide<5->{$X(X^\intercal X)^{-1}X^\intercal$}           & \onslide<5->{$X(X^\ast X)^{-1}X^\ast$} \\
        \onslide<6->{{\color{beamgreen}Least-Squares Formula}} & \onslide<6->{$A^\intercal A\widehat{x}=A^\intercal\vv{b}$} & \onslide<6->{$A^\ast A\widehat{\bv{x}}=A^\ast\bv{b}$} \\
        \onslide<7->{{\color{beamgreen}Orthonormal Columns}}   & \onslide<7->{$Q^\intercal Q=I_n$}                          & \onslide<7->{$Q^\ast Q=I_n$}\\
        \bottomrule
      \end{tabular}
    \end{center}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Sometimes, adjectives change when we pass from $\mathbb{R}$ to $\mathbb{C}$.
  \end{block}

  \newcommand{\myEQ}[3]{
    \onslide<####3>{\overset{\mybold{\textnormal{####1}}}{####2}}
  }

  \onslide<2->
  \begin{definition}
    \emph{Symmetric matrices} are analogous to \emph{Hermitian matrices}.
    \begin{align*}
      \myEQ{Symmetric Matrix}{A^\intercal=A}{3-} && \myEQ{Hermitian Matrix}{A^\ast=A}{4-}
    \end{align*}
    \onslide<5->\emph{Orthogonal matrices} are analogous to \emph{unitary
      matrices}.
    \begin{align*}
      \myEQ{Orthogonal Matrix}{Q^\intercal=Q^{-1}}{6-} && \myEQ{Unitary Matrix}{Q^\ast=Q^{-1}}{7-}
    \end{align*}
  \end{definition}

\end{frame}


\begin{sagesilent}
  set_random_seed(43)
  H1 = random_matrix(S, 2)
  H1 = H1.H*H1
  H2 = random_matrix(S, 3)
  H2 = H2.H*H2
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Each of the two matrices
    \begin{align*}
      \overset{A^\ast=A}{\sage{H1}} && \overset{A^\ast=A}{\sage{H2}}
    \end{align*}
    is \emph{Hermitian}.
  \end{example}

\end{frame}


\begin{sagesilent}
  Q = matrix.column([(0, I, 0), (1, 0, 0), (0, 0, -I)])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $Q$ given by
    \[
      Q = \sage{Q}
    \]
    \pause The equation
    \[
      \overset{Q^\ast}{\sage{Q.H}}\overset{Q}{\sage{Q}}
      = \overset{I_{\sage{Q.nrows()}}}{\sage{Q.H*Q}}
    \]
    \pause shows that $Q^\ast=\pause Q^{-1}$, so $Q$ is \emph{unitary}.
  \end{example}

\end{frame}


\section{Complex Vector Spaces}
\subsection{The Four Fundamental Subspaces}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    What are the four fundamental subspaces of a complex matrix?
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , scale=0.75
        % , every node/.style={scale=1}
        , every node/.style={transform shape}
        ]

        \pgfmathsetmacro{\width}{3}
        \pgfmathsetmacro{\height}{3.75}
        \pgfmathsetmacro{\myrot}{25}
        \pgfmathsetmacro{\myperp}{0.10*\width)}
        \pgfmathsetmacro{\myshift}{2.5*\width}

        \begin{scope}[
          , rotate=\myrot
          , transform shape
          ]

          \coordinate (On) at (0, 0);

          \draw (-\myperp, 0) -- (-\myperp, \myperp) -- (0, \myperp);
          \filldraw[fill=beamgreen!75] (On) rectangle (\width, \height);
          \filldraw[fill=black!10!violet!75] (On) rectangle (-\width, -\width);

          \coordinate (Rn) at (\width, 0.5*\height);

          \coordinate (Row) at (0.5*\width, 0.75*\height);
          \node at (Row) {$\Col(A^\ast)$};

          \coordinate (dimRow) at (0.5*\width, 0.5*\height);
          \node at (dimRow) {$\dim=\rank(A)$};

          \coordinate (Null) at (-0.5*\width, -.25*\width);
          \node at (Null) {$\Null(A)$};

          \coordinate (dimNull) at (-0.5*\width, -0.5*\width);
          \node at (dimNull) {$\dim=\nullity(A)$};
        \end{scope}

        \begin{scope}[
          , shift={(\myshift,0)}
          , rotate=-\myrot
          , transform shape
          ]

          \coordinate (Om) at (0, 0);

          \draw (\myperp, 0) -- (\myperp, \myperp) -- (0, \myperp);
          \filldraw[fill=beamblue!75] (Om) rectangle (-\width, \height);
          \filldraw[fill=red!75] (Om) rectangle (\width, -\width);

          \coordinate (Rm) at (-\width, 0.5*\height);

          \coordinate (Col) at (-0.5*\width, 0.75*\height);
          \node at (Col) {$\Col(A)$};

          \coordinate (dimCol) at (-0.5*\width, 0.5*\height);
          \node at (dimCol) {$\dim=\rank(A)$};

          \coordinate (LNull) at (0.5*\width, -.25*\width);
          \node at (LNull) {$\Null(A^\ast)$};

          \coordinate (dimLNull) at (0.5*\width, -0.5*\width);
          \node at (dimLNull) {$\dim=\nullity(A^\ast)$};

          % \coordinate (b) at (-0.75*\width, 0);
          % \draw[->, black!50!green] (Om) -- (b)
          % node[midway, sloped, above] {$\vv{b}$};

        \end{scope}


        \node[left] at (Rm) {$\mathbb{C}^m$};
        \node[right] at (Rn) {$\mathbb{C}^n$};

        \draw[->, shorten >= 0.75cm, shorten <= 0.75cm]
        (Rn) -- (Rm) node[midway, above] {$A$};

      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\subsection{Conjugation of Spaces}
\begin{sagesilent}
  set_random_seed(842)
  v1, v2 = random_matrix(S, 3, 2).columns()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{conjugate} of a vector space $V$ is
    $\overline{V}=\Set{\overline{\bv{v}}\given \bv{v}\in V}$.
  \end{definition}

  \onslide<2->
  \begin{example}
    Consider the vector space $V$ given by
    \[
      V = \Span\Set{\sage{v1}, \sage{v2}}
    \]
    \onslide<3->The conjugate of $V$ is
    \begin{align*}
      \overline{V}
      &= \onslide<4->{\Span\Set{\overline{\sage{v1}}, \overline{\sage{v2}}}} \\
      &\onslide<4->{=} \onslide<5->{\Span\Set{\sage{v1.conjugate()}, \sage{v2.conjugate()}}}
    \end{align*}

  \end{example}

\end{frame}


\subsection{Complex Eigenspaces}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Why bother with complex stuff?
  \end{block}

  \pause
  \begin{block}{Answer}
    Some matrices have complex eigenvalues!
  \end{block}

\end{frame}


\begin{sagesilent}
  A = matrix([(0, -1), (1, 0)])
  l1, l2 = -I, I
  n = A.nrows()
  Id = identity_matrix(n)
  N1 = A-l1*Id
  N2 = A-l2*Id
  v1 = vector([-I, 1])
  v2 = v1.conjugate()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the data
    \begin{align*}
      A &= \sage{A} & \EVals(A) &= \Set{\sage{l1}, \sage{l2}}
    \end{align*}
    \onslide<2->{The eigenspaces of $A$ are}
    \begin{align*}
      \onslide<2->{\mathcal{E}_A(\sage{l1})} &\onslide<2->{=} \onslide<2->{\Null\overset{\onslide<3->{\rank=}\onslide<4->{1}}{\sage{N1}} =} \onslide<5->{\Null\sage{N1.rref()} =} \onslide<6->{\Span\Set{\sage{v1}}} \\
      \onslide<7->{\mathcal{E}_A(\sage{l2})} &\onslide<7->{=} \onslide<7->{\Null\overset{\onslide<8->{\rank=}\onslide<9->{1}}{\sage{N2}} =} \onslide<10->{\Null\sage{N2.rref()} =} \onslide<11->{\Span\Set{\sage{v2}}}
    \end{align*}
    \onslide<12->The eigenvalues and eigenspaces are related by conjugation.
    \begin{gather*}
      \onslide<12->{\mathcal{E}_A(-i)}
      \onslide<12->{=} \onslide<13->{\Span\Set{\sage{v1}}}
      \onslide<13->{=} \onslide<14->{\Span\Set{\overline{\sage{v2}}}}
      \onslide<14->{=} \onslide<15->{\overline{\Span\Set{\sage{v2}}}}
      \onslide<15->{=} \onslide<16->{\overline{\mathcal{E}_A(i)}}
      \onslide<16->{=} \onslide<17->{\overline{\mathcal{E}_A(\overline{-i})}}
    \end{gather*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose $\lambda$ is an eigenvalue of a real matrix $A$. Then
    $\overline{\lambda}$ also is an eigenvalue of $A$ and
    $\mathcal{E}_A(\overline{\lambda})=\overline{\mathcal{E}_A(\lambda)}$.
  \end{theorem}

\end{frame}

\end{document}
