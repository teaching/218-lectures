from sage.all import matrix, ZZ, random_vector


def is_good(t, b):
    if len(t) != len(b):
        return False
    A = matrix.column([(1,)*len(t), t, tuple(_**2 for _ in t)])
    if A.rank() != A.ncols():
        return False
    M = (A.T*A).augment(A.T*b, subdivide=True)
    try:
        M.rref().change_ring(ZZ)
        return True
    except TypeError:
        return False


def gen_examples(m=3, x=-10, y=10):
    t, b = [random_vector(ZZ, m, x, y) for _ in xrange(2)]
    while True:
        while not is_good(t, b):
            t, b = [random_vector(m, x, y) for _ in xrange(2)]
        yield t, b
