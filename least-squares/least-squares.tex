\documentclass[usenames,dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\usepackage{tikz-3dplot}

\title{Least-Squares Approximations}
\subtitle{Math 218}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Motivation}
\subsection{Most Systems are Unsolvable}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{In ``Real Life''}
    Coefficients in systems are obtained by ``imprecise'' methods, such as
    physical measurements or floating-point rounding. \pause This imprecision
    can render a system unsolvable.
  \end{block}

  \pause
  \begin{block}{Question}
    What is the best way to ``solve'' an inconsistent system?
  \end{block}

\end{frame}



\subsection{Example}

\begin{sagesilent}
  p1 = (0, 6)
  p2 = (1, 0)
  p3 = (2, 0)
  s = Set([p1, p2, p3])
  var('a0 a1 t')
  f = a0+a1*t
  A = matrix.column([(1, 1, 1),(0, 1, 2)])
  b = vector([6, 0, 0])
  M = A.augment(b, subdivide=True)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.6\textwidth}
    \begin{block}{Problem}
      Find the line $f(t)=\sage{f}$ passing through $\sage{s}$.
    \end{block}
    \column{.4\textwidth}
    \[
      \scalebox{.75}{%
        \begin{tikzpicture}[line join=round, line cap=round, yscale=1/2]
          \coordinate (O) at (0, 0);

          \coordinate (p1) at (0, 6);
          \coordinate (p2) at (1, 0);
          \coordinate (p3) at (2, 0);

          \draw[ultra thick, <->] (-1, 0) -- (3, 0) node[right] {$x$};
          \draw[ultra thick, <->] (0, -2) -- (0, 7) node[above] {$y$};

          \node[blue] at (p1) {\textbullet};
          \node[blue] at (p2) {\textbullet};
          \node[blue] at (p3) {\textbullet};

          \node at (p1) [right] {$\sage{p1}$};
          \node at (p2) [below] {$\sage{p2}$};
          \node at (p3) [below] {$\sage{p3}$};

        \end{tikzpicture}
      }
    \]
  \end{columns}
  \onslide<2->
  \begin{block}{Answer}
    Such a line would give the system
    \newcommand{\mySys}{
      \arraycolsep=1pt
      \begin{array}{rclcrcrcr}
        \onslide<3->{f(\sage{p1[0]}) &=& \sage{p1[1]} &\iff& a_0 &+& \sage{p1[0]}\cdot a_1 &=& \sage{p1[1]}} \\
        \onslide<4->{f(\sage{p2[0]}) &=& \sage{p2[1]} &\iff& a_0 &+& \sage{p2[0]}\cdot a_1 &=& \sage{p2[1]}} \\
        \onslide<5->{f(\sage{p3[0]}) &=& \sage{p3[1]} &\iff& a_0 &+& \sage{p3[0]}\cdot a_1 &=& \sage{p3[1]}}
      \end{array}
    }
    \begin{align*}
      \mySys && \onslide<7->{\rref}\onslide<6->{\sage{M}}\onslide<7->{=\sage{M.rref()}}
    \end{align*}
    \onslide<8->{The system is inconsistent, so no such line exists!}
  \end{block}

\end{frame}



\begin{sagesilent}
  x = matrix.column([a0, a1])
  b = matrix.column(b)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Our system is of the form $A\vv{x}=\vv{b}$ where
    \begin{align*}
      A &= \sage{A} & \vv{x} &= \sage{x} & \vv{b} &= \sage{b}
    \end{align*}\pause
    The system is \emph{inconsistent}, meaning that no $\vv{x}$ solves
    $A\vv{x}=\vv{b}$.
  \end{block}

  \pause
  \begin{block}{Problem}
    $\vv{b}\notin\Col(A)$
  \end{block}

  \pause
  \begin{block}{Idea}
    Project $\vv{b}$ onto $\Col(A)$ and solve $A\widehat{x}=P\vv{b}$.
  \end{block}

\end{frame}




\section{The Method of Least Squares}
\subsection{Derivation}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \begin{columns}[onlytextwidth]
    \column{.4\textwidth}
    \begin{block}{Question}
      Consider the scalar
      \[
        E=\norm{\vv{b}-A\widehat{x}}^2
      \]
      What $\widehat{x}$ \emph{minimizes} $E$?
    \end{block}
    \column{.6\textwidth}
    \[
      \begin{tikzpicture}[
        % , scale=3/4
        , line join=round
        , line cap=round
        ]
        \coordinate(O) at (0, 0, 0);

        \coordinate (e2) at (1, 0, 0);
        \coordinate (e3) at (0, 1, 0);
        \coordinate (e1) at (0, 0, 1);

        \pgfmathsetmacro{\bx}{-1.5}
        \pgfmathsetmacro{\by}{2}
        \pgfmathsetmacro{\bz}{2}

        \coordinate (b) at ($ \bx*(e1) + \by*(e2) + \bz*(e3) $);
        \coordinate (p) at ($ \bx*(e1) + \by*(e2) + 0*(e3) $);

        \pgfmathsetmacro{\myV}{1.65}
        \coordinate (ne) at ($ \myV*\bx*(e1) + \myV*\by*(e2) $);
        \coordinate (se) at ($ -\bx*(e1) + \myV*\by*(e2) $);
        \coordinate (sw) at ($ -\bx*(e1) - \myV*\by*(e2) $);
        \coordinate (nw) at ($ \myV*\bx*(e1) - \myV*\by*(e2) $);
        \coordinate (Vsw) at ($ (se)!.65!(sw) $);
        \coordinate (Vnw) at ($ (ne)!.65!(nw) $);

        \onslide<2->{
          \filldraw[ultra thick, draw=teal, fill=teal!35]
          (ne) -- (se) -- (Vsw) -- (Vnw) -- cycle;
        }

        \onslide<6->{
          \pgfmathsetmacro{\myPerp}{1/6}
          \coordinate (u1) at ($ (O)!1cm!(p) $);
          \coordinate (u2) at ($ (O)!1cm!(b)-(p) $);
          \draw[ultra thick]
          ($ (p) + \myPerp*(u1) $)
          -- ($ (p)+\myPerp*(u1) + \myPerp*(e3) $)
          -- ($ (p)+\myPerp*(e3) $);
        }

        \onslide<4->{
          \draw[ultra thick, red, ->] (p) -- (b) node[midway, right] {$\vv{b}-A\widehat{x}$};
        }
        % \draw[ultra thick, ->, red] (p) -- (b) node[midway, right] {$\vv{b}-A\widehat{x}$};

        \onslide<2->{
          \draw[ultra thick, ->, blue] (O) -- (b) node[above] {$\vv{b}$};
        }

        \onslide<3->{
          \draw[ultra thick, ->, purple] (O) -- (p) node[midway, sloped, below] {$A\widehat{x}$};
        }

        \onslide<2->{
          \draw[teal] (Vsw) -- (se) node[near start, sloped, below] {$\Col(A)$};
        }
      \end{tikzpicture}
    \]
  \end{columns}
  \onslide<5->
  \begin{block}{Answer}
    To minimize $E=\norm{\vv{b}-A\widehat{x}}^2$, we force
    $\vv{b}-A\widehat{x}\perp\Col(A)$. \onslide<6->Then
    \begin{align*}
      \onslide<7->{\vv{b}-A\widehat{x}\in\LNull(A)} && \onslide<8->{A^\intercal (\vv{b}-A\widehat{x})=\vv{O}}
    \end{align*}
    \onslide<9->{Our desired $\widehat{x}$ is a solution to
      $A^\intercal A\widehat{x}=A^\intercal\vv{b}$.}
  \end{block}

\end{frame}


\subsection{Statement}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Solutions $\widehat{x}$ to $A^\intercal A\widehat{x}=A^\intercal\vv{b}$ are
    called the \emph{least-squares approximate solutions} to $A\vv{x}=\vv{b}$.
  \end{definition}

  \pause
  \begin{theorem}
    The \mybold{error} $E=\norm{\vv{b}-A\widehat{x}}^2$ is minimized when
    $\widehat{x}$ is a least-squares approximate solution to $A\vv{x}=\vv{b}$.
  \end{theorem}

  \pause
  \begin{theorem}
    Let $\widehat{x}$ be a least-squares approximate solution to
    $A\vv{x}=\vv{b}$ and let $P$ be projection onto $\Col(A)$. Then
    $A\widehat{x}=P\vv{b}$.
  \end{theorem}

  \pause
  \begin{theorem}
    The system $A\vv{x}=\vv{b}$ has exactly one least-squares approximate
    solution $\widehat{x}=(A^\intercal A)^{-1}A^\intercal\vv{b}$ if and only if
    $A$ has full column rank.
  \end{theorem}

\end{frame}




\subsection{Example}

\begin{sagesilent}
  A = matrix([(-2, -5), (3, 7), (3, 7)])
  b = vector([-12, 6, 2])
  bc = matrix.column(b)
  xhat = (A.T*A).inverse()*A.T*b
  M = (A.T*A).augment(A.T*b, subdivide=True)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Consider the inconsistent system $A\vv{x}=\vv{b}$ where
  \begin{align*}
    A &= \sage{A} & \vv{b} &= \sage{bc}
  \end{align*}\pause
  The least squares approximate solution $\widehat{x}$ is given by
  \[
    \rref[A^\intercal A\mid A^\intercal \vv{b}]
    = \rref\sage{M}
    = \sage{M.rref()}
  \]\pause
  This gives $\widehat{x}=\sage{xhat}$. \pause The error is
  \[
    E
    = \norm{\vv{b}-A\widehat{x}}^2
    = \norm{\sage{b}-\sage{A*xhat}}^2
    = \sage{(b-A*xhat).norm()**2}
  \]\pause
  The projection of $\vv{b}$ onto $\Col(A)$ is $P\vv{b}=A\widehat{x}=\sage{A*xhat}$.

\end{frame}



\section{Applications}
\subsection{Linear Regression}

\begin{sagesilent}
  p1 = (0, 6)
  p2 = (1, 0)
  p3 = (2, 0)
  s = Set([p1, p2, p3])
  var('a0 a1 t')
  f = a0+a1*t
  A = matrix.column([(1, 1, 1),(0, 1, 2)])
  b = vector([6, 0, 0])
  bc = matrix.column(b)
  M = (A.T*A).augment(A.T*b, subdivide=True)
  xhat = (A.T*A).inverse()*A.T*b
  l = xhat*vector([1, t])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.7\textwidth}
    The nonexistant line $f(t)=\sage{f}$ passing through our points defined
    the inconsistent system $A\vv{x}=\vv{b}$ where
    \begin{align*}
      A &= \sage{A} & \vv{x} &= \sage{x} & \vv{b} &= \sage{bc}
    \end{align*}
    \column{.4\textwidth}
    \[
      \scalebox{.75}{%
        \begin{tikzpicture}[line join=round, line cap=round, yscale=1/2]
          \coordinate (O) at (0, 0);

          \coordinate (p1) at (0, 6);
          \coordinate (p2) at (1, 0);
          \coordinate (p3) at (2, 0);

          \draw[ultra thick, <->] (-1, 0) -- (3, 0) node[right] {$x$};
          \draw[ultra thick, <->] (0, -2) -- (0, 7) node[above] {$y$};

          \node[blue] at (p1) {\textbullet};
          \node[blue] at (p2) {\textbullet};
          \node[blue] at (p3) {\textbullet};

          \node at (p1) [right] {$\sage{p1}$};
          \node at (p2) [below] {$\sage{p2}$};
          \node at (p3) [above] {$\sage{p3}$};

          \onslide<4->{
            \draw[domain=-2/3:7/3, ultra thick, variable=\t, teal, <->]
            plot ({\t},{-3*\t+5}) node[right] {$f(t)$};
          }

          \onslide<8->{
            \draw[ultra thick, red] (p1) -- (0, 5);
          }
          \onslide<10->{
            \draw[ultra thick, red] (p2) -- (1, 2);
          }
          \onslide<12->{
            \draw[ultra thick, red] (p3) -- (2, -1);
          }


        \end{tikzpicture}
      }
    \]
  \end{columns}
  \onslide<2->{The least squares approximate solution to $A\vv{x}=\vv{b}$ is given by}
  \[
    \onslide<2->{
      \rref[A^\intercal A\mid A^\intercal\vv{b}]
      = \rref\sage{M}
      = \sage{M.rref()}}
  \]
  \onslide<3->{This gives the ``line of best fit'' $f(t)=\sage{l}$. }%
  \onslide<5->{The error of this approximation is}
  \[
    \onslide<6->{E=}
    \onslide<7->{(\sage{p1[1]}-f(\sage{p1[0]}))^2+}
    \onslide<9->{(\sage{p2[1]}-f(\sage{p2[0]}))^2+}
    \onslide<11->{(\sage{p3[1]}-f(\sage{p3[0]}))^2=}
    \onslide<13>{\sage{(b-A*xhat).norm() ** 2}}
  \]

\end{frame}




\begin{sagesilent}
  var('a0 a1 t')
  f = a0+a1*t
  xhat = vector([a0, a1])
  xhatc = matrix.column(xhat)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Consider the data points $\Set{(t_1, b_1), \dotsc, (t_m, b_m)}$. \pause Let
  \newcommand{\myA}{ \left[
      \begin{array}{cc}
        1 & t_1 \\
        1 & t_2 \\
        \vdots & \vdots \\
        1 & t_m
      \end{array}
    \right]
  }
  \newcommand{\myb}{
    \left[
      \begin{array}{c}
        b_1\\ b_2\\ \vdots\\ b_m
      \end{array}
    \right]
  }
  \begin{align*}
    A &= \myA & \vv{b} &= \myb
  \end{align*}\pause
  Note that
  \newcommand{\myAT}{
    \left[
      \begin{array}{cccc}
        1 & 1 & \dotsb & 1 \\
        t_1 & t_2 & \dotsb & t_m
      \end{array}
    \right]
  }
  \newcommand{\myATA}{
    \left[
      \begin{array}{cc}
        m & \Sigma t_i \\
        \Sigma t_i & \Sigma t_i^2
      \end{array}
    \right]
  }
  \newcommand{\myATb}{
    \left[
      \begin{array}{c}
        \Sigma b_i\\ \Sigma t_i b_i
      \end{array}
    \right]
  }
  \begin{align*}
    A^\intercal A &= \myAT\myA = \myATA \\
    A^\intercal\vv{b} &= \myAT\myb = \myATb
  \end{align*}


\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Consider the data points $\Set{(t_1, b_1), \dotsc, (t_m, b_m)}$.
    Let
    \newcommand{\myA}{ \left[
        \begin{array}{cc}
          1 & t_1 \\
          1 & t_2 \\
          \vdots & \vdots \\
          1 & t_m
        \end{array}
      \right]
    }
    \newcommand{\myb}{
      \left[
        \begin{array}{c}
          b_1\\ b_2\\ \vdots\\ b_m
        \end{array}
      \right]
    }
    \begin{align*}
      A &= \myA & \vv{b} &= \myb
    \end{align*}
    The choice $\widehat{x}=\sage{xhat}$ that minimizes
    $E=\norm{A\vv{x}-\vv{b}}^2$ is the solution to
    \newcommand{\myATA}{ \left[
        \begin{array}{cc}
          m & \Sigma t_i \\
          \Sigma t_i & \Sigma t_i^2
        \end{array}
      \right]
    }
    \newcommand{\myATb}{
      \left[
        \begin{array}{c}
          \Sigma b_i\\ \Sigma t_i b_i
        \end{array}
      \right]
    }
    \[
      \underset{A^\intercal A}{\myATA}
      \underset{\widehat{x}}{\sage{xhatc}}
      =
      \underset{A^\intercal\vv{b}}{\myATb}
    \]
    The line $f(t)=\sage{f}$ is the ``line of best fit'' to the data.
  \end{theorem}

\end{frame}




\begin{sagesilent}
  p1 = (1, 6)
  p2 = (2, 5)
  p3 = (3, 7)
  p4 = (4, 10)
  s = Set([p1, p2, p3, p4])
  t, b = matrix([p1, p2, p3, p4]).columns()
  A = matrix.column([(1,) * len(t), t])
  bc = matrix.column(b)
  var('a0 a1 t')
  f = a0+a1*t
  M = (A.T*A).augment(A.T*b, subdivide=True)
  xhat = (A.T*A).inverse()*A.T*b
  l = xhat*vector([1, t])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}%
    Consider the problem of fitting a line to the depicted data points
    \begin{align*}
      A &= \sage{A} & \vv{b} &= \sage{bc}
    \end{align*}
    \column{.5\textwidth}
    \[
      \scalebox{.6}{%
        \begin{tikzpicture}[line join=round, line cap=round, yscale=1/2]
          \coordinate (O) at (0, 0);

          \coordinate (p1) at (1, 6);
          \coordinate (p2) at (2, 5);
          \coordinate (p3) at (3, 7);
          \coordinate (p4) at (4, 10);

          \draw[ultra thick, <->] (-1, 0) -- (5, 0);
          \draw[ultra thick, <->] (0, -2) -- (0, 11);

          \node[blue] at (p1) {\textbullet};
          \node[blue] at (p2) {\textbullet};
          \node[blue] at (p3) {\textbullet};
          \node[blue] at (p4) {\textbullet};

          \node at (p1) [above] {$\sage{p1}$};
          \node at (p2) [right] {$\sage{p2}$};
          \node at (p3) [right] {$\sage{p3}$};
          \node at (p4) [left] {$\sage{p4}$};

          \onslide<4->{
            \draw[domain=-1:75/14, ultra thick, variable=\t, teal, <->]
            plot ({\t},{7/2+7*\t/5}) node[right] {$f(t)$};
          }

          \onslide<7->{
            \draw[ultra thick, red] (p1) -- (1, 49/10);
          }
          \onslide<9->{
            \draw[ultra thick, red] (p2) -- (2, 63/10);
          }
          \onslide<11->{
            \draw[ultra thick, red] (p3) -- (3, 77/10);
          }
          \onslide<13->{
            \draw[ultra thick, red] (p4) -- (4, 91/10);
          }
        \end{tikzpicture}
      }
    \]
  \end{columns}
  \onslide<2->{The line of best fit $f(t)=\sage{f}$ is determined by}
  \newcommand{\mySys}{
    \left[
      \begin{array}{cc|c}
        m          & \Sigma t_i   & \Sigma b_i\\
        \Sigma t_i & \Sigma t_i^2 & \Sigma t_i b_i
      \end{array}
    \right]
  }
  \[
    \onslide<2->{\rref\mySys
    = \rref\sage{M}
    = \sage{M.rref()}}
  \]
  \onslide<3->{This gives $f(t)=\sage{l}$.}%
  \onslide<5->{The error is}
  \[
    \onslide<5->{E=}
    \onslide<6->{\pair*{\sage{p1[1]-l(t=p1[0])}}^2+}
    \onslide<8->{\pair*{\sage{p2[1]-l(t=p2[0])}}^2+}
    \onslide<10->{\pair*{\sage{p3[1]-l(t=p3[0])}}^2+}
    \onslide<12->{\pair*{\sage{p4[1]-l(t=p4[0])}}^2=}
    \onslide<14->{\sage{(b-A*xhat).norm()**2}}
  \]


\end{frame}



\subsection{Quadratic Regression}


\begin{sagesilent}
  p1 = (-2, 0)
  p2 = (-1, 0)
  p3 = (0, 1)
  p4 = (1, 0)
  p5 = (2, 0)
  s = Set([p1, p2, p3, p4, p5])
  P = matrix([p1, p2, p3, p4, p5])
  t, b = P.columns()
  A = matrix.column([(1,)*len(t), t, tuple(_**2 for _ in t)])
  var('a0 a1 a2 t')
  f = a0+a1*t+a2*t**2
  M = A.augment(b, subdivide=True)
\end{sagesilent}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.6\textwidth}%
    Consider the problem of fitting a parabola
    \[
      f(t)=\sage{f}
    \]
    to the depicted data points.
    \column{.4\textwidth}
    \[
      \scalebox{.75}{%
        \begin{tikzpicture}[line join=round, line cap=round]
          \coordinate (O) at (0, 0);

          \coordinate (p1) at (-2, 0);
          \coordinate (p2) at (-1, 0);
          \coordinate (p3) at (0, 1);
          \coordinate (p4) at (1, 0);
          \coordinate (p5) at (2, 0);

          \draw[ultra thick, <->] (-3, 0) -- (3, 0);
          \draw[ultra thick, <->] (0, -1) -- (0, 2);

          \node[blue] at (p1) {\textbullet};
          \node[blue] at (p2) {\textbullet};
          \node[blue] at (p3) {\textbullet};
          \node[blue] at (p4) {\textbullet};
          \node[blue] at (p5) {\textbullet};

          \node at (p1) [above] {$\sage{p1}$};
          \node at (p2) [below] {$\sage{p2}$};
          \node at (p3) [right] {$\sage{p3}$};
          \node at (p4) [below] {$\sage{p4}$};
          \node at (p5) [above] {$\sage{p5}$};


          % \draw[domain=-1:75/14, ultra thick, variable=\t, blue, <->]
          % plot ({\t},{7/2+7*\t/5}) node[right] {$f(t)$};

          % \draw[ultra thick, red] (p1) -- (1, 49/10);
          % \draw[ultra thick, red] (p2) -- (2, 63/10);
          % \draw[ultra thick, red] (p3) -- (3, 77/10);
          % \draw[ultra thick, red] (p4) -- (4, 91/10);
        \end{tikzpicture}
      }
    \]
  \end{columns}\pause
  This gives the system
  \[
    \rref\sage{M}=\sage{M.rref()}
  \]\pause
  The system is inconsistent, so no such parabola exists.

\end{frame}



\begin{sagesilent}
  xhat = vector([a0, a1, a2])
  M = (A.T*A).augment(A.T*b, subdivide=True)
  sxhat = (A.T*A).inverse()*A.T*b
  l = sxhat * vector([1, t, t**2])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.6\textwidth}%
    The least squares solution to the system is obtained by solving
    \[
      A^\intercal A\widehat{x}=A^\intercal\vv{b}
    \]
    for $\widehat{x}=\sage{xhat}$.
    \column{.4\textwidth}
    \[
      \scalebox{.75}{%
        \begin{tikzpicture}[line join=round, line cap=round]
          \coordinate (O) at (0, 0);

          \coordinate (p1) at (-2, 0);
          \coordinate (p2) at (-1, 0);
          \coordinate (p3) at (0, 1);
          \coordinate (p4) at (1, 0);
          \coordinate (p5) at (2, 0);

          \draw[ultra thick, <->] (-3, 0) -- (3, 0);
          \draw[ultra thick, <->] (0, -1) -- (0, 2);

          \node[blue] at (p1) {\textbullet};
          \node[blue] at (p2) {\textbullet};
          \node[blue] at (p3) {\textbullet};
          \node[blue] at (p4) {\textbullet};
          \node[blue] at (p5) {\textbullet};

          \node at (p1) [above] {$\sage{p1}$};
          \node at (p2) [below] {$\sage{p2}$};
          \node at (p3) [right] {$\sage{p3}$};
          \node at (p4) [below] {$\sage{p4}$};
          \node at (p5) [above] {$\sage{p5}$};


          \onslide<4->{
            \draw[domain=-3:3, ultra thick, variable=\t, teal, <->]
            plot ({\t},{17/35-\t*\t/7}) node[below] {$f(t)$};
          }

          % \draw[ultra thick, red] (p1) -- (1, 49/10);
          % \draw[ultra thick, red] (p2) -- (2, 63/10);
          % \draw[ultra thick, red] (p3) -- (3, 77/10);
          % \draw[ultra thick, red] (p4) -- (4, 91/10);
        \end{tikzpicture}
      }
    \]
  \end{columns}
  \onslide<2->{This gives the system}
  \[
    \onslide<2->{\rref\sage{M}=\sage{M.rref()}}
  \]
  \onslide<3->{Our ``parabola of best fit'' is thus $f(t)=\sage{l}$.}

\end{frame}




\end{document}
