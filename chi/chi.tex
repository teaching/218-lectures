\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\usepackage{booktabs}
\newcommand{\Etable}[1]{
  \begin{center}
    \begin{table}
      \centering
      % \caption*{\myotherbold{#1}}
      \begin{tabular}{rcc}
        \toprule
        \myotherbold{$\lambda$} & \myotherbold{$\gm_A(\lambda)$} & \myotherbold{$\am_A(\lambda)$} \\
        \midrule
        #1
        \bottomrule
      \end{tabular}
    \end{table}
  \end{center}
}

\title{The Characteristic Polynomial}
\subtitle{Math 218}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{The Characteristic Polynomial}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    We can find \emph{eigenvectors} by finding a basis of
    $\mathcal{E}_A(\lambda)$.
  \end{block}

  \pause
  \begin{block}{Question}
    How can we find eigenvalues?
  \end{block}

\end{frame}


\begin{sagesilent}
  latex.matrix_delimiters(left='|', right='|')
  A = matrix([(1, -1), (2, 0)])
  var('t')
  Id = identity_matrix(A.nrows())
  chi = t*Id-A
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{characteristic polynomial} of an $A$ is
    $\chi_A(t)=\det(t\cdot I_n-A)$.
  \end{definition}

  \newcommand{\myA}{
    \left[
      \begin{array}{rr}
        1 & -1 \\
        2 &  0
      \end{array}
    \right]
  }
  \newcommand{\mytId}{
    \left[
      \begin{array}{rr}
        t & 0 \\
        0 & t
      \end{array}
    \right]
  }

  \onslide<2->
  \begin{example}
    \vspace*{-\baselineskip}\setlength\belowdisplayshortskip{0pt}
    \begin{gather*}
      \chi_A(t)
      =
      \onslide<3->{\det\biggl({\overset{t\cdot I_{\sage{A.nrows()}}}{\mytId}-\overset{A}{\myA}}\biggr) =}
      \onslide<4->{\sage{chi} =}
      \onslide<5->{\sage{chi.det()} =}
      \onslide<6->{\sage{A.characteristic_polynomial(t)}}
    \end{gather*}
  \end{example}

\end{frame}


\subsection{Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose $A$ is $n\times n$. Then $\chi_A(t)$ is monic and $\deg\chi_A(t)=n$.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Why is $\chi_A(t)$ useful?
  \end{block}

  \pause
  \begin{block}{Answer}
    Suppose $r$ is a root of $\chi_A(t)$ so that
    \[
      \det(r\cdot I_n-A) = \chi_A(r) = 0
    \]
    \pause This means that $r\cdot I_n-A$ is \pause \emph{singular} \pause and
    so is $A-r\cdot I_n$.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The eigenvalues of $A$ are the roots of $\chi_A(t)$.
  \end{theorem}

  \onslide<2->
  \begin{block}{Observation}
    We can find the eigenvalues of $A$ by finding the roots of $\chi_A(t)$.
  \end{block}

  \newcommand{\myStrang}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\color<4->{red}\det(A-t\cdot I_n)$};
      \onslide<4->{
        \node[overlay, below right= 2mm and 3mm of a] (text) {same roots but not monic};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }

  \onslide<3->
  \begin{alertblock}{Warning}
    Some authors use $\myStrang=(-1)^n\cdot\det(t\cdot I_n-A)$.
  \end{alertblock}

\end{frame}


\subsection{Examples}
\begin{sagesilent}
  latex.matrix_delimiters(left='|', right='|')
  A = matrix([(1, 1), (1, 1)])
  Id = identity_matrix(A.nrows())
  chi = t*Id-A
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myA}{
    \left[
      \begin{array}{rr}
        1 & 1 \\
        1 & 1
      \end{array}
    \right]
  }
  \newcommand{\mytId}{
    \left[
      \begin{array}{rr}
        t & 0 \\
        0 & t
      \end{array}
    \right]
  }
  \newcommand{\mya}{(1)}
  \newcommand{\myb}{(-3)}
  \newcommand{\myc}{(3)}
  \newcommand{\myeqA}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$=$};
      \node[overlay, below right= 4mm and 3mm of a] (text) {$\dfrac{3-\sqrt{9-4\cdot 3}}{2}$};
      \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
    }
  }
  \begin{example}
    The computation
    \begin{gather*}
      \chi_A(t)
      =
      \det\biggl({\overset{t\cdot I_{\sage{A.nrows()}}}{\mytId}-\overset{A}{\myA}}\biggr) =
      \onslide<2->{\sage{chi} =}
      \onslide<3->{\sage{chi.det()} =}
      \onslide<4->{\sage{factor(A.characteristic_polynomial(t))}}
    \end{gather*}
    \onslide<5->{shows that $\EVals(A)=\onslide<6->{\Set{0, 2}$.}}
  \end{example}

\end{frame}


\begin{sagesilent}
  latex.matrix_delimiters(left='|', right='|')
  A = matrix([(1, -1), (1, 2)])
  Id = identity_matrix(A.nrows())
  chi = t*Id-A
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myA}{
    \left[
      \begin{array}{rr}
        1 & -1 \\
        1 &  2
      \end{array}
    \right]
  }
  \newcommand{\mytId}{
    \left[
      \begin{array}{rr}
        t & 0 \\
        0 & t
      \end{array}
    \right]
  }
  \newcommand{\mya}{(1)}
  \newcommand{\myb}{(-3)}
  \newcommand{\myc}{(3)}
  \newcommand{\myeqA}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$=$};
      \onslide<6->{
        \node[overlay, below right= 4mm and 3mm of a] (text) {$\dfrac{3-\sqrt{9-4\cdot 3}}{2}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \newcommand{\myeqB}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$=$};
      \onslide<7->{
        \node[overlay, below right= 4mm and 3mm of a] (text) {$\dfrac{3+\sqrt{9-4\cdot 3}}{2}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{example}
    The computation
    \begin{gather*}
      \chi_A(t) =
      \det\biggl({\overset{t\cdot I_{\sage{A.nrows()}}}{\mytId}-\overset{A}{\myA}}\biggr) =
      \onslide<2->{\sage{chi} =}
      \onslide<3->{\sage{chi.det()} =}
      \onslide<4->{\sage{A.characteristic_polynomial(t)}}
    \end{gather*}
    \onslide<5->shows that the eigenvalues of $A$ are
    \begin{align*}
      \lambda_1 &\myeqA \frac{3-\sqrt{3}\cdot i}{2} & \lambda_2 &\myeqB \frac{3+\sqrt{3}\cdot i}{2}
    \end{align*}
  \end{example}

\end{frame}


\begin{sagesilent}
  latex.matrix_delimiters(left='|', right='|')
  A = matrix([(-5, -12, 12), (4, 11, -12), (2, 6, -7)])
  Id = identity_matrix(A.nrows())
  chi = t*Id-A
  from functools import partial
  elem = partial(elementary_matrix, chi.nrows())
  E1 = elem(row1=0, row2=2, scale=2)
  E2 = elem(row1=1, row2=2, scale=-2)
  S1 = elem(row1=0, scale=1/(t+1))
  S2 = elem(row1=1, scale=1/(t+1))
  E3 = elem(row1=2, row2=0, scale=2)
  E4 = elem(row1=2, row2=1, scale=6)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myA}{
    \left[
      \begin{array}{rrr}
        -5 & -12 &  12 \\
        4 &  11 & -12 \\
        2 &   6 &  -7
      \end{array}
    \right]
  }
  \newcommand{\mytId}{
    \left[
      \begin{array}{rrr}
        t & 0 & 0 \\
        0 & t & 0 \\
        0 & 0 & t
      \end{array}
    \right]
  }
  \newcommand{\hello}[3]{}
  \newcommand{\myeqA}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$=$};
      \onslide<3->{
        \node[overlay, above left= 2mm and 1.25cm of a] (text) {$\begin{array}{r}R_1+2\cdot R_3 \to R_1\\ R_2-2\cdot R_3 \to R_2\end{array}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.west) -| (text.south);
      }
    }
  }
  \newcommand{\myFactorA}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{E2*E1*chi}$};
      \onslide<4->{
        \node[overlay, above right= -5mm and 1cm of a] (text) {$\begin{array}{ll}(t+1)\textnormal{ divides }R_1\\ (t+1)\textnormal{ divides }R_2\end{array}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.east) -| (text.south);
      }
    }
  }
  \newcommand{\myeqB}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$=$};
      \onslide<7->{
        \node[overlay, above left= 2mm and 0.5cm of a] (text) {$R_3+2\cdot R_1\to R_3$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.west) -| (text.south);
      }
    }
  }
  \newcommand{\myeqC}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$=$};
      \onslide<9->{
        \node[overlay, above left= 2mm and 0.5cm of a] (text) {$R_3+6\cdot R_1\to R_3$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.west) -| (text.south);
      }
    }
  }
  \begin{example}
    The computation
    \scalebox{.70}{\parbox{1.4285\linewidth}{%
        \begin{align*}
          \chi_A(t)
          &= \sage{chi} \\
          &\onslide<2->{\myeqA \myFactorA} \\
          &\onslide<5->{=(t+1)^2\sage{S2*S1*(E2*E1*chi)}} \\
          &\onslide<6->{\myeqB(t+1)^2\sage{E3*(S2*S1*(E2*E1*chi))}} \\
          &\onslide<8->{\myeqC(t+1)^2\sage{E4*E3*(S2*S1*(E2*E1*chi))}} \\
          &\onslide<10->{=(t+1)^2(t-1)}
        \end{align*}
      }}
    \onslide<11->{shows that $\EVals(A)=\onslide<12->{\Set{-1, 1}$.}}
  \end{example}

\end{frame}


\section{Algebraic Multiplicity}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    The Fundamental Theorem of Algebra says $\chi_A(t)$ factors as
    \[
      \chi_A(t)=(t-\lambda_1)^{m_1}\cdot(t-\lambda_2)^{m_2}\dotsb(t-\lambda_k)^{m_k}
    \]
    where $\Set{\lambda_1,\dotsc,\lambda_k}$ are the eigenvalues of $A$.
  \end{block}

  \pause
  \begin{definition}
    The \emph{algebraic multiplicity} of $\lambda_j$ is $\am_A(\lambda_j)=m_j$.
  \end{definition}

  \pause
  \begin{theorem}
    $\am_A(\lambda_1)+\am_A(\lambda_2)+\dotsb+\am_A(\lambda_k)=n$
  \end{theorem}

\end{frame}


\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  A = matrix([(-88, 243, 162), (-36, 101, 72), (9, -27, -25)])
  var('t')
  chi = factor(A.characteristic_polynomial(t))
  l1, l2 = set(A.eigenvalues())
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $A$ and its characteristic polynomial.
    \begin{align*}
      A &= \sage{A} & \chi_A(t) &= (t+7)^2\cdot(t-2)
    \end{align*}
    \onslide<2->We can organize the ``eigendata'' into a table.
    \Etable{
      \onslide<3->{$\sage{l1}$} & \onslide<7->{$?$} & \onslide<5->{$\sage{A.eigenvalues().count(l1)}$} \\
      \onslide<4->{$\sage{l2}$} & \onslide<8->{$?$} & \onslide<6->{$\sage{A.eigenvalues().count(l2)}$} \\
    }
  \end{example}

\end{frame}


\begin{sagesilent}
  l1, l2, l3 = -9, 11, 17
  J1 = jordan_block(l1, 2)
  J2 = jordan_block(l2, 2)
  J3 = jordan_block(l3, 1)
  J = block_diagonal_matrix([J1, J2, J3])
  n = J.nrows()
  chi = factor(J.characteristic_polynomial(t))
  Id = identity_matrix(n)
  gm = lambda l: (J-l*Id).right_nullity()
  am = lambda l: J.eigenvalues().count(l)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $J$ and its characteristic polynomial.
    \begin{gather*}
      \begin{align*}
        J &= \sage{J} & \chi_J(t) &= (t+9)^2\cdot(t-11)^2\cdot(t-17)
      \end{align*}
    \end{gather*}
    \onslide<2->Our table of eigenvalues is
    \Etable{
      \onslide<3->{$\sage{l1}$} & \onslide<9->{$\sage{gm(l1)}$} & \onslide<6->{$\sage{am(l1)}$} \\
      \onslide<4->{$\sage{l2}$} & \onslide<10->{$\sage{gm(l2)}$} & \onslide<7->{$\sage{am(l2)}$} \\
      \onslide<5->{$\sage{l3}$} & \onslide<11->{$\sage{gm(l3)}$} & \onslide<8->{$\sage{am(l3)}$} \\
    }
  \end{example}

\end{frame}


\subsection{Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $1\leq\gm_A(\lambda)\leq\am_A(\lambda)$
  \end{theorem}

\end{frame}


\begin{sagesilent}
  l1, l2, l3, l4 = -1, 0, 2, 3
  J1 = jordan_block(l1, 2)
  J2 = jordan_block(l2, 3)
  J3 = jordan_block(l3, 2)
  J4 = jordan_block(l4, 1)
  J = block_diagonal_matrix([J1, J2, J3, J4])
  set_random_seed(89041)
  P = random_matrix(ZZ, J.nrows(), algorithm='unimodular', upper_bound=3)
  A = P*J*P.inverse()
  var('t')
  chi = factor(J.characteristic_polynomial(t))
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the data
    \begin{gather*}
      \begin{align*}
        A &= \sage{A} & \chi_A(t) &= \sage{chi}
      \end{align*}
    \end{gather*}
    \onslide<2->This gives our table of eigenvalues.
    \Etable{
      \onslide<3->{$\sage{l1}$} & $\onslide<11->{1,} \onslide<12->{2}$                   & \onslide<7->{$\sage{A.eigenvalues().count(l1)}$} \\
      \onslide<4->{$\sage{l2}$} & $\onslide<13->{1,} \onslide<14->{2,} \onslide<15->{3}$ & \onslide<8->{$\sage{A.eigenvalues().count(l2)}$} \\
      \onslide<5->{$\sage{l3}$} & $\onslide<16->{1,} \onslide<17->{2}$                   & \onslide<9->{$\sage{A.eigenvalues().count(l3)}$} \\
      \onslide<6->{$\sage{l4}$} & $\onslide<18->{1}$                                     & \onslide<10->{$\sage{A.eigenvalues().count(l4)}$} \\
    }
  \end{example}

\end{frame}



\begin{sagesilent}
  var('t')
  l1, m1 = -4, 6
  l2, m2 = 2, 7
  l3, m3 = 3, 2
  l4, m4 = 0, 1
  chi = (t-l1)**m1 * (t-l2)**m2 * (t-l3)**m3 * (t-l4)**m4
  n = chi.degree(t)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose that $A$ is a matrix with
    \[
      \chi_A(t) = \sage{chi}
    \]
    \onslide<2->This gives our table of eigenvalues.
    \Etable{
      \onslide<3->{$\sage{l1}$} & \onslide<11->{$1, 2, 3, 4, 5, 6$}    & \onslide<7->{$\sage{m1}$} \\
      \onslide<4->{$\sage{l2}$} & \onslide<12->{$1, 2, 3, 4, 5, 6, 7$} & \onslide<8->{$\sage{m2}$} \\
      \onslide<5->{$\sage{l3}$} & \onslide<13->{$1, 2$}                & \onslide<9->{$\sage{m3}$} \\
      \onslide<6->{$\sage{l4}$} & \onslide<14->{$1$}                   & \onslide<10->{$\sage{m4}$} \\
    }
    \onslide<15->Note that $A$ is \emph{singular} ($\lambda=0$) \onslide<16->and $\sage{n}\times\sage{n}$ since
    \[
      \overset{\onslide<17->{\sage{m1}}}{\am_A(\sage{l1})}+
      \overset{\onslide<18->{\sage{m2}}}{\am_A(\sage{l2})}+
      \overset{\onslide<19->{\sage{m3}}}{\am_A(\sage{l3})}+
      \overset{\onslide<20->{\sage{m4}}}{\am_A(\sage{l4})}
      =
      \onslide<21->{\sage{n}}
    \]
  \end{example}

\end{frame}


\subsection{Trace and Determinant}

\begin{sagesilent}
  set_random_seed(89410)
  A = random_matrix(ZZ, 4)
  d1, d2, d3, d4 = A.diagonal()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{trace} of a matrix is the sum of its diagonal entries.
  \end{definition}

  \pause
  \begin{example}
    $\trace\sage{A}=\pause\sage{d1}+\pause(\sage{d2})+\pause\sage{d3}+\pause\sage{d4}=\pause\sage{A.trace()}$
  \end{example}

\end{frame}


\begin{sagesilent}
  latex.matrix_delimiters(left='|', right='|')
  var('a b c d')
  A = matrix([(a, b), (c, d)])
  var('t')
  Id = identity_matrix(A.nrows())
  chi = t*Id-A
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myA}{
    \left[
      \begin{array}{rr}
        a & b \\
        c & d
      \end{array}
    \right]
  }
  \newcommand{\mytId}{
    \left[
      \begin{array}{rr}
        t & 0 \\
        0 & t
      \end{array}
    \right]
  }
  \newcommand{\mychi}{
    \left|
      \begin{array}{cc}
        t-a & -b \\
        -c & t-d
      \end{array}
    \right|
  }
  \newcommand{\myTrace}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {\color<4->{beamblue}$(a+d)$};
      \onslide<4->{
        \node[overlay, above left= 2mm and -0.25cm of a] (text) {$\trace(A)$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.east);
      }
    }
  }
  \newcommand{\myDet}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {\color<5->{red}$(ad-bc)$};
      \onslide<5->{
        \node[overlay, above left= 2mm and -0.25cm of a] (text) {$\det(A)$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.east);
      }
    }
  }
  \begin{example}
    The characteristic polynomial of a generic $2\times 2$ matrix is
    \begin{gather*}
      \chi_A(t) =
      \det\biggl({\overset{t\cdot I_{\sage{A.nrows()}}}{\mytId}-\overset{A}{\myA}}\biggr) =
      \onslide<2->{\mychi =}
      \onslide<3->{t^2-\myTrace\,t+\myDet}
    \end{gather*}
    \onslide<6->{This gives $\chi_A(t)=t^2-\trace(A)\cdot t+\det(A)$.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The characteristic polynomial of every $n\times n$ matrix $A$ is of the form
    \[
      \chi_A(t)
      = t^n-\trace(A)\cdot t^{n-1}+\dotsb+(-1)^n\cdot\det(A)
    \]
    \pause The Vieta formulas then give a trace formula
    \[
      \trace(A) = \am_A(\lambda_1)\cdot\lambda_1 + \am_A(\lambda_2)\cdot\lambda_2 + \dotsb + \am_A(\lambda_k)\cdot\lambda_k
    \]
    \pause and a determinant formula
    \[
      \det(A) = \lambda_1^{\am_A(\lambda_1)}\cdot\lambda_2^{\am_A(\lambda_2)}\dotsb\lambda_k^{\am_A(\lambda_k)}
    \]
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myTrace}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$2$};
      \onslide<3->{
        \node[overlay, below left= 2mm and 1cm of a] (text) {$\trace(A)=-(-2)=2$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myDet}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$6$};
      \onslide<4->{
        \node[overlay, below left= 0.75cm and 1cm of a] (text) {$\det(A)=(-1)^{7}\cdot 6=-6$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\mySize}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$7$};
      \onslide<2->{
        \node[overlay, above left= 0mm and 1cm of a] (text) {$A$ is $7\times 7$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.east);
      }
    }
  }
  \begin{example}
    Suppose that $A$ is a matrix with characteristic polynomial
    \[
      \chi_A(t) = t^{\mySize} - \myTrace t^{6} + t^{4} - t^{3} + 9t^{2} + t + \myDet
    \]
  \end{example}

\end{frame}


\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  l1, l2, l3, l4 = -1, 0, 3, 7
  J1 = jordan_block(l1, 2)
  J2 = jordan_block(l2, 3)
  J3 = jordan_block(l3, 2)
  J4 = jordan_block(l4, 1)
  J = block_diagonal_matrix([J1, J2, J3, J4])
  set_random_seed(89041)
  P = random_matrix(ZZ, J.nrows(), algorithm='unimodular', upper_bound=3)
  A = P*J*P.inverse()
  var('t')
  chi = factor(J.characteristic_polynomial(t))
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\amA}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$1$};
      \onslide<7->{
        \node[overlay, below left= 1mm and 1cm of a] (text) {$\am_A(7)=1$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\lA}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {$7$};
      \onslide<3->{
        \node[overlay, below left= 6mm and 1cm of a] (text) {$\lambda=7$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\amB}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$2$};
      \onslide<8->{
        \node[overlay, below left= 10mm and 1cm of a] (text) {$\am_A(3)=2$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\lB}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {$3$};
      \onslide<4->{
        \node[overlay, below right= 10mm and 0.5cm of a] (text) {$\lambda=3$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \newcommand{\amC}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$2$};
      \onslide<9->{
        \node[overlay, below right=6mm and 1cm of a] (text) {$\am_A(-1)=2$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \newcommand{\lC}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {$(-1)$};
      \onslide<5->{
        \node[overlay, below right= 1mm and 1cm of a] (text) {$\lambda=-1$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \newcommand{\amD}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$3$};
      \onslide<10->{
        \node[overlay, above right=6mm and 0.25cm of a] (text) {$\am_A(0)=3$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\lD}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {$0$};
      \onslide<6->{
        \node[overlay, above right= 2mm and 0.25cm of a] (text) {$\lambda=0$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \begin{example}
    Consider the data
    \begin{gather*}
      \begin{align*}
        A &= \sage{A} & \chi_A(t) &= \sage{chi}
      \end{align*}
    \end{gather*}
    \onslide<2->We can compute $\trace(A)=\sage{A.trace()}$ with
    \[
      \trace(A)
      =
      \amA\cdot\lA
      + \amB\cdot\lB
      + \amC\cdot\lC
      + \amD\cdot\lD
      = \sage{A.trace()}
    \]
  \end{example}

\end{frame}



\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  l1, l2, l3, l4 = -1, 1, 3, 7
  J1 = jordan_block(l1, 2)
  J2 = jordan_block(l2, 3)
  J3 = jordan_block(l3, 2)
  J4 = jordan_block(l4, 1)
  J = block_diagonal_matrix([J1, J2, J3, J4])
  set_random_seed(89041)
  P = random_matrix(ZZ, J.nrows(), algorithm='unimodular', upper_bound=3)
  A = P*J*P.inverse()
  var('t')
  chi = factor(J.characteristic_polynomial(t))
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\amA}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=1mm, black] (a) {$1$};
      \onslide<7->{
        \node[overlay, above left= -1mm and 0.5cm of a] (text) {$\am_A(7)=1$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.east);
      }
    }
  }
  \newcommand{\lA}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {$7$};
      \onslide<3->{
        \node[overlay, below left= 2mm and 0.5cm of a] (text) {$\lambda=7$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\amB}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$2$};
      \onslide<8->{
        \node[overlay, above right= 8mm and 0.25cm of a] (text) {$\am_A(3)=2$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\lB}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {$3$};
      \onslide<4->{
        \node[overlay, below left= 6mm and 0.5cm of a] (text) {$\lambda=3$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\amC}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$2$};
      \onslide<9->{
        \node[overlay, above right=4mm and 0.25cm of a] (text) {$\am_A(-1)=2$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\lC}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {$(-1)$};
      \onslide<5->{
        \node[overlay, below right= 6mm and 0.25cm of a] (text) {$\lambda=-1$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \newcommand{\amD}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$3$};
      \onslide<10->{
        \node[overlay, above right=0mm and 0.25cm of a] (text) {$\am_A(1)=3$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\lD}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {$1$};
      \onslide<6->{
        \node[overlay, below right= 2mm and 0.25cm of a] (text) {$\lambda=1$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }

  \begin{example}
    Consider the data
    \vspace{0.5cm}
    \begin{gather*}
      \begin{align*}
        A &= \sage{A} & \chi_A(t) &= \sage{chi}
      \end{align*}
    \end{gather*}
    \onslide<2->We can compute $\det(A)=\sage{A.det()}$ with
    \[
      \det(A)
      = \lA^{\amA} \cdot \lB^{\amB} \cdot \lC^{\amC} \cdot \lD^{\amD}
      = \sage{A.det()}
    \]
  \end{example}

\end{frame}



\begin{sagesilent}
  l1, l2, l3, l4 = -9, 3, 10, 12
  set_random_seed(791)
  A = random_matrix(ZZ, 4, algorithm='diagonalizable', eigenvalues=(l1, l2, l3, l4), dimensions=(1, 1, 1, 1))
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{exampleblock}{Problem}
    Consider the matrix $A$ and three of its eigenvalues
    \begin{gather*}
      \begin{align*}
        A &= \sage{A} & \lambda_1 &= \sage{l1} & \lambda_2 &= \sage{l2} & \lambda_3 &= \sage{l3}
      \end{align*}
    \end{gather*}
    \onslide<2->{Does $A$ have other eigenvalues?}
  \end{exampleblock}

  \onslide<3->
  \begin{block}{Solution}
    Accounting for a fourth eigenvalue $\lambda_4$ gives
    \[
      \overset{\onslide<4->{\sage{A.trace()}}}{\trace(A)} = \overset{\onslide<5->{\sage{l1}}}{\lambda_1} + \overset{\onslide<6->{\sage{l2}}}{\lambda_2} + \overset{\onslide<7->{\sage{l3}}}{\lambda_3} + \lambda_4
    \]
    \onslide<8->{This gives a fourth eigenvalue $\lambda_4=\onslide<9->{16+9-3-10=}\onslide<10->{\sage{l4}$.}}
  \end{block}

\end{frame}


\end{document}
