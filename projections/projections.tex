\documentclass[usenames,dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\usepackage{tikz-3dplot}

\title{Projections}
\subtitle{Math 218}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Geometric Motivation}
\subsection{Visualizing Projections}

\begin{sagesilent}
  var('v1 v2 v3')
  v = vector([v1, v2, v3])
  P = diagonal_matrix([0, 0, 1])
  p = P*v
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \noindent
  \begin{minipage}{.5\textwidth}
    \begin{block}{Question}
      What is the \emph{projection} of $\vv{v}\in\mathbb{R}^3$ onto the
      $z$-axis?
    \end{block}
  \end{minipage}% This must go next to Y
  \begin{minipage}{.5\textwidth}
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , scale=2
        ]
        \coordinate (O) at (0, 0, 0);
        \coordinate (e2) at (1, 0, 0);
        \coordinate (e3) at (0, 1, 0);
        \coordinate (e1) at (0, 0, 1);

        \coordinate (b) at ($ 0.6*(e1) + 1*(e2) + 0.75*(e3) $);
        \coordinate (p) at ($ 0*(e1) + 0*(e2) + 0.75*(e3) $);

        \draw[ultra thick, ->] (O) -- (e1) node[pos=1.15] {$x$};
        \draw[ultra thick, ->] (O) -- (e2) node[pos=1.1] {$y$};
        \draw[ultra thick, ->] (O) -- (e3) node[pos=1.1] {$z$};

        \onslide<3->{
          \pgfmathsetmacro{\myPerp}{1/10}
          \coordinate (u1) at ($ (O)!1cm!(p) $);
          \coordinate (u2) at ($ (O)!1cm!(b)-(p) $);
          \draw[ultra thick]
          ($ (p)+\myPerp*(u1) $) -- ($ (p)+\myPerp*(u1)+\myPerp*(u2) $) -- ($ (p)+\myPerp*(u2) $);
        }

        \onslide<3->{
          \draw[ultra thick, dotted, red] (b) -- (p);
        }

        \draw[ultra thick, blue, ->] (O) -- (b) node[pos=1.1] {$\vv{v}$};

        \onslide<5->{
          \draw[ultra thick, purple, ->] (O) -- (p) node[midway, left] {$\vv{p}$};
        }
      \end{tikzpicture}
    \]
  \end{minipage}

  \onslide<2->
  \begin{block}{Answer}
    Draw an orthogonal ray from the tip of $\vv{v}=\sage{v}$ to the
    $z$-axis. \onslide<4->{This gives the projection
      $\vv{p}=\onslide<6->{\sage{p}$.}}  \onslide<7->{Note that}
    \newcommand{\myP}{
      \begin{array}{c}
        \overset{P}{\sage{P}}\overset{\vv{v}}{\sage{matrix.column(v)}} = \overset{\vv{p}}{\sage{matrix.column(p)}}
      \end{array}
    }
    \newcommand{\myV}{
      \begin{array}{rcl}
        \onslide<9->{\Null(P) &=&} \onslide<10->{xy\textnormal{-plane}} \\
        \onslide<11->{\Col(P) &=&} \onslide<12->{z\textnormal{-axis}}
      \end{array}
    }
    \begin{align*}
      \onslide<8->{\myP} & \myV
    \end{align*}
  \end{block}

\end{frame}



\begin{sagesilent}
  var('v1 v2 v3')
  v = vector([v1, v2, v3])
  P = diagonal_matrix([1, 1, 0])
  p = P*v
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \noindent
  \begin{minipage}{.5\textwidth}
    \begin{block}{Question}
      What is the \emph{projection} of $\vv{v}\in\mathbb{R}^3$ onto the
      $xy$-plane?
    \end{block}
  \end{minipage}% This must go next to Y
  \begin{minipage}{.5\textwidth}
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , scale=2
        ]
        \coordinate (O) at (0, 0, 0);
        \coordinate (e2) at (1, 0, 0);
        \coordinate (e3) at (0, 1, 0);
        \coordinate (e1) at (0, 0, 1);

        \coordinate (b) at ($ 0.6*(e1) + 1*(e2) + 0.75*(e3) $);
        \coordinate (p) at ($ 0.6*(e1) + 1*(e2) + 0*(e3) $);

        \draw[ultra thick, ->] (O) -- (e1) node[pos=1.15] {$x$};
        \draw[ultra thick, ->] (O) -- (e2) node[pos=1.1] {$y$};
        \draw[ultra thick, ->] (O) -- (e3) node[pos=1.1] {$z$};

        \onslide<3->{
          \pgfmathsetmacro{\myPerp}{1/10}
          \coordinate (u1) at ($ (O)!1cm!(p) $);
          \coordinate (u2) at ($ (O)!1cm!(b)-(p) $);
          \draw[ultra thick] ($ (p)+\myPerp*(u1) $) -- ($ (p)+\myPerp*(u1)+\myPerp*(u2) $) -- ($ (p)+\myPerp*(u2) $);
        }

        \onslide<3->{
          \draw[ultra thick, dotted, red] (b) -- (p);
        }
        \draw[ultra thick, blue, ->] (O) -- (b) node[pos=1.1] {$\vv{v}$};

        \onslide<5->{
          \draw[ultra thick, purple, ->] (O) -- (p) node[midway, sloped, below] {$\vv{p}$};
        }
      \end{tikzpicture}
    \]
  \end{minipage}

  \onslide<2->
  \begin{block}{Answer}
    Draw an orthogonal ray from the tip of $\vv{v}=\sage{v}$ to the
    $xy$-plane. \onslide<4->{This gives the projection
      $\vv{p}=\onslide<6->{\sage{p}$.}} \onslide<7->{Note that}
    \newcommand{\myP}{
      \begin{array}{c}
        \overset{P}{\sage{P}}\overset{\vv{v}}{\sage{matrix.column(v)}} = \overset{\vv{p}}{\sage{matrix.column(p)}}
      \end{array}
    }
    \newcommand{\myV}{
      \begin{array}{rcl}
        \onslide<9->{\Null(P) &=&} \onslide<10->{z\textnormal{-axis}} \\
        \onslide<11->{\Col(P) &=&} \onslide<12->{xy\textnormal{-plane}}
      \end{array}
    }
    \begin{align*}
      \onslide<8->{\myP} & \myV
    \end{align*}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question 1}
    What is the projection of a vector $\vv{v}$ onto a vector space $V$?
  \end{block}

  \onslide<2->
  \begin{block}{Question 2}
    Is there a matrix $P$ so that $P\vv{v}$ is the projection of $\vv{v}$ onto
    $V$? \onslide<3->
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        ]
        \coordinate(O) at (0, 0, 0);

        \coordinate (e2) at (1, 0, 0);
        \coordinate (e3) at (0, 1, 0);
        \coordinate (e1) at (0, 0, 1);

        \pgfmathsetmacro{\bx}{-1.5}
        \pgfmathsetmacro{\by}{2}
        \pgfmathsetmacro{\bz}{2}

        \coordinate (b) at ($ \bx*(e1) + \by*(e2) + \bz*(e3) $);
        \coordinate (p) at ($ \bx*(e1) + \by*(e2) + 0*(e3) $);

        \pgfmathsetmacro{\myV}{1.65}
        \coordinate (ne) at ($ \myV*\bx*(e1) + \myV*\by*(e2) $);
        \coordinate (se) at ($ -\bx*(e1) + \myV*\by*(e2) $);
        \coordinate (sw) at ($ -\bx*(e1) - \myV*\by*(e2) $);
        \coordinate (nw) at ($ \myV*\bx*(e1) - \myV*\by*(e2) $);
        \coordinate (Vsw) at ($ (se)!.65!(sw) $);
        \coordinate (Vnw) at ($ (ne)!.65!(nw) $);

        \filldraw[ultra thick, draw=teal, fill=teal!35]
        (ne) -- (se) -- (Vsw) -- (Vnw) -- cycle;

        \onslide<4->{
          \pgfmathsetmacro{\myPerp}{1/6}
          \coordinate (u1) at ($ (O)!1cm!(p) $);
          \coordinate (u2) at ($ (O)!1cm!(b)-(p) $);
          \draw[ultra thick]
          ($ (p) + \myPerp*(u1) $)
          -- ($ (p)+\myPerp*(u1) + \myPerp*(e3) $)
          -- ($ (p)+\myPerp*(e3) $);
        }

        \onslide<4->{
          \draw[ultra thick, dotted, red] (p) -- (b);
        }
        % \draw[ultra thick, ->, red] (p) -- (b) node[midway, right] {$\vv{b}-A\widehat{x}$};

        \draw[ultra thick, ->, blue] (O) -- (b) node[above] {$\vv{v}$};

        \onslide<5->{
          \draw[ultra thick, ->, purple] (O) -- (p) node[midway, sloped, below] {$P\vv{v}$};
        }

        \draw[teal] (Vsw) -- (se) node[near start, sloped, below] {$V$};
      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\section{The Projection Formula}
\subsection{The Gramian}

\begin{sagesilent}
  set_random_seed(389037)
  A = random_matrix(ZZ, 2, 3)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{Gramian} of $A$ is $A^\intercal A$.
  \end{definition}

  \onslide<2->
  \begin{example}
    Consider the matrix $A$ given by
    \[
      A=\sage{A}
    \]
    \onslide<3->The Gramian of $A$ is
    \[
      \onslide<5->{
        \overset{A^\intercal A}{\sage{A.T*A}}=
      }
      \onslide<4->{
        \overset{A^\intercal}{\sage{A.T}}\overset{A}{\sage{A}}
      }
    \]
  \end{example}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The Gramian $A^\intercal A$ of $A$ has the following properties. \pause
    \begin{description}[common rank]
    \item[square] $A^\intercal A$ is $n\times n$ if $A$ is $m\times n$ \pause
    \item[symmetric] $(A^\intercal A)^\intercal=A^\intercal(A^\intercal)^\intercal=A^\intercal A$ \pause
    \item[common rank] $\rank(A^\intercal A)=\rank(A)$
    \end{description}
  \end{theorem}

  \pause
  \begin{theorem}
    $(A^\intercal A)^{-1}$ exists if and only if $A$ has full column rank.
  \end{theorem}

\end{frame}


\subsection{Deriving the Formula}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \noindent
  \begin{minipage}{.5\textwidth}
    \begin{block}{Finding $P$ to Project Onto $V$}
      \onslide<5->{Start with a basis $\Set{\vv*{v}{1},\dotsc,\vv*{v}{d}}$.}
      \onslide<6->{Then define
        \[
          X
          =
          \begin{bmatrix}
            \vv*{v}{1} & \cdots & \vv*{v}{d}
          \end{bmatrix}
        \]
      } \onslide<7->{This means that $V=\Col(X)$.}
    \end{block}
  \end{minipage}% This must go next to Y
  \begin{minipage}{.5\textwidth}
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        ]
        \coordinate(O) at (0, 0, 0);

        \coordinate (e2) at (1, 0, 0);
        \coordinate (e3) at (0, 1, 0);
        \coordinate (e1) at (0, 0, 1);

        \pgfmathsetmacro{\bx}{-1.5}
        \pgfmathsetmacro{\by}{2}
        \pgfmathsetmacro{\bz}{2}

        \coordinate (b) at ($ \bx*(e1) + \by*(e2) + \bz*(e3) $);
        \coordinate (p) at ($ \bx*(e1) + \by*(e2) + 0*(e3) $);

        \pgfmathsetmacro{\myV}{1.65}
        \coordinate (ne) at ($ \myV*\bx*(e1) + \myV*\by*(e2) $);
        \coordinate (se) at ($ -\bx*(e1) + \myV*\by*(e2) $);
        \coordinate (sw) at ($ -\bx*(e1) - \myV*\by*(e2) $);
        \coordinate (nw) at ($ \myV*\bx*(e1) - \myV*\by*(e2) $);
        \coordinate (Vsw) at ($ (se)!.65!(sw) $);
        \coordinate (Vnw) at ($ (ne)!.65!(nw) $);

        \filldraw[ultra thick, draw=teal, fill=teal!35]
        (ne) -- (se) -- (Vsw) -- (Vnw) -- cycle;

        \onslide<3->{
          \pgfmathsetmacro{\myPerp}{1/6}
          \coordinate (u1) at ($ (O)!1cm!(p) $);
          \coordinate (u2) at ($ (O)!1cm!(b)-(p) $);

          \draw[ultra thick]
          ($ (p) + \myPerp*(u1) $)
          -- ($ (p)+\myPerp*(u1) + \myPerp*(e3) $)
          -- ($ (p)+\myPerp*(e3) $);

          \draw[ultra thick, dotted, red] (p) -- (b);
        }

        \onslide<14->{
          \draw[ultra thick, ->, red] (p) -- (b) node[midway, right] {$\vv{v}-X\widehat{x}$};
        }

        \onslide<2->{
          \draw[ultra thick, ->, blue] (O) -- (b) node[above] {$\vv{v}$};
        }

        \onslide<4->{
          \draw[ultra thick, ->, purple] (O) -- (p) node[midway, sloped, below] {$P\vv{v}\onslide<11->{=X\widehat{x}}$};
        }

        \draw[teal]
        (Vsw) -- (se)
        node[near start, sloped, below] {$V\onslide<8->{=\Col(X)}$};
      \end{tikzpicture}
    \]
  \end{minipage}

  \onslide<9->{Now, $P\vv{v}\in\Col(X)$ means
    $P\vv{v}=\onslide<10->{X\widehat{x}$.}} \onslide<12->{Then }
  \begin{align*}
    \onslide<13->{\vv{v}-X\widehat{x}\perp\Col(X)} && \onslide<15->{\vv{v}-X\widehat{x}\in\LNull(X)} && \onslide<16->{X^\intercal(\vv{v}-X\widehat{x})=}\onslide<17->{\vv{O}} \\
    \onslide<18->{X^\intercal X\widehat{x}=X^\intercal\vv{v}} && \onslide<19->{\widehat{x}=(X^\intercal X)^{-1}X^\intercal\vv{v}} && \onslide<20->{X\widehat{x}=X(X^\intercal X)^{-1}X^\intercal\vv{v}}
  \end{align*}
  \onslide<21->{This means
    $P\vv{v}=\onslide<22->{X(X^\intercal X)^{-1}X^\intercal\vv{v}$.}}
  \onslide<23->{Hence $P=\onslide<22->{X(X^\intercal X)^{-1}X^\intercal$.}}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{projection matrix onto $V$} is
    \[
      P_V = X(X^\intercal X)^{-1}X^\intercal
    \]
    where $X$ is any matrix whose columns form a basis of $V$.
  \end{definition}

\end{frame}


\begin{sagesilent}
  set_random_seed(1891)
  A = random_matrix(ZZ, 2, 3, algorithm='echelonizable', rank=1)
  a1, a2, a3 = A.columns()
  X = matrix.column([a1])
  PC = X*(X.T*X).inverse()*X.T
  n1, n2 = A.change_ring(QQ).right_kernel(basis='pivot').basis()
  Y = matrix.column([n1, n2])
  PN = Y*(Y.T*Y).inverse()*Y.T
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the computation
    \newcommand{\myRref}{
      \begin{array}{c}
        \rref\overset{A}{\sage{A}}=\sage{A.rref()}
      \end{array}
    }
    \newcommand{\myBases}{
      \begin{array}{rcl}
        \onslide<2->{\Col(A) &=&} \onslide<3->{\Span\sage{Set([a1])}} \\
        \onslide<4->{\Null(A) &=&} \onslide<5->{\Span\sage{Set([n1, n2])}}
      \end{array}
    }
    \begin{gather*}
      \begin{align*}
        \myRref & \myBases
      \end{align*}
    \end{gather*}
    \onslide<6->{Projection onto $\Col(A)$ is given by}
    \[
      \onslide<8->{\overset{P_{\Col(A)}}{\sage{PC}}=}
      \onslide<7->{\sage{X}\pair*{\sage{X.T}\sage{X}}^{-1}\sage{X.T}}
    \]
    \onslide<9->{Projection onto $\Null(A)$ is given by}
    \begin{gather*}
      \onslide<11->{\overset{P_{\Null(A)}}{\sage{PN}}=}
      \onslide<10->{\sage{Y}\pair*{\sage{Y.T}\sage{Y}}^{-1}\sage{Y.T}}
    \end{gather*}
  \end{example}

\end{frame}


\subsection{Properties of Projections}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Projection matrices are \mybold{symmetric} $P^\intercal=P$.
  \end{theorem}

  \pause
  \begin{theorem}
    Projection matrices are \mybold{idempotent} $P^2=P$.
  \end{theorem}

  \pause
  \begin{theorem}
    $\vv{v}\in V$ if and only if $P\vv{v}=\vv{v}$.
  \end{theorem}

  % \begin{block}{Advantage}
  %   To check if $\vv{v}\in V$, we need only verify that $P\vv{v}=\vv{v}$.
  % \end{block}

\end{frame}


\begin{sagesilent}
  vc = matrix.column(sum(A.column_space().basis()))
  vn = matrix.column(sum(A.row_space().basis()+A.right_kernel().basis()))
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    In our last example, we found
    \begin{align*}
      \overset{A}{\sage{A}} && \overset{P_{\Col(A)}}{\sage{PC}} && \overset{P_{\Null(A)}}{\sage{PN}}
    \end{align*}

    \pause The equations
    \begin{gather*}
      \begin{align*}
        \overset{P_{\Col(A)}}{\sage{PC}}\sage{vc} &= \sage{PC*vc} & \overset{P_{\Null(A)}}{\sage{PN}}\sage{vn} &= \sage{PN*vn}
      \end{align*}
    \end{gather*}
    \pause show that $\sage{vector(vc)}\in\Col(A)$ but
    $\sage{vector(vn)}\notin\Null(A)$.
  \end{example}

\end{frame}



\section{One-Dimensional Spaces}
\subsection{One-Dimensional Projection Formula}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Suppose that $V$ is one-dimensional.
    \[
      \begin{tikzpicture}[
        , scale=1/2
        , line join=round
        , line cap=round
        ]
        \coordinate (O) at (0, 0);
        \coordinate (v) at (2, 1);

        \onslide<3->{
          \draw[ultra thick, teal, <->] ($ -1.5*(v) $) -- ($ 1.5*(v) $) node[right] {$V$};
        }

        \onslide<2->{
          \draw[ultra thick, blue, ->] (O) -- (v) node[midway, above, sloped] {$\vv*{v}{1}$};
        }
      \end{tikzpicture}
    \]
    \onslide<4->{What is the projection $P_V$ onto $V$?}
  \end{block}

  \onslide<5->
  \begin{block}{Answer}
    Since $V=\Span\Set{\vv*{v}{1}}$, we can take $X=\vv*{v}{1}$ in the
    projection formula. \onslide<6->{Applying the formula
      $P_V=X(X^\intercal X)^{-1}X^\intercal$ gives}
    \[
      \onslide<7->{P_V
        =} \onslide<8->{\vv*{v}{1}(\vv*{v}{1}^\intercal \vv*{v}{1})^{-1}\vv*{v}{1}^\intercal
        =} \onslide<9->{\vv*{v}{1}(\vv*{v}{1}\cdot\vv*{v}{1})^{-1}\vv*{v}{1}^\intercal
        =} \onslide<10->{\frac{\vv*{v}{1}\vv*{v}{1}^\intercal}{\vv*{v}{1}\cdot\vv*{v}{1}}
        =} \onslide<11->{\frac{\vv*{v}{1}\vv*{v}{1}^\intercal}{\norm{\vv*{v}{1}}^2}}
    \]
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose that $\dim(V)=1$. Then projection onto $V$ is given by
    \[
      P_V = \frac{\vv*{v}{1}\vv*{v}{1}^\intercal}{\norm{\vv*{v}{1}}^2}
    \]
    where $\vv*{v}{1}$ is any vector parallel to the line spanned by $V$.
  \end{theorem}

  \pause
  \begin{block}{Interpretation}
    Projection is easy when $V$ is one-dimensional!
  \end{block}

\end{frame}


\begin{sagesilent}
  v = vector([2, -1])
  A = matrix.column([v, 3*v, -2*v])
  vc = matrix.column(v)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $A$ given by
    \newcommand{\myA}{A=\sage{A}}
    \newcommand{\myL}{
      \begin{array}{c}
        \begin{tikzpicture}[
          , scale=3/4
          , line join=round
          , line cap=round
          ]
          \coordinate (O) at (0, 0);
          \coordinate (v) at (2, 1);

          \onslide<3->{
            \draw[ultra thick, teal, <->] ($ -.5*(v) $) -- ($ 1.25*(v) $) node[right] {$\Col(A)$};
          }

          \onslide<2->{
            \draw[ultra thick, blue, ->] (O) -- (v) node[midway, above, sloped] {$\sage{v}$};
          }

        \end{tikzpicture}
      \end{array}
    }
    \begin{align*}
      \myA && \myL
    \end{align*}
    Compute $P_{\Col(A)}$.
  \end{example}
  \onslide<4->
  \begin{block}{Solution}
    Note that $\Col(A)=\Span\Set{\sage{v}}$. \onslide<5->This means that
    \[
      \overset{P_{\Col(A)}}{\oldfrac{1}{\sage{v*v}}\sage{vc*vc.T}}
      =
      \oldfrac{1}{\norm{\sage{v}}^2}\sage{vc}\sage{vc.T}
    \]
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \noindent
  \begin{minipage}{.5\textwidth}
    \begin{block}{Question}
      What is $P\vv{v}$ if $\dim(V)=1$?
    \end{block}
  \end{minipage}% This must go next to Y
  \begin{minipage}{.5\textwidth}
    \[
      \begin{tikzpicture}[
        , scale=1
        , line join=round
        , line cap=round
        , transform shape
        , rotate=15
        ]
        \coordinate (O) at (0, 0);
        \coordinate (v) at (1, 0);
        \coordinate (b) at (2, 2);
        \coordinate (p) at ($ (O)!(b)!(v) $);

        \pgfmathsetmacro{\myPerp}{.25}
        \coordinate (u1) at ($ (p)!1cm!(b)-(p) $);
        \coordinate (u2) at ($ (O)!1cm!(p)-(p) $);

        \onslide<5->{
          \draw[ultra thick]
          ($ (p)+\myPerp*(u1) $)
          -- ($ (p)+\myPerp*(u1)-\myPerp*(u2) $)
          -- ($ (p)-\myPerp*(u2) $);
          \draw[ultra thick, dotted, red] (b) -- (p);
        }

        \onslide<3->{
          \draw[ultra thick, teal, <->] ($ -.5*(v) $) -- ($ 3*(v) $) node[right] {$V$};
        }

        \onslide<2->{
          \draw[ultra thick, blue, ->] (O) -- (v) node[below] {$\vv*{v}{1}$};
        }

        \onslide<4->{
          \draw[ultra thick, blue, ->] (O) -- (b) node[pos=1.1] {$\vv{v}$};
        }


        \onslide<6->{
          \draw[ultra thick, purple, ->] (O) -- (p) node[below] {$P\vv{v}$};
        }
      \end{tikzpicture}
    \]
  \end{minipage}
  \onslide<7->
  \begin{block}{Answer}
    Our projection formula gives
    \[
      \onslide<8->{P\vv{v}
        =} \onslide<9->{\frac{1}{\norm{\vv*{v}{1}}^2}\vv*{v}{1}\vv*{v}{1}^\intercal\vv{v}
        =} \onslide<10->{\frac{1}{\norm{\vv*{v}{1}}^2}\vv*{v}{1}\vv*{v}{1}\cdot\vv{v}
        =} \onslide<11->{\frac{\vv*{v}{1}\cdot\vv{v}}{\norm{\vv*{v}{1}}^2}\vv*{v}{1}}
    \]
    \onslide<12->{This is commonly written as $\proj_{\vv*{v}{1}}(\vv{v})$.}
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{component of $\vv{v}$ in the direction of $\vv*{v}{1}$} is
    \[
      \comp_{\vv*{v}{1}}(\vv{v})
      =\frac{\vv*{v}{1}\cdot\vv{v}}{\norm{\vv*{v}{1}}}
    \]
    \onslide<2->{Note that $\comp_{\vv*{v}{1}}(\vv{v})$ is a \emph{scalar}.}
    \[
      \begin{tikzpicture}[
        , scale=1
        , line join=round
        , line cap=round
        , transform shape
        , rotate=15
        ]
        \coordinate (O) at (0, 0);
        \coordinate (v) at (1, 0);
        \coordinate (b) at (2, 2);
        \coordinate (p) at ($ (O)!(b)!(v) $);

        \pgfmathsetmacro{\myPerp}{.25}
        \coordinate (u1) at ($ (p)!1cm!(b)-(p) $);
        \coordinate (u2) at ($ (O)!1cm!(p)-(p) $);

        \onslide<6->{
          \draw[ultra thick]
          ($ (p)+\myPerp*(u1) $)
          -- ($ (p)+\myPerp*(u1)-\myPerp*(u2) $)
          -- ($ (p)-\myPerp*(u2) $);

          \draw[ultra thick, dotted, red] (b) -- (p);
        }

        \onslide<7->{
          \draw [
          , purple
          , ultra thick
          , decorate
          , decoration={brace, mirror, amplitude=10pt}
          ]
          (O) -- (p) node [midway, below=3pt] {$\comp_{\vv*{v}{1}}(\vv{v})$};
        }


        \onslide<5->{
          \draw[ultra thick, teal, <->] ($ -.5*(v) $) -- ($ 3*(v) $) node[right] {$V$};
        }

        \onslide<3->{
          \draw[ultra thick, blue, ->] (O) -- (v) node[above] {$\vv*{v}{1}$};
        }

        \onslide<4->{
          \draw[ultra thick, blue, ->] (O) -- (b) node[pos=1.1] {$\vv{v}$};
        }

        % \onslide<6>{
        % \draw[ultra thick, purple, ->] (O) -- (p) node[below] {$\proj_{\vv{v}}(\vv{b})$};
        % }
      \end{tikzpicture}
    \]
  \end{definition}
\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{projection} of $\vv{v}$ onto $\vv*{v}{1}$ is
    \[
      \proj_{\vv*{v}{1}}(\vv{v})
      = \comp_{\vv*{v}{1}}(\vv{v})\cdot\frac{\vv*{v}{1}}{\norm{\vv*{v}{1}}}
      = \frac{\vv*{v}{1}\cdot\vv{v}}{\norm{\vv*{v}{1}}^2}\vv*{v}{1}
    \]
    \onslide<2->{Note that $\proj_{\vv*{v}{1}}(\vv{v})$ is a \emph{vector}.}
    \[
      \begin{tikzpicture}[
        , scale=1
        , line join=round
        , line cap=round
        , transform shape
        , rotate=15
        ]
        \coordinate (O) at (0, 0);
        \coordinate (v) at (1, 0);
        \coordinate (b) at (2, 2);
        \coordinate (p) at ($ (O)!(b)!(v) $);

        \pgfmathsetmacro{\myPerp}{.25}
        \coordinate (u1) at ($ (p)!1cm!(b)-(p) $);
        \coordinate (u2) at ($ (O)!1cm!(p)-(p) $);

        \onslide<6->{
          \draw[ultra thick]
          ($ (p)+\myPerp*(u1) $)
          -- ($ (p)+\myPerp*(u1)-\myPerp*(u2) $)
          -- ($ (p)-\myPerp*(u2) $);

          \draw[ultra thick, dotted, red] (b) -- (p);
        }

        % \onslide<6->{
        % \draw [
        % , orange
        % , ultra thick
        % , decorate
        % , decoration={brace, mirror, amplitude=7pt}
        % ]
        % (O) -- (p) node [midway, below=3pt] {$\comp_{\vv{v}}(\vv{b})$};
        % }


        \onslide<5->{
          \draw[ultra thick, teal, <->] ($ -.5*(v) $) -- ($ 3*(v) $) node[right] {$V$};
        }

        \onslide<3->{
          \draw[ultra thick, blue, ->] (O) -- (v) node[above] {$\vv*{v}{1}$};
        }

        \onslide<4->{
          \draw[ultra thick, blue, ->] (O) -- (b) node[pos=1.1] {$\vv{v}$};
        }

        \onslide<7->{
          \draw[ultra thick, purple, ->] (O) -- (p) node[midway, below] {$\proj_{\vv*{v}{1}}(\vv{v})$};
        }
      \end{tikzpicture}
    \]
  \end{definition}

\end{frame}


\begin{sagesilent}
  v = vector([3, 0, -2, 1])
  w = vector([1, -3, 0, -4])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the vectors $\vv{v}$ and $\vv{w}$ given by
    \begin{align*}
      \vv{v} &= \sage{v} & \vv{w} &= \sage{w}
    \end{align*}
    Compute $\proj_{\vv{w}}(\vv{v})$.
  \end{example}
  \pause
  \begin{block}{Solution}
    The component of $\vv{v}$ in the direction of $\vv{w}$ is
    \[
      \comp_{\vv{w}}(v)
      = \frac{\vv{w}\cdot\vv{v}}{\norm{\vv{w}}}
      = \oldfrac{\sage{w}\cdot\sage{v}}{\norm{\sage{w}}}
      = \sage{(w*v)/(sqrt(w*w))}
    \]
    \pause The projection of $\vv{v}$ onto $\vv{w}$ is then
    \[
      \proj_{\vv{w}}(v)
      = \comp_{\vv{w}}(v)\cdot\frac{\vv{w}}{\norm{\vv{w}}}
      = \sage{(w*v)/(w*w)}\sage{w}
    \]
  \end{block}

\end{frame}



\section{Higher Dimensional Spaces}
\subsection{Examples}
\begin{sagesilent}
  a1 = vector([1, 0, -1])
  a3 = vector([-1, 1, 0])
  a2 = -3*a1
  A = matrix.column([a1, a2, a3])
  X = matrix.column([a1, a3])
  P = X*(X.T*X).inverse()*X.T
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $A$ given by
    \[
      A = \sage{A}
    \]
    Find $P_{\Col(A)}$.
  \end{example}
  \begin{block}{Solution}
    Note that $\Col(A)=\Span\Set{\sage{a1}, \sage{a3}}$.
    \begin{gather*}
      \overset{P_{\Col(A)}}{\sage{P}}
      =
      \overset{X}{\sage{X}}\overset{(X^\intercal X)^{-1}}{{\sage{X.T*X}}^{-1}}\overset{X^\intercal}{\sage{X.T}}
    \end{gather*}
  \end{block}

\end{frame}



\begin{sagesilent}
  b = sum(A.column_space().basis())+sum(A.left_kernel().basis())
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[Continued]
    The equation
    \[
      \overset{P_{\Col(A)}}{\sage{P}}\overset{\vv{v}}{\sage{matrix.column(b)}}=\overset{\neq\vv{v}}{\sage{P*matrix.column(b)}}
    \]
    illustrates that $\sage{b}\notin\Col(A)$.
    \[
      \begin{tikzpicture}[
        , scale=3/4
        , line join=round
        , line cap=round
        ]
        \coordinate(O) at (0, 0, 0);

        \coordinate (e2) at (1, 0, 0);
        \coordinate (e3) at (0, 1, 0);
        \coordinate (e1) at (0, 0, 1);

        \pgfmathsetmacro{\bx}{-1.5}
        \pgfmathsetmacro{\by}{2}
        \pgfmathsetmacro{\bz}{2}

        \coordinate (b) at ($ \bx*(e1) + \by*(e2) + \bz*(e3) $);
        \coordinate (p) at ($ \bx*(e1) + \by*(e2) + 0*(e3) $);

        \pgfmathsetmacro{\myV}{1.65}
        \coordinate (ne) at ($ \myV*\bx*(e1) + \myV*\by*(e2) $);
        \coordinate (se) at ($ -\bx*(e1) + \myV*\by*(e2) $);
        \coordinate (sw) at ($ -\bx*(e1) - \myV*\by*(e2) $);
        \coordinate (nw) at ($ \myV*\bx*(e1) - \myV*\by*(e2) $);
        \coordinate (Vsw) at ($ (se)!.65!(sw) $);
        \coordinate (Vnw) at ($ (ne)!.65!(nw) $);

        \onslide<4->{
          \filldraw[ultra thick, draw=teal, fill=teal!35]
          (ne) -- (se) -- (Vsw) -- (Vnw) -- cycle;
        }

        \pgfmathsetmacro{\myPerp}{1/6}
        \coordinate (u1) at ($ (O)!1cm!(p) $);
        \coordinate (u2) at ($ (O)!1cm!(b)-(p) $);

        \onslide<6->{
          \draw[ultra thick]
          ($ (p) + \myPerp*(u1) $)
          -- ($ (p)+\myPerp*(u1) + \myPerp*(e3) $)
          -- ($ (p)+\myPerp*(e3) $);

          \draw[ultra thick, dotted, red] (p) -- (b);
        }
        % \draw[ultra thick, ->, red] (p) -- (b) node[midway, right] {$\vv{b}-A\widehat{x}$};

        \onslide<5->{
          \draw[ultra thick, ->, blue] (O) -- (b) node[above] {$\vv{v}$};
        }

        \onslide<7->{
          \draw[ultra thick, ->, purple] (O) -- (p) node[midway, sloped, below] {$P\vv{v}\neq\vv{v}$};
        }

        \onslide<2->{
          \draw[ultra thick, ->, blue] (O) -- ($ .65*(Vsw) $);
        }

        \onslide<3->{
          \coordinate (myb) at ($ (Vsw)!.5!(se) $);
          \draw[ultra thick, ->, blue] (O) -- ($ .65*(myb) $);
        }

        \onslide<4->{
          \draw[teal] (Vsw) -- (se) node[near start, sloped, below] {$\Col(A)$};
        }
      \end{tikzpicture}
    \]
  \end{example}

\end{frame}



\section{Orthogonality and Projections}
\subsection{Relating $P_V$ to $P_{V^\perp}$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Problem}
    Suppose $\dim(V)=d$. Projection onto $V$ is given by
    \[
      P_V=X(X^\intercal X)^{-1}X^\intercal
    \]
    where $X^\intercal X$ is $d\times d$. \pause Large spaces are difficult to
    project onto!
  \end{block}

  \pause
  \begin{block}{Possible Solution}
    Every vector space $V\subset\mathbb{R}^n$ satisfies
    \[
      \dim(V)+\dim(V^\perp)=n
    \]
    \pause Large spaces have small orthogonal complements!
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $P_V+P_{V^\perp}=I_n$
  \end{theorem}

  \pause
  \begin{block}{Advantage}
    Projecting onto $P_{V^\perp}$ is easier when $V$ is ``big.''
  \end{block}

\end{frame}


\subsection{Examples}
\begin{sagesilent}
  A = matrix([1, -3, 2])
  v, = A.rows()
  X = matrix.column([v])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \noindent
  \begin{minipage}{.4\textwidth}
    \begin{example}
      Projection onto $\Row(A)$ is
      \begin{gather*}
        \onslide<3->{\overset{P_{\Row(A)}}{\sage{1/v.norm()**2}\sage{X*X.T}} =}
        \onslide<2->{\oldfrac{1}{\norm{\sage{v}}^2}\sage{X}\sage{X.T}}
      \end{gather*}
    \end{example}

  \end{minipage}% This must go next to Y
  \begin{minipage}{.6\textwidth}
    \[
      \begin{tikzpicture}[
        , scale=0.4
        , every node/.style={scale=0.4}
        , line join=round
        , line cap=round
        , yscale=.75
        ]

        \coordinate (O) at (0, 0);
        \coordinate (P) at (1, 2);
        \coordinate (Q) at (2, -1);
        \coordinate (Rn) at ($ 1.15*(P)-(Q) $);
        \coordinate (Row) at ($ .5*(P)-(Q) $);
        \coordinate (Null) at ($ .5*(Q)-.5*(P) $);

        \node at (Rn) {$\mathbb{R}^{3}$};

        \filldraw[ultra thick, fill=YellowOrange]
        (O) -- (P) -- ($ (P)-2*(Q) $) -- ($ -2*(Q) $) -- cycle;
        \node at (Row) {$\underset{1}{\Row(A)}$};

        \filldraw[ultra thick, fill=Green!40]
        (O) -- (Q) -- ($ (Q)-(P) $) -- ($ -1*(P) $) -- cycle;
        \node at (Null) {$\underset{2}{\Null(A)}$};

        \coordinate (v) at (-1, 2);
        \coordinate (w) at (-2, -1);
        \coordinate (Rm) at ($ 1.15*(v)-1*(w) $);
        \coordinate (Col) at ($ .5*(v)-1*(w) $);
        \coordinate (LNull) at ($ .5*(w)-.5*(v) $);

        \tikzset{
          c/.style={every coordinate/.try}
        }

        \begin{scope}[every coordinate/.style={shift={(5,0)}}]


          \node at ([c]Rm) {$\mathbb{R}^1$};

          \filldraw[ultra thick, fill=BrickRed!50]
          ([c]O) -- ([c]v) -- ([c]$ (v)-2*(w) $) -- ([c]$ -2*(w) $) -- cycle;
          \node at ([c]Col) {$\underset{1}{\Col(A)}$};

          \filldraw[ultra thick, fill=Blue!50]
          ([c]O) -- ([c]w) -- ([c]$ (w)-(v) $) -- ([c]$ -1*(v) $) -- cycle;
          \node at ([c]LNull) {$\underset{0}{\LNull(A)}$};


          \draw[ultra thick, shorten >=1.5em, shorten <=1.5em, ->]
          (Rn) -- ([c]Rm) node[midway, above] {$A=\sage{A}$};

          \pgfmathsetmacro{\mys}{.15}
          \coordinate (ort) at ([c]$ -\mys*(v) $);
          \coordinate (orb) at ([c]$ -\mys*(w) $);
          \coordinate (orm) at ([c]$ -\mys*(v)-\mys*(w) $);

          \draw[ultra thick] (ort) -- (orm) -- (orb);

        \end{scope}

        \pgfmathsetmacro{\mys}{.15}
        \coordinate (ort) at ([c]$ -\mys*(P) $);
        \coordinate (orb) at ([c]$ -\mys*(Q) $);
        \coordinate (orm) at ([c]$ -\mys*(P)-\mys*(Q) $);

        \draw[ultra thick] (ort) -- (orm) -- (orb);

      \end{tikzpicture}
    \]
  \end{minipage}
  \onslide<4->{Projection onto $\Null(A)$ is then}
  \begin{gather*}
    \onslide<6->{\overset{P_{\Null(A)}}{\sage{1/v.norm()**2}\sage{v.norm()**2*identity_matrix(3)-X*X.T}} =}
    \onslide<5->{
      \overset{I_{\sage{A.ncols()}}}{\sage{identity_matrix(3)}}
      -
      \overset{P_{\Row(A)}}{\sage{1/v.norm()**2}\sage{X*X.T}}
    }
  \end{gather*}


\end{frame}

\end{document}
