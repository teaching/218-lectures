from functools import partial
from sage.all import elementary_matrix, matrix, QQ, RDF, vector
import math


def sigfig(x, n):
    try:
        return round(x, int(n - math.ceil(math.log10(abs(x)))))
    except ValueError:
        return 0


def matrix_sigfig(A, n):
    M = matrix(*A.dimensions(), entries=map(partial(sigfig, n=n), A.list()))
    M.subdivide(*A.subdivisions())
    return M


def ex1():
    A = matrix([
        (QQ(.007), QQ(61.2), QQ(.093)),
        (QQ(4.810), QQ(-5.92), QQ(1.110)),
        (QQ(81.4), QQ(1.12), QQ(1.18))
    ])
    b = vector([QQ(61.3), QQ(0), QQ(83.7)])
    return A.augment(b, subdivide=True)


def gj1(d=3):
    sig = partial(matrix_sigfig, n=d)
    M = ex1()
    M = M.change_ring(RDF)
    E = partial(elementary_matrix, RDF, M.nrows())
    L = [M]
    E1 = E(row1=0, scale=1/M[0, 0])
    M1 = sig(E1*M)
    L.append(M1)
    E21 = E(row1=1, row2=0, scale=-M1[1, 0])
    E22 = E(row1=2, row2=0, scale=-M1[2, 0])
    M2 = sig(E22*E21*M1)
    L.append(M2)
    E3 = E(row1=1, scale=1/M2[1, 1])
    M3 = sig(E3*M2)
    L.append(M3)
    E41 = E(row1=0, row2=1, scale=-M3[0, 1])
    E42 = E(row1=2, row2=1, scale=-M3[2, 1])
    M4 = sig(E42*E41*M3)
    L.append(M4)
    E5 = E(row1=2, scale=1/M4[2, 2])
    M5 = sig(E5*M4)
    L.append(M5)
    E61 = E(row1=0, row2=2, scale=-M5[0, 2])
    E62 = E(row1=1, row2=2, scale=-M5[1, 2])
    M6 = sig(E62*E61*M5)
    L.append(M6)
    return L


def pp1(d=3):
    sig = partial(matrix_sigfig, n=d)
    M = ex1()
    M = M.change_ring(RDF)
    E = partial(elementary_matrix, RDF, M.nrows())
    L = [M]
    E1 = E(row1=0, row2=2)
    M1 = sig(E1*M)
    L.append(M1)
    E2 = E(row1=0, scale=1/M1[0, 0])
    M2 = sig(E2*M1)
    L.append(M2)
    E31 = E(row1=1, row2=0, scale=-M2[1, 0])
    E32 = E(row1=2, row2=0, scale=-M2[2, 0])
    M3 = sig(E32*E31*M2)
    L.append(M3)
    E4 = E(row1=1, row2=2)
    M4 = sig(E4*M3)
    L.append(M4)
    E5 = E(row1=1, scale=1/M4[1, 1])
    M5 = sig(E5*M4)
    L.append(M5)
    E61 = E(row1=0, row2=1, scale=-M5[0, 1])
    E62 = E(row1=2, row2=1, scale=-M5[2, 1])
    M6 = sig(E62*E61*M5)
    L.append(M6)
    E7 = E(row1=2, scale=1/M6[2, 2])
    M7 = sig(E7*M6)
    L.append(M7)
    E81 = E(row1=0, row2=2, scale=-M7[0, 2])
    E82 = E(row1=1, row2=2, scale=-M7[1, 2])
    M8 = sig(E82*E81*M7)
    L.append(M8)
    return L


def ex2():
    A = matrix([
        (QQ(.51), QQ(92.6)),
        (QQ(99), QQ(-449))
    ])
    b = vector([QQ(97.9), QQ(541)])
    return A.augment(b, subdivide=True)


def gj2(d=3):
    sig = partial(matrix_sigfig, n=d)
    M = ex2()
    M = M.change_ring(RDF)
    E = partial(elementary_matrix, RDF, M.nrows())
    L = [M]
    E1 = E(row1=0, scale=1/M[0, 0])
    M1 = sig(E1*M)
    L.append(M1)
    E2 = E(row1=1, row2=0, scale=-M1[1, 0])
    M2 = sig(E2*M1)
    L.append(M2)
    E3 = E(row1=1, scale=1/M2[1, 1])
    M3 = sig(E3*M2)
    L.append(M3)
    E4 = E(row1=0, row2=1, scale=-M3[0, 1])
    M4 = sig(E4*M3)
    L.append(M4)
    return L


def ex3():
    A = matrix([
        (QQ(1), QQ(1.04)),
        (QQ(6), QQ(6.2))
    ])
    b = vector([QQ(2.04), QQ(12.2)])
    return A.augment(b, subdivide=True)


def gj3(d=3):
    sig = partial(matrix_sigfig, n=d)
    M = ex3()
    M = M.change_ring(RDF)
    E = partial(elementary_matrix, RDF, M.nrows())
    L = [M]
    E1 = E(row1=0, scale=1/M[0, 0])
    M1 = sig(E1*M)
    L.append(M1)
    E2 = E(row1=1, row2=0, scale=-M1[1, 0])
    M2 = sig(E2*M1)
    L.append(M2)
    E3 = E(row1=1, scale=1/M2[1, 1])
    M3 = sig(E3*M2)
    L.append(M3)
    E4 = E(row1=0, row2=1, scale=-M3[0, 1])
    M4 = sig(E4*M3)
    L.append(M4)
    return L
