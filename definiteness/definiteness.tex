\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\usepackage{stackengine}
\usepackage{booktabs}

\title{The Definiteness of Hermitian Matrices}
\subtitle{Math 218}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Quadratic Forms}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{quadratic form} on $\mathbb{R}^n$ is a function
    $q:\mathbb{R}^n\to\mathbb{R}$ given by
    \[
      q(x_1, x_2, \dotsc, x_n) = \sum_{i=1}^n\sum_{j=1}^n c_{ij}\cdot x_ix_j
    \]
    where $c_{ij}\in\mathbb{R}$.
  \end{definition}

\end{frame}


\begin{sagesilent}
  set_random_seed(3190)
  var('x y')
  X = vector([x, y])
  A = random_matrix(ZZ, 2)
  q2 = expand(X*(A+A.T)*X)
  var('x1 x2 x3')
  X = vector([x1, x2, x3])
  A = random_matrix(ZZ, 3)
  q3 = expand(X*(A+A.T)*X)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[Quadratic Form on $\mathbb{R}^2$]<+->
    $q(x, y)=\sage{q2}$
  \end{example}

  \begin{example}[Quadratic Form on $\mathbb{R}^3$]<+->
    $q(x_1, x_2, x_3)=\sage{q3}$
  \end{example}

  \begin{example}[Quadratic Form on $\mathbb{R}^4$]<+->
    $q(x_1, x_2, x_3, x_4)=x_1^2+x_2^2-x_4^2$
  \end{example}

  \begin{example}<+->
    $q(x, y)=x^2-y^2+1$ is \emph{not} a quadratic form
  \end{example}

\end{frame}


\subsection{Definiteness}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}<+->
    Every quadratic form satisfies $q(\bv{O})=0$.
  \end{theorem}

  \begin{example}<+->
    For $q(x, y)=x^2+y^2-xy$ we have $q(\bv{O})=0^2+0^2-0\cdot 0=0$.
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Quadratic forms are classified according to their values at
    $\bv{x}\neq\bv{O}$.
    \begin{description}[<+->][negative semidefinite]
    \item[positive definite] $q(\bv{x})>0$
    \item[positive semidefinite] $q(\bv{x})\geq0$
    \item[negative definite] $q(\bv{x})<0$
    \item[negative semidefinite] $q(\bv{x})\leq0$
    \item[indefinite] $q(\bv{x})>0$ and $q(\bv{x})<0$
    \end{description}
  \end{definition}

\end{frame}


\begin{sagesilent}
  var('x1 x2 x3')
  X = vector([x1, x2, x3])
  A = matrix([(1, -3, 2), (0, 2, 0), (-2, 3, -4)])
  q = expand(X*(A.T*A)*X)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myQa}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\ > \ $};
      \onslide<+->{
        \node[overlay, above right= 1mm and 1cm of a] (text) {\emph{positive definite} (also \emph{positive semidefinite})};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \begin{example}<+->
    $q(x, y)=x^2+y^2 \onslide<+-> \myQa 0$
  \end{example}


  \newcommand{\myQb}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$q(x, y)=x^2-y^2$};
      \onslide<+->{
        \node[overlay, right= 1cm of a] (text) {\emph{indefinite} ($q(1, 0)>0$ and $q(0, 1)<0$)};
        \draw[overlay, <-, thick, shorten <=2pt] (a.east) -- (text.west);
      }
    }
  }
  \begin{example}<+->
    $\myQb$
  \end{example}

  \newcommand{\myQc}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\ \geq\ $};
      \onslide<+->{
        \node[overlay, above right= -2mm and 1cm of a] (text) {\stackanchor{\emph{positive semidefinite}}{(not positive definite)}};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \begin{example}<+->
    $q(x, y)=x^2+2\,xy+y^2\onslide<+->=(x+y)^2 \onslide<+-> \myQc 0$
  \end{example}

  \newcommand{\myQd}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\ =\ $};
      \onslide<+->{
        \node[overlay, below right= 2mm and 1cm of a] (text) {Finding definiteness is hard!};
        % \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{example}<+->
    $q(x_1, x_2, x_3) \myQd \sage{q}$
  \end{example}

\end{frame}


\section{Hermitian Representations}
\subsection{Description}

\begin{sagesilent}
  A = matrix([(-1, 3), (0, 6)])
  S = A+A.T-diagonal_matrix(A.diagonal())
  var('x y')
  x = vector([x, y])
  v = matrix.column(x)
  q = expand(x*S*x)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the quadratic form given by
    \[
      q(x, y) = \sage{q}
    \]
    The data in this quadratic form can be organized into the product
    \begin{gather*}
      \sage{v.T}\sage{S}\sage{v}
      = \sage{v.T}\sage{S*v}
      = \sage{q}
    \end{gather*}

  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myS}{
    \begin{blockarray}{*{5}c}
      \begin{block}{*{5}c}
        & x_1 & x_2 & \cdots & x_n \\
      \end{block}
      \begin{block}{c[*{4}c]}
        x_1    & c_{11}           & \frac{c_{12}}{2} & \cdots & \frac{c_{1n}}{2} \\
        x_2    & \frac{c_{12}}{2} & c_{22}           & \cdots & \frac{c_{2n}}{2} \\
        \vdots & \vdots           & \vdots           & \ddots & \vdots           \\
        x_n    & \frac{c_{1n}}{2} & \frac{c_{2n}}{2} & \cdots & c_{nn} \\
      \end{block}%
    \end{blockarray}%
  }
  \begin{theorem}<+->
    For $q(x_1, x_2, \dotsc, x_n)=\sum c_{ij}\cdot x_ix_j$ define
    \[
      S=\myS
    \]
    Then $S$ is Hermitian and $q(\bv{x})=\langle \bv{x}, S\bv{x}\rangle$.
  \end{theorem}
\end{frame}


\subsection{Example}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myS}{
    \begin{blockarray}{*{4}c}
      \begin{block}{*{4}c}
        & \mybox{x_1}{4-5, 7-8, 12-13} & \mybox{x_2}{9-10, 17-18, 20-21} & \mybox{x_3}{14-15, 22-23, 25-26} \\
      \end{block}
      \begin{block}{c[*{3}r]}
        \mybox{x_1}{4-5, 9-10, 14-15}    & \onslide<5->{-3}  & \onslide<10->{ 3} & \onslide<15->{-5} \\
        \mybox{x_2}{7-8, 17-18, 22-23}   & \onslide<8->{ 3}  & \onslide<18->{ 1} & \onslide<23->{-6} \\
        \mybox{x_3}{12-13, 20-21, 25-26} & \onslide<13->{-5} & \onslide<21->{-6} & \onslide<26-27>{ 7} \\
      \end{block}%
    \end{blockarray}%
  }
  \begin{example}
    Consider the quadratic form on $\mathbb{R}^3$ given by
    \[
      q(x_1, x_2, x_3)
      =
      \mybox{-3\,x_1^2}{3-5}
      \mybox{+6\,x_1x_2}{6-10}
      \mybox{-10\,x_1x_3}{11-15}
      \mybox{+x_2^2}{16-18}
      \mybox{-12\,x_2x_3}{19-23}
      \mybox{+7\,x_3^2}{24-26}
    \]
    \onslide<2->Then $q(\bv{x})=\langle\bv{x}, S\bv{x}\rangle$ where
    \[
      S = \myS
    \]
  \end{example}
\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myD}{
    \left[
      \begin{array}{rrrr}
        \lambda_1 &           &        &           \\
                  & \lambda_2 &        &           \\
                  &           & \ddots &           \\
                  &           &        & \lambda_n
      \end{array}
    \right]
  }
  \begin{example}
    Consider the quadratic form $q(\bv{x})=\inner{\bv{x}, D\bv{x}}$ where
    \begin{gather*}
      \begin{align*}
        D &= \myD & \onslide<2->{q(x_1, x_2, \dotsc, x_n)} &\onslide<2->{=} \onslide<3->{\lambda_1\cdot x_1^2+\lambda_2\cdot x_2^2+\dotsb+\lambda_n\cdot x_n^2}
      \end{align*}
    \end{gather*}
    \onslide<4->{Diagonal quadratic forms have no ``cross-terms.''}
  \end{example}
\end{frame}

\section{Determining Definiteness}
\subsection{Completing the Square}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Consider a spectral factorization $S=QDQ^\ast$ so that
    \[
      \onslide<+->{q(\bv{x}) =}
      \onslide<+->{\inner{\bv{x}, S\bv{x}} =}
      \onslide<+->{\inner{\bv{x}, QDQ^\ast\bv{x}} =}
      \onslide<+->{\inner{Q^\ast\bv{x}, DQ^\ast\bv{x}}}
    \]
    \onslide<+->{Defining $\bv{y}=Q^\ast\bv{x}$ then gives}
    \begin{gather*}
      \onslide<+->{q(\bv{x}) =}
      \onslide<+->{\inner{Q^\ast\bv{x}, DQ^\ast\bv{x}} =}
      \onslide<+->{\inner{\bv{y}, D\bv{y}} =}
      \onslide<+->{\lambda_1\cdot y_1^2+\lambda_2\cdot y_2^2+\dotsb+\lambda_n\cdot y_n^2}
    \end{gather*}
    \onslide<+->{This technique is called \emph{completing the square}.}
  \end{definition}

  \begin{block}{Observation}<+->
    Definiteness is determined by the \emph{signs of the eigenvalues}.
  \end{block}
\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The definiteness of $q(\bv{x})=\inner{\bv{x}, S\bv{x}}$ is
    \begin{description}[<+->][negative semidefinite]
    \item[positive definite] eigenvalues of $S$ are $>0$
    \item[positive semidefinite] eigenvalues of $S$ are $\geq0$
    \item[negative definite] eigenvalues of $S$ are $<0$
    \item[negative semidefinite] eigenvalues of $S$ are $\leq0$
    \item[indefinite] eigenvalues of $S$ are $>0$ and $<0$
    \end{description}
  \end{theorem}

\end{frame}

\begin{sagesilent}
  S = matrix([(-1, 1, 0), (1, -1, 0), (0, 0, -2)])
  var('x1 x2 x3 t')
  x = vector([x1, x2, x3])
  q = expand(x*S*x)
  chi = factor(S.characteristic_polynomial(t))
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myS}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\ =$};
      \onslide<2->{
        \node[overlay, above right= 0.25cm and 2mm of a] (text) {$S=\sage{S}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\myChi}{
    \left|
      \begin{array}{rrr}
        t+1 & -1  & 0 \\
        -1 & t+1 & 0 \\
        0 & 0 & t+2
      \end{array}
    \right|
  }
  \newcommand{\myChiA}{
    \left|
      \begin{array}{rrr}
        t+1 & -1 \\
        -1 & t+1 \\
      \end{array}
    \right|
  }
  \newcommand{\myChiB}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{chi}$};
      \onslide<7->{
        \node[overlay, above= 0.75cm of a] (text) {$\EVals(S)=\Set{-2, 0}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) -- (text.south);
      }
    }
  }
  \begin{example}
    Consider $q(x_1, x_2, x_3) \myS \sage{q}$.
    \begin{gather*}
      \onslide<3->{\chi_S(t) =}
      \onslide<4->{\myChi =}
      \onslide<5->{(t+2)\cdot\myChiA =}
      \onslide<6->{\myChiB}
    \end{gather*}
    \onslide<8->{The quadratic form is \emph{negative semidefinite} (not
      negative definite).}
  \end{example}
\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myS}{
    \left[
      \begin{array}{rrr}
        4 & -2 & 1 \\
        -2 &  4 & 1 \\
        1 &  1 & 1
      \end{array}
    \right]
  }
  \newcommand{\myQ}{
    \left[
      \begin{array}{rrr}
        \frac{1}{\sqrt{2}} & \frac{1}{\sqrt{3}} & \frac{1}{\sqrt{6}} \\
        -\frac{1}{\sqrt{2}} & \frac{1}{\sqrt{3}} & \frac{1}{\sqrt{6}} \\
        0                   & \frac{1}{\sqrt{3}} & \frac{-2}{\sqrt{6}}
      \end{array}
    \right]
  }
  \newcommand{\myQT}{
    \left[
      \begin{array}{rrr}
        \frac{1}{\sqrt{2}} & -\frac{1}{\sqrt{2}} & 0 \\
        \frac{1}{\sqrt{3}} & \frac{1}{\sqrt{3}}  & \frac{1}{\sqrt{3}} \\
        \frac{1}{\sqrt{6}} & \frac{1}{\sqrt{6}}  & \frac{-2}{\sqrt{6}}
      \end{array}
    \right]
  }
  \newcommand{\myD}{
    \left[
      \begin{array}{rrr}
        6 &   & \\
          & 3 & \\
          &   & 0
      \end{array}
    \right]
  }
  \begin{example}
    Consider the spectral factorization
    \begin{gather*}
      \overset{S}{\myS}
      =
      \overset{Q}{\myQ}
      \overset{D}{\myD}
      \overset{Q^\ast}{\myQT}
    \end{gather*}
    \onslide<2->{To complete the square, define $\bv{y}=Q^\ast\bv{x}$ so that}
    \begin{gather*}
      \begin{align*}
        \onslide<2->{q(x_1, x_2, x_3)}
        &\onslide<2->{=} \onslide<3->{6\cdot y_1^2+3\cdot y_2^2+0\cdot y_3^2} \\
        &\onslide<3->{=}
          \onslide<4->{6\cdot\Set*{\frac{1}{\sqrt{2}}\cdot(x_1-x_2)}^2+}
          \onslide<4->{3\cdot\Set*{\frac{1}{\sqrt{3}}\cdot\pair{x_1+x_2+x_3}}^2}\\%  \onslide<4->{0\cdot\Set*{\frac{1}{\sqrt{6}}\cdot\pair{x_1+x_2-2\,x_3}}^2} \\
        &\onslide<5->{= 3\cdot(x_1-x_2)^2+(x_1+x_2+x_3)^2}
      \end{align*}
    \end{gather*}
    \onslide<6->{The quadratic form is \emph{positive semidefinite} (not
      positive definite).}
  \end{example}

\end{frame}


\subsection{Definiteness From $S=LU$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Problem}
    Finding eigenvalues to determine definiteness is \emph{hard}.
  \end{block}

  \begin{block}{Question}<2->
    How can we efficiently determine the definiteness?
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The definiteness of $S=LU$ is determined by the pivots in $U$.
    \begin{description}[<+->][negative semidefinite]
    \item[positive definite] pivots in $U$ are $>0$
    \item[positive semidefinite] pivots in $U$ are $\geq0$
    \item[negative definite] pivots in $U$ are $<0$
    \item[negative semidefinite] pivots in $U$ are $\leq0$
    \item[indefinite] pivots in $U$ are $>0$ and $<0$
    \end{description}
  \end{theorem}

  \begin{alertblock}{Warning}<+->
    This only works with $S=LU$, not $PS=LU$.
  \end{alertblock}

\end{frame}


\begin{sagesilent}
  S1 = matrix(ZZ, [[1, 0, -3], [0, 1, -2], [-3, -2, 4]])
  _, L1, U1 = S1.LU()
  S2 = matrix(ZZ, [[9, 0, 3], [0, 1, 1], [3, 1, 3]])
  _, L2, U2 = S2.LU(pivot='nonzero')
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myUa}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\overset{U}{\sage{U1}}$};
      \onslide<2->{
        \node[overlay, below left= 1mm and -5mm of a] (text) {$S$ is \emph{indefinite}};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myUb}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\overset{U}{\sage{U2}}$};
      \onslide<4->{
        \node[overlay, below left= 1mm and -5mm of a] (text) {$S$ is \emph{positive definite}};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \begin{example}
    $
    \overset{S}{\sage{S1}}
    =
    \overset{L}{\sage{L1}}
    \myUa
    $
  \end{example}

  \begin{example}<3->
    $
    \overset{S}{\sage{S2}}
    =
    \overset{L}{\sage{L2}}
    \myUb
    $
  \end{example}

\end{frame}



\section{Semidefiniteness}
\subsection{Gramians}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    The Gramian $S=A^\ast A$ of $A$ is Hermitian.
  \end{block}

  \begin{block}{Question}<2->
    What is the definiteness of $S=A^\ast A$?
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myD}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$D$};
      \onslide<3->{
        \node[overlay, below right= 1mm and 1cm of a] (text) {$\diag(\lambda_1,\dotsc,\lambda_n)$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \newcommand{\mySqrtD}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sqrt{D}$};
      \onslide<5->{
        \node[overlay, above= 5mm of a] (text) {$\diag(\sqrt{\lambda_1},\dotsc,\sqrt{\lambda_n})$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.south);
      }
    }
  }
  \begin{theorem}
    $S$ is positive semidefinite if and only if $S$ is a Gramian $S=A^\ast A$.
  \end{theorem}
  \begin{proof}<2->
    $(\Rightarrow)$ Suppose $S$ is positive semidefinite and consider
    \[
      S=Q\myD Q^\ast
    \]
    \onslide<4->{Putting $A=\mySqrtD Q^\ast$ gives}
    \[
      \onslide<6->{S =}
      \onslide<7->{QDQ^\ast =}
      \onslide<8->{Q\sqrt{D}\sqrt{D}Q^\ast =}
      \onslide<9->{(\sqrt{D} Q^\ast)^\ast\sqrt{D}Q^\ast =}
      \onslide<10->{A^\ast A}
    \]
    \onslide<11->{$(\Leftarrow)$ Suppose $S=A^\ast A$.} \onslide<12->{Then}
    \[
      \onslide<12->{q(\bv{x}) =}
      \onslide<13->{\inner{\bv{x}, S\bv{x}} =}
      \onslide<14->{\inner{\bv{x}, A^\ast A\bv{x}} =}
      \onslide<15->{\inner{A\bv{x}, A\bv{x}} =}
      \onslide<16->{\norm{A\bv{x}}^2}
      \onslide<17->{\geq 0}
    \]
    \onslide<18->{so $S$ is positive semidefinite.\qedhere}
  \end{proof}

\end{frame}


\begin{sagesilent}
  var('x1 x2 x3')
  set_random_seed(491)
  A = random_matrix(ZZ, 2, 3)
  x = vector([x1, x2, x3])
  S = A.T*A
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the factorization
    \[
      \overset{S}{\sage{S}}
      =
      \overset{A^\ast}{\sage{A.T}}
      \overset{A}{\sage{A}}
    \]
    \onslide<2->{Immediately, we know $S$ is positive semidefinite and}
    \[
      \onslide<2->{q(\bv{x}) =}
      \onslide<3->{\norm{A\bv{x}}^2 =}
      \onslide<4->{(-x_1-3\,x_2)^2+(2\,x_2-x_2+4\,x_3)^2}
      \onslide<5->{\geq 0}
    \]
  \end{example}

\end{frame}


\subsection{Cholesky Factorizations}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myS}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$S$};
      \onslide<2->{
        \node[overlay, below left= 1mm and 0.5cm of a] (text) {positive definite};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myR}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {$R$};
      \onslide<3->{
        \node[overlay, below right= -2mm and 0.5cm of a] (text) {\stackanchor{upper triangular}{positive diagonal}};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{definition}
    A \emph{Cholesky factorization} is an equation of the form
    \[
      \myS = R^\ast\myR
    \]
  \end{definition}

  \begin{theorem}<4->
    Every positive definite matrix has a Cholesky factorization.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Cholesky Factorization Algorithm}
    Let $S$ be a Hermitian matrix.
    \begin{description}[<+->][\quad Step 1]
    \item[Step 1] Factor $S=LU$ \emph{without partial pivoting}.
      \begin{description}[<+->]\setbeamercolor{description item}{fg=red}
      \item[Not positive definite] if a row swap is required.
      \item[Not positive definite] if $U$ has a nonpositive pivot.
      \end{description}
    \item[Step 2] Let $D=\diag(\textnormal{pivots in }U)$.
    \item[Step 3] Define $R=\sqrt{D}L^\ast$.
    \end{description}
    \onslide<+->This gives a Cholesky factorization $S=R^\ast R$.
  \end{block}

\end{frame}


\begin{sagesilent}
  S = matrix([(9, 3, 20), (3, 2, 3), (20, 3, 58)])
  from functools import partial
  elem = partial(elementary_matrix, S.nrows())
  l21 = -S[1, 0] / S[0, 0]
  l31 = -S[2, 0] / S[0, 0]
  E21 = elem(row1=1, row2=0, scale=l21)
  E31 = elem(row1=2, row2=0, scale=l31)
  S1 = E31*E21*S
  l32 = -S1[2, 1] / S1[1, 1]
  E32 = elem(row1=2, row2=1, scale=l32)
  U = E32*S1
  sqrtD = diagonal_matrix(map(sqrt, U.diagonal()))
  L = matrix([(1, 0, 0), (-l21, 1, 0), (-l31, -l32, 1)])
  R = sqrtD*L.T
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the row-reductions
    \newcommand{\myStepA}{\arraycolsep=1pt\tiny
      \begin{array}{rcrcr}
        R_2 &+& (\sage{l21})\cdot R_1 &\to& R_2 \\
        R_3 &+& (\sage{l31})\cdot R_1 &\to& R_3
      \end{array}
    }
    \begin{gather*}
      \overset{S}{\sage{S}}
      \onslide<2->{\xrightarrow{\myStepA}\sage{S1}}
      \onslide<3->{\xrightarrow{R_3+(\sage{l32})\cdot R_2\to R_3}\overset{U}{\sage{U}}}
    \end{gather*}
    \onslide<4->{This gives $S=LU$ and the Cholesky factorization $S=R^\ast R$
      where}
    \begin{gather*}
      \begin{align*}
        \onslide<4->{S} &\onslide<4->{= \overset{L}{\sage{L}}\overset{U}{\sage{U}}} & \onslide<5->{\overset{R}{\sage{R}}} &\onslide<5->{= \overset{\sqrt{D}}{\sage{sqrtD}}\overset{L^\ast}{\sage{L.T}}}
      \end{align*}
    \end{gather*}
    \onslide<6->{Note that $S$ is positive definite and}
    \begin{gather*}
      \onslide<6->{q(\bv{x}) =}
      \onslide<7->{\norm{R\bv{x}}^2 =}
      \onslide<8->{\Set*{3\,x_1+x_2+(\sfrac{20}{3})\cdot x_3}^2+\Set*{x_2-(\sfrac{11}{3})\cdot x_3}^2+\Set*{(\sfrac{1}{3})\cdot x_3}^2}
      \onslide<9->{> 0}
    \end{gather*}
  \end{example}
\end{frame}


\begin{sagesilent}
  S = matrix([(1, 3, 11), (3, 9, 39), (11, 39, 60)])
  from functools import partial
  elem = partial(elementary_matrix, S.nrows())
  l21 = -S[1, 0] / S[0, 0]
  l31 = -S[2, 0] / S[0, 0]
  E21 = elem(row1=1, row2=0, scale=l21)
  E31 = elem(row1=2, row2=0, scale=l31)
  S1 = E31*E21*S
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the row-reductions
    \newcommand{\myStepA}{\arraycolsep=1pt\tiny
      \begin{array}{rcrcr}
        R_2 &+& (\sage{l21})\cdot R_1 &\to& R_2 \\
        R_3 &+& (\sage{l31})\cdot R_1 &\to& R_3
      \end{array}
    }
    \[
      \onslide<2->{\overset{S}{\sage{S}}}
      \onslide<3->{\xrightarrow{\onslide<4->{\myStepA}}}\onslide<5->{\sage{S1}}
    \]
    \onslide<6->{A row-swap is needed, so $S$ is \emph{not positive definite}.}
  \end{example}

\end{frame}

\end{document}
