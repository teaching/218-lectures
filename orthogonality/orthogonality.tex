\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}

\title{Orthogonality}
\subtitle{Math 218}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Geometric Motivation}
\subsection{Organizing Orthogonal Vectors}

\begin{sagesilent}
  A = matrix.column(QQ, [1, -7, 3])
  a, = A.columns()
  B = A.left_kernel(basis='pivot').basis_matrix().T
  b1, b2 = B.columns()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question 1}
    Are $V=\Span\sage{Set([a])}$ and $W=\Span\sage{Set([b1])}$ related?
  \end{block}
  \noindent
  \begin{minipage}{.5\textwidth}
    \onslide<2->{
    \begin{block}{Answer}
      The equation
      \[
        \sage{a}\cdot\sage{b1}=\sage{a*b1}
      \]
      means that the vectors in $V$ are \emph{orthogonal} to the vectors in $W$.
    \end{block}
    }
  \end{minipage}% This must go next to Y
  \begin{minipage}{.5\textwidth}
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , transform shape
        , rotate=15]

        \coordinate (O) at (0, 0, 0);
        \coordinate (e2) at (1, 0, 0);
        \coordinate (e3) at (0, 1, 0);
        \coordinate (e1) at (0, 0, 1);


        \onslide<7->{
        \pgfmathsetmacro{\myPerp}{.2}
        \draw[ultra thick, black]
        ($ \myPerp*(e3) $) -- ($ \myPerp*(e3) + \myPerp*(e2) $)  -- ($ \myPerp*(e2) $);
        }

        \onslide<5->{
        \pgfmathsetmacro{\myStretch}{3/2}
        \draw[ultra thick, <->, teal]
        ($ -\myStretch*(e3) $) -- ($ \myStretch*(e3) $) node[above] {$V$};
        }

        \onslide<6->{
        \draw[ultra thick, <->, teal]
        ($ -\myStretch*(e2) $) -- ($ \myStretch*(e2) $) node[right] {$W$};
        }

        \onslide<3->{
          \draw[ultra thick, ->, blue] (O) -- (e3);
        }

        \onslide<4->{
          \draw[ultra thick, ->, blue] (O) -- (e2);
        }
      \end{tikzpicture}
    \]
  \end{minipage}

  \onslide<8->
  \begin{block}{Math-Speak}
    We say that $V$ and $W$ are \emph{orthogonal spaces} and write $V\perp W$.
  \end{block}

\end{frame}



\begin{sagesilent}
  var('w1 w2 w3')
  w = vector([w1, w2, w3])
  ws = w(w1=7*w2-3*w3)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question 2}
    Can we describe \emph{all} vectors orthogonal to $V=\Span\sage{Set([a])}$?
  \end{block}
  \noindent
  \begin{minipage}{.6\textwidth}
    \onslide<2->{
    \begin{block}{Answer}
      $\sage{w}\perp V$ means
      \begin{gather*}
        0
        = \onslide<3->{\sage{a}\cdot\sage{w}
        =} \onslide<4->{\sage{a*w}}
      \end{gather*}
      \onslide<5->{ The $\vv{w}$ orthogonal to $V$ are of the form
      \[
        \vv{w}
        = w_2\sage{b1}+w_3\sage{b2}
      \]
      }
    \end{block}
    }
  \end{minipage}% This must go next to Y
  \begin{minipage}{.4\textwidth}
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , transform shape
        , rotate=15]

        \coordinate (O) at (0, 0, 0);
        \coordinate (e2) at (1, 0, 0);
        \coordinate (e3) at (0, 1, 0);
        \coordinate (e1) at (0, 0, 1);

        \onslide<7->{
        \pgfmathsetmacro{\myStretch}{3/2}
        \draw[ultra thick, <->, teal]
        ($ -\myStretch*(e3) $) -- ($ \myStretch*(e3) $) node[above] {$V$};
        }

        \onslide<10->{
        \pgfmathsetmacro{\myPlaneX}{2}
        \pgfmathsetmacro{\myPlaneY}{2.5}
        \filldraw[ultra thick, fill=teal!30, draw=teal]
        ($ \myPlaneY*(e2) $)
        -- ($ -\myPlaneX*(e1) $)
        -- ($ -\myPlaneY*(e2) $)
        -- ($ \myPlaneX*(e1) $)
        -- cycle
        node[left, teal] {$W$};
        }

        \onslide<7->{
        \draw[ultra thick, dotted, <->, teal]
        ($ -\myStretch*(e3) $) -- ($ \myStretch*(e3) $);
        }

        \onslide<11->{
        \pgfmathsetmacro{\myPerp}{.2}
        \draw[ultra thick, black]
        ($ \myPerp*(e3) $) -- ($ \myPerp*(e3) + \myPerp*(e2) $)  -- ($ \myPerp*(e2) $);

        \draw[ultra thick, black]
        ($ \myPerp*(e3) $) -- ($ \myPerp*(e3) + \myPerp*(e1) $)  -- ($ \myPerp*(e1) $);
        }

        \onslide<6->{
          \draw[ultra thick, ->, blue] (O) -- (e3);
        }
        \onslide<8->{
          \draw[ultra thick, ->, blue] (O) -- (e2);
        }
        \onslide<9->{
          \draw[ultra thick, ->, blue] (O) -- (e1);
          }
      \end{tikzpicture}
    \]
  \end{minipage}
  \onslide<12->{ $W=\Span\Set{\sage{b1}, \sage{b2}}$ organizes all of the
    vectors orthogonal to $V$.}

  \onslide<13->{
  \begin{block}{Math-Speak}
    We say $W$ is the \emph{orthogonal complement} of $V$ and write $W=V^\perp$.
  \end{block}
  }

\end{frame}


\section{Orthogonal Complements}
\subsection{Statements About Dimension}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    We write $V\subset W$ if every vector in $V$ is also in $W$.
  \end{definition}

  \pause
  \begin{theorem}
    $V\subset W$ implies $\dim(V)\leq\dim(W)$
  \end{theorem}

  \pause
  \begin{theorem}
    $V=W$ means $V\subset W$ and $\dim(V)=\dim(W)$
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}[$V\subset W$]
    The \emph{codimension} of $V$ in $W$ is $\codim_W(V)=\dim(W)-\dim(V)$.
  \end{definition}

  \onslide<2->{
  \begin{example}
    Let $A$ be $m\times n$. \onslide<3->{Then $\Null(A)\subset\mathbb{R}^n$ and}
    \begin{align*}
      \onslide<4->{\codim_{\mathbb{R}^n}{\Null(A)}
      &=} \onslide<5->{\dim(\mathbb{R}^n)-\dim\Null(A) \\
      &=} \onslide<6->{n-\Set{n-\rank(A)} \\
      &=} \onslide<7->{n-n+\rank(A) \\
      &=} \onslide<8->{\rank(A)
      &=} \onslide<9->{\dim\Col(A)}
    \end{align*}
  \end{example}
  }

\end{frame}



\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{orthogonal complement} of $V\subset\mathbb{R}^n$ is the collection
    $V^\perp$ of all vectors in $\mathbb{R}^n$ orthogonal to every vector in
    $V$.
  \end{definition}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $V^\perp$ is a vector space.
  \end{theorem}
  \pause
  \begin{proof}
    We can check the axioms individually.
    \begin{enumerate}[<+->]
    \item Note that $\vv{O}\in V^\perp$ since $\vv{v}\in V$ implies
      $\vv{O}\cdot\vv{v}=0$.
    \item Suppose that $\vv*{w}{1},\vv*{w}{2}\in V^\perp$. Then $\vv{v}\in V$
      implies
      \[
        (\vv*{w}{1}+\vv*{w}{2})\cdot\vv{v}
        = \vv*{w}{1}\cdot\vv{v}+\vv*{w}{2}\cdot\vv{v}
        = 0+0
        = 0
      \]
      This means that $\vv*{w}{1}+\vv*{w}{2}\in V^\perp$.
    \item Suppose that $\vv{w}\in V^\perp$. Then $\vv{v}\in V$ implies
      \[
        (c\cdot\vv{w})\cdot\vv{v}=c\cdot(\vv{w}\cdot\vv{v})=c\cdot0=0
      \]
      This means that $c\cdot\vv{w}\in V^\perp$.
    \end{enumerate}
    \onslide<+-> All axioms pass!
  \end{proof}

\end{frame}


\begin{sagesilent}
  A = matrix.column(QQ, [(-2, 7, 12), (-1, 3, 5)])
  a1, a2 = A.columns()
  var('w1 w2 w3')
  w = vector([w1, w2, w3])
  n, = A.T.right_kernel(basis='pivot').basis()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the vector space $V$ given by
    \[
      V = \Col\sage{A}
    \]
    \onslide<2->{The vectors $\vv{w}=\sage{w}$ in $V^\perp$ are defined by the equations}
    \begin{gather*}
      \begin{align*}
        \onslide<3->{
        \begin{array}{rcrcrcr}
          -2\,w_1 &+& 7\,w_2 &+& 12\,w_3 &=& 0 \\
          -\,w_1 &+& 3\,w_2 &+&  5\,w_3 &=& 0
        \end{array}}&&
                       \onslide<4->{\rref\sage{A.T}=\sage{A.T.rref()}}
      \end{align*}
    \end{gather*}
    \onslide<5->{This means that $V^\perp=\Span\sage{Set([n])}$.}
  \end{example}

  \onslide<6->{
  \begin{block}{Note}
    $V\subset\mathbb{R}^3$ with $\dim(V)=2$ and $\dim(V^\perp)=3-2=1$.
  \end{block}
  }

\end{frame}


\subsection{Properties of Orthogonal Complements}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Orthogonal complements have the following properties.
    \begin{description}[order-reversing]\pause
    \item[order-reversing] $V\subset W$ implies $W^\perp\subset V^\perp$ \pause
    \item[involution] $(V^\perp)^\perp=V$\pause
    \item[codimension] $\codim_{\mathbb{R}^n}(V^\perp)=\dim(V)$
    \end{description}
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    The codimension formula $\dim(V)=\codim_{\mathbb{R}^n}(V^\perp)$ gives
    \[
      \dim(V)
      = \pause\codim_{\mathbb{R}^n}(V^\perp)
      = \pause\dim(\mathbb{R}^n)-\dim(V^\perp)
      = \pause n-\dim(V^\perp)
    \]
    \pause It follows that
    \[
      \dim(V)+\dim(V^\perp)=n
    \]
    \pause This means that $V^\perp$ is ``small'' if $V$ is ``big'' and vice-versa.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Our previous example gave
    \begin{align*}
      V &= \Col\sage{A} & V^\perp &= \Col\sage{matrix.column(n)}
    \end{align*}
    \onslide<2->{We may interpret this example geometrically}
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , transform shape
        , rotate=15]

        \coordinate (O) at (0, 0, 0);
        \coordinate (e2) at (1, 0, 0);
        \coordinate (e3) at (0, 1, 0);
        \coordinate (e1) at (0, 0, 1);

        \onslide<8->{
        \pgfmathsetmacro{\myStretch}{3/2}
        \draw[ultra thick, <->, teal]
        ($ -\myStretch*(e3) $) -- ($ \myStretch*(e3) $) node[above] {$V^\perp$};
        }

        \onslide<5->{
        \pgfmathsetmacro{\myPlaneX}{2}
        \pgfmathsetmacro{\myPlaneY}{2.5}
        \filldraw[ultra thick, fill=teal!30, draw=teal]
        ($ \myPlaneY*(e2) $)
        -- ($ -\myPlaneX*(e1) $)
        -- ($ -\myPlaneY*(e2) $)
        -- ($ \myPlaneX*(e1) $)
        -- cycle
        node[left, teal] {$V$};
        }

        \onslide<8->{
        \draw[ultra thick, dotted, <->, teal]
        ($ -\myStretch*(e3) $) -- ($ \myStretch*(e3) $);
        }

        \onslide<7->{
        \pgfmathsetmacro{\myPerp}{.2}
        \draw[ultra thick, black]
        ($ \myPerp*(e3) $) -- ($ \myPerp*(e3) + \myPerp*(e2) $)  -- ($ \myPerp*(e2) $);

        \draw[ultra thick, black]
        ($ \myPerp*(e3) $) -- ($ \myPerp*(e3) + \myPerp*(e1) $)  -- ($ \myPerp*(e1) $);
        }

        \onslide<6->{
          \draw[ultra thick, ->, blue] (O) -- (e3);
          }

        \onslide<4->{
          \draw[ultra thick, ->, blue] (O) -- (e2);
          }

        \onslide<3->{
          \draw[ultra thick, ->, blue] (O) -- (e1);
          }
      \end{tikzpicture}
    \]
  \end{example}

\end{frame}



\section{Orthogonality of the Four Fundamental Subspaces}
\subsection{$\Col(A)^\perp$ and $\LNull(A)^\perp$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    What are the orthogonal complements of the four fundamental subspaces?
    \[
      \begin{tikzpicture}[
        , scale=.8
        % , every node/.style={scale=0.4}
        , line join=round
        , line cap=round
        ]

        \coordinate (O) at (0, 0);
        \coordinate (P) at (1, 2);
        \coordinate (Q) at (2, -1);
        \coordinate (Rn) at ($ 1.15*(P)-(Q) $);
        \coordinate (Row) at ($ .5*(P)-(Q) $);
        \coordinate (Null) at ($ .5*(Q)-.5*(P) $);

        \node at (Rn) {$\mathbb{R}^n$};

        \filldraw[ultra thick, fill=YellowOrange]
        (O) -- (P) -- ($ (P)-2*(Q) $) -- ($ -2*(Q) $) -- cycle;
        \node at (Row) {$\underset{\onslide<2->{\Row(A)^\perp=?}}{\Row(A)}$};

        \filldraw[ultra thick, fill=Green!40]
        (O) -- (Q) -- ($ (Q)-(P) $) -- ($ -1*(P) $) -- cycle;
        \node at (Null) {$\underset{\onslide<3->{\Null(A)^\perp=?}}{\Null(A)}$};

        \coordinate (v) at (-1, 2);
        \coordinate (w) at (-2, -1);
        \coordinate (Rm) at ($ 1.15*(v)-1*(w) $);
        \coordinate (Col) at ($ .5*(v)-1*(w) $);
        \coordinate (LNull) at ($ .5*(w)-.5*(v) $);

        \tikzset{
          c/.style={every coordinate/.try}
        }

        \begin{scope}[every coordinate/.style={shift={(5,0)}}]


          \node at ([c]Rm) {$\mathbb{R}^m$};

          \filldraw[ultra thick, fill=BrickRed!50]
          ([c]O) -- ([c]v) -- ([c]$ (v)-2*(w) $) -- ([c]$ -2*(w) $) -- cycle;
          \node at ([c]Col) {$\underset{\onslide<4->{\Col(A)^\perp=?}}{\Col(A)}$};

          \filldraw[ultra thick, fill=Blue!50]
          ([c]O) -- ([c]w) -- ([c]$ (w)-(v) $) -- ([c]$ -1*(v) $) -- cycle;
          \node at ([c]LNull) {$\underset{\onslide<5->{\LNull(A)^\perp=?}}{\LNull(A)}$};


          \draw[ultra thick, shorten >=1.5em, shorten <=1.5em, ->]
          (Rn) -- ([c]Rm) node[midway, above] {$A$};

        \end{scope}

      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myV}[2]{
    \begin{bmatrix}
      ####1_1\\ ####1_2\\ \vdots\\ ####1_####2
    \end{bmatrix}
  }
  \newcommand{\myv}[2]{
    \left\langle
      ####1_1, ####1_2, \dotsc, ####1_####2
    \right\rangle
  }
  \newcommand{\myH}[2]{
    \begin{bmatrix}
      ####1_1& ####1_2& \dotsb& ####1_####2
    \end{bmatrix}
  }
  \begin{block}{Observation}
    Consider the two vectors $\vv{v}$ and $\vv{w}$ given by
    \begin{align*}
      \vv{v} &= \myv{v}{n} & \vv{w} &= \myv{w}{n}
    \end{align*}
    \onslide<2->{If we view $\vv{v}$ and $\vv{w}$ as $n\times 1$ matrices, then}
    \begin{align*}
      \onslide<3->{\vv{v}^\intercal\vv{w}
      &=} \onslide<4->{\myH{v}{n}\myV{w}{n} \\
      &=} \onslide<5->{v_1\cdot w_1+v_2\cdot w_2+\dotsb+v_n\cdot w_n \\
      &=} \onslide<6->{\vv{v}\cdot\vv{w}}
    \end{align*}
    \onslide<7->{The dot product can be written as matrix multiplication.}
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Are $\Col(A)$ and $\LNull(A)$ related?
  \end{block}
  \pause
  \begin{block}{Answer}
    Consider $\vv{v}\in\Col(A)$ and $\vv{w}\in\LNull(A)$. \pause We have
    \begin{align*}
      \vv{v} &= A\vv{x} & A^\intercal\vv{w} &= \vv{O}
    \end{align*}
    \pause It follows that
    \[
      \vv{v}\cdot\vv{w}
      = \pause\vv{v}^\intercal\vv{w}
      = \pause(A\vv{x})^\intercal\vv{w}
      = \pause\vv{x}^\intercal A^\intercal\vv{w}
      = \pause\vv{x}^\intercal\vv{O}
      = \pause0
    \]
    \pause So $\Col(A)\perp\LNull(A)$. \pause Furthermore,
    \[
      \dim\LNull(A)
      = \pause m-\dim\Col(A)
      = \pause\dim\Col(A)^\perp
    \]
    \pause This means that $\Col(A)^\perp=\LNull(A)$.
  \end{block}
\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $\Col(A)^\perp=\LNull(A)$ and $\LNull(A)^\perp=\Col(A)$
    \[
      \begin{tikzpicture}[
        , scale=.8
        % , every node/.style={scale=0.4}
        , line join=round
        , line cap=round
        ]

        \coordinate (O) at (0, 0);
        \coordinate (P) at (1, 2);
        \coordinate (Q) at (2, -1);
        \coordinate (Rn) at ($ 1.15*(P)-(Q) $);
        \coordinate (Row) at ($ .5*(P)-(Q) $);
        \coordinate (Null) at ($ .5*(Q)-.5*(P) $);

        \node at (Rn) {$\mathbb{R}^n$};

        \filldraw[ultra thick, fill=YellowOrange]
        (O) -- (P) -- ($ (P)-2*(Q) $) -- ($ -2*(Q) $) -- cycle;
        \node at (Row) {$\Row(A)$};

        \filldraw[ultra thick, fill=Green!40]
        (O) -- (Q) -- ($ (Q)-(P) $) -- ($ -1*(P) $) -- cycle;
        \node at (Null) {$\Null(A)$};

        \coordinate (v) at (-1, 2);
        \coordinate (w) at (-2, -1);
        \coordinate (Rm) at ($ 1.15*(v)-1*(w) $);
        \coordinate (Col) at ($ .5*(v)-1*(w) $);
        \coordinate (LNull) at ($ .5*(w)-.5*(v) $);

        \tikzset{
          c/.style={every coordinate/.try}
        }

        \begin{scope}[every coordinate/.style={shift={(5,0)}}]


          \node at ([c]Rm) {$\mathbb{R}^m$};

          \filldraw[ultra thick, fill=BrickRed!50]
          ([c]O) -- ([c]v) -- ([c]$ (v)-2*(w) $) -- ([c]$ -2*(w) $) -- cycle;
          \node at ([c]Col) {$\underset{\onslide<2->{\LNull(A)^\perp}}{\Col(A)}$};

          \filldraw[ultra thick, fill=Blue!50]
          ([c]O) -- ([c]w) -- ([c]$ (w)-(v) $) -- ([c]$ -1*(v) $) -- cycle;
          \node at ([c]LNull) {$\underset{\onslide<3->{\Col(A)^\perp}}{\LNull(A)}$};


          \draw[ultra thick, shorten >=1.5em, shorten <=1.5em, ->]
          (Rn) -- ([c]Rm) node[midway, above] {$A$};

          \pgfmathsetmacro{\mys}{.15}
          \coordinate (ort) at ([c]$ -\mys*(v) $);
          \coordinate (orb) at ([c]$ -\mys*(w) $);
          \coordinate (orm) at ([c]$ -\mys*(v)-\mys*(w) $);

          \onslide<4->{
            \draw[ultra thick] (ort) -- (orm) -- (orb);
          }

        \end{scope}

        % \pgfmathsetmacro{\mys}{.15}
        % \coordinate (ort) at ([c]$ -\mys*(P) $);
        % \coordinate (orb) at ([c]$ -\mys*(Q) $);
        % \coordinate (orm) at ([c]$ -\mys*(P)-\mys*(Q) $);

        % \draw[ultra thick] (ort) -- (orm) -- (orb);

      \end{tikzpicture}
    \]
  \end{theorem}

\end{frame}


\begin{sagesilent}
  A = matrix([(1, 3, 0, 5), (-2, -5, 4, -12), (1, 0, -11, 11), (-2, 1, 25, -24)])
  c1, c2, c3 = A.column_space().basis()
  l, = A.change_ring(QQ).left_kernel(basis='pivot').basis()
  b = c1-l
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $A$ satisfying
    \begin{align*}
      A &= \sage{A} & \LNull(A) &= \Col\sage{matrix.column(l)}
    \end{align*}
    Determine if $A\vv{x}=\vv{b}$ is consistent for $\vv{b}=\sage{b}$.
  \end{example}
  \pause
  \begin{block}{Solution}
    $A\vv{x}=\vv{b}$ consistent means
    $\vv{b}\in\Col(A)=\pause\LNull(A)^\perp$. \pause But
    \[
      \sage{b}\cdot\sage{l}=\sage{l*b}\neq0
    \]
    \pause This means that $A\vv{x}=\vv{b}$ is \pause \emph{not consistent}.
  \end{block}
\end{frame}



\subsection{$\Row(A)^\perp$ and $\Null(A)^\perp$}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    What are the orthogonal complements of the four fundamental subspaces?
    \[
      \begin{tikzpicture}[
        , scale=.8
        % , every node/.style={scale=0.4}
        , line join=round
        , line cap=round
        ]

        \coordinate (O) at (0, 0);
        \coordinate (P) at (1, 2);
        \coordinate (Q) at (2, -1);
        \coordinate (Rn) at ($ 1.15*(P)-(Q) $);
        \coordinate (Row) at ($ .5*(P)-(Q) $);
        \coordinate (Null) at ($ .5*(Q)-.5*(P) $);

        \node at (Rn) {$\mathbb{R}^n$};

        \filldraw[ultra thick, fill=YellowOrange]
        (O) -- (P) -- ($ (P)-2*(Q) $) -- ($ -2*(Q) $) -- cycle;
        \node at (Row) {$\underset{\onslide<5->{\Row(A)^\perp=?}}{\Row(A)}$};

        \filldraw[ultra thick, fill=Green!40]
        (O) -- (Q) -- ($ (Q)-(P) $) -- ($ -1*(P) $) -- cycle;
        \node at (Null) {$\underset{\onslide<6->{\Null(A)^\perp=?}}{\Null(A)}$};

        \coordinate (v) at (-1, 2);
        \coordinate (w) at (-2, -1);
        \coordinate (Rm) at ($ 1.15*(v)-1*(w) $);
        \coordinate (Col) at ($ .5*(v)-1*(w) $);
        \coordinate (LNull) at ($ .5*(w)-.5*(v) $);

        \tikzset{
          c/.style={every coordinate/.try}
        }

        \begin{scope}[every coordinate/.style={shift={(5,0)}}]


          \node at ([c]Rm) {$\mathbb{R}^m$};

          \filldraw[ultra thick, fill=BrickRed!50]
          ([c]O) -- ([c]v) -- ([c]$ (v)-2*(w) $) -- ([c]$ -2*(w) $) -- cycle;
          \node at ([c]Col) {$\underset{\onslide<2->{\LNull(A)^\perp}}{\Col(A)}$};

          \filldraw[ultra thick, fill=Blue!50]
          ([c]O) -- ([c]w) -- ([c]$ (w)-(v) $) -- ([c]$ -1*(v) $) -- cycle;
          \node at ([c]LNull) {$\underset{\onslide<3->{\Col(A)^\perp}}{\LNull(A)}$};


          \draw[ultra thick, shorten >=1.5em, shorten <=1.5em, ->]
          (Rn) -- ([c]Rm) node[midway, above] {$A$};


          \pgfmathsetmacro{\mys}{.15}
          \coordinate (ort) at ([c]$ -\mys*(v) $);
          \coordinate (orb) at ([c]$ -\mys*(w) $);
          \coordinate (orm) at ([c]$ -\mys*(v)-\mys*(w) $);

          \onslide<4->{
            \draw[ultra thick] (ort) -- (orm) -- (orb);
          }

        \end{scope}

        % \pgfmathsetmacro{\mys}{.15}
        % \coordinate (ort) at ([c]$ -\mys*(P) $);
        % \coordinate (orb) at ([c]$ -\mys*(Q) $);
        % \coordinate (orm) at ([c]$ -\mys*(P)-\mys*(Q) $);

        % \draw[ultra thick] (ort) -- (orm) -- (orb);

      \end{tikzpicture}
    \]
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Are $\Row(A)$ and $\Null(A)$ related?
  \end{block}

  \pause
  \begin{block}{Answer}
    $
      \Row(A)^\perp
      = \pause\Col(A^\intercal)^\perp
      = \pause\LNull(A^\intercal)
      = \pause\Null(A)
      $
  \end{block}

\end{frame}





\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $\Col(A)^\perp=\LNull(A)$ and $\LNull(A)^\perp=\Col(A)$
    \[
      \begin{tikzpicture}[
        , scale=.8
        % , every node/.style={scale=0.4}
        , line join=round
        , line cap=round
        ]

        \coordinate (O) at (0, 0);
        \coordinate (P) at (1, 2);
        \coordinate (Q) at (2, -1);
        \coordinate (Rn) at ($ 1.15*(P)-(Q) $);
        \coordinate (Row) at ($ .5*(P)-(Q) $);
        \coordinate (Null) at ($ .5*(Q)-.5*(P) $);

        \node at (Rn) {$\mathbb{R}^n$};

        \filldraw[ultra thick, fill=YellowOrange]
        (O) -- (P) -- ($ (P)-2*(Q) $) -- ($ -2*(Q) $) -- cycle;
        \node at (Row) {$\underset{\onslide<5->{\Null(A)^\perp}}{\Row(A)}$};

        \filldraw[ultra thick, fill=Green!40]
        (O) -- (Q) -- ($ (Q)-(P) $) -- ($ -1*(P) $) -- cycle;
        \node at (Null) {$\underset{\onslide<6->{\Row(A)^\perp}}{\Null(A)}$};

        \coordinate (v) at (-1, 2);
        \coordinate (w) at (-2, -1);
        \coordinate (Rm) at ($ 1.15*(v)-1*(w) $);
        \coordinate (Col) at ($ .5*(v)-1*(w) $);
        \coordinate (LNull) at ($ .5*(w)-.5*(v) $);

        \tikzset{
          c/.style={every coordinate/.try}
        }

        \begin{scope}[every coordinate/.style={shift={(5,0)}}]


          \node at ([c]Rm) {$\mathbb{R}^m$};

          \filldraw[ultra thick, fill=BrickRed!50]
          ([c]O) -- ([c]v) -- ([c]$ (v)-2*(w) $) -- ([c]$ -2*(w) $) -- cycle;
          \node at ([c]Col) {$\underset{\onslide<2->{\LNull(A)^\perp}}{\Col(A)}$};

          \filldraw[ultra thick, fill=Blue!50]
          ([c]O) -- ([c]w) -- ([c]$ (w)-(v) $) -- ([c]$ -1*(v) $) -- cycle;
          \node at ([c]LNull) {$\underset{\onslide<3->{\Col(A)^\perp}}{\LNull(A)}$};


          \draw[ultra thick, shorten >=1.5em, shorten <=1.5em, ->]
          (Rn) -- ([c]Rm) node[midway, above] {$A$};


          \pgfmathsetmacro{\mys}{.15}
          \coordinate (ort) at ([c]$ -\mys*(v) $);
          \coordinate (orb) at ([c]$ -\mys*(w) $);
          \coordinate (orm) at ([c]$ -\mys*(v)-\mys*(w) $);

          \onslide<4->{
            \draw[ultra thick] (ort) -- (orm) -- (orb);
          }

        \end{scope}

        \pgfmathsetmacro{\mys}{.15}
        \coordinate (ort) at ([c]$ -\mys*(P) $);
        \coordinate (orb) at ([c]$ -\mys*(Q) $);
        \coordinate (orm) at ([c]$ -\mys*(P)-\mys*(Q) $);

        \onslide<6->{
          \draw[ultra thick] (ort) -- (orm) -- (orb);
          }

      \end{tikzpicture}
    \]
  \end{theorem}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Why is all of this useful?
  \end{block}
  \pause
  \begin{block}{Answer}
    We now have alternative ways to ``test'' if a vector lives in one of the
    four fundamental subspaces.
    \begin{description}[$\vv{v}\in\LNull(A)$]\pause
    \item[$\vv{v}\in\Col(A)$] means $\vv{v}\perp\LNull(A)$\pause
    \item[$\vv{v}\in\LNull(A)$] means $\vv{v}\perp\Col(A)$\pause
    \item[$\vv{v}\in\Row(A)$] means $\vv{v}\perp\Null(A)$\pause
    \item[$\vv{v}\in\Null(A)$] means $\vv{v}\perp\Row(A)$
    \end{description}
  \end{block}

\end{frame}


\subsection{$\vec{x}=\vec{x}_R+\vec{x}_N$ Decompositions}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $A$ be $m\times n$ with $\rank(A)=r$. \onslide<2->{ Suppose we have bases}
    \begin{align*}
      \onslide<2->{\Row(A) &= \Set{\vv*{v}{1},\dotsc,\vv*{v}{r}}} & \onslide<3->{\Null(A) &= \Set{\vv*{w}{1},\dotsc,\vv*{w}{n-r}}}
    \end{align*}
    \onslide<4->{Then every $\vv{x}\in\mathbb{R}^n$ may be written as}
    \begin{align*}
      \onslide<4->{\vv{x}
      &=}
        \onslide<5->{\underbrace{a_1\cdot\vv*{v}{1}+\dotsb+a_r\cdot\vv*{v}{r}}_{=\vv*{x}{R}\in\Row(A)}}
        \onslide<6->{+
        \underbrace{b_1\cdot\vv*{w}{1}+\dotsb+b_{n-r}\cdot\vv*{w}{n-r}}_{=\vv*{x}{N}\in\Null(A)} \\}
      &\onslide<7->{= \vv*{x}{R} + \vv*{x}{N}}
    \end{align*}
    \onslide<8-> Note that
    $A\vv{x}=\onslide<9->A(\vv*{x}{R} + \vv*{x}{N})=\onslide<10->A\vv*{x}{R} + A\vv*{x}{N}=\onslide<11->
    A\vv*{x}{R}+\vv{O}=\onslide<12->A\vv*{x}{R}$.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{The $\vv{x}=\vv*{x}{R}+\vv*{x}{N}$ Decomposition}
    In $\vv{x}=\vv*{x}{R}+\vv*{x}{N}$, we call
    \begin{description}\pause
    \item[$\vv*{x}{R}$] the \emph{projection} of $\vv{x}$ onto $\Row(A)$\pause
    \item[$\vv*{x}{N}$] the \emph{projection} of $\vv{x}$ onto $\Null(A)$
    \end{description}
    \pause Since $\Row(A)^\perp=\Null(A)$, we are guaranteed
    $\vv*{x}{R}\cdot\vv*{x}{N}=0$.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \[
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , scale=4/5
      ]

      \coordinate (O) at (0, 0);
      \coordinate (P) at (1, 2);
      \coordinate (Q) at (2, -1);
      \coordinate (Rn) at ($ 1.15*(P)-(Q) $);
      \coordinate (Row) at ($ .5*(P)-(Q) $);
      \coordinate (Null) at ($ .5*(Q)-.5*(P) $);

      \node at (Rn) {$\mathbb{R}^n$};

      \filldraw[ultra thick, fill=YellowOrange]
      (O) -- (P) -- ($ (P)-2*(Q) $) -- ($ -2*(Q) $) -- cycle;
      \node at (Row) {$\underset{\dim=\operatorname{rank}(A)}{\operatorname{Row}(A)}$};


      \filldraw[ultra thick, fill=Green!40]
      (O) -- (Q) -- ($ (Q)-(P) $) -- ($ -1*(P) $) -- cycle;
      \node at (Null) {\scriptsize$\underset{\dim=n-\operatorname{rank}(A)}{\operatorname{Null}(A)}$};

      \coordinate (v) at (-1, 2);
      \coordinate (w) at (-2, -1);
      \coordinate (Rm) at ($ 1.15*(v)-1*(w) $);
      \coordinate (Col) at ($ .5*(v)-1*(w) $);
      \coordinate (LNull) at ($ .5*(w)-.5*(v) $);
      \coordinate (b) at ($ .85*(v) $);

      \coordinate (xr) at ($ .5*(P) $);
      \coordinate (xn) at ($ .5*(Q) $);

      \coordinate (x) at ($ (xr)+(xn) $);


      \onslide<4->{
        \node at (xr) {\textbullet};
        \node at (xr) [left] {\scriptsize$\vv*{x}{R}$};

        \node at (xn) {\textbullet};
        \node at (xn) [below] {\scriptsize$\vv*{x}{N}$};
      }

      \onslide<2->{
        \node at (x) {\textbullet};
        \node at (x) [below right] {\scriptsize$\vv{x}$};
      }

      \onslide<5->{
        \draw[ultra thick, dashed] (xr) -- (x) -- (xn);
      }

      \tikzset{
        c/.style={every coordinate/.try}
      }

      \begin{scope}[every coordinate/.style={shift={(6,0)}}]


        \node at ([c]Rm) {$\mathbb{R}^m$};

        \filldraw[ultra thick, fill=BrickRed!50]
        ([c]O) -- ([c]v) -- ([c]$ (v)-2*(w) $) -- ([c]$ -2*(w) $) -- cycle;
        \node at ([c]Col) {$\underset{\dim=\operatorname{rank}(A)}{\operatorname{Col}(A)}$};


        \filldraw[ultra thick, fill=Blue!50]
        ([c]O) -- ([c]w) -- ([c]$ (w)-(v) $) -- ([c]$ -1*(v) $) -- cycle;
        \node at ([c]LNull) {\scriptsize$\underset{\dim=m-\operatorname{rank}(A)}{\operatorname{LNull}(A)}$};


        \draw[ultra thick, shorten >=1.5em, shorten <=1.5em, ->]
        (Rn) -- ([c]Rm) node[midway, above] {$A$};


        % \path[bend right, ultra thick, shorten >=1.5em,shorten <=1.5em]
        % ([c]Rm) edge["$A^\intercal$"'] (Rn);

        % \path[bend right, ultra thick, ->, shorten >=1.5em,shorten <=1.5em]
        % ([c]Rm) edge (Rn);

        \onslide<6->{
          \draw[ultra thick, shorten >=.25em, shorten <=.5em, ->]
          (xn) -- ([c]O) node[midway, above, sloped] {\tiny$A\vv*{x}{N}=\vv{O}$};
        }

        \onslide<3->{
          \node at ([c]b) {\textbullet};
          \node at ([c]b) [right] {$\vv{b}$};
        }

        \onslide<7->{
          \draw[ultra thick, shorten >=.5em, shorten <=.5em, ->]
          (xr) -- ([c]b) node[midway, above, sloped] {\tiny$A\vv*{x}{R}=\vv{b}$};
        }

        \onslide<3->{
          \draw[ultra thick, shorten >=.5em, shorten <=.5em, ->]
          (x) -- ([c]b) node[midway, below, sloped] {\tiny$A\vv{x}=\vv{b}$};
        }

        \pgfmathsetmacro{\mys}{.15}
        \coordinate (ort) at ([c]$ -\mys*(v) $);
        \coordinate (orb) at ([c]$ -\mys*(w) $);
        \coordinate (orm) at ([c]$ -\mys*(v)-\mys*(w) $);

        \draw[ultra thick] (ort) -- (orm) -- (orb);

      \end{scope}


      \pgfmathsetmacro{\mys}{.15}
      \coordinate (ort) at ([c]$ -\mys*(P) $);
      \coordinate (orb) at ([c]$ -\mys*(Q) $);
      \coordinate (orm) at ([c]$ -\mys*(P)-\mys*(Q) $);

      \draw[ultra thick] (ort) -- (orm) -- (orb);

    \end{tikzpicture}
  \]

\end{frame}


\begin{sagesilent}
  A = matrix([(1, -3, 3), (-5, 16, -17)])
  v1, v2 = A.row_space().basis()
  w, = A.right_kernel().basis()
  x = 2*v1+v2-3*w
  B = matrix.column([v1, v2, w])
  M = B.augment(x, subdivide=True)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Consider the matrix $A$ and the vector $\vv{x}$ given by
  \begin{align*}
    A &= \sage{A} & \vv{x} &= \sage{matrix.column(x)}
  \end{align*}
  \onslide<2->{Bases of $\Row(A)$ and $\Null(A)$ are given by}
  \begin{gather*}
  \begin{align*}
    \onslide<3->{\Row(A) &= \Span\sage{Set([v1, v2])}} &
    \onslide<4->{\Null(A) &= \Span\sage{Set([w])}}
  \end{align*}
\end{gather*}

  \onslide<5->{To compute $\vv{x}=\vv*{x}{R}+\vv*{x}{N}$, note that}
  \[
    \onslide<6->{\rref\sage{M}=\sage{M.rref()}}
  \]
  \onslide<7->{So $\vv*{x}{R}=2\cdot\sage{v1}+\sage{v2}$ and $\vv*{x}{N}=-3\cdot\sage{w}$.}

\end{frame}



\end{document}
