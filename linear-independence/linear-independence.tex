\documentclass{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{caption}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
\usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{arrows, calc, decorations.pathreplacing, matrix, positioning}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\myrows}{rows}
\DeclareMathOperator{\mycolumns}{columns}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Span}{Span}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\mybmat}[1]{
  \begin{bmatrix}
    #1
  \end{bmatrix}}




\title{Linear Independence}
\subtitle{Math 218}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\usepackage{currfile}
\usepackage{xstring}


% https://tex.stackexchange.com/questions/91691/beamer-set-mode-mid-presentation
\makeatletter
\newcommand\changemode[1]{%
  \gdef\beamer@currentmode{#1}}
\makeatother


\begin{document}

\IfSubStr*{\currfilename}{handout}{
  \changemode{handout}% options are handout, beamer, trans
}{}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \tableofcontents[sections={1-2}]
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={2-}]
  % \end{columns}
\end{frame}


\section{Geometric Motivation}
\subsection{Counting ``Directions''}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    What does $\Span\Set{\vv{v}}$ ``look like''?
  \end{block}

  \onslide<2->
  \begin{block}{Answer}
    $\Span\Set{\vv{v}}$ consists of all ``multiples'' of $\Set{\vv{v}}$.
    \[
      \begin{tikzpicture}[line join=round, line cap=round]
        \coordinate (O) at (0, 0);
        \coordinate (v) at (1/2, 1/4); 

        \onslide<12->{
          \draw[ultra thick, <->, teal] ($ -4*(v) $) -- ($ 4*(v) $) node[right] {$\Span\Set{\vv{v}}$};
        }

        \onslide<4>{
          \draw[ultra thick, ->, teal] (O) -- ($ 2*(v) $) node[below right] {$2\cdot\vv{v}$};
        }
        \onslide<4-11>{
          \draw[ultra thick, ->, teal] (O) -- ($ 2*(v) $);
        }

        \onslide<6>{
          \draw[ultra thick, ->, teal] (O) -- ($ 3*(v) $) node[below right] {$3\cdot\vv{v}$};
        }
        \onslide<6-11>{
          \draw[ultra thick, ->, teal] (O) -- ($ 3*(v) $);
        }


        \onslide<8>{
          \draw[ultra thick, ->, teal] (O) -- ($ -2*(v) $) node[below right] {$-2\cdot\vv{v}$};
        }
        \onslide<8-11>{
          \draw[ultra thick, ->, teal] (O) -- ($ -2*(v) $);
        }


        \onslide<10>{
          \draw[ultra thick, ->, teal] (O) -- ($ -3*(v) $) node[below right] {$-3\cdot\vv{v}$};
        }
        \onslide<10-11>{
          \draw[ultra thick, ->, teal] (O) -- ($ -3*(v) $);
        }
        
        \onslide<3->
        \draw[ultra thick, ->, blue] (O) -- (v) node[midway, above, sloped] {$\vv{v}$};
        
      \end{tikzpicture}
    \]
    \onslide<13->{Assuming $\vv{v}\neq\vv{O}$, $\Span\Set{\vv{v}}$ is the
      \emph{line containing} $\vv{v}$.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    What does $\Span\Set{\vv*{v}{1}, \vv*{v}{2}}$ ``look like''?
  \end{block}

  \onslide<2->
  \begin{block}{Answer}
    $\Span\Set{\vv*{v}{1}, \vv*{v}{2}}$ consists of all ``linear combinations'' of $\Set{\vv*{v}{1}, \vv*{v}{2}}$.
    \[
      \tdplotsetmaincoords{70}{110}
      \begin{tikzpicture}[baseline=3ex, line cap=round, line join=round, tdplot_main_coords]
        \onslide<8->{
          \draw[white] (0, -2, 0) -- (-3, 0, 0) node[midway, above, sloped, teal] {$\Span\Set{\vv*{v}{1}, \vv*{v}{2}}$};
          \filldraw[draw=teal, very thick, fill=teal!20,] (9/2,1,0) -- (0,-2,0) -- (-3, 0, 0) -- (3/2, 3, 0) -- cycle;
        }

        \onslide<3->{
          \draw[ultra thick, blue, ->] (0, 0, 0) -- (2, 0, 0) node[below] {$\vv*{v}{1}$};
          \draw[ultra thick, red, ->] (0, 0, 0) -- (0, 1, 0) node[below] {$\vv*{v}{2}$};
        }

        \onslide<4-8>{
          \draw[ultra thick, violet, ->] (0, 0, 0) -- (2, 1, 0);
        }
        \onslide<5-8>{
          \draw[ultra thick, violet, ->] (0, 0, 0) -- (-1, 1, 0);
        }
        \onslide<6-8>{
          \draw[ultra thick, violet, ->] (0, 0, 0) -- (-1, -1, 0);
        }
        \onslide<7-8>{
          \draw[ultra thick, violet, ->] (0, 0, 0) -- (-3/2, 1/4, 0);
        }
      \end{tikzpicture}
    \]
    \onslide<10->{Assuming $\vv*{v}{1}$ and $\vv*{v}{2}$ \emph{are not
        parallel}, $\Span\Set{\vv*{v}{1}, \vv*{v}{2}}$ is the \emph{plane
        containing $\vv*{v}{1}$ and $\vv*{v}{2}$}.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{$\Span\Set{\vv{v}}$}
    Defines a one-directional object (line) if $\vv{v}\neq\vv{O}$.
  \end{block}

  \pause
  \begin{block}{$\Span\Set{\vv*{v}{1}, \vv*{v}{2}}$}
    Defines a two-directional object (plane) if $\vv*{v}{1}$ and $\vv*{v}{2}$
    \emph{are not parallel}.
  \end{block}

  \pause
  \begin{block}{Question}
    How can we determine if
    $\Span\Set{\vv*{v}{1}, \vv*{v}{2},\dotsc, \vv*{v}{n}}$ defines an
    $n$-directional object?
  \end{block}

  \pause
  \begin{block}{Answer}
    $\Span\Set{\vv*{v}{1}, \vv*{v}{2},\dotsc, \vv*{v}{n}}$ defines an
    $n$-directional object if the list
    $\Set{\vv*{v}{1}, \vv*{v}{2},\dotsc, \vv*{v}{n}}$ is \emph{linearly
      independent}.
  \end{block}

\end{frame}


\begin{sagesilent}
  set_random_seed(897)
  A = random_matrix(ZZ, 3, 4, algorithm='echelonizable', rank=1)
  while 0 in A._list() or len(set(A.columns()+(-A).columns())) != 2*A.ncols(): A = random_matrix(ZZ, 3, 4, algorithm='echelonizable', rank=1)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $A$ given by
    \[
      A=\sage{A}
    \]
    \onslide<2->{Note that $\Col(A)\subset\mathbb{R}^{\sage{A.nrows()}}$.}
    \onslide<3->{ Each column is a multiple of the first column.}
    \[
      \begin{tikzpicture}[line join=round, line cap=round]
        \coordinate (O) at (0, 0);
        \coordinate (v) at (1/2, 1/4);


        \onslide<5-7>{
          \draw[ultra thick, ->, teal] (O) -- ($ 3*(v) $) node[below right] {$3\cdot\vv*{a}{1}$};
        }
        \onslide<6-7>{
          \draw[ultra thick, ->, teal] (O) -- ($ -4*(v) $) node[below right] {$-4\cdot\vv*{a}{1}$};
        }
        \onslide<7>{
          \draw[ultra thick, ->, teal] (O) -- ($ -2*(v) $) node[below right] {$-2\cdot\vv*{a}{1}$};
        }

        \onslide<8->{
          \draw[ultra thick, <->, teal] ($ -4*(v) $) -- ($ 4*(v) $) node[right] {$\Col(A)$};
        }

        \onslide<4->{
          \draw[ultra thick, ->, blue] (O) -- (v) node[above, sloped] {$\vv*{a}{1}$};
        }
        
      \end{tikzpicture}
    \]
    \onslide<9->{This illustrates that $\Col(A)=\Span\Set{\sage{A.columns()[0]}}$.}
  \end{example}
  

\end{frame}



\begin{sagesilent}
  a1 = vector([1, -8, 3, 2])
  a3 = vector([7, 3, -2, -9])
  a2 = 2*a1
  a4 = -a1+a3
  A = matrix.column([a1, a2, a3, a4])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $A$ given by
    \[
      A=\sage{A}
    \]
    \onslide<2->{Note that $\Col(A)\subset\mathbb{R}^{\sage{A.nrows()}}$.}
    \[
      \begin{tikzpicture}[line join=round, line cap=round]
        \coordinate (O) at (0, 0);
        \coordinate (a1) at (1, 1/2);
        \coordinate (a2) at (-1/2, 3/2);

        \onslide<4->{
          \draw[ultra thick, ->, teal] (O) -- ($ 2*(a1) $) node[below right] {$2\cdot\vv*{a}{1}$};
        }
        \onslide<5->{
          \draw[ultra thick, ->, blue] (O) -- (a2) node[above] {$\vv*{a}{3}$};
        }
        \onslide<6->{
          \draw[ultra thick, ->, teal] (O) -- ($ -1*(a1)+(a2) $) node[above] {$-\vv*{a}{1}+\vv*{a}{3}$};
        }

        \onslide<3->{
          \draw[ultra thick, ->, blue] (O) -- (a1) node[above, sloped] {$\vv*{a}{1}$};          
        }
        
      \end{tikzpicture}
    \]
    \onslide<7->{This means that $\Col(A)=\Span\Set{\sage{a1}, \sage{a3}}$.}
  \end{example}  

\end{frame}



\section{Background}
\subsection{Linear Combinations as Matrix Multiplication}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Let $\Set{\vv*{v}{1}, \vv*{v}{2}, \dotsc, \vv*{v}{n}}$ be a list of vectors
    in $\mathbb{R}^m$. 
    \onslide<2->{Every linear combination
      \[
        c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+\dotsb+c_n\cdot\vv*{v}{n}=\vv{b}
      \]
      is of the form $A\vv{c}=\vv{b}$ where}%
    \onslide<3->{
      \begin{align*}
        A &= \mybmat{\vv*{v}{1} & \vv*{v}{2} & \dotsb & \vv*{v}{n}} &
                                                                      \vv{c} &= \mybmat{c_1\\ c_2\\ \vdots\\ c_n} & 
                                                                                                                    \vv{b} &= \mybmat{b_1\\ b_2\\ \vdots\\ b_m}
      \end{align*}}%
    \onslide<4->{Note that $A$ is an $m\times n$ matrix.}
  \end{block}
\end{frame}


\begin{sagesilent}
  u1 = vector([1, 31])
  u2 = vector([0, -3])
  u3 = vector([7, -5])
  u4 = vector([-5, -3])
  v1 = matrix.column([1, 31])
  v2 = matrix.column([0, -3])
  v3 = matrix.column([7, -5])
  v4 = matrix.column([-5, -3])
  A = matrix.column([u1, u2, u3, u4])
  b = matrix.column([33,-11])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The linear combination 
    \[
      c_1\cdot\sage{v1}+c_2\cdot\sage{v2}+c_3\cdot\sage{v3}+c_4\cdot\sage{v4}=\sage{b}
    \]
    may be written as\pause
    \[
      \sage{A}\mybmat{c_1\\ c_2\\ c_3\\ c_4}=\sage{b}
    \]
  \end{example}
\end{frame}


\subsection{An Important Observation}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Let $\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{n}}$ be a list of vectors in
    $\mathbb{R}^m$. The equation
    \begin{equation}
      \label{eq:linind}
      c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+\dotsb+c_n\cdot\vv*{v}{n}=\vv{O}\tag{$\ast$}
    \end{equation}
    can always be solved by $c_1=c_2=\dotsb=c_n=0$.
    \pause This is called the \emph{trivial linear combination}.
  \end{block}

  \pause
  \begin{block}{Question}
    When is \eqref{eq:linind} solved by a \emph{nontrivial linear combination}?
  \end{block}
\end{frame}


\section{Definitions and Examples}
\subsection{Definitions}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A list of vectors $\Set{\vv*{v}{1}, \vv*{v}{2}, \dotsc, \vv*{v}{n}}$ in
    $\mathbb{R}^m$ is \emph{linearly independent} if the only solution to
    \[
      c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+\dotsb+c_n\cdot\vv*{v}{n}=\vv{O}
    \]
    is the trivial solution $c_1=c_2=\dotsb=c_n=0$. %
    \onslide<2->{The list $\Set{\vv*{v}{1}, \vv*{v}{2}, \dotsc, \vv*{v}{n}}$ is
      \emph{linearly dependent} if it is not linearly independent.}
  \end{definition}


  \begin{block}{\onslide<3->{Note}}
    \onslide<3->{The list $\Set{\vv*{v}{1}, \vv*{v}{2}, \dotsc, \vv*{v}{n}}$ is
      \emph{linearly dependent} if the equation
      \[
        c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+\dotsb+c_n\cdot\vv*{v}{n}=\vv{O}
      \]
      has a nontrivial solution.}
  \end{block}
\end{frame}


\subsection{Examples}

% \begin{sagesilent}
%   A = random_matrix(ZZ, 3, 4, algorithm='echelonizable', rank=3)
%   v1, v2, v3, v4 = [matrix.column(col) for col in A.columns()]
%   c1, c2, c3, c4 = A.right_kernel().matrix().list()
%   z = A*A.right_kernel().matrix()
% \end{sagesilent}
\begin{sagesilent}
  A = random_matrix(ZZ, 3, 4, algorithm='echelonizable', rank=2)
  v1, v2, v3, v4 = [matrix.column(col) for col in A.columns()]
  c1, c2, c3, c4 = A.change_ring(QQ).right_kernel(basis='pivot').basis()[0]
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Note that
    \[
      (\sage{c1})\sage{v1}
      +(\sage{c2})\sage{v2}
      +(\sage{c3})\sage{v3}
      +(\sage{c4})\sage{v4}
      =\sage{0*v1}
    \]
    \pause
    This means that columns of
    \[
      \sage{A}
    \]
    are linearly dependent.
  \end{example}
\end{frame}


\begin{sagesilent}
  set_random_seed(89)
  A = random_matrix(ZZ, 4, 3, algorithm='echelonizable', rank=3, upper_bound=4)
  v1, v2, v3 = [matrix.column(col) for col in A.columns()]
  z = zero_vector(A.nrows())
  M = A.augment(z, subdivide=True)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}
  
  \begin{example}
    Determine if the list $\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3}}$ is linearly
    independent where
    \begin{align*}
      \vv*{v}{1} &= \sage{vector(v1)} &
                                        \vv*{v}{2} &= \sage{vector(v2)} &
                                                                          \vv*{v}{3} &= \sage{vector(v3)} 
    \end{align*}
    \pause To determine if $\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3}}$ is linearly
    independent, we consider
    \[
      c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+c_3\cdot\vv*{v}{3}=\vv{O}
    \]\pause
    This gives the system 
    \[
      \sage{M}\pause\rightsquigarrow\sage{M.rref()}
    \]\pause
    Thus $c_1=c_2=c_3=0$. \pause Hence $\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3}}$
    is linearly independent.
  \end{example}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose that $\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3},\vv*{v}{4}}$ is linearly
    independent. Show that $\Set{\vv*{v}{1}-\vv*{v}{4}, \vv*{v}{2}-\vv*{v}{4},
      \vv*{v}{3}-\vv*{v}{4}}$ is linearly independent.
  \end{example}
  \pause
  \begin{block}{Solution}
    Suppose that 
    \[
      c_1\cdot(\vv*{v}{1}-\vv*{v}{4})
      +c_2\cdot(\vv*{v}{2}-\vv*{v}{4})
      +c_3\cdot(\vv*{v}{3}-\vv*{v}{4})
      =\vv{O}
    \]\pause
    Then
    \begin{equation}
      c_1\cdot\vv*{v}{1}
      +c_2\cdot\vv*{v}{2}
      +c_3\cdot\vv*{v}{3}
      +(-c_1-c_2-c_3)\cdot\vv*{v}{4}
      =\vv{O}\label{eq:mypf}\tag{$\ast$}
    \end{equation}\pause
    Since $\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3},\vv*{v}{4}}$ is linearly
    independent, each coefficient in \eqref{eq:mypf} must be zero. \pause In
    particular, $c_1=c_2=c_3=0$.
  \end{block}
\end{frame}


\section{The Linear Independence Test}
\subsection{Statement}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    The equation
    \[
      c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+\dotsb+c_k\cdot\vv*{v}{k}=\vv{O}
    \]
    is given by the augmented matrix
    \[
      \left[
        \begin{array}{cccc|c}
          \vv*{v}{1} & \vv*{v}{2} & \dotsb & \vv*{v}{k} & \vv{O}
        \end{array}\right]
    \]\pause
    So, $\Set{\vv*{v}{1}, \vv*{v}{2}, \dotsc,\vv*{v}{k}}$ is linearly
    independent if and only if
    \[
      A=\mybmat{\vv*{v}{1} & \vv*{v}{2} & \dotsb & \vv*{v}{k}}
    \]
    has \pause\emph{full column rank}.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[The Linear Independence Test]
    The list $\Set{\vv*{v}{1}, \vv*{v}{2}, \dotsc, \vv*{v}{k}}$ is linearly
    independent if and only if
    \[
      \mybmat{\vv*{v}{1} & \vv*{v}{2} & \dotsb & \vv*{v}{k}}
    \]
    has full column rank.
  \end{theorem}

\end{frame}


\subsection{Example}


\begin{sagesilent}
  A = random_matrix(ZZ, 4, 3, algorithm='echelonizable', rank=2)
  v1, v2, v3 = [matrix.column(col) for col in A.columns()]  
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Determine if the list
    \[
      \Set*{\sage{vector(v1)},\sage{vector(v2)},\sage{vector(v3)}}
    \]
    is linearly independent.
  \end{example}
  \pause
  \begin{block}{Solution}
    Note that
    \[
      \rref\sage{A}=\sage{A.rref()}
    \]\pause
    Since $\textnormal{rank}=2<\#\mycolumns$, the list is \emph{linearly
      dependent}.
  \end{block}
\end{frame}


\begin{sagesilent}
  A = random_matrix(ZZ, 4, 3, algorithm='echelonizable', rank=3)
  v1, v2, v3 = [matrix.column(col) for col in A.columns()]  
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Determine if the list
    \[
      \Set*{\sage{vector(v1)},\sage{vector(v2)},\sage{vector(v3)}}
    \]
    is linearly independent.
  \end{example}
  \pause
  \begin{block}{Solution}
    Note that
    \[
      \rref\sage{A}=\sage{A.rref()}
    \]\pause
    Since $\textnormal{rank}=3=\#\mycolumns$, the list is \emph{linearly
      independent}.
  \end{block}
\end{frame}


\begin{sagesilent}
  A = random_matrix(ZZ, 3, 4, algorithm='echelonizable', rank=3)
  v1, v2, v3, v4 = [matrix.column(col) for col in A.columns()]  
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Determine if the list
    \[
      \Set*{\sage{v1}, \sage{v2}, \sage{v3}, \sage{v4}}
    \]
    is linearly independent.
  \end{example}
  \pause
  \begin{block}{Solution}
    The matrix
    \[
      A=\sage{A}
    \]
    satisfies $\rank(A)\leq \pause 3$. \pause Since
    $\rank(A)\neq\#\mycolumns=4$, the list is not linearly independent.
  \end{block}
\end{frame}


\section{The ``Pivot Columns'' of a Matrix}
\subsection{Definition}

\begin{sagesilent}
  a1 = vector([3, -2, 1, 13])
  a2 = -3*a1
  a3 = vector([7, 2, -6, 0])
  A = matrix.column([a1, a2, a3])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{pivot columns} of a matrix $A$ are the columns of $A$ that
    correspond to pivot columns in $\rref(A)$.
  \end{definition}

  \pause
  \begin{example}
    Consider the calculation
    \[
      \rref\overset{A}{\sage{A}}=\sage{A.rref()}
    \]
    \pause The pivot columns of $A$ are $\sage{a1}$ and $\sage{a3}$.
  \end{example}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The pivot columns of a matrix are linearly independent.
  \end{theorem}

  \pause
  \begin{theorem}
    Let $\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{d}}$ be the pivot columns of $A$. Then
    \[
      \Col(A)=\Col(\begin{bmatrix}\vv*{v}{1} & \vv*{v}{2} & \cdots & \vv*{v}{d}\end{bmatrix})
    \]
    In particular, every column of $A$ is a linear combination of the pivot
    columns of $A$.
  \end{theorem}

\end{frame}


\subsection{Example}
\begin{sagesilent}
  R = matrix([(1, -3, 4, 0, 7), (0, 0, 0, 1, 6), (0, 0, 0, 0, 0)])
  set_random_seed(79147)
  A = random_matrix(ZZ, R.nrows(), algorithm='unimodular')*R
  a1, a2, a3, a4, a5 = A.columns()
  Ap = matrix.column([a1, a4])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the calculation
    \[
      \rref\sage{A}=\sage{A.rref()}
    \]
    \onslide<2->{This shows that}
    \[
      \onslide<2->{\Col\sage{A}=\Col\sage{Ap}}
    \]
    \onslide<3->{We also have}
    \begin{align*}
      \onslide<3->{\vv*{a}{2}} &\onslide<3->{=} \onslide<4->{-3\cdot\vv*{a}{1}} & \onslide<5->{\vv*{a}{3}} &\onslide<5->{=} \onslide<6->{4\cdot\vv*{a}{1}} & \onslide<7->{\vv*{a}{5}} &\onslide<7->{=} \onslide<8->{7\cdot\vv*{a}{1}+6\cdot\vv*{a}{4}}
    \end{align*}
  \end{example}

\end{frame}

\end{document}
