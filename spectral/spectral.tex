\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\usepackage{booktabs}

\title{The Spectral Theorem}
\subtitle{Math 218}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Motivation}
\subsection{Comparing Eigenspaces}
\begin{sagesilent}
  S = matrix([(1, 2), (2, -2)])
  l1, l2 = S.eigenvalues()
  set_random_seed(8401)
  A = random_matrix(ZZ, S.nrows(), algorithm='diagonalizable', eigenvalues=(l1, l2), dimensions=(1, 1))
  Id = identity_matrix(S.nrows())
  v1, = (A-l1*Id).change_ring(ZZ).right_kernel().basis()
  v2, = (A-l2*Id).change_ring(ZZ).right_kernel().basis()
  w1, = (S-l1*Id).change_ring(ZZ).right_kernel().basis()
  w2, = (S-l2*Id).change_ring(ZZ).right_kernel().basis()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myA}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{A}$};
      \onslide<7->{
        \node[overlay, above right= 1mm and 3mm of a] (text) {$A$ is \emph{not Hermitian}};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\myS}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{S}$};
      \onslide<6->{
        \node[overlay, above right= 1mm and 3mm of a] (text) {$S$ is \emph{Hermitian}};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\EA}{
    \begin{aligned}
      \mathcal{E}_A(\sage{l1}) &= \Span\Set{\sage{v1}} \\
      \mathcal{E}_A(\sage{l2}) &= \Span\Set{\sage{v2}}
    \end{aligned}
  }
  \newcommand{\ES}{
    \begin{aligned}
      \mathcal{E}_S(\sage{l1}) &= \Span\Set{\sage{w1}} \\
      \mathcal{E}_S(\sage{l2}) &= \Span\Set{\sage{w2}}
    \end{aligned}
  }
  \newcommand{\myEA}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , <->
      , scale=1/2
      ]

      \coordinate (v) at (2, -1);
      \coordinate (w) at (1, -1);

      \draw (-3, 0) -- (3, 0);
      \draw (0, -3) -- (0, 3);

      \draw[red] ($ -1.5*(v) $) -- ($ 1.5*(v) $);
      \draw[beamblue] ($ -3*(w) $) -- ($ 3*(w) $);
    \end{tikzpicture}
  }
  \newcommand{\myES}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , <->
      , scale=1/2
      ]

      \coordinate (O) at (0, 0);
      \coordinate (v) at (2, 1);
      \coordinate (w) at (1, -2);

      \draw (-3, 0) -- (3, 0);
      \draw (0, -3) -- (0, 3);

      \draw[red] ($ -1.5*(v) $) -- ($ 1.5*(v) $);
      \draw[beamblue] ($ -1.5*(w) $) -- ($ 1.5*(w) $);

      \pgfmathsetmacro{\myc}{0.2}
      \coordinate (uv) at ($ (O)!\myc!(v) $);
      \coordinate (uw) at ($ (O)!-\myc!(w) $);
      \onslide<5->{\draw[-] (uv) -- ($ (uv)+(uw) $) -- (uw);}
    \end{tikzpicture}
  }
  \newcommand{\mycap}[2]{\mybold{\textnormal{eigenspaces of $####1$ \emph{####2 orthogonal}}}}
  \begin{example}
    Consider the data
    \begin{gather*}
      \begin{align*}
        A &= \myA & \EA && S &= \myS & \ES
      \end{align*}
    \end{gather*}
    \onslide<2->{The eigenvalues are the same, but the eigenspaces are different.}
    \begin{align*}
      \onslide<3->{\overset{\mycap{A}{not}}{\myEA}} && \onslide<4->{\overset{\mycap{S}{are}}{\myES}}
    \end{align*}
  \end{example}

\end{frame}

\begin{sagesilent}
  set_random_seed(789)
  S = GaussianIntegers(names='i')
  _, i = S.gens()
  A = random_matrix(ZZ, 2)
  A = A.T*A
  B = random_matrix(ZZ, 3)
  B = B.T*B-B*B.T
  C = matrix([(1, 1-3*i, -i), (1+3*i, 2, 4), (i, 4, 0)])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Background}
    The \emph{spectral theorem} describes eigenspaces of  Hermitian matrices.
  \end{block}

  \onslide<2->
  \begin{definition}
    We say that $S$ is \emph{Hermitian} if $S^\ast=S$.
  \end{definition}

  \onslide<3->
  \begin{example}
    Each of the matrices
    \begin{align*}
      \overset{\onslide<4->{\textnormal{real-symmetric}}}{\sage{A}} && \overset{\onslide<5->{\textnormal{real-symmetric}}}{\sage{B}} && \overset{\onslide<6->{\textnormal{Hermitian}}}{\sage{C}}
    \end{align*}
    is Hermitian.
  \end{example}

\end{frame}


\section{The Spectral Theorem}
\subsection{Part I}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[The Spectral Theorem, Part I]
    The eigenvalues of a Hermitian matrix $S$ are real.
  \end{theorem}
  \onslide<2->
  \begin{proof}
    Choose a unit vector $\bv{v}\in\mathcal{E}_S(\lambda)$.
    \onslide<3->{Then}
    \begin{align*}
      \onslide<3->{\lambda}
      &\onslide<3->{=} \onslide<4->{\lambda\cdot\norm{\bv{v}}^2}        & &\onslide<8->{=} \onslide<9->{\inner{S\bv{v}, \bv{v}}} \\
      &\onslide<4->{=} \onslide<5->{\lambda\cdot\inner{\bv{v}, \bv{v}}} & &\onslide<9->{=} \onslide<10->{\inner{\lambda\cdot\bv{v}, \bv{v}}} \\
      &\onslide<5->{=} \onslide<6->{\inner{\bv{v}, \lambda\cdot\bv{v}}} & &\onslide<10->{=} \onslide<11->{\overline{\lambda}\cdot\inner{\bv{v}, \bv{v}}} \\
      &\onslide<6->{=} \onslide<7->{\inner{\bv{v}, S\bv{v}}}            & &\onslide<11->{=} \onslide<12->{\overline{\lambda}\cdot\norm{\bv{v}}^2} \\
      &\onslide<7->{=} \onslide<8->{\inner{S^\ast\bv{v}, \bv{v}}}       & &\onslide<12->{=} \onslide<13->{\overline{\lambda}}
    \end{align*}
    \onslide<14->{So, $\lambda=\overline{\lambda}$ which means
      $\lambda\in\mathbb{R}$.\qedhere}
  \end{proof}

\end{frame}




\begin{sagesilent}
  set_random_seed(789)
  S = GaussianIntegers(names='i')
  _, i = S.gens()
  A = random_matrix(ZZ, 2)
  A = A.T*A
  B = random_matrix(ZZ, 3)
  B = B.T*B-B*B.T
  C = matrix([(1, 1-3*i, -i), (1+3*i, 2, 4), (i, 4, 0)])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The eigenvalues of
    \begin{gather*}
      \begin{align*}
        \overset{\onslide<2->{\textnormal{real-symmetric}}}{\sage{A}} && \overset{\onslide<3->{\textnormal{real-symmetric}}}{\sage{B}} && \overset{\onslide<4->{\textnormal{Hermitian}}}{\sage{C}}
      \end{align*}
    \end{gather*}
    are all real.
  \end{example}

\end{frame}



\subsection{Part II}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[The Spectral Theorem, Part II]
    The eigenspaces of a Hermitian $S$ are orthogonal to one another.
  \end{theorem}
  \onslide<2->
  \begin{proof}
    Consider $\bv{v}_1\in\mathcal{E}_S(\lambda_1)$ and
    $\bv{v}_2\in\mathcal{E}_S(\lambda_2)$ where $\lambda_1\neq\lambda_2.$
    \onslide<3->{Then}
    \begin{gather*}
      \begin{align*}
        \onslide<3->{\inner{\bv{v}_1, \bv{v}_2}}
        &\onslide<3->{=} \onslide<4->{\oldfrac{\lambda_1-\lambda_2}{\lambda_1-\lambda_2}\cdot\inner{\bv{v}_1, \bv{v}_2}}                                          &&\onslide<8->{=} \onslide<9->{\oldfrac{1}{\lambda_1-\lambda_2}\cdot\Set{\inner{\lambda_1\cdot\bv{v}_1, \bv{v}_2}-\inner{\bv{v}_1, S\bv{v}_2}}} \\
        &\onslide<4->{=} \onslide<5->{\oldfrac{1}{\lambda_1-\lambda_2}\cdot\inner{\bv{v}_1, (\lambda_1-\lambda_2)\cdot\bv{v}_2}}                                  &&\onslide<9->{=}  \onslide<10->{\oldfrac{1}{\lambda_1-\lambda_2}\cdot\Set{\inner{S\bv{v}_1, \bv{v}_2}-\inner{\bv{v}_1, S\bv{v}_2}}} \\
        &\onslide<5->{=} \onslide<6->{\oldfrac{1}{\lambda_1-\lambda_2}\cdot\inner{\bv{v}_1, \lambda_1\cdot\bv{v}_2-\lambda_2\cdot\bv{v}_2}}                       &&\onslide<10->{=} \onslide<11->{\oldfrac{1}{\lambda_1-\lambda_2}\cdot\Set{\inner{\bv{v}_1, S^\ast\bv{v}_2}-\inner{\bv{v}_1, S\bv{v}_2}}} \\
        &\onslide<6->{=} \onslide<7->{\oldfrac{1}{\lambda_1-\lambda_2}\cdot\Set{\lambda_1\cdot\inner{\bv{v}_1, \bv{v}_2}-\inner{\bv{v}_1, S\bv{v}_2}}}            &&\onslide<11->{=} \onslide<12->{\oldfrac{1}{\lambda_1-\lambda_2}\cdot\Set{\inner{\bv{v}_1, S\bv{v}_2}-\inner{\bv{v}_1, S\bv{v}_2}}} \\
        &\onslide<7->{=} \onslide<8->{\oldfrac{1}{\lambda_1-\lambda_2}\cdot\Set{\inner{\overline{\lambda_1}\cdot\bv{v}_1, \bv{v}_2}-\inner{\bv{v}_1, S\bv{v}_2}}} &&\onslide<12->{=} \onslide<13->{0}
      \end{align*}
    \end{gather*}
    \onslide<14->{This shows that $\bv{v}_1\perp\bv{v}_2$. } \onslide<15->{Hence
      $\mathcal{E}_S(\lambda_1)\perp\mathcal{E}_S(\lambda_2)$. \qedhere}
  \end{proof}

\end{frame}


\begin{sagesilent}
  A = matrix([(4, -1, -1), (-1, 4, 1), (-1, 1, 4)])
  l1, l2, l2 = A.eigenvalues()
  n = A.ncols()
  Id = identity_matrix(n)
  v1, = matrix.column(A-l1*Id).right_kernel().basis()
  w1, w2 = matrix.column(A-l2*Id).right_kernel().basis()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myES}{
    \begin{aligned}
      \mathcal{E}_S(\sage{l1}) &= \Span\Set{{\color{red}\sage{v1}}} \\
      \mathcal{E}_S(\sage{l2}) &= \Span\Set{{\color{beamgreen}\sage{w1}}, {\color{beamblue}\sage{w2}}}
    \end{aligned}
  }
  \newcommand{\myPerp}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\mathcal{E}_S(\sage{l1})\perp\mathcal{E}_S(\sage{l2})$};
      \onslide<7->{
        \node[overlay, below right= 2mm and 3mm of a] (text) {expected because $S$ is \emph{real symmetric}};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{example}
    Consider the data
    \begin{align*}
      S &= \sage{A} & \myES
    \end{align*}
    \onslide<2->{The computations}
    \begin{align*}
      \onslide<2->{{\color{red}\sage{v1}}\cdot{\color{beamgreen}\sage{w1}}} &\onslide<2->{=} \onslide<3->{\sage{v1*w1}} & \onslide<4->{{\color{red}\sage{v1}}\cdot{\color{beamblue}\sage{w2}}} &\onslide<4->{=} \onslide<5->{\sage{v1*w2}}
    \end{align*}
    \onslide<6->{verify that $\myPerp$.}
  \end{example}

\end{frame}


\subsection{Part III}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[The Spectral Theorem, Part III]
    Every Hermitian matrix is diagonalizable.
  \end{theorem}

  \pause
  \begin{proof}
    Hard.
  \end{proof}

\end{frame}


\begin{sagesilent}
  set_random_seed(789)
  S = GaussianIntegers(names='i')
  _, i = S.gens()
  A = random_matrix(ZZ, 2)
  A = A.T*A
  B = random_matrix(ZZ, 3)
  B = B.T*B-B*B.T
  C = matrix([(1, 1-3*i, -i), (1+3*i, 2, 4), (i, 4, 0)])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Each of the matrices
    \begin{align*}
      \overset{\onslide<2->{\textnormal{real-symmetric}}}{\sage{A}} && \overset{\onslide<3->{\textnormal{real-symmetric}}}{\sage{B}} && \overset{\onslide<4->{\textnormal{Hermitian}}}{\sage{C}}
    \end{align*}
    is diagonalizable.
  \end{example}
\end{frame}


\section{Spectral Factorizations}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myS}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$S$};
      \onslide<2->{
        \node[overlay, below left= 2mm and 3mm of a] (text) {\emph{Hermitian} $(S^\ast=S)$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myQ}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {$Q$};
      \onslide<3->{
        \node[overlay, below left= 6mm and 3mm of a] (text) {\emph{unitary} $(Q^\ast=Q^{-1})$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myD}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {$D$};
      \onslide<4->{
        \node[overlay, below right= 2mm and 3mm of a] (text) {\emph{real diagonal}};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{theorem}[The Spectral Theorem]
    $S$ is Hermitian if and only if $S$ has a ``spectral factorization''
    \[
      \myS = \myQ \myD Q^\ast
    \]
  \end{theorem}

\end{frame}


\subsection{Factorization Algorithm}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Spectral Factorization Algorithm}
    The following steps produce a spectral factorization $S=QDQ^\ast$.
    \begin{description}[\quad Step 1]
    \item<2->[Step 1] Find bases of each $\mathcal{E}_S(\lambda)$.
    \item<3->[Step 2] Apply Gram-Schmidt to each basis of
      $\mathcal{E}_S(\lambda)$.
    \item<4->[Step 3] Put the orthonormal bases of $\mathcal{E}_S(\lambda)$ in the columns
      of $Q$.
    \item<5->[Step 4] Put the eigenvalues of $S$ on the diagonal of $D$.
    \end{description}
  \end{block}

\end{frame}


\begin{sagesilent}
  A = matrix([(4, -1, -1), (-1, 4, 1), (-1, 1, 4)])
  l1, l2, l2 = A.eigenvalues()
  n = A.ncols()
  Id = identity_matrix(n)
  v1, = matrix.column(A-l1*Id).right_kernel().basis()
  w1, w2 = matrix.column(A-l2*Id).right_kernel().basis()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myES}{
    \begin{aligned}
      \mathcal{E}_S(\sage{l2}) &= \Span\Set{{\color<5->{red}\sage{w1}}, \sage{w2}} \\
      \mathcal{E}_S(\sage{l1}) &= \Span\Set{{\color<13->{beamgreen}\sage{v1}}}
    \end{aligned}
  }
  \newcommand{\myQ}{
    \left[
      \begin{array}{crr}
        \onslide<6->{{\color{red}\frac{1}{\sqrt{2}}}} & \onslide<10->{{\color{beamblue}\frac{1}{\sqrt{6}}}}  & \onslide<14->{{\color{beamgreen}\frac{1}{\sqrt{3}}}}\\
        \onslide<6->{{\color{red}0}}                  & \onslide<10->{{\color{beamblue}\frac{2}{\sqrt{6}}}}  & \onslide<14->{{\color{beamgreen}\frac{-1}{\sqrt{3}}}}\\
        \onslide<6->{{\color{red}\frac{1}{\sqrt{2}}}} & \onslide<10->{{\color{beamblue}\frac{-1}{\sqrt{6}}}} & \onslide<14->{{\color{beamgreen}\frac{-1}{\sqrt{3}}}}
      \end{array}
    \right]
  }
  \newcommand{\myD}{
    \left[
      \begin{array}{rrr}
        \onslide<7->{{\color{red}3}} &                                    & \\
                                     & \onslide<11->{{\color{beamblue}3}} & \\
                                     &                                    & \onslide<15->{{\color{beamgreen}6}}
      \end{array}
    \right]
  }
  \newcommand{\myQa}{
    \left[
      \begin{array}{crr}
        \onslide<8->{{\color{red}\frac{1}{\sqrt{2}}}}        & \onslide<8->{{\color{red}0}}                          & \onslide<8->{{\color{red}\frac{1}{\sqrt{2}}}} \\
        \onslide<12->{{\color{beamblue}\frac{1}{\sqrt{6}}}}  & \onslide<12->{{\color{beamblue}\frac{2}{\sqrt{6}}}}   & \onslide<12->{{\color{beamblue}\frac{-1}{\sqrt{6}}}} \\
        \onslide<16->{{\color{beamgreen}\frac{1}{\sqrt{3}}}} & \onslide<16->{{\color{beamgreen}\frac{-1}{\sqrt{3}}}} & \onslide<16->{{\color{beamgreen}\frac{-1}{\sqrt{3}}}}
      \end{array}
    \right]
  }
  \begin{example}
    Consider the data
    \begin{align*}
      S &= \sage{A} & \myES
    \end{align*}
    \onslide<2->{Applying Gram-Schmidt to the given basis of
      $\mathcal{E}_S(\sage{l2})$ gives}
    \[
      \onslide<2->{\sage{w2}-\oldfrac{\sage{w1}\cdot\sage{w2}}{\sage{w1}\cdot\sage{w1}}\sage{w1} =}
      \onslide<3->{\sage{1/2}\cdot{\color<9->{beamblue}\sage{2*(w2-(w1*w2)/(w1*w1) * w1)}}}
    \]
    \onslide<4->{Our spectral factorization is thus}
    \begin{gather*}
      \onslide<4->{
        \overset{S}{\sage{A}}
        =
        \overset{Q}{\myQ}
        \overset{D}{\myD}
        \overset{Q^\ast}{\myQa}
      }
    \end{gather*}
  \end{example}

\end{frame}


\begin{sagesilent}
  A = matrix([(3, 1-I), (1+I, 2)])
  l1, l2 = A.eigenvalues()
  n = A.nrows()
  Id = identity_matrix(n)
  v1, = (A-l1*Id).right_kernel().basis()
  v2, = (A-l2*Id).right_kernel().basis()
  v2 = 2*v2
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myEQ}{
    \begin{aligned}
      \mathcal{E}_S(\sage{l1}) &= \Span\Set{{\color<3->{red}\sage{v1}}}      \\
      \mathcal{E}_S(\sage{l2}) &= \Span\Set{{\color<7->{beamblue}\sage{v2}}}
    \end{aligned}
  }
  \newcommand{\myQ}{
    \left[
      \begin{array}{cc}
        \onslide<4->{{\color{red}\frac{1}{\sqrt{3}}}}      & \onslide<8->{{\color{beamblue}\frac{2}{\sqrt{6}}}}     \\
        \onslide<4->{{\color{red}\frac{(-i-1)}{\sqrt{3}}}} & \onslide<8->{{\color{beamblue}\frac{(i+1)}{\sqrt{6}}}}
      \end{array}
    \right]
  }
  \newcommand{\myD}{
    \left[
      \begin{array}{cc}
        \onslide<5->{{\color{red}1}} &                                   \\
                                     & \onslide<9->{{\color{beamblue}4}}
      \end{array}
    \right]
  }
  \newcommand{\myQa}{
    \left[
      \begin{array}{cc}
        \onslide<6->{{\color{red}\frac{1}{\sqrt{3}}}}       & \onslide<6->{{\color{red}\frac{(i-1)}{\sqrt{3}}}}       \\
        \onslide<10->{{\color{beamblue}\frac{2}{\sqrt{6}}}} & \onslide<10->{{\color{beamblue}\frac{(-i+1)}{\sqrt{6}}}}
      \end{array}
    \right]
  }
  \begin{example}
    Consider the data
    \begin{align*}
      S &= \sage{A} & \myEQ
    \end{align*}
    \onslide<2->{This gives a spectral factorization}
    \begin{gather*}
      \onslide<2->{
        \overset{S}{\sage{A}}
        =
        \overset{Q}{\myQ}
        \overset{D}{\myD}
        \overset{Q^\ast}{\myQa}
      }
    \end{gather*}
  \end{example}


\end{frame}


\end{document}
