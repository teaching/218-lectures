\documentclass[usenames,dvipsnames]{beamer}

\usepackage{mathtools} % loads amsmath
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{blkarray, bigstrut}
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{multicol}
\usepackage{hyperref}
\hypersetup{colorlinks, linkcolor=., urlcolor=blue}
\usepackage{sagetex}
\usepackage{siunitx}
\newcommand{\mydollars}[1]{\SI[round-precision=2,round-mode=places,round-integer-to-decimal]{#1}[\$]{}}
\DeclareSIUnit{\mph}{mph}
\usepackage{xparse}
\usepackage{xfrac}
% \usepackage{showframe}          % for testing
\usepackage{tikz}
\usetikzlibrary{
  , arrows
  , automata
  , calc
  , cd
  , decorations
  , decorations.pathmorphing
  , decorations.pathreplacing
  , fit
  , matrix
  , positioning
  , shapes
  , shapes.geometric
}
\usepackage{tikz-3dplot}
\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\text{\ding{51}}}%
\newcommand{\xmark}{\text{\ding{55}}}%



\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\boxpivot}[1]{
  \filldraw[draw=blue, thick, fill=blue!20, fill opacity=.25] (m-#1.north east) -- (m-#1.north west) -- (m-#1.south west) -- (m-#1.south east) -- cycle;
}
\newcommand{\boxcell}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-#2-#3.north east) -- (#1-#2-#3.north west) -- (#1-#2-#3.south west) -- (#1-#2-#3.south east) -- cycle;
}
\newcommand{\boxrow}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-#2-1.north west) -- (#1-#2-1.south west) -- (#1-#2-#3.south east) -- (#1-#2-#3.north east) -- cycle;
}
\newcommand{\boxcol}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-1-#2.north west) -- (#1-#3-#2.south west) -- (#1-#3-#2.south east) -- (#1-1-#2.north east) -- cycle;
}



\ExplSyntaxOn
\NewDocumentCommand{\gcenter}{m}
{
  \begin{center}
    \seq_set_split:Nnn \l_tmpa_seq { \\ } { #1 }
    \seq_map_inline:Nn \l_tmpa_seq
    {
      \seq_set_split:Nnn \l_tmpb_seq { & } { ##1 }
      \seq_use:Nn \l_tmpb_seq { \hfil }
      \\
    }
  \end{center}
}
\ExplSyntaxOff

\newcommand{\mybold}[1]{{\usebeamercolor[fg]{example text}{#1}}}
\newcommand{\myotherbold}[1]{{\usebeamercolor[fg]{title}{#1}}}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}

\setbeamertemplate{caption}{\raggedright\insertcaption\par}


\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\RR}{\mathbb{R}}

\let\oldfrac\frac% Store \frac
\renewcommand{\frac}[2]{%
  \mathchoice
  {\oldfrac{#1}{#2}}% display style
  {\sfrac{#1}{#2}}% text style
  {\sfrac{#1}{#2}}% script style
  {\sfrac{#1}{#2}}% script-scr#ipt style
}

\theoremstyle{definition}
\newtheorem{algorithm}{Algorithm}


\title{Elementary Matrices}
\subtitle{Math 218}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}


\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}

\usepackage{currfile}
\usepackage{xstring}


% https://tex.stackexchange.com/questions/91691/beamer-set-mode-mid-presentation
\makeatletter
\newcommand\changemode[1]{%
  \gdef\beamer@currentmode{#1}}
\makeatother


\begin{document}

\IfSubStr*{\currfilename}{handout}{
  \changemode{handout}% options are handout, beamer, trans
}{}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1-3}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={4-}]
  % \end{columns}
\end{frame}


\section{Motivation}
\subsection{What We Know}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    The reduced row echelon form $\rref(A)$ of a matrix $A$ allows us to analyze
    the matrix.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Adjectives Describing Matrices}
    Given $\rref(A)$, we define
    \begin{description}[<+->]
    \item[$\rank(A)$] as the number of pivot columns in $\rref(A)$
    \item[$\nullity(A)$] as the number of nonpivot columns in $\rref(A)$
    \end{description}
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Adjectives Describing Linear Systems}
    A linear system $[A\mid\vv{b}]$ is
    \begin{description}[<+->]
    \item[consistent] if $\rref[A\mid\vv{b}]$ has no pivot in the augmented column
    \item[inconsistent] if $\rref[A\mid\vv{b}]$ has a pivot in the augmented column
    \end{description}
    \onslide<+->{A variable in a consistent system is}
    \begin{description}[<+->]
    \item[free] if it corresponds to a nonpivot column in $\rref[A\mid\vv{b}]$
    \item[dependent] if it corresponds to a pivot column in $\rref[A\mid\vv{b}]$
    \end{description}
  \end{block}

\end{frame}




\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Solving Linear Systems}
    To solve any consistent linear system $[A\mid\vv{b}]$, use
    $\rref[A\mid\vv{b}]$ to write the dependent variables in terms of the free
    variables.
  \end{block}

\end{frame}


\begin{sagesilent}
  set_random_seed(803)
  A = random_matrix(ZZ, 4, 7, algorithm='echelonizable', rank=4)
  while A.pivots() != (0, 2, 3, 5): A = random_matrix(ZZ, 4, 7, algorithm='echelonizable', rank=4)
  b = A.columns()[-1]
  A = matrix.column(A.columns()[:-1])
  M = A.augment(b, subdivide=True)
  var('x1 x2 x3 x4 x5 x6')
  vx = matrix.column([x1, x2, x3, x4, x5, x6])
  vxs = matrix.column([3+4*x2-5*x5, x2, -1-5*x5, 1-4*x5, x5, 1])
  vxp = matrix.column([3, 0, -1, 1, 0, 1])
  vx2 = matrix.column([4, 1, 0, 0, 0, 0])
  vx5 = matrix.column([-5, 0, -5, -4, 1, 0])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  The solutions to
  \[
    \sage{M.rref()}
  \]
  are given by
  \[
    \vv{x}
    = \pause\sage{vx}
    = \pause\sage{vxs}
    = \pause\sage{vxp}+x_2\sage{vx2}+x_5\sage{vx5}
  \]

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    The Gau\ss-Jordan algorithm uses \emph{elementary row operations} to compute
    $\rref(A)$.
  \end{block}

\end{frame}


\begin{sagesilent}
  set_random_seed(8432057)
  A = random_matrix(ZZ, 3, 4, algorithm='echelonizable', rank=2)
  E1 = elementary_matrix(3, row1=0, row2=1)
  E2 = elementary_matrix(3, row1=0, scale=-1/5)
  E3 = elementary_matrix(3, row1=2, row2=0, scale=1)
  E4 = elementary_matrix(3, row1=1, scale=-1/4)
  E5 = elementary_matrix(3, row1=0, row2=1, scale=6/5)
  E6 = elementary_matrix(3, row1=2, row2=1, scale=21/5)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myStepA}{\scriptsize
    \begin{array}{rcrcr}
      R_1 &+& (\frac{6}{5})\cdot R_2 &\to& R_1 \\
      R_3 &+& (\frac{21}{5})\cdot R_2 &\to& R_3
    \end{array}
  }
  \newcommand{\mysage}[1]{\small\sage{####1}}
  \begin{align*}
    \mysage{A}
    &\onslide<2->{\xrightarrow{
      \onslide<3->{R_1\leftrightarrow R_2}
      }}
      \onslide<4->{\mysage{E1*A}} \\
    &\onslide<5->{\xrightarrow{
      \onslide<6->{(-\frac{1}{5})\cdot R_1\to R_1}
      }}
      \onslide<7->{\mysage{E2*E1*A}} \\
    &\onslide<8->{\xrightarrow{
      \onslide<9->{R_3+R_1\to R_3}
      }}
      \onslide<10->{\mysage{E3*E2*E1*A}} \\
    &\onslide<11->{\xrightarrow{
      \onslide<12->{(-\frac{1}{4})\cdot R_2\to R_2}
      }}
      \onslide<13->{\mysage{E4*E3*E2*E1*A}} \\
    &\onslide<14->{\xrightarrow{
      \onslide<15->{\myStepA}
      }}
      \onslide<16->{\mysage{E6*E5*E4*E3*E2*E1*A}}
  \end{align*}

\end{frame}



\subsection{What We Want}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question 1}
    How can we instruct a computer to conduct an elementary row operation?
  \end{block}

  \pause
  \begin{block}{Question 2}
    Can we find a matrix $E$ satisfying $EA=\rref(A)$? \pause The equation
    \[
      E[A\mid\vv{b}]
      = [EA\mid E\vv{b}]
      = [\rref(A)\mid E\vv{b}]
    \]
    could then help us solve systems.
  \end{block}

\end{frame}



\section{Elementary Matrices}
\subsection{Definition}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    There are three elementary row operations.
    \begin{description}[<+(1)->][Row Switching]
    \item[Row Switching] $R_i\leftrightarrow R_j$
    \item[Row Scaling] $c\cdot R_i\to R_i$
    \item[Row Addition] $R_i+c\cdot R_j\to R_i$
    \end{description}
  \end{block}

  \pause
  \begin{definition}
    An \emph{$n\times n$ elementary matrix} is a matrix obtained by performing
    exactly one elementary row operation on $I_n$.
  \end{definition}
\end{frame}


\begin{sagesilent}
  I2 = identity_matrix(2)
  I3 = identity_matrix(3)
  I4 = identity_matrix(4)
  E2 = elementary_matrix(2, row1=1, scale=-7)
  E3 = elementary_matrix(3, row1=1, row2=0, scale=-1/3)
  E4 = elementary_matrix(4, row1=1, row2=3)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    \begin{align*}
      \onslide<1->{\sage{I2} &\xrightarrow{-7\cdot R_2\to R_2}}                    \onslide<2->{\underset{[-7\cdot R_2\to R_2]}{\sage{E2}}} \\
      \onslide<3->{\sage{I3} &\xrightarrow{R_2-(\frac{1}{3})\cdot R_1\to R_2}} \onslide<4->{\underset{[R_2-(\frac{1}{3})\cdot R_1\to R_2]}{\sage{E3}}} \\
      \onslide<5->{\sage{I4} &\xrightarrow{R_2\leftrightarrow R_4}}                \onslide<6->{\underset{[R_2\leftrightarrow R_4]}{\sage{E4}}}
    \end{align*}
  \end{example}
\end{frame}

\subsection{Row Operations}


\begin{sagesilent}
  set_random_seed(45802)
  E = elementary_matrix(3, row1=2, row2=0, scale=-4)
  A = random_matrix(ZZ, 3)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Multiplication by an elementary matrix on the left of a matrix results in
    performing the corresponding elementary row-operation.
  \end{theorem}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrices
    \begin{align*}
      E &= \underset{[R_3-4\cdot R_1\to R_3]}{\sage{E}} & A &= \sage{A}
    \end{align*}
    \pause
    Then
    \[
      EA=\sage{E}\sage{A}=\sage{E*A}
    \]
  \end{example}
\end{frame}



\subsection{$EA=R$ Factorizations}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Idea}
    To compute $\rref(A)$ we perform elementary row operations.
    \[
      \onslide<2->{A}
      \onslide<3->{\xrightarrow{\operatorname{op}_1} A_1}
      \onslide<4->{\xrightarrow{\operatorname{op}_2} A_2}
      \onslide<5->{\xrightarrow{\operatorname{op}_3} A_3}
      \onslide<6->{\to\dotsb}
      \onslide<7->{\xrightarrow{\operatorname{op}_r} \rref(A)}
    \]
    \onslide<8->{Each operation corresponds to multiplication on the left by an
      elementary matrix.}
    \[
      \onslide<14->{E_r}\onslide<13->{\dotsb} \onslide<12->{E_3}\onslide<11->{E_2}\onslide<10->{E_1}\onslide<9->{A}\onslide<15->{=\rref(A)}
    \]
    \onslide<16->{By defining $E=E_r\dotsb E_3E_2E_1$, we obtain $EA=\rref(A)$.}
  \end{block}
\end{frame}




\begin{sagesilent}
  A = matrix.column([(1, 4, 0), (2, 9, 1)])
  E1 = elementary_matrix(3, row1=1, row2=0, scale=-4)
  E2 = elementary_matrix(3, row1=0, row2=1, scale=-2)
  E3 = elementary_matrix(3, row1=2, row2=1, scale=-1)
  E = E3*E2*E1
  var('b1, b2, b3')
  b = vector([b1, b2, b3])
  M = A.change_ring(SR).augment(b, subdivide=True)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the row reductions
    \newcommand{\myStepA}{\scriptsize
      \begin{array}{rcrcr}
        R_1 &-& 2\cdot R_2 &\to& R_1 \\
        R_3 &-& R_2 &\to& R_3
      \end{array}
    }
    \[
      \onslide<1->{\underset{A}{\sage{A}}}
      \onslide<2->{\xrightarrow{
          \onslide<3->{R_2-4\cdot R_1\to R_2}
        }}
      \onslide<4->{\sage{E1*A}}
      \onslide<5->{\xrightarrow{
          \onslide<6->{\myStepA}
        }}
      \onslide<7->{\underset{\rref(A)}{\sage{E3*E2*E1*A}}}
    \]
    \onslide<8->{These operations define elementary matrices}
    \begin{align*}
      \onslide<9->{E_1 &=} \onslide<10->{\sage{E1}} & \onslide<11->{E_2 &=} \onslide<12->{\sage{E2}} & \onslide<13->{E_3 &=} \onslide<14->{\sage{E3}}
    \end{align*}
    \onslide<15>{This gives $EA=\rref(A)$ where $E=E_3E_2E_1$.}
  \end{example}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[continued]
    We now have $EA=\rref(A)$ where
    \begin{align*}
      E &= \sage{E} & A &= \sage{A}
    \end{align*}\pause
    To determine the $\vv{b}=\sage{b}$ that make $A\vv{x}=\vv{b}$ consistent, we
    compute
    \[
      E[A\mid\vv{b}]
      = [EA\mid E\vv{b}]
      = \sage{E*M}
    \]\pause
    The vectors $\vv{b}=\sage{b}$ that make $A\vv{x}=\vv{b}$ consistent are the
    vectors that satisfy \pause $\sage{E[-1]*b}=0$. \pause These are the vectors orthogonal to
    \pause $\sage{E[-1]}$.
  \end{example}

\end{frame}



\section{Inverting Elementary Matrices}
\subsection{Reversing Elementary Row Operations}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Important}
    Every elementary row operation is reversible.
  \end{block}

\end{frame}


\begin{sagesilent}
  set_random_seed(2138094)
  A = random_matrix(ZZ, 4, 2)
  E = elementary_matrix(A.nrows(), row1=1, row2=2)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[Reversing Row Switches]
    \[
      \sage{A}
      \onslide<1->{\xrightarrow{
          \onslide<1->{R_2\leftrightarrow R_3}
        }}
      \onslide<1->{\sage{E*A}}
      \onslide<2->{\xrightarrow{
          \onslide<3->{R_2\leftrightarrow R_3}
        }}
      \onslide<2->{\sage{E*E*A}}
    \]
  \end{example}

\end{frame}



\begin{sagesilent}
  E = elementary_matrix(A.nrows(), row1=1, scale=-22)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[Reversing Row Scaling]
    \[
      \sage{A}
      \onslide<1->{\xrightarrow{
          \onslide<1->{-22\cdot R_2\to R_2}
        }}
      \onslide<1->{\sage{E*A}}
      \onslide<2->{\xrightarrow{
          \onslide<3->{(-\frac{1}{22})\cdot R_2\to R_2}
        }}
      \onslide<2->{\sage{A}}
    \]
  \end{example}

\end{frame}






\begin{sagesilent}
  E = elementary_matrix(A.nrows(), row1=2, row2=0, scale=-3)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[Reversing Row Addition]
    \[
      \sage{A}
      \xrightarrow{R_3-3\cdot R_1\to R_3}\sage{E*A}
      \onslide<2->{\xrightarrow{
          \onslide<3->{R_3+3\cdot R_1\to R_3}
        }}
      \onslide<2->{\sage{A}}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{The Reverse of a Row Operation}
    Every row operation is reversed by another row operation.
    \begin{description}[<+->][$R_i+c\cdot R_j\to R_i$]
    \item[$R_i\leftrightarrow R_j$] is reversed by
      \mybold{$R_i\leftrightarrow R_j$}
    \item[$c\cdot R_i\to R_i$] is reversed by
      \mybold{$(\frac{1}{c})\cdot R_i\to R_i$}
    \item[$R_i+c\cdot R_j\to R_i$] is reversed by
      \mybold{$R_i-c\cdot R_j\to R_i$}
    \end{description}
  \end{block}

\end{frame}


\subsection{The Inverse of an Elementary Matrix}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    What happens when we multiply an elementary matrix by the ``reverse''
    elementary matrix?
  \end{block}

\end{frame}



\begin{sagesilent}
  E = elementary_matrix(4, row1=1, row2=3)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[Row Switching]
    \[
      \underset{R_2\leftrightarrow R_4}{\sage{E}}
      \underset{R_2\leftrightarrow R_4}{\sage{E.inverse()}}
      = \pause\sage{E*E.inverse()}
      = \pause I_{\sage{E.nrows()}}
    \]
  \end{example}

\end{frame}



\begin{sagesilent}
  E = elementary_matrix(3, row1=2, scale=-6)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[Row Scaling]
    \[
      \underset{-6\cdot R_3\to R_3}{\sage{E}}
      \underset{(-\frac{1}{6})\cdot R_3\to R_3}{\sage{E.inverse()}}
      = \pause\sage{E*E.inverse()}
      = \pause I_{\sage{E.nrows()}}
    \]
  \end{example}

\end{frame}



\begin{sagesilent}
  E = elementary_matrix(5, row1=2, row2=0, scale=7)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[Row Addition]
    \[
      \underset{R_3+7\cdot R_1\to R_3}{\sage{E}}
      \underset{R_3-7\cdot R_1\to R_3}{\sage{E.inverse()}}
      = \pause\sage{E*E.inverse()}
      = \pause I_{\sage{E.nrows()}}
    \]
  \end{example}

\end{frame}




\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The ``reverse'' elementary matrix $E^{-1}$ of an elementary matrix $E$
    satisfies the equation $EE^{-1}=E^{-1}E=I$.
  \end{theorem}

\end{frame}


\subsection{$A=E^{-1}R$ From $EA=R$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Idea}
    Previously, we used the Gau\ss-Jordan algorithm to obtain
    \[
      E_r\dotsb E_3 E_2 E_1A=\rref(A)
    \]\pause
    Multiplying both sides of this equation on the left by
    \[
      E^{-1}=E_1^{-1}E_2^{-1}E_3^{-1}\dotsb E_r^{-1}
    \]
    gives the matrix factorization $A=E^{-1}R$ where $R=\rref(A)$.
  \end{block}

\end{frame}




\begin{sagesilent}
  A = matrix.column([(1, 4, 0), (2, 9, 1)])
  E1 = elementary_matrix(3, row1=1, row2=0, scale=-4)
  E2 = elementary_matrix(3, row1=0, row2=1, scale=-2)
  E3 = elementary_matrix(3, row1=2, row2=1, scale=-1)
  E = E3*E2*E1
  var('b1, b2, b3')
  b = vector([b1, b2, b3])
  M = A.change_ring(SR).augment(b, subdivide=True)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the row reductions
    \newcommand{\myStepA}{\scriptsize
      \begin{array}{rcrcr}
        R_1 &-& 2\cdot R_2 &\to& R_1 \\
        R_3 &-& R_2 &\to& R_3
      \end{array}
    }
    \[
      \underset{A}{\sage{A}}
      \xrightarrow{R_2-4\cdot R_1\to R_2}
      \sage{E1*A}
      \xrightarrow{\myStepA}\underset{\rref(A)}{\sage{E3*E2*E1*A}}
    \]
    \onslide<2->{The inverse elementary matrices are}
    \begin{align*}
      \onslide<3->{E_1^{-1} &=} \onslide<4->{\sage{E1.inverse()}} & \onslide<5->{E_2^{-1} &=} \onslide<6->{\sage{E2.inverse()}} & \onslide<7->{E_3^{-1} &=} \onslide<8->{\sage{E3.inverse()}}
    \end{align*}
    \onslide<9->{This gives $A=E^{-1}R$ where $E^{-1}=E_1^{-1}E_2^{-1}E_3^{-1}$
      and $R=\rref(A)$.}
  \end{example}

\end{frame}



\end{document}
