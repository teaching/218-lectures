from sage.all import matrix, random_matrix, ZZ


def my_random_matrix(m, n, r):
    return random_matrix(ZZ, m, n, algorithm='echelonizable', rank=r)


def myR(m, n, r, pivots):
    A = my_random_matrix(m, n, r)
    while A.pivots() != pivots:
        A = my_random_matrix(m, n, r)
    coeffs = A.columns()[:-1]
    b = A.columns()[-1]
    M = matrix.column(coeffs).augment(b, subdivide=True)
    return M.rref()
