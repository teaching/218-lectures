\documentclass[usenames, dvipsnames]{beamer}

\usepackage{mathtools} % loads amsmath
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{blkarray, bigstrut}
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{multicol}
\usepackage{hyperref}
\hypersetup{colorlinks, linkcolor=., urlcolor=blue}
\usepackage{sagetex}
\usepackage{siunitx}
\DeclareSIUnit{\mph}{mph}
\usepackage{xparse}
\usepackage{xfrac}
% \usepackage{showframe}          % for testing
\usepackage{tikz}
\usetikzlibrary{
  , angles
  , arrows
  , automata
  , calc
  , cd
  , decorations
  , decorations.pathmorphing
  , decorations.pathreplacing
  , fit
  , matrix
  , patterns
  , positioning
  , quotes
  , shapes
  , shapes.geometric
}
\usepackage{tikz-3dplot}
\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\text{\ding{51}}}%
\newcommand{\xmark}{\text{\ding{55}}}%


\ExplSyntaxOn
\NewDocumentCommand{\gcenter}{m}
{
  \begin{center}
    \seq_set_split:Nnn \l_tmpa_seq { \\ } { #1 }
    \seq_map_inline:Nn \l_tmpa_seq
    {
      \seq_set_split:Nnn \l_tmpb_seq { & } { ##1 }
      \seq_use:Nn \l_tmpb_seq { \hfil }
      \\
    }
  \end{center}
}
\ExplSyntaxOff

\newcommand{\mybold}[1]{{\usebeamercolor[fg]{example text}{#1}}}
\newcommand{\myotherbold}[1]{{\usebeamercolor[fg]{title}{#1}}}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%
\DeclarePairedDelimiter\inner{\langle}{\rangle}%

% \newcommand{\semitransp}[2][35]{\color{fg!#1}#2}

\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\LNull}{LNull}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\gm}{gm}
\DeclareMathOperator{\am}{am}
\DeclareMathOperator{\trace}{trace}
\DeclareMathOperator{\proj}{proj}


\setbeamertemplate{caption}{\raggedright\insertcaption\par}


\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\RR}{\mathbb{R}}

\let\oldfrac\frac% Store \frac
\renewcommand{\frac}[2]{%
  \mathchoice
  {\oldfrac{#1}{#2}}% display style
  {\sfrac{#1}{#2}}% text style
  {\sfrac{#1}{#2}}% script style
  {\sfrac{#1}{#2}}% script-scr#ipt style
}


\newcommand{\smallsage}[1]{{\small\sage{#1}}}
\newcommand{\footsage}[1]{{\footnotesize\sage{#1}}}
\newcommand{\scriptsage}[1]{{\scriptstyle\sage{#1}}}
\newcommand{\tinysage}[1]{{\tiny\sage{#1}}}


\theoremstyle{definition}
\newtheorem{algorithm}{Algorithm}


\title{Jordan Canonical Form}
\subtitle{Math 218}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}


\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}

\usepackage{currfile}
\usepackage{xstring}


% https://tex.stackexchange.com/questions/91691/beamer-set-mode-mid-presentation
\makeatletter
\newcommand\changemode[1]{%
  \gdef\beamer@currentmode{#1}}
\makeatother


\begin{document}

\IfSubStr*{\currfilename}{handout}{
  \changemode{handout}% options are handout, beamer, trans
}{}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents[sections={1-}]
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1-4}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={5-}]
  % \end{columns}
\end{frame}


\section{Motivation}
\subsection{Nondiagonalizable Matrices Exist}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    A matrix is \emph{diagonalizable} if it is similar to a diagonal matrix.
  \end{block}

  \pause
  \begin{block}{Tests For Diagonalizability}
    An $n\times n$ matrix $A$ is diagonalizable if and only if
    \[
      \gm_A(\lambda_1)+\gm_A(\lambda_2)+\dotsb+\gm_A(\lambda_k)=n
    \]\pause
    Equivalently, each eigenvalue $\lambda$ must satisfy
    $\gm_A(\lambda)=\am_A(\lambda)$.
  \end{block}

\end{frame}



\begin{sagesilent}
  set_random_seed(17924)
  l = -8
  n = 3
  J = jordan_block(l, n)
  P = random_matrix(ZZ, n, algorithm='unimodular')
  A = P*J*P.inverse()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{problem}
    Some matrices are not diagonalizable.
  \end{problem}

  \pause
  \begin{example}
    Consider the matrix $A$ given by
    \newcommand{\myEvals}{
      \begin{array}{rcc}
        \multicolumn{1}{c}{\lambda} & \gm_A(\lambda) & \am_A(\lambda) \\ \hline
        \sage{l}                    & 1              & 3
      \end{array}
    }
    \begin{align*}
      A &= \sage{A} & \myEvals
    \end{align*}
    Since $\gm_A(\sage{l})\neq\am_A(\sage{l})$, the matrix $A$ is not
    diagonalizable!
  \end{example}

\end{frame}




\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    What do we do with nondiagonalizable matrices?
  \end{block}

\end{frame}



\section{Jordan Canonical Form}
\subsection{Block Diagonal Matrices}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $\Set{A_1, A_2, \dotsc, A_k}$ be matrices. The \emph{block diagonal
      matrix} with diagonal $\Set{A_1, A_2, \dotsc, A_k}$ is
    \[
      A_1\oplus A_2\oplus\dotsb\oplus A_k
      =
      \left[
        \begin{array}{cccc}
          A_1 &&& \\
              & A_2 && \\
              &     & \ddots & \\
              &     &        & A_k
        \end{array}
      \right]
    \]
  \end{definition}

\end{frame}



\begin{sagesilent}
  set_random_seed(4792109)
  A = random_matrix(ZZ, 2)
  B = random_matrix(ZZ, 2, 3)
  C = random_matrix(ZZ, 1)
  D = block_diagonal_matrix(A, B, C)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrices $A$, $B$, and $C$ given by
    \begin{align*}
      A &= \sage{A} & B &= \sage{B} & C &= \sage{C}
    \end{align*}\pause
    The block diagonal matrix with diagonal $\Set{A, B, C}$ is
    \[
      A\oplus B\oplus C = \sage{D}
    \]
  \end{example}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    It is often convenient to write
    \[
      A^{\oplus k}
      =
      \left[
        \begin{array}{cccc}
          A &&& \\
            & A && \\
            &   & \ddots & \\
            &   &        & A
        \end{array}
      \right]
    \]
  \end{block}

\end{frame}


\begin{sagesilent}
  set_random_seed(7991)
  A = random_matrix(ZZ, 2)
  A3 = block_diagonal_matrix(A, A, A)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix
    \[
      A=\sage{A}
    \]
    The matrix $A^{\oplus 3}$ is given by
    \[
      A^{\oplus 3}
      =
      \sage{A3}
    \]
  \end{example}

\end{frame}



\subsection{Jordan Blocks}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The $n\times n$ \emph{Jordan block} with eigenvalue $\lambda$ is
    \[
      J_n(\lambda)
      =
      \left[
        \begin{array}{ccccc}
          \lambda & 1 &&& \\
                  & \lambda & 1 \\
                  &         & \lambda & \ddots \\
                  &         &         & \ddots & 1 \\
                  &         &         &        & \lambda
        \end{array}
      \right]
    \]
  \end{definition}

\end{frame}




\begin{sagesilent}
  l1, n1 = -3, 2
  J1 = jordan_block(l1, n1)
  l2, n2 = 47, 3
  J2 = jordan_block(l2, n2)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    \begin{align*}
      J_{\sage{n1}}(\sage{l1}) &= \sage{J1} & J_{\sage{n2}}(\sage{l2}) &= \sage{J2}
    \end{align*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{The Characteristic Polynomial of a Jordan Block}
    The characteristic polynomial of $J_n(\lambda)$ is
    \[
      \chi_{J_n(\lambda)}(t)
      =\pause
      \left\lvert
        \begin{array}{ccccc}
          t-\lambda & -1 &&& \\
                    & t-\lambda & -1 \\
                    &         & t-\lambda & \ddots \\
                    &         &         & \ddots & -1 \\
                    &         &         &        & t-\lambda
        \end{array}
      \right\rvert
      =\pause (t-\lambda)^n
    \]\pause
    This means that $J_n(\lambda)$ has exactly one eigenvalue $\lambda$ and with
    algebraic multiplicity $n$.
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{The Eigenspace of a Jordan Block}
    The eigenspace $E_\lambda$ of $J_n(\lambda)$ is
    \[
      E_\lambda
      = \pause\Null
      \left[
        \begin{array}{ccccc}
          0 & 1 &&& \\
            & 0 & \ddots \\
            &         & \ddots  & 1 \\
            &         &         & 0
        \end{array}
      \right]
      = \pause\Col
      \left[
        \begin{array}{c}
          1\\ 0\\ \vdots\\ 0
        \end{array}
      \right]
    \]\pause
    The geometric multiplicity of $\lambda$ as an eigenvalue of $J_n(\lambda)$
    is \pause one.
  \end{block}

\end{frame}


\subsection{The Jordan Canonical Forms of a Matrix}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Jordan Canonical Form]
    Let $A$ be a matrix with distinct eigenvalues
    $\Set{\lambda_1, \lambda_2, \dotsc, \lambda_r}$. Then $A=PJP^{-1}$ where
    \[
      J = J(\lambda_1)\oplus J(\lambda_2)\oplus\dotsb\oplus J(\lambda_r)
    \]
    and each $J(\lambda_i)$ is of the form
    \[
      J(\lambda_i) = J_{n_1}(\lambda_i)\oplus J_{n_2}(\lambda_i)\oplus\dotsb\oplus J_{n_k}(\lambda_i)
    \]
    where $k=\gm_A(\lambda_i)$ and $n_1+n_2+\dotsc+n_k=\am_A(\lambda_i)$.
  \end{theorem}

  \pause
  \begin{definition}
    The matrix $J$ is called a \emph{Jordan canonical form} of $A$. The matrix
    $J$ is unique, up to a reordering of the Jordan blocks.
  \end{definition}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Idea}
    Every square matrix $A$ is similar to a \emph{block diagonal} $J$. The
    multiplicities of each eigenvalue $\lambda$ are
    \begin{description}
    \item<2->[$\am_A(\lambda)$] number of times $\lambda$ appears on the diagonal of
      $J$
    \item<3->[$\gm_A(\lambda)$] number of Jordan blocks in $J$ corresponding to $\lambda$
    \end{description}
  \end{block}

\end{frame}


\begin{sagesilent}
  A = matrix([(9, 10, -11, 2), (9, -3, 1, 9), (10, 0, -3, 10), (-2, -10, 11, 5)])
  J, P = A.jordan_form(transformation=True)
  l1, l1, l2, l2 = A.eigenvalues()
  I = identity_matrix(A.nrows())
  gm1 = (A-l1*I).right_nullity()
  gm2 = (A-l2*I).right_nullity()
  am1 = J.diagonal().count(l1)
  am2 = J.diagonal().count(l2)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $A$ given by
    \newcommand{\myEvals}{
      \begin{array}{rcc}
        \multicolumn{1}{c}{\lambda} & \gm_A(\lambda) & \am_A(\lambda) \\ \hline
        \sage{l1}                   & \sage{gm1}     & \sage{am1} \\
        \sage{l2}                   & \sage{gm2}     & \sage{am2}
      \end{array}
    }
    \begin{align*}
      A &= \sage{A} & \myEvals
    \end{align*}\pause
    A Jordan canonical form of $A$ is
    $J=J_{n_1}(\sage{l1})\oplus J_{n_2}(\sage{l2})$ where $n_1=\sage{am1}$ and
    $n_2=\sage{am2}$. \pause Thus
    \[
      J
      = J_{\sage{am1}}(\sage{l1})\oplus J_{\sage{am2}}(\sage{l2})
      = \sage{J}
    \]\pause
    This means that $A=PJP^{-1}$.
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Finding $P$ in $A=PJP^{-1}$ is \emph{difficult} and requires the theory of
    \emph{generalized eigenvectors}.
  \end{block}

  \pause
  \begin{definition}
    A \emph{generalized eigenvector} of $A$ with rank $m$ is a vector $\vv{v}$
    satisfying $(A-\lambda\cdot I_n)^m\vv{v}=\vv{O}$ but
    $(A-\lambda\cdot I_n)^{m-1}\vv{v}\neq\vv{O}$.
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $A$ given by
    \[
      A = \sage{A}
    \]\pause
    Then $A=PJP^{-1}$ where
    \begin{align*}
      P &= \sage{P} & J &= \sage{J}
    \end{align*}
  \end{example}

\end{frame}



\begin{sagesilent}
  J1 = jordan_block(-8, 2)
  J2 = jordan_block(-8, 2)
  J3 = jordan_block(17, 3)
  J4 = jordan_block(17, 2)
  J5 = jordan_block(17, 1)
  J = block_diagonal_matrix(J1, J2, J3, J4, J5)
  l1, l2 = sorted(set(J.eigenvalues()))
  I = identity_matrix(J.nrows())
  am1 = J.diagonal().count(l1)
  gm1 = (J-l1*I).right_nullity()
  am2 = J.diagonal().count(l2)
  gm2 = (J-l2*I).right_nullity()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose that a Jordan canonical form of $A$ is
    \begin{align*}
      J
      &= \tinysage{J} \\
      &= J_{2}(-8)\oplus J_{2}(-8)\oplus J_{3}(17)\oplus J_{2}(17)\oplus J_1(17)
    \end{align*}\pause
    The table of eigenvalues is
    \[
      \begin{array}{rcc}
        \multicolumn{1}{c}{\lambda} & \gm_A(\lambda) & \am_A(\lambda) \\ \hline
        \sage{l1}                   & \sage{gm1}     & \sage{am1} \\
        \sage{l2}                   & \sage{gm2}     & \sage{am2}
      \end{array}
    \]
  \end{example}

\end{frame}



\subsection{The Possible Jordan Forms Given $\chi_A(t)$}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Factoring $\chi_A(t)$ gives a list of \emph{possible} Jordan forms.
  \end{block}

\end{frame}




\begin{sagesilent}
  var('t')
  chi = (t+1)*(t-3)**2
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose $\chi_A(t)=\sage{chi}$.
    \newcommand{\myEvals}{
      \begin{array}{rcc}
        \multicolumn{1}{c}{\lambda} & \gm_A(\lambda) & \am_A(\lambda) \\ \hline
        -1                          & 1              & 1 \\
         3                          & 1, 2           & 2
      \end{array}
    }%
    \gcenter{$\myEvals$ && $J=J(-1)\oplus J(3)$}%
    \pause
    The ``sectors'' $J(-1)$ and $J(3)$ of $J$ depend on the geometric
    multiplicities. \pause
    \newcommand{\myEvalsA}{
      \begin{array}{cl}
        \multicolumn{1}{c}{\gm_A(-1)} & J(-1) \\ \hline
         1                            & J_1(-1)
      \end{array}
    }%
    \newcommand{\myEvalsB}{
      \begin{array}{cl}
        \multicolumn{1}{c}{\gm_A(3)} & \multicolumn{1}{c}{J(3)} \\ \hline
        1                            & J_2(3) \\
        2                            & J_1(3)\oplus J_1(3)
      \end{array}
    }%
    \gcenter{$\myEvalsA$ && $\myEvalsB$}%
    \pause
    This gives two possible Jordan canonical forms
    \begin{align*}
      J &= J_1(-1)\oplus J_2(3) & J &= J_1(-1)\oplus J_1(3)\oplus J_1(3)
    \end{align*}
  \end{example}

\end{frame}




\begin{sagesilent}
  var('t')
  chi = (t+3)**4*(t-1)**2
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose $\chi_A(t)=\sage{chi}$.
    \newcommand{\myEvals}{
      \begin{array}{rcc}
        \multicolumn{1}{c}{\lambda} & \gm_A(\lambda) & \am_A(\lambda) \\ \hline
        -3                          & 1, 2, 3, 4     & 4 \\
         1                          & 1, 2           & 2
      \end{array}
    }%
    \pause
    \gcenter{$\myEvals$ && $J=J(-3)\oplus J(1)$}%
    \pause
    The ``sectors'' $J(-3)$ and $J(1)$ of $J$ depend on the geometric
    multiplicities. \pause
    \newcommand{\myEvalsA}{
      \begin{array}{cl}
        \multicolumn{1}{c}{\gm_A(-3)} & \multicolumn{1}{c}{J(-3)} \\ \hline
        1                             & J_4(-3) \\
        2                             & J_1(-3)\oplus J_3(-3) \\
                                      & J_2(-3)^{\oplus 2} \\
        3                             & J_1(-3)^{\oplus 2}\oplus J_2(-3) \\
        4                             & J_1(-3)^{\oplus 4}
      \end{array}
    }%
    \newcommand{\myEvalsB}{
      \begin{array}{cl}
        \multicolumn{1}{c}{\gm_A(1)} & \multicolumn{1}{c}{J(1)} \\ \hline
        1                            & J_2(1) \\
        2                            & J_1(1)\oplus J_1(1)
      \end{array}
    }%
    \gcenter{$\myEvalsA$ && $\myEvalsB$}%
    \pause
    This gives ten possible Jordan canonical forms.
  \end{example}

\end{frame}






\end{document}
