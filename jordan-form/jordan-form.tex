\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\usepackage{stackengine}
\usepackage{booktabs}

\title{Jordan Canonical Form}
\subtitle{Math 218}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Motivation}
\subsection{Nondiagonalizable Matrices}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    A matrix is \emph{diagonalizable} if it is similar to a diagonal matrix.
  \end{block}

  \begin{block}{Tests for Diagonalizability}<2->
    An $n\times n$ matrix $A$ is diagonalizable if and only if
    \[
      \sum_{\lambda\in\EVals(A)}\gm_A(\lambda)=n
    \]
    Equivalently, each eigenvalue $\lambda$ must satisfy
    $\gm_A(\lambda)=\am_A(\lambda)$.
  \end{block}

\end{frame}


\begin{sagesilent}
  A = jordan_block(-8, 3)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Problem}
    Some matrices are not diagonalizable.
  \end{block}

  \newcommand{\myEvals}{
    \begin{array}{rcc}
      \toprule
      \myotherbold{\lambda} & \myotherbold{\gm_A(\lambda)} & \myotherbold{\am_A(\lambda)} \\
      \midrule
      \onslide<3->{-8} & \onslide<4->{1} & \onslide<5->{3} \\
      \bottomrule
    \end{array}
  }
  \begin{example}<2->
    Consider the data
    \begin{align*}
      A &= \sage{A} & \myEvals
    \end{align*}
    \onslide<6->{Since $\gm_A(-8)\neq\am_A(-8)$, the matrix $A$ is not
      diagonalizable!}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    What do we do with nondiagonalizable matrices?
  \end{block}

\end{frame}


\section{Jordan Canonical Form}
\subsection{Block Diagonal Matrices}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{block diagonal matrix} with diagonal $\Set{A_1, A_2, \dotsc, A_k}$
    is
    \[
      A_1\oplus A_2\oplus\dotsb\oplus A_k
      =
      \left[
        \begin{array}{rrrr}
          A_1 &     &        &     \\
              & A_2 &        &     \\
              &     & \ddots &     \\
              &     &        & A_k
        \end{array}
      \right]
    \]
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myA}{
    \left[
      \begin{array}{rr}
        {\color<3->{red}-4} & {\color<3->{red}1} \\
        {\color<3->{red}-6} & {\color<3->{red}0}
      \end{array}
    \right]
  }
  \newcommand{\myB}{
    \left[
      \begin{array}{rrr}
        {\color<5->{beamblue}2} & {\color<5->{beamblue}0} & {\color<5->{beamblue}-10} \\
        {\color<5->{beamblue}-2} & {\color<5->{beamblue}-1} & {\color<5->{beamblue}-1}
      \end{array}
    \right]
  }
  \newcommand{\myC}{
    \left[
      \begin{array}{r}
        {\color<7->{beamgreen}-28}
      \end{array}
    \right]
  }
  \begin{example}
    Consider the matrices $A$, $B$, and $C$ given by
    \begin{align*}
      A &= \myA & B &= \myB & C &= \myC
    \end{align*}
    \onslide<2->The block diagonal matrix with diagonal $\Set{A, B, C}$ is
    \[
      A\oplus B\oplus C
      =
      \left[
        \begin{array}{rr|rrr|r}
          \onslide<4->{\color<4->{red}-4} & \onslide<4->{\color<4->{red}1} & \onslide<9->{0}                      & \onslide<9->{0}                      & \onslide<9->{0}                       & \onslide<9->{0}                        \\
          \onslide<4->{\color<4->{red}-6} & \onslide<4->{\color<4->{red}0} & \onslide<9->{0}                      & \onslide<9->{0}                      & \onslide<9->{0}                       & \onslide<9->{0}                        \\ \hline
          \onslide<9->{0}                 & \onslide<9->{0}                & \onslide<6->{\color<6->{beamblue}2}  & \onslide<6->{\color<6->{beamblue}0}  & \onslide<6->{\color<6->{beamblue}-10} & \onslide<9->{0}                        \\
          \onslide<9->{0}                 & \onslide<9->{0}                & \onslide<6->{\color<6->{beamblue}-2} & \onslide<6->{\color<6->{beamblue}-1} & \onslide<6->{\color<6->{beamblue}-1}  & \onslide<9->{0}                        \\ \hline
          \onslide<9->{0}                 & \onslide<9->{0}                & \onslide<9->{0}                      & \onslide<9->{0}                      & \onslide<9->{0}                       & \onslide<8->{\color<8->{beamgreen}-28}
        \end{array}
      \right]
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    It is often convenient to write
    \[
      A^{\oplus k}
      =
      \left[
        \begin{array}{cccc}
          A &&& \\
            & A && \\
            &   & \ddots & \\
            &   &        & A
        \end{array}
      \right]
    \]
  \end{definition}

\end{frame}


\begin{sagesilent}
  set_random_seed(7991)
  A = random_matrix(ZZ, 2)
  A3 = block_diagonal_matrix(A, A, A)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix
    \[
      A=\sage{A}
    \]
    \pause The matrix $A^{\oplus 3}$ is given by
    \[
      A^{\oplus 3}
      =
      \sage{A3}
    \]
  \end{example}

\end{frame}


\subsection{Jordan Blocks}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The $n\times n$ \emph{Jordan block} with eigenvalue $\lambda$ is
    \[
      J_n(\lambda)
      =
      \left[
        \begin{array}{ccccc}
          \lambda & 1       &         &        &         \\
                  & \lambda & 1       &        &         \\
                  &         & \lambda & \ddots &         \\
                  &         &         & \ddots & 1       \\
                  &         &         &        & \lambda
        \end{array}
      \right]
    \]
  \end{definition}

\end{frame}


\begin{sagesilent}
  l1, n1 = -3, 2
  J1 = jordan_block(l1, n1)
  l2, n2 = 47, 3
  J2 = jordan_block(l2, n2)
  l3, n3 = -19, 5
  J3 = jordan_block(l3, n3)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The Jordan blocks $J_{\sage{n1}}(\sage{l1})$ and $J_{\sage{n2}}(\sage{l2})$
    are given by
    \begin{align*}
      J_{\sage{n1}}(\sage{l1}) &= \sage{J1} & J_{\sage{n2}}(\sage{l2}) &= \sage{J2}
    \end{align*}
    \pause The Jordan block $J_{\sage{n3}}(\sage{l3})$ is given by
    \[
      J_{\sage{n3}}(\sage{l3})
      =
      \sage{J3}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The characteristic polynomial of $J_n(\lambda)$ is
    \[
      \chi_{J_n(\lambda)}(t)
      =\pause
      \left\lvert
        \begin{array}{ccccc}
          t-\lambda & -1        &           &        &           \\
                    & t-\lambda & -1        &        &           \\
                    &           & t-\lambda & \ddots &           \\
                    &           &           & \ddots & -1        \\
                    &           &           &        & t-\lambda
        \end{array}
      \right\rvert
      =\pause (t-\lambda)^n
    \]
    \pause The only eigenvalue of $J_n(\lambda)$ is $\lambda$ and
    $\am_{J_n(\lambda)}=n$.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The only eigenspace of $J_n(\lambda)$ is
    \[
      \mathcal{E}_{J_n(\lambda)}
      = \pause\Null
      \left[
        \begin{array}{cccc}
          0 & 1       &         &   \\
            & 0       & \ddots  &   \\
            &         & \ddots  & 1 \\
            &         &         & 0
        \end{array}
      \right]
      = \pause\Span\Set{\langle1, 0,\dotsc, 0\rangle}
    \]\pause
    The geometric multiplicity of $\lambda$ as an eigenvalue of $J_n(\lambda)$
    is \pause one.
  \end{theorem}

\end{frame}


\subsection{Jordan Canonical Form}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Jordan Canonical Form]
    Suppose $\EVals(A)=\Set{\lambda_1, \lambda_2, \dotsc, \lambda_r}$. Then
    $A=PJP^{-1}$ where
    \[
      J = J(\lambda_1)\oplus J(\lambda_2)\oplus\dotsb\oplus J(\lambda_r)
    \]
    and each $J(\lambda_i)$ is of the form
    \[
      J(\lambda_i) = J_{n_1}(\lambda_i)\oplus J_{n_2}(\lambda_i)\oplus\dotsb\oplus J_{n_k}(\lambda_i)
    \]
    where $k=\gm_A(\lambda_i)$ and $n_1+n_2+\dotsc+n_k=\am_A(\lambda_i)$.
  \end{theorem}

  \pause
  \begin{definition}
    The matrix $J$ is called a \emph{Jordan canonical form} of $A$.
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Interpretation}
    Every square matrix $A$ is similar to a \emph{block diagonal} $J$.
    \begin{description}[<+(1)->]
    \item[$\am_A(\lambda)$] number of times $\lambda$ appears on the diagonal of
      $J$
    \item[$\gm_A(\lambda)$] number of Jordan blocks in $J$ corresponding to $\lambda$
    \end{description}
  \end{block}

\end{frame}


\begin{sagesilent}
  A = matrix([(9, 10, -11, 2), (9, -3, 1, 9), (10, 0, -3, 10), (-2, -10, 11, 5)])
  J, P = A.jordan_form(transformation=True)
  l1, l1, l2, l2 = A.eigenvalues()
  I = identity_matrix(A.nrows())
  gm1 = (A-l1*I).right_nullity()
  gm2 = (A-l2*I).right_nullity()
  am1 = J.diagonal().count(l1)
  am2 = J.diagonal().count(l2)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $A$ given by
    \newcommand{\myEvals}{
      \begin{array}{rcc}
        \toprule
        \myotherbold{\lambda} & \myotherbold{\gm_A(\lambda)} & \myotherbold{\am_A(\lambda)} \\
        \midrule
        \sage{l1}                   & \sage{gm1}     & \sage{am1} \\
        \sage{l2}                   & \sage{gm2}     & \sage{am2} \\
        \bottomrule
      \end{array}
    }
    \begin{align*}
      A &= \sage{A} & \myEvals
    \end{align*}\pause
    A Jordan canonical form of $A$ is
    $J=J_{n_1}(\sage{l1})\oplus J_{n_2}(\sage{l2})$ where
    \begin{align*}
      n_1 &= \am_A(\sage{l1}) = \sage{am1} & n_2 &= \am_A(\sage{l2}) = \sage{am2}
    \end{align*}
    \pause Thus
    \[
      J
      = J_{\sage{am1}}(\sage{l1})\oplus J_{\sage{am2}}(\sage{l2})
      = \sage{J}
    \]
  \end{example}

\end{frame}


\begin{sagesilent}
  J1 = jordan_block(-8, 2)
  J2 = jordan_block(-8, 2)
  J3 = jordan_block(17, 3)
  J4 = jordan_block(17, 2)
  J5 = jordan_block(17, 1)
  J = block_diagonal_matrix(J1, J2, J3, J4, J5)
  l1, l2 = sorted(set(J.eigenvalues()))
  I = identity_matrix(J.nrows())
  am1 = J.diagonal().count(l1)
  gm1 = (J-l1*I).right_nullity()
  am2 = J.diagonal().count(l2)
  gm2 = (J-l2*I).right_nullity()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myEvals}{
    \begin{array}{rcc}
      \toprule
      \myotherbold{\lambda} & \myotherbold{\gm_A(\lambda)} & \myotherbold{\am_A(\lambda)} \\
      \midrule
      \onslide<2->{\sage{l1}}                   & \onslide<4->{\sage{gm1}}     & \onslide<6->{\sage{am1}} \\
      \onslide<3->{\sage{l2}}                   & \onslide<5->{\sage{gm2}}     & \onslide<7->{\sage{am2}} \\
      \bottomrule
    \end{array}
  }
  \begin{example}
    Suppose that a Jordan canonical form of $A$ is
    \begin{gather*}
      \begin{align*}
        J &= \overset{J_{2}(-8)\oplus J_{2}(-8)\oplus J_{3}(17)\oplus J_{2}(17)\oplus J_1(17)}{\sage{J}} & \myEvals
      \end{align*}
    \end{gather*}
  \end{example}

\end{frame}


\subsection{Enumerating Possible Jordan Forms}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Knowing $\am_A(\lambda)$ and $\gm_A(\lambda)$ gives a list of possible
    Jordan forms.
  \end{block}

\end{frame}


\begin{sagesilent}
  var('t')
  chi = (t+1)*(t-3)**2
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose $\chi_A(t)=\sage{chi}$.
    \newcommand{\myEvals}{
      \begin{array}{rcc}
        \multicolumn{1}{c}{\lambda} & \gm_A(\lambda) & \am_A(\lambda) \\ \hline
        -1                          & 1              & 1 \\
         3                          & 1, 2           & 2
      \end{array}
    }%
    \gcenter{$\myEvals$ && $J=J(-1)\oplus J(3)$}%
    \pause
    The ``sectors'' $J(-1)$ and $J(3)$ depend on $\gm_A(-1)$ and $\gm_A(3)$.
    \pause
    \newcommand{\myEvalsA}{
      \begin{array}{cl}
        \multicolumn{1}{c}{\gm_A(-1)} & J(-1) \\ \hline
         1                            & J_1(-1)
      \end{array}
    }%
    \newcommand{\myEvalsB}{
      \begin{array}{cl}
        \multicolumn{1}{c}{\gm_A(3)} & \multicolumn{1}{c}{J(3)} \\ \hline
        1                            & J_2(3) \\
        2                            & J_1(3)\oplus J_1(3)
      \end{array}
    }%
    \gcenter{$\myEvalsA$ && $\myEvalsB$}%
    \pause
    This gives two possible Jordan canonical forms
    \begin{align*}
      J &= J_1(-1)\oplus J_2(3) & J &= J_1(-1)\oplus J_1(3)\oplus J_1(3)
    \end{align*}
  \end{example}

\end{frame}




\begin{sagesilent}
  var('t')
  chi = (t+3)**4*(t-1)**2
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose $\chi_A(t)=\sage{chi}$.
    \newcommand{\myEvals}{
      \begin{array}{rcc}
        \multicolumn{1}{c}{\lambda} & \gm_A(\lambda) & \am_A(\lambda) \\ \hline
        -3                          & 1, 2, 3, 4     & 4 \\
         1                          & 1, 2           & 2
      \end{array}
    }%
    \pause
    \gcenter{$\myEvals$ && $J=J(-3)\oplus J(1)$}%
    \pause
    The ``sectors'' $J(-3)$ and $J(1)$ depend on $\gm_A(-3)$ and $\gm_A(1)$. \pause
    \newcommand{\myEvalsA}{
      \begin{array}{cl}
        \multicolumn{1}{c}{\gm_A(-3)} & \multicolumn{1}{c}{J(-3)} \\ \hline
        1                             & J_4(-3) \\
        2                             & J_1(-3)\oplus J_3(-3) \\
                                      & J_2(-3)^{\oplus 2} \\
        3                             & J_1(-3)^{\oplus 2}\oplus J_2(-3) \\
        4                             & J_1(-3)^{\oplus 4}
      \end{array}
    }%
    \newcommand{\myEvalsB}{
      \begin{array}{cl}
        \multicolumn{1}{c}{\gm_A(1)} & \multicolumn{1}{c}{J(1)} \\ \hline
        1                            & J_2(1) \\
        2                            & J_1(1)\oplus J_1(1)
      \end{array}
    }%
    \gcenter{$\myEvalsA$ && $\myEvalsB$}%
    \pause
    This gives ten possible Jordan canonical forms.
  \end{example}

\end{frame}



\end{document}
