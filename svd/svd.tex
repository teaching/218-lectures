\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\usepackage{stackengine}
\usepackage{booktabs}

\usepackage{caption}
\usepackage{xfp}
\ExplSyntaxOn
\NewDocumentCommand{\filesize}{O{B}m}
{
  \fpeval{ round( \pdffilesize { #2 } / \fp_use:c { c_brian_#1_fp } , 2) }
  \,#1
}

\fp_const:Nn \c_brian_B_fp { 1 }
\fp_const:Nn \c_brian_KiB_fp { 1024 }
\fp_const:Nn \c_brian_MiB_fp { 1024*1024 }
\fp_const:Nn \c_brian_GiB_fp { 1024*1024*1024 }
\ExplSyntaxOff

\title{Singular Value Decomposition}
\subtitle{Math 218}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Image Compression}
\subsection{A Cat Picture}
\newcommand{\mymocha}[2]{
  \begin{frame}

    \frametitle{\secname}
    \framesubtitle{\subsecname}

    \begin{figure}[h]
      \centering
      \includegraphics[height=.725\textheight]{./mocha/#1}
      \caption*{#2 (\filesize[KiB]{./mocha/#1})}
      \label{fig:color}
    \end{figure}

  \end{frame}
}

\mymocha{mocha.jpg}{Uncompressed Color Image}
\mymocha{mocha-grayed.jpg}{Uncompressed Gray Image}
\mymocha{mocha-compressed-500.jpg}{$N=500$ Compression}
\mymocha{mocha-compressed-400.jpg}{$N=400$ Compression}
\mymocha{mocha-compressed-300.jpg}{$N=300$ Compression}
\mymocha{mocha-compressed-200.jpg}{$N=200$ Compression}
\mymocha{mocha-compressed-100.jpg}{$N=100$ Compression}
\mymocha{mocha-compressed-50.jpg}{$N=50$ Compression}
\mymocha{mocha-compressed-25.jpg}{$N=25$ Compression}
\mymocha{mocha-compressed-10.jpg}{$N=10$ Compression}
\mymocha{mocha-compressed-5.jpg}{$N=5$ Compression}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{table}[h]
    \centering
    \begin{tabular}{lr}
      Image & Size \\ \hline
      Uncompressed (Color) & \filesize[KiB]{./mocha/mocha.jpg} \\
      Uncompressed (Gray)  & \filesize[KiB]{./mocha/mocha-grayed.jpg} \\
      Compressed ($N=500$) & \filesize[KiB]{./mocha/mocha-compressed-500.jpg} \\
      Compressed ($N=400$) & \filesize[KiB]{./mocha/mocha-compressed-400.jpg} \\
      Compressed ($N=300$) & \filesize[KiB]{./mocha/mocha-compressed-300.jpg} \\
      Compressed ($N=200$) & \filesize[KiB]{./mocha/mocha-compressed-200.jpg} \\
      Compressed ($N=100$) & \filesize[KiB]{./mocha/mocha-compressed-100.jpg} \\
      Compressed ($N=50$)  & \filesize[KiB]{./mocha/mocha-compressed-50.jpg} \\
      Compressed ($N=25$)  & \filesize[KiB]{./mocha/mocha-compressed-25.jpg} \\
      Compressed ($N=10$)  & \filesize[KiB]{./mocha/mocha-compressed-10.jpg} \\
      Compressed ($N=5$)   & \filesize[KiB]{./mocha/mocha-compressed-5.jpg}
    \end{tabular}
  \end{table}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question 1}
    How can we ``compress'' an image?
  \end{block}

  \pause
  \begin{block}{Question 2}
    What does $N$-compression mean?
  \end{block}

  \pause
  \begin{block}{Question 3}
    Is ``lossless quality'' compression possible?
  \end{block}
\end{frame}


\subsection{Rank One Example}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myRed}{
    \begin{array}{c}
      \begin{tikzpicture}[line join=round, line cap=round, scale=1/5]
        \fill[red] (0, 0) rectangle (16, 9);

        \path (0, 0) -- (0, 9)
        node[rotate=0, midway, sloped, above] {$m$ pixels};

        \path (0, 9) -- (16, 9)
        node[rotate=0, midway, sloped, above] {$n$ pixels};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myA}{
    \left[
      \begin{array}{rrr}
        r      & \cdots & r      \\
        \vdots & \ddots & \vdots \\
        r      & \cdots & r
      \end{array}
    \right]
  }
  \newcommand{\myUncompressed}{
    \left[
      \begin{array}{rrr}
        r & r & r \\
        r & r & r
      \end{array}
    \right]
  }
  \newcommand{\myCompressed}{
    \left[
      \begin{array}{r}
        1 \\
        1
      \end{array}
    \right]
    \left[
      \begin{array}{rrr}
        r & r & r
      \end{array}
    \right]
  }
  \begin{example}
    The data in a red rectangle can be represented by an $m\times n$ matrix.
    \begin{gather*}
      \myRed
      \leftrightsquigarrow
      \myA
    \end{gather*}
    \onslide<2->We can ``compress'' our image by factoring.
    \[
      \overset{\onslide<3->{\stackanchor{\scriptsize``uncompressed''}{$\scriptstyle2\times 3=6$}}}{\myUncompressed}
      =
      \overset{\onslide<4->{\stackanchor{\scriptsize``compressed''}{$\scriptstyle2+3=5$}}}{\myCompressed}
    \]
    \onslide<5->{By factoring, we ``lossesly compress'' our image and save one
      unit!}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myRed}{
    \begin{array}{c}
      \begin{tikzpicture}[line join=round, line cap=round, scale=1/5]
        \fill[red] (0, 0) rectangle (16, 9);

        \path (0, 0) -- (0, 9)
        node[rotate=0, midway, sloped, above] {$3840$ pixels};

        \path (0, 9) -- (16, 9)
        node[rotate=0, midway, sloped, above] {$2160$ pixels};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myA}{
    \left[
      \begin{array}{rrr}
        r      & \cdots & r      \\
        \vdots & \ddots & \vdots \\
        r      & \cdots & r
      \end{array}
    \right]
  }
  \newcommand{\myUncompressed}{
    \left[
      \begin{array}{rrr}
        r & r & r \\
        r & r & r
      \end{array}
    \right]
  }
  \newcommand{\myCompressed}{
    \left[
      \begin{array}{r}
        1 \\
        \vdots \\
        1
      \end{array}
    \right]
    \left[
      \begin{array}{rrr}
        r & \cdots & r
      \end{array}
    \right]
  }
  \begin{example}
    A 4k UHD monitor has resolution $3840\times 2160$.
    \begin{gather*}
      \myRed
      \leftrightsquigarrow
      \overset{\onslide<2->{\stackanchor{\scriptsize``uncompressed''}{$\scriptstyle3840\times 2160=\sage{3840*2160}$}}}{\myA}
      =
      \overset{\onslide<3->{\stackanchor{\scriptsize``compressed''}{$\scriptstyle3840+2160=\sage{3840+2160}$}}}{\myCompressed}
    \end{gather*}
    \onslide<4->{Our compression is \emph{much smaller}!}
  \end{example}

\end{frame}

\subsection{Rank Two Example}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\mySweden}{
    \begin{array}{c}
      \begin{tikzpicture}[line join=round, line cap=round, scale=1/2]
        % http://www.schemecolor.com/sweden-flag-colors.php

        % Name: Medium Persian Blue
        % Hex: #006AA7
        % RGB: (0, 106, 167)
        % CMYK: 1, 0.365, 0, 0.345

        % Name: Philippine Yellow
        % Hex: #FECC00
        % RGB: (254, 204, 0)
        % CMYK: 0, 0.196, 1, 0.003

        \definecolor{sblue}{HTML}{006AA7}
        \definecolor{syellow}{HTML}{FECC00}

        \fill[sblue] (0, 0) rectangle (16, 10);
        \fill[syellow] (5, 10) rectangle (7, 0);
        \fill[syellow] (0, 4) rectangle (16, 6);

        \path (0, 10) -- (5, 10) node[midway, above] {$5\,n$};
        \path (5, 10) -- (7, 10) node[midway, above] {$2\,n$};
        \path (7, 10) -- (16, 10) node[midway, above] {$9\,n$};

        \path (0, 0) -- (0, 4) node[midway, left] {$4\,n$};
        \path (0, 4) -- (0, 6) node[midway, left] {$2\,n$};
        \path (0, 6) -- (0, 10) node[midway, left] {$4\,n$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myS}{
    \left[
      \begin{array}{cccccccccc}
        b      & \cdots & b      & y      & \cdots & y      & b      & \cdots\cdots\cdots\cdots\cdots\cdots & b      \\
        \vdots & \ddots & \vdots & \vdots & \ddots & \vdots & \vdots & \ddots                               & \vdots \\
        b      & \cdots & b      & y      & \cdots & y      & b      & \cdots\cdots\cdots\cdots\cdots\cdots & b      \\
        y      & \cdots & y      & y      & \cdots & y      & y      & \cdots\cdots\cdots\cdots\cdots\cdots & y      \\
        \vdots & \ddots & \vdots & \vdots & \ddots & \vdots & \vdots & \ddots                               & \vdots \\
        y      & \cdots & y      & y      & \cdots & y      & y      & \cdots\cdots\cdots\cdots\cdots\cdots & y      \\
        b      & \cdots & b      & y      & \cdots & y      & b      & \cdots\cdots\cdots\cdots\cdots\cdots & b      \\
        \vdots & \ddots & \vdots & \vdots & \ddots & \vdots & \vdots & \ddots                               & \vdots \\
        b      & \cdots & b      & y      & \cdots & y      & b      & \cdots\cdots\cdots\cdots\cdots\cdots & b
      \end{array}
    \right]
  }
  \begin{example}
    The Swedish flag is slightly more complicated than a solid image.
    \begin{gather*}
      \mySweden
      \onslide<2->{
        \leftrightsquigarrow
        S =
        \overset{{\color{red}10\,n\times16\,n=160\,n^2}}{\myS}
      }
    \end{gather*}
    \onslide<3->{Note that $\rank(S)=\onslide<4->{2$.}}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myS}{
    \left[
      \begin{array}{cccccccccc}
        b      & \cdots & b      & y      & \cdots & y      & b      & \cdots\cdots\cdots & b      \\
        \vdots & \ddots & \vdots & \vdots & \ddots & \vdots & \vdots & \ddots             & \vdots \\
        b      & \cdots & b      & y      & \cdots & y      & b      & \cdots\cdots\cdots & b      \\
        y      & \cdots & y      & y      & \cdots & y      & y      & \cdots\cdots\cdots & y      \\
        \vdots & \ddots & \vdots & \vdots & \ddots & \vdots & \vdots & \ddots             & \vdots \\
        y      & \cdots & y      & y      & \cdots & y      & y      & \cdots\cdots\cdots & y      \\
        b      & \cdots & b      & y      & \cdots & y      & b      & \cdots\cdots\cdots & b      \\
        \vdots & \ddots & \vdots & \vdots & \ddots & \vdots & \vdots & \ddots             & \vdots \\
        b      & \cdots & b      & y      & \cdots & y      & b      & \cdots\cdots\cdots & b
      \end{array}
    \right]
  }
  \newcommand{\myUa}{
    \left[
      \begin{array}{c}
        1\\ \vdots\\ 1\\ 0\\ \vdots\\ 0\\ 1\\ \vdots\\ 1
      \end{array}
    \right]
  }
  \newcommand{\myVa}{
    \left[
      \begin{array}{ccccccccc}
        b & \cdots & b & y & \cdots & y & b & \cdots & b
      \end{array}
    \right]
  }
  \newcommand{\myUb}{
    \left[
      \begin{array}{c}
        0\\ \vdots\\ 0\\ 1\\ \vdots\\ 1\\ 0\\ \vdots\\ 0
      \end{array}
    \right]
  }
  \newcommand{\myVb}{
    \left[
      \begin{array}{ccc}
        y & \cdots & y
      \end{array}
    \right]
  }
  \begin{example}
    Since $\rank(S)=2$, our ``compression'' requires two terms.
    \begin{gather*}
      \overset{{\color{red}10\,n\times16\,n}}{\myS}
      \onslide<2->{=}
      \onslide<2->{\overset{{\color{red}10\,n\times 1}}{\myUa}\overset{{\color{red}1\times 16\,n}}{\myVa}+}
      \onslide<3->{\overset{{\color{red}10\,n\times 1}}{\myUb}\overset{{\color{red}1\times 16\,n}}{\myVb}}
    \end{gather*}
    \onslide<4->To fit a 4k monitor, $n=216$ and the amount of data is
    \begin{gather*}
      \begin{align*}
        \overset{\textnormal{\mybold{uncompressed}}}{10\cdot216\times16\cdot216=\sage{160*216**2}} && \onslide<5->{\overset{\textnormal{\mybold{compressed (lossless)}}}{10\cdot216+16\cdot216+10\cdot216+16\cdot 216=\sage{2*10*216+2*16*216}}}
      \end{align*}
    \end{gather*}
    \onslide<6->{We could save more space (sacrificing quality) by dropping a term.}
  \end{example}

\end{frame}


\section{Singular Value Decomposition}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    What can we learn from our ``image compression'' examples?
  \end{block}

  \begin{block}{Lesson 1}<2->
    A decomposition of the form
    \[
      A = \sigma_1\cdot\bv{u}_1\bv{v}_1^\ast+\sigma_2\cdot\bv{u}_2\bv{v}_2^\ast+\dotsb+\sigma_r\cdot\bv{u}_r\bv{v}_r^\ast
    \]
    allows us to lossesly compress the data in $A$.
  \end{block}

  \begin{block}{Lesson 2}<3->
    Dropping terms from this decomposition
    \[
      A \approx \sigma_1\cdot\bv{u}_1\bv{v}_1^\ast+\sigma_2\cdot\bv{u}_2\bv{v}_2^\ast+\dotsb+\sigma_k\cdot\bv{u}_k\bv{v}_k^\ast
    \]
    further compresses the data at the expense of quality.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myA}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\color<2->{red}A$};
      \onslide<2->{
        \node[overlay, below left= 1mm and 3mm of a] (text) {$m\times n$ with rank $r$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myU}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamblue
    ]{
      \node[inner sep=0pt, black] (a) {$\color<3->{beamblue}U$};
      \onslide<3->{
        \node[overlay, below left= 6mm and 3mm of a] (text) {$m\times r$ with $U^\ast U=I_{r}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\mySigma}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\color<4->{red}\Sigma$};
      \onslide<4->{
        \node[overlay, above right= 0mm and 3mm of a] (text) {$\diag(\sigma_1\geq\sigma_2\geq\dotsb\geq\sigma_r>0)$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\myV}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , beamgreen
    ]{
      \node[inner sep=0pt, black] (a) {$\color<5->{beamgreen}V$};
      \onslide<5->{
        \node[overlay, below right= 1mm and 3mm of a] (text) {$n\times r$ with $V^\ast V=I_{r}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{definition}
    A \emph{singular value decomposition} is a factorization of the form
    \[
      \myA = \myU\mySigma\myV^\ast
      \vspace{1cm}
    \]
    \onslide<6->The scalars $\Set{\sigma_1,\dotsc,\sigma_r}$ are the \emph{singular values}
    of $A$ and the sum
    \[
      A \approx \sigma_1\cdot\bv{u}_1\bv{v}_1^\ast+\sigma_2\cdot\bv{u}_2\bv{v}_2^\ast+\dotsb+\sigma_k\cdot\bv{u}_k\bv{v}_k^\ast
    \]
    is the \emph{rank $k$ approximation of $A$}.
  \end{definition}

\end{frame}


\begin{sagesilent}
  A = matrix([(3, 2, 2), (2, 3, -2)])
  S = diagonal_matrix([5, 3])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the singular value decomposition
    \newcommand{\myU}{
      \left[
        \begin{array}{rr}
          \frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}}  \\
          \frac{1}{\sqrt{2}} & -\frac{1}{\sqrt{2}}
        \end{array}
      \right]
    }
    \newcommand{\myVT}{
      \left[
        \begin{array}{rrr}
          \frac{1}{\sqrt{2}}  & \frac{1}{\sqrt{2}}   & 0 \\
          \frac{1}{\sqrt{18}} & -\frac{1}{\sqrt{18}} & \frac{4}{\sqrt{18}}
        \end{array}
      \right]
    }
    \newcommand{\myUa}{
      \left[
        \begin{array}{rr}
          \frac{1}{\sqrt{2}} \\
          \frac{1}{\sqrt{2}}
        \end{array}
      \right]
    }
    \newcommand{\myUb}{
      \left[
        \begin{array}{rr}
          \frac{1}{\sqrt{2}}  \\
          -\frac{1}{\sqrt{2}}
        \end{array}
      \right]
    }
    \newcommand{\myVa}{
      \left[
        \begin{array}{rrr}
          \frac{1}{\sqrt{2}}  & \frac{1}{\sqrt{2}}   & 0
        \end{array}
      \right]
    }
    \newcommand{\myVb}{
      \left[
        \begin{array}{rrr}
          \frac{1}{\sqrt{18}} & -\frac{1}{\sqrt{18}} & \frac{4}{\sqrt{18}}
        \end{array}
      \right]
    }
    \begin{gather*}
      \overset{A}{\sage{A}}
      =
      \overset{U}{\myU}
      \overset{\Sigma}{\sage{S}}
      \overset{V^\ast}{\myVT}
    \end{gather*}
    \pause The singular values of $A$ are $\Set{5, 3}$. \pause Our
    ``compressed'' sum is
    \begin{gather*}
      \overset{A}{\sage{A}}
      =
      5\cdot\myUa\myVa
      +
      3\cdot\myUb\myVb
    \end{gather*}
    \pause The \emph{rank one approximation} of $A$ is
    \begin{gather*}
      \overset{A}{\sage{A}}
      \approx
      5\cdot\myUa\myVa
      =
      \left[
        \begin{array}{rrr}
          \frac{5}{2} & \frac{5}{2} & 0 \\
          \frac{5}{2} & \frac{5}{2} & 0
        \end{array}
      \right]
    \end{gather*}
  \end{example}

\end{frame}


\subsection{Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myU}{
    \left[
      \begin{array}{rrrr}
        \bv{u}_1 & \bv{u}_2 & \cdots & \bv{u}_r
      \end{array}
    \right]
  }
  \newcommand{\mySigma}{
    \left[
      \begin{array}{rrrr}
        \sigma_1 &          &        &          \\
                 & \sigma_2 &        &          \\
                 &          & \ddots &          \\
                 &          &        & \sigma_r
      \end{array}
    \right]
  }
  \newcommand{\myV}{
    \left[
      \begin{array}{c}
        \bv{v}_1^\ast \\ \bv{v}_2^\ast \\ \vdots \\ \bv{v}_r^\ast
      \end{array}
    \right]
  }
  \begin{theorem}
    Consider a singular value decomposition
    \[
      A
      =
      \overset{U}{\myU}
      \overset{\Sigma}{\mySigma}
      \overset{V^\ast}{\myV}
    \]
    \pause Then the columns of $U$ and $V$ are related via the formulas
    \begin{align*}
      A\bv{v}_j &= \sigma_j\cdot\bv{u}_j & A^\ast\bv{u}_j &= \sigma_j\cdot\bv{v}_j
    \end{align*}
    \pause Moreover, $A^\ast=V\Sigma U^\ast$ is a singular value decomposition
    of $A^\ast$.
  \end{theorem}

\end{frame}


\begin{sagesilent}
  A = matrix([(3, 2, 2), (2, 3, -2)])
  S = diagonal_matrix([5, 3])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the singular value decomposition
    \newcommand{\myU}{
      \left[
        \begin{array}{rr}
          \frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}}  \\
          \frac{1}{\sqrt{2}} & -\frac{1}{\sqrt{2}}
        \end{array}
      \right]
    }
    \newcommand{\myVT}{
      \left[
        \begin{array}{rrr}
          \frac{1}{\sqrt{2}}  & \frac{1}{\sqrt{2}}   & 0 \\
          \frac{1}{\sqrt{18}} & -\frac{1}{\sqrt{18}} & \frac{4}{\sqrt{18}}
        \end{array}
      \right]
    }
    \newcommand{\myUa}{
      \left[
        \begin{array}{rr}
          \frac{1}{\sqrt{2}} \\
          \frac{1}{\sqrt{2}}
        \end{array}
      \right]
    }
    \newcommand{\myUb}{
      \left[
        \begin{array}{rr}
          \frac{1}{\sqrt{2}}  \\
          -\frac{1}{\sqrt{2}}
        \end{array}
      \right]
    }
    \newcommand{\myVa}{
      \left[
        \begin{array}{r}
          \frac{1}{\sqrt{2}}  \\ \frac{1}{\sqrt{2}}   \\ 0
        \end{array}
      \right]
    }
    \newcommand{\myVb}{
      \left[
        \begin{array}{r}
          \frac{1}{\sqrt{18}} \\ -\frac{1}{\sqrt{18}} \\ \frac{4}{\sqrt{18}}
        \end{array}
      \right]
    }
    \begin{gather*}
      \overset{A}{\sage{A}}
      =
      \overset{U}{\myU}
      \overset{\Sigma}{\sage{S}}
      \overset{V^\ast}{\myVT}
    \end{gather*}
    \pause The columns of $U$ and $V$ are related via the formulas
    \begin{gather*}
      \begin{align*}
        \overset{A}{\sage{A}}\overset{\bv{v}_1}{\myVa} &= 5\cdot\overset{\bv{u}_1}{\myUa} & \overset{A^\ast}{\sage{A.T}}\overset{\bv{u}_1}{\myUa} &= 5\cdot\overset{\bv{v}_1}{\myVa} \\
        \overset{A}{\sage{A}}\overset{\bv{v}_2}{\myVb} &= 3\cdot\overset{\bv{u}_2}{\myUb} & \overset{A^\ast}{\sage{A.T}}\overset{\bv{u}_2}{\myUb} &= 3\cdot\overset{\bv{v}_2}{\myVb}
      \end{align*}
    \end{gather*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Consider a singular value decomposition
    \[
      A = U\Sigma V^\ast
    \]
    The columns of $U$ form an orthonormal basis of $\Col(A)$.
  \end{theorem}

  \begin{corollary}<2->
    The columns of $V$ form an orthonormal basis of $\Col(A^\ast)$.
  \end{corollary}

\end{frame}


\begin{sagesilent}
  A = matrix([(3, 2, 2), (2, 3, -2)])
  S = diagonal_matrix([5, 3])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The singular value decomposition
    \newcommand{\myU}{
      \left[
        \begin{array}{rr}
          \frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}}  \\
          \frac{1}{\sqrt{2}} & -\frac{1}{\sqrt{2}}
        \end{array}
      \right]
    }
    \newcommand{\myVT}{
      \left[
        \begin{array}{rrr}
          \frac{1}{\sqrt{2}}  & \frac{1}{\sqrt{2}}   & 0 \\
          \frac{1}{\sqrt{18}} & -\frac{1}{\sqrt{18}} & \frac{4}{\sqrt{18}}
        \end{array}
      \right]
    }
    \newcommand{\myV}{
      \left[
        \begin{array}{rrr}
          \frac{1}{\sqrt{2}}  & \frac{1}{\sqrt{18}} \\
          \frac{1}{\sqrt{2}}  & -\frac{1}{\sqrt{18}} \\
          0                   & \frac{4}{\sqrt{18}}
        \end{array}
      \right]
    }
    \newcommand{\myUa}{
      \left[
        \begin{array}{rr}
          \frac{1}{\sqrt{2}} \\
          \frac{1}{\sqrt{2}}
        \end{array}
      \right]
    }
    \newcommand{\myUb}{
      \left[
        \begin{array}{rr}
          \frac{1}{\sqrt{2}}  \\
          -\frac{1}{\sqrt{2}}
        \end{array}
      \right]
    }
    \newcommand{\myVa}{
      \left[
        \begin{array}{r}
          \frac{1}{\sqrt{2}}  \\ \frac{1}{\sqrt{2}}   \\ 0
        \end{array}
      \right]
    }
    \newcommand{\myVb}{
      \left[
        \begin{array}{r}
          \frac{1}{\sqrt{18}} \\ -\frac{1}{\sqrt{18}} \\ \frac{4}{\sqrt{18}}
        \end{array}
      \right]
    }
    \begin{gather*}
      \overset{A}{\sage{A}}
      =
      \overset{U}{\myU}
      \overset{\Sigma}{\sage{S}}
      \overset{V^\ast}{\myVT}
    \end{gather*}
    gives orthonormal bases
    \begin{gather*}
      \begin{align*}
        \Col\overset{A}{\sage{A}} &= \Col\overset{U}{\myU} & \Col\overset{A^\ast}{\sage{A.T}} &= \Col\overset{V}{\myV}
      \end{align*}
    \end{gather*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Consider a singular value decomposition
    \[
      A = U\Sigma V^\ast
    \]
    Projection onto each of the four fundamental subspaces is given by
    \begin{align*}
      P_{\Col(A)} &= UU^\ast & P_{\Null(A^\ast)} &= I_m-UU^\ast \\
      P_{\Col(A^\ast)} &= VV^\ast & P_{\Null(A)} &= I_n-VV^\ast
    \end{align*}
  \end{theorem}

\end{frame}


\begin{sagesilent}
  A = matrix([(3, 2, 2), (2, 3, -2)])
  S = diagonal_matrix([5, 3])
  Idm = identity_matrix(A.nrows())
  Idn = identity_matrix(A.ncols())
  PCol = A*A.pseudoinverse()
  PRow = A.pseudoinverse()*A
  PNull = Idn-PRow
  PLNull = Idm-PCol
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the singular value decomposition
    \newcommand{\myU}{
      \left[
        \begin{array}{rr}
          \frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}}  \\
          \frac{1}{\sqrt{2}} & -\frac{1}{\sqrt{2}}
        \end{array}
      \right]
    }
    \newcommand{\myUT}{
      \left[
        \begin{array}{rr}
          \frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}}  \\
          \frac{1}{\sqrt{2}} & -\frac{1}{\sqrt{2}}
        \end{array}
      \right]
    }
    \newcommand{\myVT}{
      \left[
        \begin{array}{rrr}
          \frac{1}{\sqrt{2}}  & \frac{1}{\sqrt{2}}   & 0 \\
          \frac{1}{\sqrt{18}} & -\frac{1}{\sqrt{18}} & \frac{4}{\sqrt{18}}
        \end{array}
      \right]
    }
    \newcommand{\myV}{
      \left[
        \begin{array}{rrr}
          \frac{1}{\sqrt{2}}  & \frac{1}{\sqrt{18}} \\
          \frac{1}{\sqrt{2}}  & -\frac{1}{\sqrt{18}} \\
          0                   & \frac{4}{\sqrt{18}}
        \end{array}
      \right]
    }
    \begin{gather*}
      \overset{A}{\sage{A}}
      =
      \overset{U}{\myU}
      \overset{\Sigma}{\sage{S}}
      \overset{V^\ast}{\myVT}
    \end{gather*}
    \pause Projection onto $\Col(A^\ast)$ is given by
    \[
      \overset{P_{\Col(A^\ast)}}{\sage{PRow}} = \overset{V}{\myV}\overset{V^\ast}{\myVT}
    \]
  \end{example}

\end{frame}


\subsection{Algorithm}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    How can we compute $A=U\Sigma V^\ast$?
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{The SVD Algorithm}
    The following steps compute $A=U\Sigma V^\ast$.
    \begin{description}[<+->][Step 1]
    \item[Step 1] Order the positive eigenvalues of $A^\ast A$ as
      \[
        \lambda_1\geq\lambda_2\geq\dotsb\geq\lambda_r
      \]
      accounting for algebraic multiplicity.
    \item[Step 2] Use the spectral theorem to factor $A^\ast A$ as
      \[
        A^\ast A=VDV^\ast
      \]
      where $D=\diag(\lambda_1, \lambda_2, \dotsc, \lambda_r)$.
    \item[Step 3] Define $\Sigma=\sqrt{D}$ and $U=AV\Sigma^{-1}$.
    \end{description}
  \end{block}

\end{frame}


\begin{sagesilent}
  A = matrix.column([(0, 1, 1), (1, 1, 0)])
  D = diagonal_matrix([3, 1])
  Sigma = diagonal_matrix(map(sqrt, D.diagonal()))
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myV}{
    \left[
      \begin{array}{rr}
        \frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}} \\
        \frac{1}{\sqrt{2}} & \frac{-1}{\sqrt{2}}
      \end{array}
    \right]
  }
  \newcommand{\myVT}{
    \left[
      \begin{array}{rr}
        \frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}} \\
        \frac{1}{\sqrt{2}} & \frac{-1}{\sqrt{2}}
      \end{array}
    \right]
  }
  \newcommand{\mySigmai}{
    \left[
      \begin{array}{rr}
        \frac{1}{\sqrt{3}} & 0 \\
        0                  & 1
      \end{array}
    \right]
  }
  \newcommand{\myU}{
    \left[
      \begin{array}{rr}
        \frac{1}{\sqrt{6}} & \frac{-1}{\sqrt{2}} \\
        \frac{2}{\sqrt{6}} & 0                   \\
        \frac{1}{\sqrt{6}} & \frac{1}{\sqrt{2}}
      \end{array}
    \right]
  }
  \begin{example}
    Consider the data
    \begin{gather*}
      \overset{A^\ast}{\sage{A.T}}\overset{A}{\sage{A}}
      = \sage{A.T*A}
      = \overset{V}{\myV}\overset{D}{\sage{D}}\overset{V^\ast}{\myVT}
    \end{gather*}
    \onslide<2->{We then define}
    \begin{gather*}
      \begin{align*}
        \onslide<3->{\Sigma} &\onslide<3->{= \sage{Sigma}} & \onslide<4->{\overset{U}{\myU}} &\onslide<4->{= \overset{A}{\sage{A}}\overset{V}{\myV}\overset{\Sigma^{-1}}{\mySigmai}}
      \end{align*}
    \end{gather*}
    \onslide<5->{This gives our singular value decomposition
      $A=U\Sigma V^\ast$.}
    % \[
    %   \overset{A}{\sage{A}}
    %   =
    %   \overset{U}{\myU}
    %   \overset{\Sigma}{\sage{Sigma}}
    %   \overset{V^\ast}{\myVT}
    % \]
  \end{example}

\end{frame}


\begin{sagesilent}
  A = matrix(ZZ, [[-1, 0, 0], [-1, 2, 2], [0, 0, 0], [2, 0, 0]])
  D = diagonal_matrix([10, 4])
  Sigma = diagonal_matrix(map(sqrt, D.diagonal()))
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myU}{
    \left[
      \begin{array}{rr}
        \frac{-1}{\sqrt{30}} & \frac{-1}{\sqrt{6}} \\
        \frac{-5}{\sqrt{30}} & \frac{1}{\sqrt{6}}  \\
        0                    & 0                   \\
        \frac{2}{\sqrt{30}}  & \frac{2}{\sqrt{6}}
      \end{array}
    \right]
  }
  \newcommand{\myV}{
    \left[
      \begin{array}{rr}
        \frac{1}{\sqrt{3}}  & \frac{2}{\sqrt{6}} \\
        \frac{-1}{\sqrt{3}} & \frac{1}{\sqrt{6}} \\
        \frac{-1}{\sqrt{3}} & \frac{1}{\sqrt{6}}
      \end{array}
    \right]
  }
  \newcommand{\myVT}{
    \left[
      \begin{array}{rrr}
        \frac{1}{\sqrt{3}} & \frac{-1}{\sqrt{3}} & \frac{-1}{\sqrt{3}} \\
        \frac{2}{\sqrt{6}} & \frac{1}{\sqrt{6}}  & \frac{1}{\sqrt{6}}
      \end{array}
    \right]
  }
  \begin{example}
    Consider the singular value decomposition
    \begin{gather*}
      \overset{A}{\sage{A}}
      =
      \overset{U}{\myU}
      \overset{\Sigma}{\sage{Sigma}}
      \overset{V^\ast}{\myVT}
    \end{gather*}
  \end{example}

\end{frame}


\begin{sagesilent}
  A = matrix(ZZ, [[-1, -1, 0], [1, 0, 1], [-4, -1, -1], [1, 1, 1]])
  D = diagonal_matrix([23, 1, 1])
  Sigma = diagonal_matrix(map(sqrt, D.diagonal()))
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myVT}{
    \left[
      \begin{array}{rrr}
        \frac{3}{\sqrt{11}}   & \frac{1}{\sqrt{11}}   & \frac{1}{\sqrt{11}}  \\
        \frac{1}{\sqrt{10}}   & 0                     & \frac{-3}{\sqrt{10}} \\
        \frac{-3}{\sqrt{110}} & \frac{10}{\sqrt{110}} & \frac{-1}{\sqrt{110}}
      \end{array}
    \right]
  }
  \newcommand{\myU}{
    \left[
      \begin{array}{rrr}
        -\frac{4}{\sqrt{253}}  & -\frac{1}{\sqrt{10}} & -\frac{7}{\sqrt{110}} \\
        \frac{4}{\sqrt{253}}   & -\frac{2}{\sqrt{10}} & -\frac{4}{\sqrt{110}} \\
        -\frac{14}{\sqrt{253}} & -\frac{1}{\sqrt{10}} &  \frac{3}{\sqrt{110}} \\
        \frac{5}{\sqrt{253}}   & -\frac{2}{\sqrt{10}} &  \frac{6}{\sqrt{110}}
      \end{array}
    \right]
  }
  \begin{example}
    Consider the singular value decomposition
    \begin{gather*}
      \overset{A}{\sage{A}}
      =
      \overset{U}{\myU}
      \overset{\Sigma}{\sage{Sigma}}
      \overset{V^\ast}{\myVT}
    \end{gather*}
  \end{example}

\end{frame}

\end{document}
