\documentclass{article}

\usepackage{fullpage}
\usepackage{fitzmath}

\begin{document}

\title{SVD Examples}
\author{Brian David Fitzpatrick}
\maketitle

% \section*{Final Exam Information}
% \begin{itemize}
% \item When: Monday 16-December 2019 from 2:00 PM to 5:00 PM
% \item Where: Physics 130
% \item What: Three-hour cumulative final exam.
% \end{itemize}

% \section*{Brian's Office Hours}
% \begin{itemize}
% \item Monday 9-December from 1:00 PM to 3:00 PM (Hudson Hall 222)
% \item Tuesday 10-December from 1:00 PM to 3:00 PM (Hudson Hall 222)
% \item Friday 13-December from 11:00 AM to 1:00 PM (Hudson Hall 222)
% \end{itemize}

% \newpage
\section*{Example I}
\begin{sagesilent}
  A = matrix(ZZ, [[-1, 0, 0], [-1, 2, 2], [0, 0, 0], [2, 0, 0]])
\end{sagesilent}

Consider the data
\begin{align*}
  A &= \sage{A} & \overset{A^\ast}{\sage{A.T}}\overset{A}{\sage{A}} &= \sage{A.T*A}
\end{align*}
The characteristic polynomial of $A^\ast A$ is then
\begin{sagesilent}
  latex.matrix_delimiters(left='|', right='|')
  var('t')
  chi = t*identity_matrix(A.ncols())-A.T*A
  from functools import partial
  elem = partial(elementary_matrix, A.ncols())
  E1 = elem(row1=1, row2=2, scale=-1)
  E2 = elem(row1=1, scale=1/t)
  E3 = elem(row1=0, row2=1, scale=-2)
  E3 = elem(row1=2, row2=1, scale=4)*E3
\end{sagesilent}
\begin{align*}
  \chi_{A^\ast A}(t)
  &= \sage{chi} \\
  &= \sage{E1*chi} \\
  &= t\cdot\sage{E2*(E1*chi)} \\
  &= t\cdot\sage{E3*E2*(E1*chi)} \\
  &= t\cdot\Set{\sage{(E3*E2*(E1*chi)).det()}} \\
  &= t\cdot\Set{\sage{expand((E3*E2*(E1*chi)).det())}} \\
  &= t\cdot\sage{factor((A.T*A).characteristic_polynomial(t)) / t}
\end{align*}
The eigenspaces of the positive eigenvalues of $A^\ast A$ are
\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  E = lambda l: A.T*A - l*identity_matrix(A.ncols())
  K = lambda l: Set(E(l).right_kernel().basis())
\end{sagesilent}
\begin{gather*}
  \begin{align*}
    \mathcal{E}_{A^\ast A}(10) &= \Null\sage{E(10)} = \Span\sage{K(10)} &
                                                                          \mathcal{E}_{A^\ast A}(4) &= \Null\sage{E(4)} = \Span\sage{K(4)}
  \end{align*}
\end{gather*}
This gives the factorization
\newcommand{\myV}{
  \left[
    \begin{array}{rr}
      \frac{1}{\sqrt{3}}  & \frac{2}{\sqrt{6}} \\
      \frac{-1}{\sqrt{3}} & \frac{1}{\sqrt{6}} \\
      \frac{-1}{\sqrt{3}} & \frac{1}{\sqrt{6}}
    \end{array}
  \right]
}
\newcommand{\myVT}{
  \left[
    \begin{array}{rrr}
      \frac{1}{\sqrt{3}} & \frac{-1}{\sqrt{3}} & \frac{-1}{\sqrt{3}} \\
      \frac{2}{\sqrt{6}} & \frac{1}{\sqrt{6}}  & \frac{1}{\sqrt{6}}
    \end{array}
  \right]
}
\begin{sagesilent}
  D = diagonal_matrix([10, 4])
\end{sagesilent}
\[
  \overset{A^\ast A}{\sage{A.T*A}}
  =
  \overset{V}{\myV}
  \overset{D}{\sage{D}}
  \overset{V^\ast}{\myVT}
\]
Now, the formulas $\Sigma=\sqrt{D}$ and $U=AV\Sigma^{-1}$ give
\begin{sagesilent}
  Sigma = diagonal_matrix(map(sqrt, D.diagonal()))
\end{sagesilent}
\newcommand{\mySigmai}{
  \left[
    \begin{array}{rr}
      \frac{1}{\sqrt{10}} & 0           \\
      0                   & \frac{1}{2}
    \end{array}
  \right]
}
\newcommand{\myU}{
  \left[
    \begin{array}{rr}
      \frac{-1}{\sqrt{30}} & \frac{-1}{\sqrt{6}} \\
      \frac{-5}{\sqrt{30}} & \frac{1}{\sqrt{6}}  \\
      0                    & 0                   \\
      \frac{2}{\sqrt{30}}  & \frac{2}{\sqrt{6}}
    \end{array}
  \right]
}
\begin{align*}
  \Sigma &= \sage{Sigma} & \overset{U}{\myU} &= \overset{A}{\sage{A}}\overset{V}{\myV}\overset{\Sigma^{-1}}{\mySigmai}
\end{align*}
This gives the singular value decomposition
\[
  \overset{A}{\sage{A}}
  =
  \overset{U}{\myU}
  \overset{\Sigma}{\sage{Sigma}}
  \overset{V^\ast}{\myVT}
\]
Our ``compressed sum'' is
\newcommand{\myUa}{
  \left[
    \begin{array}{r}
      \frac{-1}{\sqrt{30}} \\
      \frac{-5}{\sqrt{30}} \\
      0                    \\
      \frac{2}{\sqrt{30}}
    \end{array}
  \right]
}
\newcommand{\myUb}{
  \left[
    \begin{array}{r}
      \frac{-1}{\sqrt{6}} \\
      \frac{1}{\sqrt{6}}  \\
      0                   \\
      \frac{2}{\sqrt{6}}
    \end{array}
  \right]
}
\newcommand{\myVa}{
  \left[
    \begin{array}{rrr}
      \frac{1}{\sqrt{3}} & \frac{-1}{\sqrt{3}} & \frac{-1}{\sqrt{3}}
    \end{array}
  \right]
}
\newcommand{\myVb}{
  \left[
    \begin{array}{rrr}
      \frac{2}{\sqrt{6}} & \frac{1}{\sqrt{6}}  & \frac{1}{\sqrt{6}}
    \end{array}
  \right]
}
\[
  \overset{A}{\sage{A}}
  =
  \sqrt{10}\cdot\myUa\myVa
  + 2\cdot\myUb\myVb
\]
The ``rank one'' approximation is
\begin{sagesilent}
  R1 = matrix([(-1/3, 1/3, 1/3), (-5/3, 5/3, 5/3), (0, 0, 0), (2/3, -2/3, -2/3)])
\end{sagesilent}
\[
  \overset{A}{\sage{A}}
  \approx
  \sqrt{10}\cdot\myUa\myVa
  = \sage{R1}
\]

\newpage
\section*{Example II}

\begin{sagesilent}
  A = matrix([(-1, -1, 0), (1, 0, 1), (-4, -1, -1), (1, 1, 1)])
\end{sagesilent}

Consider the data
\begin{align*}
  A &= \sage{A} & \overset{A^\ast}{\sage{A.T}}\overset{A}{\sage{A}} &= \sage{A.T*A}
\end{align*}
The characteristic polynomial of $A^\ast A$ is then
\begin{sagesilent}
  latex.matrix_delimiters(left='|', right='|')
  var('t')
  chi = t*identity_matrix(A.ncols())-A.T*A
  from functools import partial
  elem = partial(elementary_matrix, A.ncols())
  E1 = elem(row1=1, row2=2, scale=-1)
  E1 = E1*elem(row1=0, row2=2, scale=-3)
  E2 = elem(row1=0, scale=1/(t-1))
  E2 = E2*elem(row1=1, scale=1/(t-1))
  E3 = elem(row1=2, row2=0, scale=6)
  E4 = elem(row1=2, row2=1, scale=2)
\end{sagesilent}
\begin{align*}
  \chi_{A^\ast A}(t)
  &= \sage{chi} \\
  &= \sage{E1*chi} \\
  &= (t-1)^2\cdot\sage{E2*(E1*chi)} \\
  &= (t-1)^2\cdot\sage{E3*E2*(E1*chi)} \\
  &= (t-1)^2\cdot\sage{E4*E3*E2*(E1*chi)} \\
  &= (t-1)^2\cdot(t-23)
\end{align*}
The eigenspaces of the positive eigenvalues of $A^\ast A$ are
\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  E = lambda l: A.T*A - l*identity_matrix(A.ncols())
  K = lambda l: Set(E(l).right_kernel().basis())
\end{sagesilent}
\begin{gather*}
  \begin{align*}
    \mathcal{E}_{A^\ast A}(1) &= \Null\sage{E(1)} = \Span\sage{K(1)} &
                                                                       \mathcal{E}_{A^\ast A}(23) &= \Null\sage{E(23)} = \Span\sage{K(23)}
  \end{align*}
\end{gather*}
Applying Gram-Schmidt to our basis of $\mathcal{E}_{A^\ast A}(1)$ gives
\begin{sagesilent}
  v1, v2 = K(1)
\end{sagesilent}
\[
  \sage{v2}-\oldfrac{\sage{v1}\cdot\sage{v2}}{\sage{v1}\cdot\sage{v1}}\sage{v1}
  =
  \sage{1/10}\cdot\sage{10*(v2-(v1*v2)/(v1*v1)*v1)}
\]
This gives the spectral factorization
\begin{sagesilent}
  D = diagonal_matrix([23, 1, 1])
\end{sagesilent}
\renewcommand{\myV}{
  \left[
    \begin{array}{rrr}
      \frac{3}{\sqrt{11}} & \frac{1}{\sqrt{10}}  & \frac{-3}{\sqrt{110}} \\
      \frac{1}{\sqrt{11}} & 0                    & \frac{10}{\sqrt{110}} \\
      \frac{1}{\sqrt{11}} & \frac{-3}{\sqrt{10}} & \frac{-1}{\sqrt{110}}
    \end{array}
  \right]
}
\renewcommand{\myVT}{
  \left[
    \begin{array}{rrr}
      \frac{3}{\sqrt{11}}   & \frac{1}{\sqrt{11}}   & \frac{1}{\sqrt{11}}  \\
      \frac{1}{\sqrt{10}}   & 0                     & \frac{-3}{\sqrt{10}} \\
      \frac{-3}{\sqrt{110}} & \frac{10}{\sqrt{110}} & \frac{-1}{\sqrt{110}}
    \end{array}
  \right]
}
\[
  \overset{A^\ast A}{\sage{A.T*A}}
  = \overset{V}{\myV}\overset{D}{\sage{D}}\overset{V^\ast}{\myVT}
\]
\begin{sagesilent}
  Sigma = diagonal_matrix(map(sqrt, D.diagonal()))
\end{sagesilent}
Now, the formulas $\Sigma=\sqrt{D}$ and $U=AV\Sigma^{-1}$ give
\renewcommand{\mySigmai}{
  \left[
    \begin{array}{rrr}
      \frac{1}{\sqrt{23}} & 0 & 0 \\
      0                   & 1 & 0 \\
      0                   & 0 & 1
    \end{array}
  \right]
}
\renewcommand{\myU}{
  \left[
    \begin{array}{rrr}
      -\frac{4}{\sqrt{253}}  & -\frac{1}{\sqrt{10}} & -\frac{7}{\sqrt{110}} \\
      \frac{4}{\sqrt{253}}   & -\frac{2}{\sqrt{10}} & -\frac{4}{\sqrt{110}} \\
      -\frac{14}{\sqrt{253}} & -\frac{1}{\sqrt{10}} &  \frac{3}{\sqrt{110}} \\
      \frac{5}{\sqrt{253}}   & -\frac{2}{\sqrt{10}} &  \frac{6}{\sqrt{110}}
    \end{array}
  \right]
}
\begin{gather*}
  \begin{align*}
    \Sigma &= \sage{Sigma} & \overset{U}{\myU} &= \overset{A}{\sage{A}}\overset{V}{\myV}\overset{\Sigma^{-1}}{\mySigmai}
  \end{align*}
\end{gather*}
This gives the singular value decomposition
\[
  \overset{A}{\sage{A}}
  =
  \overset{U}{\myU}
  \overset{\Sigma}{\sage{Sigma}}
  \overset{V^\ast}{\myVT}
\]
Our ``compressed sum'' is
\renewcommand{\myUa}{
  \left[
    \begin{array}{r}
      -\frac{4}{\sqrt{253}}   \\
      \frac{4}{\sqrt{253}}    \\
      -\frac{14}{\sqrt{253}}  \\
      \frac{5}{\sqrt{253}}
    \end{array}
  \right]
}
\renewcommand{\myUb}{
  \left[
    \begin{array}{r}
      -\frac{1}{\sqrt{10}}  \\
      -\frac{2}{\sqrt{10}}  \\
      -\frac{1}{\sqrt{10}}  \\
      -\frac{2}{\sqrt{10}}
    \end{array}
  \right]
}
\newcommand{\myUc}{
  \left[
    \begin{array}{r}
      -\frac{7}{\sqrt{110}} \\
      -\frac{4}{\sqrt{110}} \\
       \frac{3}{\sqrt{110}} \\
       \frac{6}{\sqrt{110}}
    \end{array}
  \right]
}
\renewcommand{\myVa}{
  \left[
    \begin{array}{rrr}
      \frac{3}{\sqrt{11}} & \frac{1}{\sqrt{11}} & \frac{1}{\sqrt{11}}
    \end{array}
  \right]
}
\renewcommand{\myVb}{
  \left[
    \begin{array}{rrr}
      \frac{1}{\sqrt{10}} & 0 & \frac{-3}{\sqrt{10}}
    \end{array}
  \right]
}
\newcommand{\myVc}{
  \left[
    \begin{array}{rrr}
      \frac{-3}{\sqrt{110}} & \frac{10}{\sqrt{110}} & \frac{-1}{\sqrt{110}}
    \end{array}
  \right]
}
\begin{gather*}
  \overset{A}{\sage{A}}
  =
  \sqrt{23}\cdot\myUa\myVa+
  1\cdot\myUb\myVb+
  1\cdot\myUc\myVc
\end{gather*}
The rank two approximation is
\begin{gather*}
  \overset{A}{\sage{A}}
  =
  \sqrt{23}\cdot\myUa\myVa+
  1\cdot\myUb\myVb
\end{gather*}
The rank one approximation is
\begin{gather*}
  \overset{A}{\sage{A}}
  =
  \sqrt{23}\cdot\myUa\myVa
\end{gather*}




\end{document}
