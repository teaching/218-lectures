\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\usepackage{booktabs}

\title{Diagonalization}
\subtitle{Math 218}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\begin{sagesilent}
  D = diagonal_matrix([1, 2])
  P = matrix(ZZ, [[1, -2], [2, -3]])
  p1, p2 = map(matrix.column, P.columns())
  A = P*D*P.inverse()
\end{sagesilent}

\section{Motivation}
\subsection{Matrix Powers}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myDP}{
    \left[
      \begin{array}{ll}
        1^{100} & 0 \\ 0 & 2^{100}
      \end{array}
    \right]
  }
  \newcommand{\myEQ}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$=$};
      \onslide<3->{
        \node[overlay, below right= 6mm and 1cm of a] (text) {easy because the matrix is \emph{diagonal}};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.west);
      }
    }
  }
  \begin{example}
    ${\sage{D}}^{100}\myEQ\onslide<2->{\myDP}$
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{exampleblock}{Problem}
    ${\sage{A}}^{100}=?$
  \end{exampleblock}

  \onslide<2->
  \begin{block}{Bad Idea}
    We can try doing lots of matrix multiplication.
    \begin{gather*}
      \begin{align*}
        \onslide<3->{\overset{A^2}{\sage{A**2}}} &\onslide<3->{=} \onslide<4->{\overset{A}{\sage{A}}\overset{A}{\sage{A}}}           & \onslide<5->{\overset{A^4}{\sage{A**4}}}     &\onslide<5->{=} \onslide<6->{\overset{A^2}{\sage{A**2}}\overset{A^2}{\sage{A**2}}} \\
        \onslide<7->{\overset{A^8}{\sage{A**8}}} &\onslide<7->{=} \onslide<8->{\overset{A^4}{\sage{A**4}}\overset{A^4}{\sage{A**4}}} & \onslide<9->{\overset{A^{16}}{\sage{A**16}}} &\onslide<9->{=} \onslide<10->{\overset{A^8}{\sage{A**8}}\overset{A^8}{\sage{A**8}}}
      \end{align*}
    \end{gather*}
    \onslide<11->{This is a pretty bad idea.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myAP}{
    \left[
      \begin{array}{cc}
        4\cdot 2^{100} & 2-2\cdot 2^{100}\\
        6\cdot 2^{100} & 4-3\cdot 2^{100}
      \end{array}
    \right]
  }
  \newcommand{\myDP}{
    \left[
      \begin{array}{ll}
        1^{100} & 0 \\ 0 & 2^{100}
      \end{array}
    \right]
  }
  \begin{block}{Good Idea}
    Consider the matrix factorization
    \[
      \overset{A}{\sage{A}}
      =
      \overset{P}{\sage{P}}
      \overset{D}{\sage{D}}
      \overset{P^{-1}}{\sage{P.inverse()}}
    \]
    \onslide<2->{The $n$th power of $A$ is given by}
    \[
      \onslide<2->{A^n =}
      \onslide<3->{(PDP^{-1})\cdot (PDP^{-1})\dotsb (PDP^{-1}) =}
      \onslide<4->{PD^nP^{-1}}
    \]
    \onslide<5->{We can now easily compute $A^{100}$ with}
    \begin{gather*}
      \onslide<6->{\overset{A^{100}}{\myAP} = \overset{P}{\sage{P}} \overset{D^{100}}{\myDP} \overset{P^{-1}}{\sage{P.inverse()}}}
    \end{gather*}
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Where did the factorization $A=PDP^{-1}$ come from?
  \end{block}

  \newcommand{\myP}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\overset{P}{\sage{P}}$};
      \onslide<7->{
      \node[overlay, below left= 2mm and -6mm of a] (text) {\emph{eigenvectors} in columns};
      \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myD}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\overset{D}{\sage{D}}$};
      \onslide<8->{
      \node[overlay, below left= 6mm and -6mm of a] (text) {\emph{eigenvalues} on diagonal};
      \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myEq}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$=$};
      \onslide<9->{
      \node[overlay, above right= 1cm and 0.25cm of a] (text) {$AP=PD$ means $A=PDP^{-1}$};
      \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \onslide<2->
  \begin{block}{Answer}
    Multiplying $A$ by each column of $P$ gives
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\overset{A}{\sage{A}}\sage{p1}} &\onslide<2->{=} \onslide<3->{1\cdot\sage{p1}} & \onslide<4->{\overset{A}{\sage{A}}\sage{p2}} &\onslide<4->{=} \onslide<5->{2\cdot\sage{p2}}
      \end{align*}
    \end{gather*}
    \onslide<6->{Putting this data together gives}
    \[
      \onslide<6->{\overset{A}{\sage{A}}\myP \myEq \overset{P}{\sage{P}}\myD}
    \]
  \end{block}

\end{frame}


\section{Similar Matrices}
\subsection{Definition}

\begin{sagesilent}
  B = matrix.column([(0, 1, 2), (0, 0, 1), (0, 0, -3)])
  set_random_seed(8941)
  P = random_matrix(ZZ, 3, algorithm='unimodular')
  A = P*B*P.inverse()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    We say $A$ is \emph{similar} to $B$ (and write $A\sim B$) if $A=PBP^{-1}$.
  \end{definition}

  \pause
  \begin{example}
    The factorization
    \begin{gather*}
      \begin{align*}
        \overset{A}{\sage{A}} = \overset{P}{\sage{P}}\overset{B}{\sage{B}}\overset{P^{-1}}{\sage{P.inverse()}}
      \end{align*}
    \end{gather*}
    indicates that $A\sim B$.
  \end{example}

\end{frame}


\subsection{Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose that $A\sim B$ so $A=PBP^{-1}$. \pause Then
    \begin{description}
    \item[rank] $\rank(A)=\rank(B)$ \pause
    \item[char-poly] $\chi_A(t)=\chi_B(t)$ \pause
    \item[trace] $\trace(A)=\trace(B)$ \pause
    \item[det] $\det(A)=\det(B)$ \pause
    \item[e-vals] $\EVals(A)=\EVals(B)$ \pause
    \item[alg-mults] $\am_A(\lambda)=\am_B(\lambda)$ \pause
    \item[geo-mults] $\gm_A(\lambda)=\gm_B(\lambda)$ \pause
    \item[e-spaces] $\mathcal{E}_A(\lambda)=P\cdot\mathcal{E}_B(\lambda)$
    \end{description}
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider again our similar matrices
    \begin{gather*}
      \begin{align*}
        \overset{A}{\sage{A}} = \overset{P}{\sage{P}}\overset{B}{\sage{B}}\overset{P^{-1}}{\sage{P.inverse()}}
      \end{align*}
    \end{gather*}
    \onslide<2->{The matrix $B$ is \emph{much} easier to understand than $A$.}
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\rank(A)} &\onslide<2->{=} \onslide<3->{\rank(B) =} \onslide<4->{\sage{B.rank()}} & \onslide<5->{\trace(A)} &\onslide<5->{=} \onslide<6->{\trace(B) =} \onslide<7->{\sage{B.trace()}} & \onslide<8->{\det(A)} &\onslide<8->{=} \onslide<9->{\det(B) =} \onslide<10->{\sage{B.det()}}
      \end{align*}
    \end{gather*}
    \onslide<11->{We also have $\chi_A(t)=\onslide<12->{\chi_B(t)=}\onslide<13->{t^2\cdot(t+3)$.}}
    % \begin{center}
    %   \begin{table}
    %     \centering
    %     %     \caption*{\myotherbold{#1}}
    %     \begin{tabular}{rcc}
    %       \toprule
    %       \myotherbold{$\lambda$} & \myotherbold{$\gm_A(\lambda)=\gm_B(\lambda)$} & \myotherbold{$\am_A(\lambda)=\am_A(\lambda)$} \\
    %       \midrule
    %       $0$ & $1$ & $2$ \\
    %       $-3$ & $1$ & $1$ \\
    %       \bottomrule
    %     \end{tabular}
    %   \end{table}
    % \end{center}
  \end{example}

\end{frame}


\begin{sagesilent}
  v1 = vector([0, 3, 1])
  v2 = vector([0, 0, 1])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The similarity relation is also useful for understanding eigenvalues.
    \begin{gather*}
      \begin{align*}
        \overset{A}{\sage{A}} = \overset{P}{\sage{P}}\overset{B}{\sage{B}}\overset{P^{-1}}{\sage{P.inverse()}}
      \end{align*}
    \end{gather*}
    \onslide<2->{The eigenspaces of $B$ are}
    \begin{align*}
      \onslide<2->{\mathcal{E}_B(0)} &\onslide<2->{=} \onslide<3->{\Span\Set{\sage{v1}}} & \onslide<4->{\mathcal{E}_B(-3)} &\onslide<4->{=} \onslide<5->{\Span\Set{\sage{v2}}}
    \end{align*}
    \onslide<6->{The equation $\mathcal{E}_A(\lambda)=P\cdot\mathcal{E}_B(\lambda)$ gives}
    \begin{gather*}
      \begin{align*}
        \onslide<7->{\mathcal{E}_A(0)} &\onslide<7->{=} \onslide<8->{\Span\Set{P\cdot\sage{v1}}}  & \onslide<10->{\mathcal{E}_A(-3)} &\onslide<10->{=} \onslide<11->{\Span\Set{P\cdot\sage{v2}}} \\
                                       &\onslide<8->{=} \onslide<9->{\Span\Set{\sage{P*v1}}}      &                                  &\onslide<11->{=} \onslide<12->{\Span\Set{\sage{P*v2}}}
      \end{align*}
    \end{gather*}
  \end{example}

\end{frame}


\section{Diagonalizable Matrices}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    We say that $A$ is \emph{diagonalizable} if $A$ is similar to a diagonal matrix.
  \end{definition}

  \pause
  \begin{block}{Observation}
    Diagonalizable means $A=PDP^{-1}$ where $D$ is diagonal.
  \end{block}

  \pause
  \begin{alertblock}{Warning}
    Strang writes $A=X\Lambda X^{-1}$.
  \end{alertblock}

\end{frame}


\begin{sagesilent}
  l1, l2 = -7, 4
  D = diagonal_matrix([l1, l1, l2])
  set_random_seed(2871)
  P = random_matrix(ZZ, D.nrows(), algorithm='unimodular', upper_bound=4)
  A = P*D*P.inverse()
  e1, e2, e3 = identity_matrix(A.nrows()).columns()
  p1, p2, p3 = P.columns()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myPA}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{p1}$};
      \onslide<9->{
      \node[overlay, below left= 1mm and -0.25cm of a] (text) {$1$st column of $P$};
      \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myPB}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{p2}$};
      \onslide<10->{
      \node[overlay, below left= 6mm and -0.25cm of a] (text) {$2$nd column of $P$};
      \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \newcommand{\myPC}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\sage{p3}$};
      \onslide<13->{
      \node[overlay, below left= 1mm and 0.125cm of a] (text) {$3$rd column of $P$};
      \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (text.east);
      }
    }
  }
  \begin{example}
    Consider the factorization
    \begin{gather*}
      \overset{A}{\sage{A}}
      =
      \overset{P}{\sage{P}}
      \overset{D}{\sage{D}}
      \overset{P^{-1}}{\sage{P.inverse()}}
    \end{gather*}
    \onslide<2->{The eigenspaces of $D$ are}
    \begin{align*}
      \onslide<2->{\mathcal{E}_D(\sage{l1})} &\onslide<2->{=} \onslide<3->{\Span\Set{\sage{e1}, \sage{e2}}} & \onslide<4->{\mathcal{E}_D(\sage{l2})} &\onslide<4->{=} \onslide<5->{\Span\Set{\sage{e3}}}
    \end{align*}
    \onslide<6->{The eigenspaces of $A$ are then}
    \begin{gather*}
      \begin{align*}
        \onslide<7->{\mathcal{E}_A(\sage{l1})} &\onslide<7->{=} \onslide<8->{\Span\Set{\myPA, \myPB}} & \onslide<11->{\mathcal{E}_D(\sage{l2})} &\onslide<11->{=} \onslide<12->{\Span\Set{\myPC}}
      \end{align*}
    \end{gather*}

  \end{example}

\end{frame}



\begin{sagesilent}
  A = matrix([(0, 1), (0, 0)])
  var('t')
  chi = A.characteristic_polynomial(t)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the data
    \begin{align*}
      A &= \sage{A} & \chi_A(t) &= \sage{chi}
    \end{align*}
    \onslide<2->{If $A$ were diagonalizable, the only choice for $D$ would be}
    \begin{align*}
      \onslide<2->{D} &\onslide<2->{=} \onslide<3->{\sage{zero_matrix(A.nrows())}} & \onslide<4->{A} &\onslide<4->{=} \onslide<5->{P\overset{D}{\sage{zero_matrix(A.nrows())}}P^{-1} =} \onslide<6->{\sage{zero_matrix(A.nrows())}}\onslide<7->{\neq A}
    \end{align*}
    \onslide<8->{The matrix $A$ is \emph{not diagonalizable}!}
  \end{example}

  \onslide<9->
  \begin{block}{Observation}
    Some matrices are not diagonalizable!
  \end{block}

\end{frame}


\subsection{Criteria for Diagonalizability}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    How can we determine if a matrix is diagonalizable?
  \end{block}

  \pause
  \begin{theorem}[Tests for Diagonalizability]
    An $n\times n$ matrix $A$ is diagonalizable if and only if
    \[
      \sum_{\lambda\in\EVals(A)}\gm_A(\lambda)=n
    \]
    \pause or (equivalently) if each eigenvalue $\lambda$ satisfies
    $\gm_A(\lambda)=\am_A(\lambda)$.
  \end{theorem}

\end{frame}


\begin{sagesilent}
  l1, l2 = -7, 19
  set_random_seed(702)
  A = random_matrix(ZZ, 3, algorithm='diagonalizable', eigenvalues=(l1, l2), dimensions=(1, 2))
  Id = identity_matrix(A.nrows())
  N = lambda l: A-l*Id
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the data
    \begin{align*}
      A &= \sage{A} & \EVals(A) &= \Set{\sage{l1}, \sage{l2}}
    \end{align*}
    \onslide<2->{The geometric multiplicities of the eigenvalues are}
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\gm_A(\sage{l1})} &\onslide<2->{=} \onslide<3->{\nullity\overset{\onslide<4->{\rank=}\onslide<5->{2}}{\sage{N(l1)}} =} \onslide<6->{\sage{N(l1).right_nullity()}} & \onslide<7->{\gm_A(\sage{l2})} &\onslide<8->{=} \onslide<9->{\nullity\overset{\onslide<10->{\rank=}\onslide<11->{1}}{\sage{N(l2)}} =} \onslide<12->{\sage{N(l2).right_nullity()}}
      \end{align*}
    \end{gather*}
    \onslide<13->{Summing the geometric multiplicities gives}
    \[
      \onslide<14->{\gm_A(\sage{l1})+\gm_A(\sage{l2})=}\onslide<15->{\sage{N(l1).right_nullity()}+\sage{N(l2).right_nullity()}=}\onslide<16->{\sage{N(l1).right_nullity()+N(l2).right_nullity()}}
    \]
    \onslide<17->{The matrix $A$ \emph{is diagonalizable}.}
  \end{example}

\end{frame}


\begin{sagesilent}
  l1, l2 = -4, 3
  J1 = jordan_block(l1, 2)
  J2 = jordan_block(l2, 2)
  J = block_diagonal_matrix([J1, J2])
  P = random_matrix(ZZ, J.nrows(), algorithm='unimodular', upper_bound=4)
  A = P*J*P.inverse()
  var('t')
  chi = factor(A.characteristic_polynomial(t))
  Id = identity_matrix(A.nrows())
  N = lambda l: A-l*Id
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the data
    \begin{gather*}
      \begin{align*}
        A &= \sage{A} & \chi_A(t) &= \sage{chi}
      \end{align*}
    \end{gather*}
    \onslide<2->{The geometric multiplicity of $\lambda=\sage{l1}$ is}
    \begin{gather*}
      \onslide<2->{\gm_A(\sage{l1}) =}
      \onslide<3->{\nullity\overset{A+4\cdot I_4}{\sage{N(l1)}} =}
      \onslide<4->{\nullity\overset{\rref(A+4\cdot I_4)}{\sage{N(l1).rref()}} =}
      \onslide<5->{\sage{N(l1).right_nullity()} <}
      \onslide<6->{\am_A(\sage{l1})}
    \end{gather*}
    \onslide<7->{The matrix $A$ is \emph{not diagonalizable} since $\gm_A(\sage{l1})\neq\am_A(\sage{l1})$.}
  \end{example}

\end{frame}


\subsection{The Diagonalization Algorithm}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    How do we find $A=PDP^{-1}$?
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{The Diagonalization Algorithm}
    To diagonalize an $n\times n$ matrix $A$, start with empty $P$ and $D$. \pause
    \begin{description}[\quad Step 1]
    \item[Step 1] Find the eigenvalues of $A$ and test if $A$ is diagonalizable\pause
      \begin{description}[\quad Test 1]
      \item[Test 1] $\gm_A(\lambda)=\am_A(\lambda)$\pause
      \item[Test 2] $\sum \gm_A(\lambda)=n$\pause
      \end{description}
    \item[Step 2] Loop through each eigenvalue $\lambda$.\pause
      \begin{description}
      \item[Find a basis] $\mathcal{E}_A(\lambda)=\Span\Set{\bv{v}_1,\dotsc,\bv{v}_d}$\pause
      \item[Alter $P$] Put $\Set{\bv{v}_1,\dotsc,\bv{v}_d}$ into the columns.\pause
      \item[Alter $D$] Put $\lambda$ on the diagonal $d$ times.\pause
      \end{description}
    \end{description}
    This produces $A=PDP^{-1}$.
  \end{block}

\end{frame}



\begin{sagesilent}
  set_random_seed(81120)
  P = random_matrix(ZZ, 3, algorithm='unimodular', upper_bound=4)
  l1, l2, l3 = -7, -7, 18
  D = diagonal_matrix([l1, l2, l3])
  A = P*D*P.inverse()
  p1, p2, p3 = P.columns()
  p11, p12, p13, p21, p22, p23, p31, p32, p33 = P._list()
  var('t')
  chi = factor(A.characteristic_polynomial())
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myP}{
    \left[
      \begin{array}{rrr}
        \onslide<4->{{\color{red}\sage{p11}}} & \onslide<7->{{\color{beamblue}\sage{p12}}} & \onslide<10->{{\color{beamgreen}\sage{p13}}} \\
        \onslide<4->{{\color{red}\sage{p21}}} & \onslide<7->{{\color{beamblue}\sage{p22}}} & \onslide<10->{{\color{beamgreen}\sage{p23}}} \\
        \onslide<4->{{\color{red}\sage{p31}}} & \onslide<7->{{\color{beamblue}\sage{p32}}} & \onslide<10->{{\color{beamgreen}\sage{p33}}}
      \end{array}
    \right]
  }
  \newcommand{\myD}{
    \left[
      \begin{array}{rrr}
        \onslide<5->{{\color{red}\sage{l1}}} &                                           &           \\
                                             & \onslide<8->{{\color{beamblue}\sage{l2}}} &           \\
                                             &                                           & \onslide<11->{{\color{beamgreen}\sage{l3}}}
      \end{array}
    \right]
  }
  \begin{example}
    Consider the data
    \begin{gather*}
      \begin{align*}
        \overset{A}{\sage{A}} &&  \overset{P}{\myP} && \overset{D}{\myD}
      \end{align*}
    \end{gather*}
    \onslide<2->The eigenspaces of $A$ are
    \begin{gather*}
      \begin{align*}
        \mathcal{E}_A(\sage{l1}) &= \Span\Set{{\color<3->{red}\sage{p1}}, {\color<6->{beamblue}\sage{p2}}} & \mathcal{E}_A(\sage{l3}) &= \Span\Set{{\color<9->{beamgreen}\sage{p3}}}
      \end{align*}
    \end{gather*}
    \onslide<12->This gives $A=PDP^{-1}$.
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\scalemath}[2]{\scalebox{####1}{\mbox{\ensuremath{\displaystyle ####2}}}}
  \newcommand{\myScale}{0.75}
  \newcommand{\myP}{
    \left[
      \scalemath{\myScale}{
        \begin{array}{rrr}
          {\color{red}\sage{p11}} & {\color{beamblue}\sage{p12}} & {\color{beamgreen}\sage{p13}} \\
          {\color{red}\sage{p21}} & {\color{beamblue}\sage{p22}} & {\color{beamgreen}\sage{p23}} \\
          {\color{red}\sage{p31}} & {\color{beamblue}\sage{p32}} & {\color{beamgreen}\sage{p33}}
        \end{array}
      }
    \right]
  }
  \newcommand{\myD}{
    \left[
      \scalemath{\myScale}{
        \begin{array}{rrr}
          {\color{red}\sage{l1}} &                             &           \\
                                 & {\color{beamblue}\sage{l2}} &           \\
                                 &                             & {\color{beamgreen}\sage{l3}}
        \end{array}
      }
    \right]
  }
  \newcommand{\myPx}{
    \left[
      \scalemath{\myScale}{
        \begin{array}{rrr}
          {\color{beamblue}\sage{p12}} & {\color{red}\sage{p11}} & {\color{beamgreen}\sage{p13}} \\
          {\color{beamblue}\sage{p22}} & {\color{red}\sage{p21}} & {\color{beamgreen}\sage{p23}} \\
          {\color{beamblue}\sage{p32}} & {\color{red}\sage{p31}} & {\color{beamgreen}\sage{p33}}
        \end{array}
      }
    \right]
  }
  \newcommand{\myDx}{
    \left[
      \scalemath{\myScale}{
        \begin{array}{rrr}
          {\color{beamblue}\sage{l2}} &                             &           \\
                                      & {\color{red}\sage{l1}}      &           \\
                                      &                             & {\color{beamgreen}\sage{l3}}
        \end{array}
      }
    \right]
  }
  \newcommand{\myPy}{
    \left[
      \scalemath{\myScale}{
        \begin{array}{rrr}
          {\color{beamgreen}\sage{p13}} & {\color{beamblue}\sage{p12}} & {\color{red}\sage{p11}}  \\
          {\color{beamgreen}\sage{p23}} & {\color{beamblue}\sage{p22}} & {\color{red}\sage{p21}}  \\
          {\color{beamgreen}\sage{p33}} & {\color{beamblue}\sage{p32}} & {\color{red}\sage{p31}}
        \end{array}
      }
    \right]
  }
  \newcommand{\myDy}{
    \left[
      \scalemath{\myScale}{
        \begin{array}{rrr}
          {\color{beamgreen}\sage{l3}} &                             &           \\
                                       & {\color{beamblue}\sage{l2}}      &           \\
                                       &                             & {\color{red}\sage{l1}}
        \end{array}
      }
    \right]
  }
  \newcommand{\myPz}{
    \left[
      \scalemath{\myScale}{
        \begin{array}{rrr}
          {\color{beamblue}\sage{p12}} & {\color{beamgreen}\sage{p13}} & {\color{red}\sage{p11}}  \\
          {\color{beamblue}\sage{p22}} & {\color{beamgreen}\sage{p23}} & {\color{red}\sage{p21}}  \\
          {\color{beamblue}\sage{p32}} & {\color{beamgreen}\sage{p33}} & {\color{red}\sage{p31}}
        \end{array}
      }
    \right]
  }
  \newcommand{\myDz}{
    \left[
      \scalemath{\myScale}{
        \begin{array}{rrr}
          {\color{beamblue}\sage{l2}} &                             &           \\
                                      & {\color{beamgreen}\sage{l3}}      &           \\
                                      &                             & {\color{red}\sage{l1}}
        \end{array}
      }
    \right]
  }
  \begin{example}
    Suppose we have $A=PDP^{-1}$ where
    \begin{align*}
      P &= \myP & D &= \myD
    \end{align*}
    \onslide<2->Other suitable choices for $A=PDP^{-1}$ are
    \begin{align*}
      \onslide<2->{P}&\onslide<2->{=\myPx} & \onslide<3->{D} &\onslide<3->{=\myDx} \\
      \onslide<4->{P}&\onslide<4->{=\myPy} & \onslide<5->{D} &\onslide<5->{=\myDy} \\
      \onslide<6->{P}&\onslide<6->{=\myPz} & \onslide<7->{D} &\onslide<7->{=\myDz}
    \end{align*}
  \end{example}

\end{frame}



\begin{sagesilent}
  A = matrix(SR, [(0, 1), (-1, 0)])
  D, P = A.jordan_form(transformation=True, subdivide=False)
  l1, l2 = D.diagonal()
  p1, p2 = P.columns()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myP}{
    \left[
      \begin{array}{rr}
        \onslide<4->{{\color{red}1}}  & \onslide<7->{{\color{beamblue}1}} \\
        \onslide<4->{{\color{red}-i}} & \onslide<7->{{\color{beamblue}i}}
      \end{array}
    \right]
  }
  \newcommand{\myD}{
    \left[
      \begin{array}{rr}
        \onslide<5->{{\color{red}-i}} & \\
                                      & \onslide<8->{{\color{beamblue}i}}
      \end{array}
    \right]
  }
  \newcommand{\myPx}{{\color<3->{red}\langle 1, -i\rangle}}
  \newcommand{\myPy}{{\color<6->{beamblue}\langle 1, i\rangle}}
  \begin{example}
    Consider the data
    \begin{gather*}
      \begin{align*}
        A &= \sage{A} & \mathcal{E}_A(\sage{l1}) &= \Span\Set{\myPx} & \mathcal{E}_A(\sage{l2}) &= \Span\Set{\myPy}
      \end{align*}
    \end{gather*}
    \onslide<2->This gives the diagonalization $A=PDP^{-1}$ where
    \begin{align*}
      P &= \myP & D &= \myD
    \end{align*}
  \end{example}

\end{frame}


% \section{The Fibonacci Numbers}
% \subsection{Definition}

% \begin{sagesilent}
%   f0, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10 = fibonacci_sequence(11)
% \end{sagesilent}
% \begin{frame}

%   \frametitle{\secname}
%   \framesubtitle{\subsecname}

%   \begin{definition}
%     The \emph{Fibonacci sequence} $\Set{F_k}$ is recursively defined by
%     \begin{align*}
%       \onslide<2->{F_0} &\onslide<2->{=} \onslide<3->{0} & \onslide<4->{F_1} &\onslide<4->{=} \onslide<5->{1} & \onslide<6->{F_{k+2}} &\onslide<6->{=} \onslide<7->{F_{k+1} + F_k}
%     \end{align*}
%     \onslide<8->{Evidently,
%       $\Set{F_k}=\Set{\sage{f0}, \sage{f1}, \sage{f2}, \sage{f3}, \sage{f4},
%         \sage{f5}, \sage{f6}, \sage{f7}, \sage{f8}, \sage{f9}, \sage{f10},
%         \dotsc}$.}
%   \end{definition}

% \end{frame}


% \subsection{Difference Equation}
% \begin{sagesilent}
%   A = matrix([(1, 1), (1, 0)])
% \end{sagesilent}
% \begin{frame}

%   \frametitle{\secname}
%   \framesubtitle{\subsecname}

%   \begin{block}{Question}
%     Can we efficiently compute $F_{100}$?
%   \end{block}

%   \newcommand{\vk}[2][k+1]{\langle F_{####1}, F_{####2}\rangle}
%   \newcommand{\mycol}[2]{\left[\begin{array}{c}####1\\ ####2\end{array}\right]}
%   \newcommand{\ck}[2][k+1]{\left[\begin{array}{c} F_{####1}\\ F_{####2}\end{array}\right]}
%   \newcommand{\myA}{
%     \left[
%       \begin{array}{rr}
%         1 & 1 \\ 1 & 0
%       \end{array}
%     \right]
%   }
%   \onslide<2->
%   \begin{block}{Observation}
%     Define $\bv{u}_k=\vk{k}$ so that
%     \[
%       \onslide<3->{\overset{\bv{u}_{k+1}}{\ck[k+2]{k+1}} =}
%       \onslide<4->{\mycol{F_{k+1}+F_k}{F_{k+1}} =}
%       \onslide<5->{\overset{A}{\myA}\overset{\bv{u}_k}{\ck{k}}}
%     \]
%     \onslide<6->{The equation $\bv{u}_{k+1}=A\bv{u}_k$ then gives}
%     \begin{align*}
%       \onslide<7->{\bv{u}_1} &\onslide<7->{=} \onslide<8->{A\bv{u}_0} & \onslide<9->{\bv{u}_2} &\onslide<9->{=} \onslide<10->{A\bv{u}_1 =} \onslide<11->{A^2\bv{u}_0} & \onslide<12->{\bv{u}_3} &\onslide<12->{=} \onslide<13->{A\bv{u}_2 =} \onslide<14->{A^3\bv{u}_0}
%     \end{align*}
%     \onslide<15->{Continuing this pattern gives $\bv{u}_k=A^k\bv{u}_0$.}
%   \end{block}

% \end{frame}


% \begin{frame}

%   \frametitle{\secname}
%   \framesubtitle{\subsecname}

%   \newcommand{\myA}{
%     \tikz[
%     , line join=round
%     , line cap=round
%     , baseline=(a.base)
%     , red
%     ]{
%       \node[inner sep=0pt, black] (a) {$A$};
%       \onslide<2->{
%       \node[overlay, above right= -2mm and 0.25cm of a] (text) {$A=\sage{A}$};
%       \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
%       }
%     }
%   }
%   \newcommand{\myDiff}{
%     \left[
%       \begin{array}{cr}
%         1-\lambda & 1 \\ 1 & -\lambda
%       \end{array}
%     \right]
%   }
%   \newcommand{\myRref}{
%     \left[
%       \begin{array}{rr}
%         1 & -\lambda \\ 0 & 0
%       \end{array}
%     \right]
%   }
%   \newcommand{\myP}{
%     \left[
%       \begin{array}{cc}
%         \lambda_1 & \lambda_2 \\
%         1 & 1
%       \end{array}
%     \right]
%   }
%   \newcommand{\myD}{
%     \left[
%       \begin{array}{rr}
%         \lambda_1 &  \\
%                   & \lambda_2
%       \end{array}
%     \right]
%   }
%   \begin{block}{Strategy}
%     We can compute $A^k$ by diagonalizing $\myA$.
%     \begin{align*}
%       \onslide<3->{\chi_A(t)} &\onslide<3->{=} \onslide<4->{t^2-t-1} & \onslide<5->{\lambda_1} &\onslide<5->{=} \onslide<6->{\frac{1-\sqrt{5}}{2}} & \onslide<7->{\lambda_2} &\onslide<7->{=} \onslide<8->{\frac{1+\sqrt{5}}{2}}
%     \end{align*}
%     \onslide<9->{Each eigenspace is of the form}
%     \[
%       \onslide<9->{\mathcal{E}_A(\lambda) =}
%       \onslide<10->{\Null\myDiff =}
%       \onslide<11->{\Null\myRref =}
%       \onslide<12->{\Span\Set{\langle \lambda, 1\rangle}}
%     \]
%     \onslide<13->{Our diagonalization $A=PDP^{-1}$ is given by}
%     \begin{align*}
%       \onslide<13->{P} &\onslide<13->{=} \onslide<14->{\myP} & \onslide<15->{D} &\onslide<15->{=} \onslide<16->{\myD}
%     \end{align*}
%   \end{block}

% \end{frame}


% \begin{frame}

%   \frametitle{\secname}
%   \framesubtitle{\subsecname}

%   \newcommand{\myU}[2]{
%     \left[
%       \begin{array}{c}
%         ####1 \\ ####2
%       \end{array}
%     \right]
%   }
%   \newcommand{\myP}{
%     \left[
%       \begin{array}{cc}
%         \lambda_1 & \lambda_2 \\
%         1 & 1
%       \end{array}
%     \right]
%   }
%   \newcommand{\myD}{
%     \left[
%       \begin{array}{rr}
%         \lambda_1^k &  \\
%                     & \lambda_2^k
%       \end{array}
%     \right]
%   }
%   \newcommand{\myPi}{
%     \frac{1}{\lambda_1-\lambda_2}
%     \left[
%       \begin{array}{rr}
%         1 & -\lambda_2 \\
%         -1 & \lambda_1
%       \end{array}
%     \right]
%   }
%   \newcommand{\myPDk}{
%     \left[
%       \begin{array}{ll}
%         \lambda_1^{k+1} & \lambda_2^{k+1} \\
%         \lambda_1^k & \lambda_2^k
%       \end{array}
%     \right]
%   }
%   \newcommand{\myPadjU}{
%     \left[
%       \begin{array}{rr}
%         1\\ -1
%       \end{array}
%     \right]
%   }
%   \newcommand{\myUk}{
%     \left[
%       \begin{array}{c}
%         \lambda_2^{k+1}-\lambda_1^{k+1} \\ \lambda_2^k-\lambda_1^k
%       \end{array}
%     \right]
%   }
%   \begin{block}{Computation}
%     Now, the equation $\bv{u}_k=A^k\bv{u}_0$ takes the form
%     \begin{gather*}
%       \begin{align*}
%         \onslide<2->{\overset{\bv{u}_k}{\myU{F_{k+1}}{F_{k}}}}
%         &\onslide<2->{=}
%           \onslide<3->{\overset{P}{\myP}
%           \overset{D^k}{\myD}
%           \overset{P^{-1}}{\pair*{\myPi}}
%           \overset{\bv{u}_0}{\myU{1}{0}}} \\
%         &\onslide<3->{=} \onslide<4->{-\frac{1}{\sqrt{5}}\myPDk\myPadjU} \\
%         &\onslide<4->{=} \onslide<5->{\frac{1}{\sqrt{5}}\myUk}
%       \end{align*}
%     \end{gather*}
%     \onslide<6->{This gives a formula for the $k$th Fibonacci number}
%     \[
%       \onslide<6->{F_k=}\onslide<7->{\frac{\lambda_2^k-\lambda_1^k}{\sqrt{5}}}
%     \]
%     \onslide<8->{In particular, $F_{100}=\onslide<9->{\sage{fibonacci(100)}$.}}
%   \end{block}

% \end{frame}


% \subsection{The Golden Ratio}
% \begin{frame}

%   \frametitle{\secname}
%   \framesubtitle{\subsecname}

%   \begin{block}{Observation}
%     The number $\phi=\displaystyle\frac{1+\sqrt{5}}{2}$ is called the
%     \emph{golden ratio}.\pause
%     \[
%       \begin{tikzpicture}[line join=round, line cap=round]
%         \coordinate (start) at (0, 0);
%         \coordinate (end) at (9, 0);

%         \pgfmathsetmacro{\myPhi}{2/(1+sqrt(5))}
%         \coordinate (a) at ($ \myPhi*(end) $);

%         \coordinate (tick) at (0, 3pt);

%         \draw[ultra thick] (start) -- (a) node [midway, above] {$a$};
%         \draw[ultra thick] (a) -- (end) node [midway, above] {$b$};

%         \draw[ultra thick] ($ (start)+(tick) $) -- ($ (start)-(tick) $);
%         \draw[ultra thick] ($ (end)+(tick) $) -- ($ (end)-(tick) $);
%         \draw[ultra thick] ($ (a)+(tick) $) -- ($ (a)-(tick) $);
%       \end{tikzpicture}
%     \]\pause
%     This figure is considered aesthetically pleasing when
%     \[
%       \frac{a}{b}=\frac{a+b}{a}
%     \]\pause
%     By defining $\phi=\frac{a}{b}$, we obtain \pause
%     \[
%       \phi
%       = \pause\frac{a}{b}
%       = \pause\frac{a+b}{a}
%       = \pause1+\frac{b}{a}
%       = \pause1+\frac{1}{\phi}
%     \]\pause
%     Then $\phi^2=\pause\phi+1$ \pause so that $\phi^2-\phi-1=\pause 0$.
%   \end{block}


% \end{frame}

\end{document}
