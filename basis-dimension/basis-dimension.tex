\documentclass[usenames,dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}

\title{Bases and Dimension}
\subtitle{Math 218}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Geometric Motivation}
\subsection{Visualizing Vector Spaces}


\begin{sagesilent}
  v = vector([1, -7, 3])
  N = matrix(QQ, v).right_kernel(basis='pivot').basis_matrix()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    What does $\mathcal{L}=\Span\Set{\sage{v}}$ ``look like''?
  \end{block}
  \noindent
  \begin{minipage}{.6\textwidth}
    \onslide<2->
    \begin{block}{Answer}
      $\mathcal{L}$ consists of all ``multiples'' of $\sage{v}$. \onslide<5->
      $\mathcal{L}$ ``looks like'' a \emph{line} in
      $\mathbb{R}^{\sage{len(v)}}$.
    \end{block}
  \end{minipage}% This must go next to Y
  \begin{minipage}{.4\textwidth}
    \[
      \begin{tikzpicture}[line join=round, line cap=round]
        \coordinate(O) at (0, 0);
        \coordinate (v) at (2, 1);

        \onslide<4->
        \draw[ultra thick, teal, <->] ($ -.75*(v) $) -- ($ .75*(v) $) node[right] {$\mathcal{L}$};

        \onslide<3->
        \draw[ultra thick, blue, ->] (O) -- ($ .5*(v) $); % node[midway, above, sloped] {$\sage{v}$};
      \end{tikzpicture}
    \]
  \end{minipage}

  \onslide<6->
  \begin{block}{Note}
    We can represent $\mathcal{L}$ in many different ways.
    \[
      \onslide<7->
      \mathcal{L}
      =\Col\overset{\onslide<11->{\rank=1}}{\sage{matrix.column(2*v)}}
      \onslide<8->=\Col\overset{\onslide<12->{\rank=1}}{\sage{matrix.column([-2*v, 3*v])}}
      \onslide<9->=\Null\overset{\onslide<13->{\nullity=1}}{\sage{N}}
    \]
    \onslide<10-> We only ``need'' \emph{one} $\vv{v}$ to define
    $\mathcal{L}=\Span\Set{\vv{v}}$.
  \end{block}

\end{frame}



\begin{sagesilent}
  A = matrix.column([(3, -2, 1), (-6, 5, 5)])
  a1, a2 = A.columns()
  a3 = a1+a2
  N = A.T.right_kernel().basis_matrix()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    What does $\mathcal{P}=\Span\Set{\sage{a1}, \sage{a2}}$ ``look like''?
  \end{block}
  \noindent
  \begin{minipage}{.6\textwidth}
    \onslide<2->
    \begin{block}{Answer}
      $\mathcal{P}$ consists of all \emph{linear combinations} of $\sage{a1}$
      and $\sage{a2}$. \onslide<6-> $\mathcal{P}$ ``looks like'' a \emph{plane}
      in $\mathbb{R}^3$.
    \end{block}
  \end{minipage}% This must go next to Y
  \begin{minipage}{.4\textwidth}
    \[
      \begin{tikzpicture}[line cap=round, line join=round]
        \coordinate (O) at (0, 0);
        \coordinate (a1) at (2, 1);
        \coordinate (a2) at (1, -1);

        \pgfmathsetmacro{\myC}{.85}
        \onslide<5->
        \filldraw[ultra thick, draw=teal, fill=teal!35]
        ($ \myC*(a1) $) --
        ($ \myC*(a2) $) --
        ($ -\myC*(a1) $) --
        ($ -\myC*(a2) $) -- cycle
        node[above left, teal] {$\mathcal{P}$};

        \pgfmathsetmacro{\myP}{.65}
        \onslide<3->
        \draw[ultra thick, blue, ->] (O) -- ($ \myP*\myC*(a1) $);
        \onslide<4->
        \draw[ultra thick, blue, ->] (O) -- ($ \myP*\myC*(a2) $);
      \end{tikzpicture}
    \]
  \end{minipage}

  \onslide<7->
  \begin{block}{Note}
    We can represent $\mathcal{P}$ in many different ways.
    \begin{gather*}
      \onslide<8->{
      \mathcal{P}
      = \Col\overset{\onslide<12->{\rank=2}}{\sage{matrix.column([2*a1, -3*a2])}}}
      \onslide<9->{= \Col\overset{\onslide<13->{\rank=2}}{\sage{A.augment(a3)}}}
      \onslide<10->{= \Null\overset{\onslide<14->{\nullity=2}}{\sage{N}}}
    \end{gather*}
    \onslide<11-> We only ``need'' \emph{two} $\vv*{v}{1}$ and $\vv*{v}{2}$ to
    define $\mathcal{P}=\Span\Set{\vv*{v}{1}, \vv*{v}{2}}$.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Geometrically, the ``size'' of $\mathcal{L}$ is \emph{one} and the ``size''
    of $\mathcal{P}$ is \emph{two}.
  \end{block}

  \pause
  \begin{block}{Question}
    Is there a useful way to measure the ``size'' of a vector space $V$?
  \end{block}

  \pause
  \begin{block}{Answer}
    The \emph{dimension} of a vector space $V$ measures its ``size.'' \pause To
    define $\dim(V)$, we need to define the concept of a \emph{basis} of $V$.
  \end{block}

\end{frame}


\section{Definitions and Properties}
\subsection{Definition of a Basis}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Suppose that $V=\Span\Set{\vv*{v}{1}, \vv*{v}{2}, \dotsc, \vv*{v}{d}}$. We
    say that the list $\Set{\vv*{v}{1}, \vv*{v}{2}, \dotsc, \vv*{v}{d}}$ is a
    \emph{basis} of $V$ if it is linearly independent.
  \end{definition}

\end{frame}



\begin{sagesilent}
  var('x1 x2 x3')
  e1, e2, e3 = identity_matrix(SR, 3).columns()
  x = vector([x1, x2, x3])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the vectors $\vv*{e}{1}$, $\vv*{e}{2}$, and $\vv*{e}{3}$ given by
    \begin{align*}
      \vv*{e}{1} &= \sage{e1} & \vv*{e}{2} &= \sage{e2} & \vv*{e}{3} &= \sage{e3}
    \end{align*}
    \pause Note that every $\vv{x}\in\mathbb{R}^3$ may be written as
    \[
      \vv{x}
      = \sage{x}
      = x_1\cdot\sage{e1}+x_2\cdot\sage{e2}+x_3\cdot\sage{e3}
    \]
    \pause This means that
    $\mathbb{R}^3=\Span\Set{\vv*{e}{1}, \vv*{e}{2}, \vv*{e}{3}}$. \pause
    Moreover, the equation
    \[
      \rank\begin{bmatrix}\vv*{e}{1} & \vv*{e}{2} & \vv*{e}{3}\end{bmatrix}=3
    \]
    implies that $\Set{\vv*{e}{1}, \vv*{e}{2}, \vv*{e}{3}}$ is \emph{linearly
      independent}. \pause This means that
    $\Set{\vv*{e}{1}, \vv*{e}{2}, \vv*{e}{3}}$ is a basis of $\mathbb{R}^3$.
  \end{example}

\end{frame}




\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The list $\Set{\vv*{e}{1},\vv*{e}{2},\dotsc,\vv*{e}{n}}$ is a basis of
    $\mathbb{R}^n$.
  \end{theorem}

\end{frame}




\subsection{Properties of Bases}

\begin{sagesilent}
  set_random_seed(749)
  A = random_matrix(ZZ, 3, 5)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose that $\Set{\vv*{v}{1},\dotsc,\vv*{v}{d}}$ is a basis of $V$ and that
    $\Set{\vv*{w}{1},\dotsc,\vv*{w}{k}}$ is linearly independent in $V$. Then
    $k\leq d$.
  \end{theorem}

  \pause
  \begin{example}
    Recall that $\Set{\vv*{e}{1}, \vv*{e}{2}, \vv*{e}{3}}$ is a basis of
    $\mathbb{R}^3$. \pause This theorem imlies that the columns of
    \[
      \sage{A}
    \]
    form a \emph{linearly \pause dependent} list because
    $\sage{A.ncols()}>\pause\sage{A.nrows()}$.
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose that $\Set{\vv*{v}{1},\dotsc,\vv*{v}{k}}$ and
    $\Set{\vv*{w}{1},\dotsc,\vv*{w}{\ell}}$ are bases of $V$. Then $k=\ell$.
  \end{theorem}

  \pause
  \begin{block}{Note}
    This theorem says that any two bases of $V$ have the same number of vectors.
  \end{block}

\end{frame}





\subsection{Definition of Dimension}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Suppose that $\Set{\vv*{v}{1},\dotsc, \vv*{v}{d}}$ is a basis of $V$. The
    \emph{dimension} of $V$ is $\dim(V)=d$.
  \end{definition}

  \pause
  \begin{block}{Note}
    The dimension of $V$ is unambiguous since any two bases of $V$ have the same
    number of vectors.
  \end{block}

  \pause
  \begin{block}{Intuition}
    The dimension of $V$ is a measurement of how ``large'' $V$ is.
  \end{block}

  \pause
  \begin{example}
    $\dim(\mathbb{R}^n)=\pause n$ \pause because
    $\Set{\vv*{e}{1},\dotsc,\vv*{e}{n}}$ is a basis of $\mathbb{R}^n$.
  \end{example}

\end{frame}




\subsection{Examples}
\begin{sagesilent}
  set_random_seed(37913)
  A = random_matrix(ZZ, 3, 1, algorithm='echelonizable', rank=1)
  a1, = A.columns()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    \noindent
    \begin{minipage}{.6\textwidth}
      Consider the vector space $V$ given by
      \[
        V = \Col\overset{\onslide<7->{\rank=\sage{A.rank()}}}{\sage{A}}
      \]
      What is the dimension of $V$?
    \end{minipage}% This must go next to Y
    \begin{minipage}{.4\textwidth}
      \[
        \begin{tikzpicture}[line join=round, line cap=round]
          \coordinate(O) at (0, 0);
          \coordinate (v) at (2, 1);

          \onslide<3->{
            \draw[ultra thick, teal, <->] ($ -.75*(v) $) -- ($ .75*(v) $) node[right] {$V$};
          }

          \onslide<2->{
            \draw[ultra thick, blue, ->] (O) -- ($ .5*(v) $); % node[midway, above, sloped] {$\sage{v}$};
          }
        \end{tikzpicture}
      \]
    \end{minipage}
  \end{example}

  \onslide<4->{
  \begin{block}{Solution}
    Note that $V=\Span\Set{\sage{a1}}$ and $\Set{\sage{a1}}$ is linearly
    independent. \onslide<5-> So, $\Set{\sage{a1}}$ is a basis of $V$ and
    $\dim(V)=\sage{A.rank()}$.
  \end{block}
  }

  \onslide<6->{
  \begin{block}{Math-Speak}
    ``$V$ is a \emph{one-dimensional} vector subspace of $\mathbb{R}^{\sage{A.nrows()}}$.''
  \end{block}
  }

\end{frame}


\begin{sagesilent}
  set_random_seed(3791)
  A = random_matrix(ZZ, 4, 2, algorithm='echelonizable', rank=2)
  a1, a2 = A.columns()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    \noindent
    \begin{minipage}{.6\textwidth}
      Consider the vector space $V$ given by
      \[
        V = \Col\overset{\onslide<10->{\rank=\sage{A.rank()}}}{\sage{A}}
      \]
      What is the dimension of $V$?
    \end{minipage}% This must go next to Y
    \begin{minipage}{.4\textwidth}
      \[
        \begin{tikzpicture}[line cap=round, line join=round]
          \coordinate (O) at (0, 0);
          \coordinate (a1) at (2, 1);
          \coordinate (a2) at (1, -1);

          \pgfmathsetmacro{\myC}{.85}
          \onslide<4->{
            \filldraw[ultra thick, draw=teal, fill=teal!35]
            ($ \myC*(a1) $) --
            ($ \myC*(a2) $) --
            ($ -\myC*(a1) $) --
            ($ -\myC*(a2) $) -- cycle
            node[above left, teal] {$V$};
          }

          \pgfmathsetmacro{\myP}{.65}
          \onslide<2->{
            \draw[ultra thick, blue, ->] (O) -- ($ \myP*\myC*(a1) $);
          }
          \onslide<3->{
            \draw[ultra thick, blue, ->] (O) -- ($ \myP*\myC*(a2) $);
          }
        \end{tikzpicture}
      \]
    \end{minipage}
  \end{example}

  \onslide<5->{
    \begin{block}{Solution}
      Let $\beta=\Set{\sage{a1}, \sage{a2}}$.  \onslide<6-> Note that $\beta$ is
      linearly independent and spans $V$. \onslide<7-> So, $\beta$ is a basis of $V$ and
      $\dim(V)=\onslide<8->\sage{A.rank()}$.
    \end{block}
  }

  \onslide<9->{
    \begin{block}{Math-Speak}
      ``$V$ is a \emph{two-dimensional} vector subspace of $\mathbb{R}^{\sage{A.nrows()}}$.''
    \end{block}
  }

\end{frame}


\begin{sagesilent}
  set_random_seed(971)
  A = random_matrix(ZZ, 6, 4, algorithm='echelonizable', rank=4)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the vector space $V$ given by
    \[
      V = \Col\overset{\rank(A)=\sage{A.rank()}}{\scalebox{.75}{$\sage{A}$}}
    \]
    What is the dimension of $V$?
  \end{example}

  \pause
  \begin{block}{Solution}
    The columns of $A$ are linearly independent and span $V$. \pause So, the
    columns of $A$ form a basis of $V$ and $\dim(V)=4$.
  \end{block}

  \pause
  \begin{block}{Math-Speak}
    ``$V$ is a \emph{four-dimensional} vector subspace of
    $\mathbb{R}^{\sage{A.nrows()}}$.''
  \end{block}

\end{frame}


\section{The Four Fundamental Subspaces}
\subsection{Bases of Column Spaces}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Questions}
    How can we find a basis of $\Col(A)$? \onslide<2->What is $\dim\Col(A)$?
    \onslide<3->
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , scale=4/5
        ]

        \coordinate (O) at (0, 0);
        \coordinate (P) at (1, 2);
        \coordinate (Q) at (2, -1);
        \coordinate (Rn) at ($ 1.15*(P)-(Q) $);
        \coordinate (Row) at ($ .5*(P)-(Q) $);
        \coordinate (Null) at ($ .5*(Q)-.5*(P) $);

        \node at (Rn) {$\mathbb{R}^n$};

        \filldraw[ultra thick, fill=YellowOrange]
        (O) -- (P) -- ($ (P)-2*(Q) $) -- ($ -2*(Q) $) -- cycle;
        \node at (Row) {$\Row(A)$};

        \filldraw[ultra thick, fill=Green!40]
        (O) -- (Q) -- ($ (Q)-(P) $) -- ($ -1*(P) $) -- cycle;
        \node at (Null) {$\Null(A)$};

        \coordinate (v) at (-1, 2);
        \coordinate (w) at (-2, -1);
        \coordinate (Rm) at ($ 1.15*(v)-1*(w) $);
        \coordinate (Col) at ($ .5*(v)-1*(w) $);
        \coordinate (LNull) at ($ .5*(w)-.5*(v) $);

        \tikzset{
          c/.style={every coordinate/.try}
        }

        \begin{scope}[every coordinate/.style={shift={(5,0)}}]


          \node at ([c]Rm) {$\mathbb{R}^m$};

          \filldraw[ultra thick, fill=BrickRed!50]
          ([c]O) -- ([c]v) -- ([c]$ (v)-2*(w) $) -- ([c]$ -2*(w) $) -- cycle;
          \node at ([c]Col) {$\underset{\onslide<4->{\dim=?}}{\Col(A)}$};

          \filldraw[ultra thick, fill=Blue!50]
          ([c]O) -- ([c]w) -- ([c]$ (w)-(v) $) -- ([c]$ -1*(v) $) -- cycle;
          \node at ([c]LNull) {$\LNull(A)$};


          \draw[ultra thick, shorten >=1.5em, shorten <=1.5em, ->]
          (Rn) -- ([c]Rm) node[midway, above] {$A$};

        \end{scope}

      \end{tikzpicture}
    \]
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    The columns of $A$ span $\Col(A)$. However, the columns of $A$ might not be
    linearly independent.
  \end{block}

  \pause
  \begin{block}{Idea}
    ``Purge'' the \emph{nonpivot columns} of $A$ to obtain a basis of $\Col(A)$.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[``Pivot Bases of $\Col(A)$'']
    The pivot columns of $A$ form a basis of $\Col(A)$.
  \end{theorem}

  \pause
  \begin{theorem}[``Reduced Bases of $\Col(A)$'']
    The nonzero rows of $\rref(A^\intercal)$ form a basis of $\Col(A)$.
  \end{theorem}

  \pause
  \begin{theorem}
    $\dim\Col(A)=\rank(A)$
  \end{theorem}

\end{frame}




\begin{sagesilent}
  set_random_seed(82)
  A = random_matrix(ZZ, 4, 3, algorithm='echelonizable', rank=2)
  a1, a2, a3 = A.columns()
  r1, r2, r3 = A.T.rref().rows()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the computations
    \begin{gather*}
      \begin{align*}
        \rref\overset{A}{\sage{A}} &= \sage{A.rref()} & \rref\overset{A^\intercal}{\sage{A.T}} &= \sage{A.T.rref()}
      \end{align*}
    \end{gather*}
    \pause The ``pivot basis'' and the ``reduced basis'' of $\Col(A)$ are given
    by
    \begin{gather*}
      \begin{align*}
        \overset{\myotherbold{\textnormal{``pivot basis''}}}{\Set{\sage{a1}, \sage{a2}}} && \overset{\myotherbold{\textnormal{``reduced basis''}}}{\Set{\sage{r1}, \sage{r2}}}
      \end{align*}
    \end{gather*}
    \pause Note that $\dim\Col(A)=\rank(A)=\sage{A.rank()}$.
  \end{example}

  \pause
  \begin{block}{Math-Speak}
    ``$\Col(A)$ is a \emph{two-dimensional} vector subspace of $\mathbb{R}^4$.''
  \end{block}
\end{frame}


\begin{sagesilent}
  set_random_seed(831)
  A = random_matrix(ZZ, 3, 5, algorithm='echelonizable', rank=2)
  while 1 in A.pivots(): A = random_matrix(ZZ, 3, 5, algorithm='echelonizable', rank=2)
  a1, a2, a3, a4, a5 = map(matrix.column, A.columns())
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the vector space $V$ given by
    \[
      V = \Span\Set*{\sage{a1}, \sage{a2}, \sage{a3}, \sage{a4}, \sage{a5}}
    \]
    \pause Note that $V=\Col(A)$ where
    \[
      \rref\overset{A}{\sage{A}}=\sage{A.rref()}
    \]
    \pause So $\dim(V)=\pause\sage{A.rank()}$ \pause and
    $\Set{\sage{vector(a1)}, \sage{vector(a3)}}$ is a basis of $V$.
  \end{example}

\end{frame}


\subsection{Bases of Null Spaces}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Questions}
    How can we find a basis of $\Null(A)$? \onslide<2-> What is $\dim\Null(A)$? \onslide<3->
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , scale=4/5
        ]

        \coordinate (O) at (0, 0);
        \coordinate (P) at (1, 2);
        \coordinate (Q) at (2, -1);
        \coordinate (Rn) at ($ 1.15*(P)-(Q) $);
        \coordinate (Row) at ($ .5*(P)-(Q) $);
        \coordinate (Null) at ($ .5*(Q)-.5*(P) $);

        \node at (Rn) {$\mathbb{R}^n$};

        \filldraw[ultra thick, fill=YellowOrange]
        (O) -- (P) -- ($ (P)-2*(Q) $) -- ($ -2*(Q) $) -- cycle;
        \node at (Row) {$\Row(A)$};

        \filldraw[ultra thick, fill=Green!40]
        (O) -- (Q) -- ($ (Q)-(P) $) -- ($ -1*(P) $) -- cycle;
        \node at (Null) {$\underset{\onslide<6->{\dim=?}}{\Null(A)}$};

        \coordinate (v) at (-1, 2);
        \coordinate (w) at (-2, -1);
        \coordinate (Rm) at ($ 1.15*(v)-1*(w) $);
        \coordinate (Col) at ($ .5*(v)-1*(w) $);
        \coordinate (LNull) at ($ .5*(w)-.5*(v) $);

        \tikzset{
          c/.style={every coordinate/.try}
        }

        \begin{scope}[every coordinate/.style={shift={(5,0)}}]


          \node at ([c]Rm) {$\mathbb{R}^m$};

          \filldraw[ultra thick, fill=BrickRed!50]
          ([c]O) -- ([c]v) -- ([c]$ (v)-2*(w) $) -- ([c]$ -2*(w) $) -- cycle;
          \node at ([c]Col) {$\underset{\onslide<4->{\dim=}\onslide<5->{\rank(A)}}{\Col(A)}$};

          \filldraw[ultra thick, fill=Blue!50]
          ([c]O) -- ([c]w) -- ([c]$ (w)-(v) $) -- ([c]$ -1*(v) $) -- cycle;
          \node at ([c]LNull) {$\LNull(A)$};


          \draw[ultra thick, shorten >=1.5em, shorten <=1.5em, ->]
          (Rn) -- ([c]Rm) node[midway, above] {$A$};

        \end{scope}

      \end{tikzpicture}
    \]
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    When solving $A\vv{x}=\vv{O}$, the number of ``degrees of freedom'' is the
    number of free variables. \pause This suggests
    $\dim\Null(A)=\pause\nullity(A)$.
  \end{block}

\end{frame}




\begin{sagesilent}
  set_random_seed(17319)
  A = random_matrix(ZZ, 3, 4, algorithm='echelonizable', rank=2).rref()
  while 1 in A.pivots(): A = random_matrix(ZZ, 3, 4, algorithm='echelonizable', rank=2).rref()
  var('x1 x2 x3 x4')
  x = matrix.column([x1, x2, x3, x4])
  var('c1 c2')
  xs = x(x1=2*c1+2*c2, x3=-3*c2, x2=c1, x4=c2)
  B = jacobian(xs, (c1, c2))
  v1, v2 = map(matrix.column, B.columns())
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}
  \begin{example}
    Consider the general solution $\vv{x}$ to $A\vv{x}=\vv{O}$ given by
    \begin{gather*}
      \begin{align*}
        A &= \sage{A} & \vv{x} &= \sage{xs} = c_1\sage{v1}+c_2\sage{v2}
      \end{align*}
    \end{gather*}
    \pause This shows that
    \[
      \Null\overset{A}{\sage{A}}
      =
      \Col\overset{\rank(B)=\sage{B.rank()}}{\sage{B}}
    \]
    \pause The columns of $B$ form a basis of $\Null(A)$. \pause So
    $\dim\Null(A)=\pause\sage{A.right_nullity()}$.
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[``Pivot Bases of $\Null(A)$'']
    Use $\rref(A)$ to write the general solution $\vv{x}$ to $A\vv{x}=\vv{O}$ as
    \[
      \vv{x}
      = c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+\dotsb+c_d\cdot\vv*{v}{d}
    \]
    Then $\Set{\vv*{v}{1}, \vv*{v}{d}, \dotsc,\vv*{v}{d}}$ form a basis of
    $\Null(A)$.
  \end{theorem}

  \pause
  \begin{theorem}
    $\dim\Null(A)=\nullity(A)$
  \end{theorem}

\end{frame}



\begin{sagesilent}
  a = vector([3, -2, 8])
  A = matrix.column([a, -8*a, 6*a])
  var('x1 x2 x3')
  x = matrix.column([x1, x2, x3])
  var('c1 c2')
  xs = x(x1=8*c1-6*c2, x2=c1, x3=c2)
  B = jacobian(xs, (c1, c2))
  v1, v2 = map(matrix.column, B.columns())
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the computation
    \[
      \rref\overset{A}{\sage{A}} = \sage{A.rref()}
    \]
    \pause So $\dim\Null(A)=\nullity(A)=\pause\sage{A.right_nullity()}$. \pause
    Solving $A\vv{x}=\vv{O}$ gives
    \[
      \vv{x}
      = \sage{x}
      = \sage{xs}
      = c_1\sage{v1}+c_2\sage{v2}
    \]
    \pause The ``pivot basis'' of $\Null(A)$ is
    $\Set{\sage{vector(v1)}, \sage{vector(v2)}}$.
  \end{example}

\end{frame}


\subsection{Bases of Row Spaces}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Questions}
    How can we find a basis of $\Row(A)$? \onslide<2-> What is $\dim\Row(A)$?
    \onslide<3->
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , scale=4/5
        ]

        \coordinate (O) at (0, 0);
        \coordinate (P) at (1, 2);
        \coordinate (Q) at (2, -1);
        \coordinate (Rn) at ($ 1.15*(P)-(Q) $);
        \coordinate (Row) at ($ .5*(P)-(Q) $);
        \coordinate (Null) at ($ .5*(Q)-.5*(P) $);

        \node at (Rn) {$\mathbb{R}^n$};

        \filldraw[ultra thick, fill=YellowOrange]
        (O) -- (P) -- ($ (P)-2*(Q) $) -- ($ -2*(Q) $) -- cycle;
        \node at (Row) {$\underset{\onslide<7->{\dim=?}}{\Row(A)}$};

        \filldraw[ultra thick, fill=Green!40]
        (O) -- (Q) -- ($ (Q)-(P) $) -- ($ -1*(P) $) -- cycle;
        \node at (Null) {$\underset{\onslide<5->{\scriptscriptstyle\dim=}\onslide<6->{n-\rank(A)}}{\Null(A)}$};

        \coordinate (v) at (-1, 2);
        \coordinate (w) at (-2, -1);
        \coordinate (Rm) at ($ 1.15*(v)-1*(w) $);
        \coordinate (Col) at ($ .5*(v)-1*(w) $);
        \coordinate (LNull) at ($ .5*(w)-.5*(v) $);

        \tikzset{
          c/.style={every coordinate/.try}
        }

        \begin{scope}[every coordinate/.style={shift={(5,0)}}]


          \node at ([c]Rm) {$\mathbb{R}^m$};

          \filldraw[ultra thick, fill=BrickRed!50]
          ([c]O) -- ([c]v) -- ([c]$ (v)-2*(w) $) -- ([c]$ -2*(w) $) -- cycle;
          \node at ([c]Col) {$\underset{\onslide<4->{\dim=\rank(A)}}{\Col(A)}$};

          \filldraw[ultra thick, fill=Blue!50]
          ([c]O) -- ([c]w) -- ([c]$ (w)-(v) $) -- ([c]$ -1*(v) $) -- cycle;
          \node at ([c]LNull) {$\LNull(A)$};


          \draw[ultra thick, shorten >=1.5em, shorten <=1.5em, ->]
          (Rn) -- ([c]Rm) node[midway, above] {$A$};

        \end{scope}

      \end{tikzpicture}
    \]
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    $\Row(A)=\Col(A^\intercal)$
  \end{block}

  \pause
  \begin{theorem}[``Pivot Basis'' of $\Row(A)$]
    The pivot columns of $A^\intercal$ form a basis of $\Row(A)$.
  \end{theorem}

  \pause
  \begin{theorem}[``Reduced Basis'' of $\Row(A)$]
    The nonzero rows of $\rref(A)$ form a basis of $\Row(A)$.
  \end{theorem}

  \pause
  \begin{theorem}
    $\dim\Row(A)=\rank(A)$
  \end{theorem}

\end{frame}


\begin{sagesilent}
  set_random_seed(110)
  A = random_matrix(ZZ, 4, 3, algorithm='echelonizable', rank=2)
  a1, a2, a3, a4 = A.rows()
  r1, r2, r3, r4= A.rref().rows()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the computations
    \begin{gather*}
      \begin{align*}
        \rref\overset{A}{\sage{A}} &= \sage{A.rref()} & \rref\overset{A^\intercal}{\sage{A.T}} &= \sage{A.T.rref()}
      \end{align*}
    \end{gather*}
    \pause The ``pivot basis'' and the ``reduced basis'' of $\Row(A)$ are given
    by
    \begin{gather*}
      \begin{align*}
        \overset{\myotherbold{\textnormal{``pivot basis''}}}{\sage{Set([a1, a2])}} && \overset{\myotherbold{\textnormal{``reduced basis''}}}{\Set{\sage{r1}, \sage{r2}}}
      \end{align*}
    \end{gather*}
    \pause Note that $\dim\Row(A)=\pause\rank(A)=\pause\sage{A.rank()}$.
  \end{example}

  \pause
  \begin{block}{Math-Speak}
    ``$\Row(A)$ is a \emph{two-dimensional} vector subspace of $\mathbb{R}^{\sage{A.ncols()}}$.''
  \end{block}
\end{frame}





\subsection{Bases of Left Null Spaces}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Questions}
    How can we find a basis of $\LNull(A)$? \onslide<2-> What is
    $\dim\LNull(A)$? \onslide<3->
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , scale=4/5
        ]

        \coordinate (O) at (0, 0);
        \coordinate (P) at (1, 2);
        \coordinate (Q) at (2, -1);
        \coordinate (Rn) at ($ 1.15*(P)-(Q) $);
        \coordinate (Row) at ($ .5*(P)-(Q) $);
        \coordinate (Null) at ($ .5*(Q)-.5*(P) $);

        \node at (Rn) {$\mathbb{R}^n$};

        \filldraw[ultra thick, fill=YellowOrange]
        (O) -- (P) -- ($ (P)-2*(Q) $) -- ($ -2*(Q) $) -- cycle;
        \node at (Row) {$\underset{\onslide<6->{\dim=}\onslide<7->{\rank(A)}}{\Row(A)}$};

        \filldraw[ultra thick, fill=Green!40]
        (O) -- (Q) -- ($ (Q)-(P) $) -- ($ -1*(P) $) -- cycle;
        \node at (Null) {$\underset{\scriptscriptstyle\onslide<5->{\dim=n-\rank(A)}}{\Null(A)}$};

        \coordinate (v) at (-1, 2);
        \coordinate (w) at (-2, -1);
        \coordinate (Rm) at ($ 1.15*(v)-1*(w) $);
        \coordinate (Col) at ($ .5*(v)-1*(w) $);
        \coordinate (LNull) at ($ .5*(w)-.5*(v) $);

        \tikzset{
          c/.style={every coordinate/.try}
        }

        \begin{scope}[every coordinate/.style={shift={(5,0)}}]


          \node at ([c]Rm) {$\mathbb{R}^m$};

          \filldraw[ultra thick, fill=BrickRed!50]
          ([c]O) -- ([c]v) -- ([c]$ (v)-2*(w) $) -- ([c]$ -2*(w) $) -- cycle;
          \node at ([c]Col) {$\underset{\onslide<4->{\dim=\rank(A)}}{\Col(A)}$};

          \filldraw[ultra thick, fill=Blue!50] ([c]O) -- ([c]w) --
          ([c]$ (w)-(v) $) -- ([c]$ -1*(v) $) -- cycle; \node at ([c]LNull)
          {$\underset{\onslide<8->{\dim=?}}{\LNull(A)}$};


          \draw[ultra thick, shorten >=1.5em, shorten <=1.5em, ->]
          (Rn) -- ([c]Rm) node[midway, above] {$A$};

        \end{scope}

      \end{tikzpicture}
    \]
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    $\LNull(A)=\Null(A^\intercal)$
  \end{block}

  \pause
  \begin{theorem}[``Pivot Basis'' of $\LNull(A)$]
    Use $\rref(A^\intercal)$ to write the general solution $\vv{x}$ to
    $A^\intercal\vv{x}=\vv{O}$ as
    \[
      \vv{x}
      = c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+\dotsb+c_d\cdot\vv*{v}{d}
    \]
    Then $\Set{\vv*{v}{1}, \vv*{v}{d}, \dotsc,\vv*{v}{d}}$ form a basis of
    $\LNull(A)$.
  \end{theorem}

  \pause
  \begin{theorem}
    $\dim\LNull(A)=\nullity(A^\intercal)\pause=\corank(A)$
  \end{theorem}

\end{frame}





\begin{sagesilent}
  a = vector([3, -6, 5, 2])
  A = matrix.column([a, -7*a, 11*a]).T
  var('x1 x2 x3')
  x = matrix.column([x1, x2, x3])
  var('c1 c2')
  xs = x(x1=7*c1-11*c2, x2=c1, x3=c2)
  B = jacobian(xs, (c1, c2))
  v1, v2 = map(matrix.column, B.columns())
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the computation
    \[
      \rref\overset{A^\intercal}{\sage{A.T}} = \sage{A.T.rref()}
    \]
    \pause So
    $\dim\LNull(A)=\pause\nullity(A^\intercal)=\pause\sage{A.left_nullity()}$. \pause
    Solving $A^\intercal\vv{x}=\vv{O}$ gives
    \[
      \vv{x}
      = \sage{x}
      = \sage{xs}
      = c_1\sage{v1}+c_2\sage{v2}
    \]
    \pause The ``pivot basis'' of $\LNull(A)$ is
    $\Set{\sage{vector(v1)}, \sage{vector(v2)}}$.
  \end{example}

\end{frame}



\section{The Rank-Nullity Theorem}
\subsection{Statement}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    The dimensions of the four fundamental subspaces of $A$ can be inferred from
    $\rank(A)$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , scale=4/5
        ]

        \coordinate (O) at (0, 0);
        \coordinate (P) at (1, 2);
        \coordinate (Q) at (2, -1);
        \coordinate (Rn) at ($ 1.15*(P)-(Q) $);
        \coordinate (Row) at ($ .5*(P)-(Q) $);
        \coordinate (Null) at ($ .5*(Q)-.5*(P) $);

        \node at (Rn) {$\mathbb{R}^n$};

        \filldraw[ultra thick, fill=YellowOrange]
        (O) -- (P) -- ($ (P)-2*(Q) $) -- ($ -2*(Q) $) -- cycle;
        \node at (Row) {$\underset{\onslide<6->{\dim=}\onslide<7->{\rank(A)}}{\Row(A)}$};

        \filldraw[ultra thick, fill=Green!40]
        (O) -- (Q) -- ($ (Q)-(P) $) -- ($ -1*(P) $) -- cycle;
        \node at (Null) {$\underset{\scriptscriptstyle\onslide<4->{\dim=}\onslide<5->{n-\rank(A)}}{\Null(A)}$};

        \coordinate (v) at (-1, 2);
        \coordinate (w) at (-2, -1);
        \coordinate (Rm) at ($ 1.15*(v)-1*(w) $);
        \coordinate (Col) at ($ .5*(v)-1*(w) $);
        \coordinate (LNull) at ($ .5*(w)-.5*(v) $);

        \tikzset{
          c/.style={every coordinate/.try}
        }

        \begin{scope}[every coordinate/.style={shift={(5,0)}}]


          \node at ([c]Rm) {$\mathbb{R}^m$};

          \filldraw[ultra thick, fill=BrickRed!50]
          ([c]O) -- ([c]v) -- ([c]$ (v)-2*(w) $) -- ([c]$ -2*(w) $) -- cycle;
          \node at ([c]Col) {$\underset{\onslide<2->{\dim=}\onslide<3->{\rank(A)}}{\Col(A)}$};

          \filldraw[ultra thick, fill=Blue!50]
          ([c]O) -- ([c]w) -- ([c]$ (w)-(v) $) -- ([c]$ -1*(v) $) -- cycle;
          \node at ([c]LNull) {$\underset{\scriptscriptstyle\onslide<8->{\dim=}\onslide<9->{m-\rank(A)}}{\LNull(A)}$};


          \draw[ultra thick, shorten >=1.5em, shorten <=1.5em, ->]
          (Rn) -- ([c]Rm) node[midway, above] {$A$};

        \end{scope}

      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    Every $m\times n$ matrix $A$ satisfies $\rank(A)+\nullity(A)=n$.
  \end{block}

  \pause
  \begin{theorem}[The Rank-Nullity Theorem]
    Every $m\times n$ matrix $A$ satisfies $\dim\Col(A)+\dim\Null(A)=n$.
  \end{theorem}

\end{frame}




\subsection{Example}
\begin{sagesilent}
  set_random_seed(3801)
  c1 = vector([1, -3, 2])
  c2 = vector([-4, 6, 8])
  n1 = vector([2, 3, 9])
  n2 = vector([7, -8, 4])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \noindent
  \begin{minipage}{.5\textwidth}
    \begin{example}
      Construct a matrix $A$ satisfying
      \begin{align*}
        \sage{c1}, \sage{c2} &\in\Col(A) \\
        \sage{n1}, \sage{n2} &\in\Null(A)
      \end{align*}
      \onslide<7->{Note that $A$ is $3\times 3$.}
    \end{example}
  \end{minipage}% This must go next to Y
  \begin{minipage}{.5\textwidth}
    \onslide<2->
    \[
      \begin{tikzpicture}[
        , scale=0.4
        , every node/.style={scale=0.4}
        , line join=round
        , line cap=round
        ]

        \coordinate (O) at (0, 0);
        \coordinate (P) at (1, 2);
        \coordinate (Q) at (2, -1);
        \coordinate (Rn) at ($ 1.15*(P)-(Q) $);
        \coordinate (Row) at ($ .5*(P)-(Q) $);
        \coordinate (Null) at ($ .5*(Q)-.5*(P) $);

        \node at (Rn) {$\mathbb{R}^{n\onslide<6->{=3}}$};

        \filldraw[ultra thick, fill=YellowOrange]
        (O) -- (P) -- ($ (P)-2*(Q) $) -- ($ -2*(Q) $) -- cycle;
        \node at (Row) {$\Row(A)$};

        \filldraw[ultra thick, fill=Green!40]
        (O) -- (Q) -- ($ (Q)-(P) $) -- ($ -1*(P) $) -- cycle;
        \node at (Null) {$\underset{\onslide<4->{\begin{matrix}\sage{n1}\\ \sage{n2}\end{matrix}}}{\Null(A)}$};

        \coordinate (v) at (-1, 2);
        \coordinate (w) at (-2, -1);
        \coordinate (Rm) at ($ 1.15*(v)-1*(w) $);
        \coordinate (Col) at ($ .5*(v)-1*(w) $);
        \coordinate (LNull) at ($ .5*(w)-.5*(v) $);

        \tikzset{
          c/.style={every coordinate/.try}
        }

        \begin{scope}[every coordinate/.style={shift={(5,0)}}]


          \node at ([c]Rm) {$\mathbb{R}^{m\onslide<5->{=3}}$};

          \filldraw[ultra thick, fill=BrickRed!50]
          ([c]O) -- ([c]v) -- ([c]$ (v)-2*(w) $) -- ([c]$ -2*(w) $) -- cycle;
          \node at ([c]Col) {$\underset{\onslide<3->{\begin{matrix}\sage{c1}\\ \sage{c2}\end{matrix}}}{\Col(A)}$};

          \filldraw[ultra thick, fill=Blue!50]
          ([c]O) -- ([c]w) -- ([c]$ (w)-(v) $) -- ([c]$ -1*(v) $) -- cycle;
          \node at ([c]LNull) {$\LNull(A)$};


          \draw[ultra thick, shorten >=1.5em, shorten <=1.5em, ->]
          (Rn) -- ([c]Rm) node[midway, above] {$A$};

        \end{scope}

      \end{tikzpicture}
    \]
  \end{minipage}

  \onslide<8->
  \begin{block}{Solution}
    The lists $\Set{\sage{c1}, \sage{c2}}$ and $\Set{\sage{n1}, \sage{n2}}$ are
    linearly independent\onslide<9->, so $\dim\Col(A)\geq \onslide<10->2$
    \onslide<11->and $\dim\Null(A)\geq \onslide<12->2$.  \onslide<13->But now
    \[
      \dim\Col(A)+\dim\Null(A)
      \geq \onslide<14->2+2
      = \onslide<15->4 > \onslide<16->3
    \]
    \onslide<17-> which contradicts the Rank-Nullity theorem. \onslide<18-> No
    such $A$ exists!
  \end{block}


\end{frame}


\end{document}