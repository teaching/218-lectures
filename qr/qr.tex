\documentclass[usenames,dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}

\title{$A=QR$ Factorizations}
\subtitle{Math 218}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Motivation}
\subsection{$A^\intercal A=I_n$ Makes Life Easier}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  At this stage, we have encountered the operations of \emph{transposition} and
  \emph{inversion}. \onslide<2->{Both operations are}
  \begin{align*}
    \onslide<3->{\textnormal{\myotherbold{order-reversing}}} && \onslide<4->{(AB)^\intercal &= B^\intercal A^\intercal} & \onslide<5->{(AB)^{-1} &= B^{-1}A^{-1}} \\
    \onslide<6->{\textnormal{\myotherbold{involutions}}}     && \onslide<7->{(A^\intercal)^\intercal &= A} & \onslide<8->{(A^{-1})^{-1} &= A}
  \end{align*}
  \onslide<9->{These operations help us study the system $A\vv{x}=\vv{b}$.}%
  \begin{description}
  \item<10->[approximations] multiply by $A^\intercal$ to obtain
    $A^\intercal A\widehat{x}=A^\intercal\vv{b}$
  \item<11->[exact solutions] multiply by $A^{-1}$ to obtain
    $A^{-1}A\vv{x}=A^{-1}\vv{b}$
  \end{description}

\end{frame}



\begin{sagesilent}
  set_random_seed(409855830)
  A = random_matrix(ZZ, 3)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Problem}
    Transposition and inversion behave similarly, but one is easier to compute
    than the other.

    \pause

    \begin{block}{Transposition is Easy}
      \[
        {\sage{A}}^\intercal=\sage{A.T}
      \]
    \end{block}

    \pause

    \begin{block}{Inversion is Hard}
      \[
        {\sage{A}}^{-1}=\sage{A.inverse()}
      \]
    \end{block}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation 1}
    Life would be easier if transposition were as useful as inversion and
    inversion were as simple as transposition.
  \end{block}

  \onslide<2->
  \begin{block}{Observation 2}
    When $A^\intercal A=I_n$, many ``difficult'' problems become ``easy.''
    \begin{description}
    \item<3->[projection] $P=A(A^\intercal A)^{-1}A^\intercal$ becomes
      \onslide<4->{$P=AA^\intercal$}
    \item<5->[least squares] $A^\intercal A\widehat{x}=A^\intercal\vv{b}$
      becomes \onslide<6->{$\widehat{x}=A^\intercal\vv{b}$}
    \end{description}
  \end{block}

\end{frame}


\begin{sagesilent}
  A = matrix.column([(sqrt(2)/2, 0, -sqrt(2)/2), (0, 1, 0)])
  c1, c2 = A.column_space().basis()
  l1, = A.left_kernel().basis()
  b = 3*c1-c2+l1
  bc = matrix.column(b)
\end{sagesilent}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $A$ and the vector $\vv{b}$ given by
    \newcommand{\myA}{
      \left[
        \begin{array}{rr}
          \frac{1}{\sqrt{2}} & 0 \\
          0 & 1 \\
          -\frac{1}{\sqrt{2}} & 0
        \end{array}
      \right]
    }
    \newcommand{\myAT}{
      \left[
        \begin{array}{rrr}
          \frac{1}{\sqrt{2}} & 0 & -\frac{1}{\sqrt{2}} \\
          0 & 1 & 0
        \end{array}
      \right]
    }
    \begin{align*}
      A &= \myA & \vv{b} &= \sage{bc}
    \end{align*}
    \pause Note that $A^\intercal A=I_{\sage{A.ncols()}}$. \pause Projection
    onto $\Col(A)$ is given by
    \[
      \overset{P_{\Col(A)}=AA^\intercal}{
        \sage{A*A.T}
      }
      =
      \overset{A}{\myA}\overset{A^\intercal}{\myAT}
    \]
    \pause The system $A^\intercal A\widehat{x}=A^\intercal\vv{b}$ is solved by
    $\widehat{x} = A^\intercal\vv{b} = \langle\frac{6}{\sqrt{2}}, -1\rangle$.
  \end{example}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{New Goal}
    Work with $Q$ satisfying $Q^\intercal Q=I_n$ rather than $A$ with
    $A^\intercal A\neq I_n$.
  \end{block}

\end{frame}

\subsection{Orthonormality and $Q^\intercal Q=I_n$}

\begin{sagesilent}
  e1, e2, e3 = identity_matrix(3).columns()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Recall our visual representation of $\mathbb{R}^3$.
  \begin{columns}[onlytextwidth, t]
    \column{.3\textwidth}
    \[
      \tdplotsetmaincoords{70}{110}
      \begin{tikzpicture}[tdplot_main_coords, scale=4/5]
        \draw[thick,<->] (-3,0,0) -- (3,0,0);
        \draw[thick,<->] (0,-2,0) -- (0,2,0);
        \draw[thick,<->] (0,0,-2) -- (0,0,2);

        \draw[ultra thick, ->, blue] (0, 0, 0) -- (2, 0, 0) node [above left] {$\vv*{e}{1}$};
        \draw[ultra thick, ->, red] (0, 0, 0) -- (0, 4/3, 0) node [below] {$\vv*{e}{2}$};
        \draw[ultra thick, ->, violet] (0, 0, 0) -- (0, 0, 4/3) node [right] {$\vv*{e}{3}$};
      \end{tikzpicture}
    \]
    \column{.7\textwidth}\pause
    \begin{block}{Standard Basis Vectors}
      $\vv*{e}{1}=\sage{e1}\, \vv*{e}{2}=\sage{e2}\, \vv*{e}{3}=\sage{e3}$
    \end{block}\pause
    \begin{block}{Unit Vectors}
      $\norm{\vv*{e}{1}}=1\quad \norm{\vv*{e}{2}}=1\quad \norm{\vv*{e}{3}}=1$
    \end{block}\pause
    \begin{block}{Mutually Orthogonal}
      $\vv*{e}{1}\cdot\vv*{e}{2}=0\quad \vv*{e}{1}\cdot\vv*{e}{3}=0\quad \vv*{e}{2}\cdot\vv*{e}{3}=0$
    \end{block}

  \end{columns}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Now, suppose that we \emph{rotate} the axes.
  \newcommand{\myOld}{
    \tdplotsetmaincoords{70}{110}
    \begin{tikzpicture}[tdplot_main_coords]
      \draw[thick,<->] (-3,0,0) -- (3,0,0);
      \draw[thick,<->] (0,-2,0) -- (0,2,0);
      \draw[thick,<->] (0,0,-2) -- (0,0,2);

      \draw[ultra thick, ->, blue] (0, 0, 0) -- (2, 0, 0) node [above left] {$\vv*{e}{1}$};
      \draw[ultra thick, ->, red] (0, 0, 0) -- (0, 4/3, 0) node [below] {$\vv*{e}{2}$};
      \draw[ultra thick, ->, violet] (0, 0, 0) -- (0, 0, 4/3) node [right] {$\vv*{e}{3}$};
    \end{tikzpicture}
  }
  \newcommand{\myNew}{
    \tdplotsetmaincoords{70}{110}
    \begin{tikzpicture}[tdplot_main_coords, rotate=30]
      \draw[thick,<->] (-3,0,0) -- (3,0,0);
      \draw[thick,<->] (0,-2,0) -- (0,2,0);
      \draw[thick,<->] (0,0,-2) -- (0,0,2);

      \draw[ultra thick, ->, blue] (0, 0, 0) -- (2, 0, 0) node [above left] {$\vv*{q}{1}$};
      \draw[ultra thick, ->, red] (0, 0, 0) -- (0, 4/3, 0) node [below] {$\vv*{q}{2}$};
      \draw[ultra thick, ->, violet] (0, 0, 0) -- (0, 0, 4/3) node [right] {$\vv*{q}{3}$};
    \end{tikzpicture}
  }
  \[
    \begin{tikzcd}[column sep=large]
      \myOld\arrow[]{r}{\textnormal{rotate}} \pgfmatrixnextcell\myNew
    \end{tikzcd}
  \]\pause
  After rotation, $\Set{\vv*{e}{1},\vv*{e}{2},\vv*{e}{3}}$ become
  $\Set{\vv*{q}{1},\vv*{q}{2},\vv*{q}{3}}$.

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Or, suppose that we \emph{reflect} the axes.
  \newcommand{\myOld}{
    \tdplotsetmaincoords{70}{110}
    \begin{tikzpicture}[tdplot_main_coords]
      \draw[thick,<->] (-3,0,0) -- (3,0,0);
      \draw[thick,<->] (0,-2,0) -- (0,2,0);
      \draw[thick,<->] (0,0,-2) -- (0,0,2);

      \draw[ultra thick, ->, blue] (0, 0, 0) -- (2, 0, 0) node [above left] {$\vv*{e}{1}$};
      \draw[ultra thick, ->, red] (0, 0, 0) -- (0, 4/3, 0) node [below] {$\vv*{e}{2}$};
      \draw[ultra thick, ->, violet] (0, 0, 0) -- (0, 0, 4/3) node [right] {$\vv*{e}{3}$};
    \end{tikzpicture}
  }
  \newcommand{\myNew}{
    \tdplotsetmaincoords{70}{110}
    \begin{tikzpicture}[tdplot_main_coords]
      \draw[thick,<->] (-3,0,0) -- (3,0,0);
      \draw[thick,<->] (0,-2,0) -- (0,2,0);
      \draw[thick,<->] (0,0,-2) -- (0,0,2);

      \draw[ultra thick, ->, blue] (0, 0, 0) -- (-2, 0, 0) node [above left] {$\vv*{q}{1}$};
      \draw[ultra thick, ->, red] (0, 0, 0) -- (0, 4/3, 0) node [below] {$\vv*{q}{2}$};
      \draw[ultra thick, ->, violet] (0, 0, 0) -- (0, 0, -4/3) node [right] {$\vv*{q}{3}$};
    \end{tikzpicture}
  }
  \[
    \begin{tikzcd}[column sep=large]
      \myOld\arrow[]{r}{\textnormal{reflect}} \pgfmatrixnextcell \myNew
    \end{tikzcd}
  \]\pause
  After reflection, $\Set{\vv*{e}{1},\vv*{e}{2},\vv*{e}{3}}$ become
  $\Set{\vv*{q}{1},\vv*{q}{2},\vv*{q}{3}}$.

\end{frame}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Rotation and Reflection Preserve Length}
    We have not ``stretched'' the axes, so
    \begin{align*}
      \norm{\vv*{q}{1}} &= 1 & \norm{\vv*{q}{2}} &= 1 & \norm{\vv*{q}{3}} &= 1
    \end{align*}\pause
    Each of $\Set{\vv*{q}{1}, \vv*{q}{2}, \vv*{q}{3}}$ is a unit vector.
  \end{block}

  \pause
  \begin{block}{Rotation and Reflection Preserve Angles}
    We have not altered any angles, so
    \begin{align*}
      \vv*{q}{1}\cdot\vv*{q}{2} &= 0 & \vv*{q}{1}\cdot\vv*{q}{3} &= 0 & \vv*{q}{2}\cdot\vv*{q}{3} &= 0
    \end{align*}\pause
    The list $\Set{\vv*{q}{1}, \vv*{q}{2}, \vv*{q}{3}}$ is \emph{mutually
      orthogonal}.
  \end{block}

\end{frame}


\begin{sagesilent}
  Q1 = matrix([[12, 5], [-5, 12]])
  d1 = 13
  Q2 = matrix.column([[4, 2, 2, -1], [-2, 4, -1, -2], [1, -2, -2, -4]])
  d2 = 5
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A mutually orthogonal list of unit vectors is called \emph{orthonormal}.
  \end{definition}

  \pause
  \begin{example}
    Each of the matrices
    \begin{align*}
      \sage{1/d1}\sage{Q1} && \sage{1/d2}\sage{Q2}
    \end{align*}
    has orthonormal columns.
  \end{example}

\end{frame}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    What do orthonormal lists have to do with $Q^\intercal Q=I_n$?
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myQ}{
    \left[
      \begin{array}{cccc}
        \vv*{q}{1} & \vv*{q}{2} & \cdots & \vv*{q}{n}
      \end{array}
    \right]
  }
  \newcommand{\myQT}{
    \left[
      \begin{array}{ccc}
        \vv*{q}{1} \\ \vv*{q}{2} \\ \vdots \\ \vv*{q}{n}
      \end{array}
    \right]
  }
  \newcommand{\myQTQ}{
    \left[
      \begin{array}{ccccc}
        \vv*{q}{1}\cdot\vv*{q}{1} & \vv*{q}{1}\cdot\vv*{q}{2} & \cdots & \vv*{q}{1}\cdot\vv*{q}{n}  \\
        \vv*{q}{2}\cdot\vv*{q}{1} & \vv*{q}{2}\cdot\vv*{q}{2} & \cdots & \vv*{q}{2}\cdot\vv*{q}{n}  \\
        \vdots                    & \vdots                    & \ddots & \vdots                     \\
        \vv*{q}{n}\cdot\vv*{q}{1} & \vv*{q}{n}\cdot\vv*{q}{2} & \cdots & \vv*{q}{n}\cdot\vv*{q}{n}
      \end{array}
    \right]
  }
  \newcommand{\myIn}{
    \left[
      \begin{array}{ccccc}
        1      & 0      & \cdots & 0      \\
        0      & 1      & \cdots & 0      \\
        \vdots & \vdots & \ddots & \vdots \\
        0      & 0      & \cdots & 1
      \end{array}
    \right]
  }
  \begin{block}{Observation}
    Suppose that the columns of $Q=\myQ$ are orthonormal. \onslide<2->{Then}
    \begin{gather*}
      \onslide<2->{\overset{Q^\intercal}{\myQT}\overset{Q}{\myQ}=}
      \onslide<3->{\overset{Q^\intercal Q}{\myQTQ=}}
      \onslide<4->{\overset{I_n}{\myIn}}
    \end{gather*}
    \onslide<5->{Matrices with orthonormal columns satisfy $Q^\intercal Q=I$.}
  \end{block}

\end{frame}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $Q^\intercal Q=I_n$ if and only if $Q$ has orthonormal columns.
  \end{theorem}

  \pause
  \begin{theorem}
    Suppose $Q$ has orthonormal columns. Then $P_{\Col(Q)}=QQ^\intercal$.
  \end{theorem}

  \pause
  \begin{theorem}
    Suppose $Q$ has orthonormal columns. Then the least squares approximate
    solution to $Q\vv{x}=\vv{b}$ is $\widehat{x}=Q^\intercal\vv{b}$.
  \end{theorem}

\end{frame}



\begin{sagesilent}
  Q_unscaled = matrix([(-2, -1), (-2, 2), (-1, -2)])
  scale = 1/3
  c1, c2 = Q_unscaled.column_space().basis()
  l1, = Q_unscaled.left_kernel().basis()
  b = c1+2*l1
  bc = matrix.column(b)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $Q$, whose columns are orthonormal
    \begin{align*}
      Q &= \sage{scale}\sage{Q_unscaled} & \vv{b} &= \sage{bc}
    \end{align*}
    \pause The least squares approximate solution to $Q\vv{x}=\vv{b}$ is
    \[
      \overset{\widehat{x}}{\sage{scale*Q_unscaled.T*bc}}
      =
      \overset{Q^\intercal}{\sage{scale}\sage{Q_unscaled.T}}
      \overset{\vv{b}}{\sage{bc}}
    \]
    \pause Note that $\vv{b}\notin\Col(Q)$ since the projection of $\vv{b}$ onto
    $\Col(Q)$ is
    \[
      P\vv{b}
      = QQ^\intercal\vv{b}
      = Q\widehat{x}
      = \sage{scale*Q_unscaled*scale*Q_unscaled.T*b}
      \neq \sage{b}
    \]
  \end{example}

\end{frame}

\section{$A=QR$ Factorizations}
\subsection{Definition and Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    What if we are working with $A$ and $A^\intercal A\neq I_n$?
  \end{block}

  \onslide<2->
  \begin{definition}
    A \emph{$QR$-factorization} of a matrix $A$ is an equation of the form
    \[
      \tikzmark{A}A=\tikzmark{Q}Q\tikzmark{R}R
    \]
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , remember picture
      , overlay
      ]

      \onslide<3->
      \draw[<-, thick, blue]
      ([shift={(3pt,-2pt)}]pic cs:A) |- ([shift={(-10pt, -10pt)}]pic cs:A)
      node[anchor=east] {$m\times n$ with rank $r$};

      \onslide<4->
      \draw[<-, thick, purple]
      ([shift={(4pt,-2pt)}]pic cs:Q) |- ([shift={(-15pt,-25pt)}]pic cs:Q)
      node[anchor=east] {$m\times r$ with $Q^\intercal Q=I_r$};

      \onslide<5->
      \draw[<-, thick, red]
      ([shift={(4pt,-2pt)}]pic cs:R) |- ([shift={(15pt,-25pt)}]pic cs:R)
      node[anchor=west] {$r\times n$ upper-triangular};
    \end{tikzpicture}
  \end{definition}

\end{frame}


\begin{sagesilent}
  Q = matrix(QQ, [[-2/3, -4/15, -1/5], [2/3, 1/5, -4/15], [1/3, -14/15, 2/15], [0, -2/15, -14/15]])
  R = matrix(QQ, [[3, -3, -2], [0, 15, -7], [0, 0, 1]])
  A = Q*R
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The equation
    \[
      \overset{A}{\sage{A}}
      =
      \overset{Q}{\sage{Q}}
      \overset{R}{\sage{R}}
    \]
    is a $QR$ factorization of $A$.
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The columns of $Q$ form a basis of $\Col(A)$.
  \end{theorem}

  \pause
  \begin{theorem}
    $\rank(A)=\rank(R)$
  \end{theorem}

  \pause
  \begin{theorem}
    $R$ is nonsingular if and only if $A$ has full column rank.
  \end{theorem}

  \pause
  \begin{theorem}
    $A$ and $R$ have the same Gramian.
  \end{theorem}
  \pause
  \begin{proof}
    $A^\intercal A
    \pause= (QR)^\intercal(QR)
    \pause= R^\intercal Q^\intercal QR
    \pause= R^\intercal R$
  \end{proof}

\end{frame}


\subsection{$A=QR$ and Projections}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $P_{\Col(A)}=QQ^\intercal$
  \end{theorem}
  \pause
  \begin{proof}
    The columns of $Q$ form a basis of $\Col(A)$, so $\Col(A)=\Col(Q)$. This
    means that $P_{\Col(A)}=P_{\Col(Q)}=QQ^\intercal$.
  \end{proof}

  \pause
  \begin{block}{Advantage}
    We now have two projection formulas
    \begin{align*}
      P_{\Col(A)} &= X(X^\intercal X)^{-1}X^\intercal & P_{\Col(A)} &= QQ^\intercal
    \end{align*}
    \pause We avoid the ``difficult'' inversion $(X^\intercal X)^{-1}$ with
    $A=QR$.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the $A=QR$ factorization
    \[
      \overset{A}{\sage{A}}
      =
      \overset{Q}{\sage{1/15}\sage{15*Q}}
      \overset{R}{\sage{R}}
    \]
    Projection onto $\Col(A)$ is given by
    \begin{gather*}
      \overset{P_{\Col(A)}}{\sage{1/15**2}\sage{15**2*Q*Q.T}}
      =
      \overset{Q}{\sage{1/15}\sage{15*Q}}
      \overset{Q^\intercal}{\sage{1/15}\sage{15*Q.T}}
    \end{gather*}
  \end{example}

\end{frame}



\subsection{$A=QR$ and Least Squares}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose that $A$ has full column rank. Then the least squares problem
    $A^\intercal A\widehat{x}=A^\intercal\vv{b}$ reduces to
    $R\widehat{x}=Q^\intercal\vv{b}$.
  \end{theorem}
  \pause
  \begin{proof}
    First, $A^\intercal A\widehat{x}=A^\intercal\vv{b}$ reduces to
    $R^\intercal R\widehat{x}=R^\intercal Q^\intercal\vv{b}$. \pause
    Left-multiplying by $(R^\intercal)^{-1}$ then gives
    $R\widehat{x}=Q^\intercal\vv{b}$.
  \end{proof}

  \pause
  \begin{block}{Advantage}
    We now have two forms of the least squares problem
    \begin{align*}
      A^\intercal A\widehat{x} &= A^\intercal\vv{b} & R\widehat{x} &= Q^\intercal\vv{b}
    \end{align*}
    \pause The system $R\widehat{x}=Q^\intercal\vv{b}$ is quickly solved with
    \pause \emph{back substitution}.
  \end{block}

\end{frame}



\begin{sagesilent}
  c1, c2, c3 = A.change_ring(ZZ).column_space().basis()
  l1, = A.change_ring(ZZ).left_kernel().basis()
  b = c1-c2+l1
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the $A=QR$ factorization and the vector $\vv{b}$ given by
    \begin{gather*}
      \begin{align*}
        \overset{A}{\sage{A}}
        &=
          \overset{Q}{\sage{1/15}\sage{15*Q}}
          \overset{R}{\sage{R}} & \vv{b} &= \sage{matrix.column(b)}
      \end{align*}
    \end{gather*}
    \onslide<2->{Note that $A^\intercal A\widehat{x}=A^\intercal\vv{b}$ reduces to
      $R\widehat{x}=Q^\intercal\vv{b}$}\onslide<3->{, which is}
    \[
      \onslide<4->{
      \begin{array}{rcrcrcrcl}
        3\,x_1 &-&  3\,x_2 &-& 2\,x_3 &=&  1 &\onslide<7->{\to& x_1=\oldfrac{1+3(0)+2(1)}{3}=1} \\
               & & 15\,x_2 &-& 7\,x_3 &=& -7 &\onslide<6->{\to& x_2=\oldfrac{-7+7(1)}{15}=0}    \\
               & &         & &    x_3 &=&  1 &\onslide<5->{\to& x_3=1}
      \end{array}
      }
    \]
    \onslide<8->{This gives $\widehat{x}=\sage{A.pseudoinverse()*b}$.}
  \end{example}

\end{frame}


\section{Computing $A=QR$}
\subsection{The Gram-Schmidt Algorithm}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    How can we compute $A=QR$?
  \end{block}

  \pause
  \begin{block}{Idea}
    Start by finding an \emph{orthonormal basis} of $\Col(A)$.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Can any basis $\Set{\vv*{v}{1}, \vv*{v}{2}}$ of $V$ be converted to an
    \emph{orthonormal basis} $\Set{\vv*{q}{1}, \vv*{q}{2}}$ of $V$?
    \[
      \begin{tikzpicture}[
        , scale=1
        , line join=round
        , line cap=round
        , transform shape
        , rotate=15
        ]
        \coordinate (O) at (0, 0);

        \coordinate (v1) at (4, 0);
        \coordinate (v2) at (3, 2);

        \coordinate (w1) at (v1);
        \coordinate (w2) at ($ (v2)-(O)!(v2)!(v1) $);

        \coordinate (q1) at ($ (O)!1cm!(w1) $);
        \coordinate (q2) at ($ (O)!1cm!(w2) $);

        \onslide<2-3>{
          \draw[ultra thick, blue, ->]
          (O) -- (v1) node[right] {$\vv*{v}{1}$};
        }

        \onslide<3->{
          \draw[ultra thick, blue, ->]
          (O) -- (v2) node[pos=1.1] {$\vv*{v}{2}$};
        }

        \pgfmathsetmacro{\myPerp}{.25}
        \onslide<7->{
          \draw[ultra thick]
          ($ \myPerp*(q1) $)
          -- ($ \myPerp*(q1)+\myPerp*(q2) $)
          -- ($ \myPerp*(q2) $);
        }

        \onslide<5-6>{
          \draw[ultra thick]
          ($ (O)!(v2)!(v1)+\myPerp*(q1)$)
          -- ($ (O)!(v2)!(v1)+\myPerp*(q1)+\myPerp*(q2) $)
          -- ($ (O)!(v2)!(v1)+\myPerp*(q2)$);
        }

        \onslide<5-6>{
          \draw[ultra thick, red, ->]
          ($ (O)!(v2)!(v1) $) -- (v2)
          node[midway, right] {$\vv*{w}{2}=\onslide<6->{\vv*{v}{2}-\proj_{\vv*{w}{1}}(\vv*{v}{2})}$};
        }

        \onslide<4->{
          \draw[ultra thick, red, ->]
          (O) -- (w1) node[right] {$\vv*{w}{1}=\vv*{v}{1}$};
        }

        \onslide<7->{
          \draw[ultra thick, red, ->]
          (O) -- (w2) node[above] {$\vv*{w}{2}=\vv*{v}{2}-\proj_{\vv*{w}{1}}(\vv*{v}{2})$};
        }

        \onslide<8->{
          \draw[ultra thick, purple, ->]
          (O) -- (q1) node[below] {$\vv*{q}{1}=\onslide<9->{\frac{\vv*{w}{1}}{\norm{\vv*{w}{1}}}}$};
        }

        \onslide<10->{
          \draw[ultra thick, purple, ->]
          (O) -- (q2) node[left] {$\vv*{q}{2}=\onslide<11->{\frac{\vv*{w}{2}}{\norm{\vv*{w}{2}}}}$};
        }
      \end{tikzpicture}
    \]
  \end{example}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{The Gram-Schmidt Algorithm}
    Let $\Set{\vv*{v}{1},\dotsc,\vv*{v}{d}}$ be a basis of
    $V\subset\mathbb{R}^n$. \onslide<2->{Define new vectors}
    \[\def\arraystretch{1.85}
      \begin{array}{rclrcl}
        \onslide<3->{\vv*{w}{1} &=& \vv*{v}{1}} & \onslide<9->{\vv*{q}{1} &=& \displaystyle\frac{\vv*{w}{1}}{\norm{\vv*{w}{1}}}} \\
        \onslide<4->{\vv*{w}{2} &=& \vv*{v}{2}-\proj_{\vv*{w}{1}}(\vv*{v}{2})} & \onslide<10->{\vv*{q}{2} &=& \displaystyle\frac{\vv*{w}{2}}{\norm{\vv*{w}{2}}}} \\
        \onslide<5->{\vv*{w}{3} &=& \vv*{v}{3}-\proj_{\vv*{w}{1}}(\vv*{v}{3})-\proj_{\vv*{w}{2}}(\vv*{v}{3})} & \onslide<11->{\vv*{q}{3} &=& \displaystyle\frac{\vv*{w}{3}}{\norm{\vv*{w}{3}}}} \\
                                &\onslide<6->{\vdots}&                                                                              &            &\onslide<12->{\vdots}& \\
        \onslide<7->{\vv*{w}{d} &=& \vv*{v}{d}-\proj_{\vv*{w}{1}}(\vv*{v}{d})-\dotsb-\proj_{\vv*{w}{d-1}}(\vv*{v}{d})} & \onslide<13->{\vv*{q}{d} &=& \displaystyle\frac{\vv*{w}{d}}{\norm{\vv*{w}{d}}}}
      \end{array}
    \]
    \onslide<8->{Then $\Set{\vv*{w}{1},\vv*{w}{2},\dotsc,\vv*{w}{d}}$ is a
      \emph{mutually orthogonal} basis of $V$} \onslide<14->{and
      $\Set{\vv*{q}{1},\vv*{q}{2},\dotsc,\vv*{q}{d}}$ is an \emph{orthonormal}
      basis of $V$.}
  \end{block}

\end{frame}

\begin{sagesilent}
  A = matrix(ZZ, [[0, -1, 9], [0, 2, 14], [1, -2, -4], [0, 2, -2]])
  v1, v2, v3 = A.columns()
  proj = lambda w, v: (w*v)/(w*w)*w
  w1 = v1
  w2 = v2 - proj(w1, v2)
  w3 = v3 - proj(w1, v3) - proj(w2, v3)
  q1 = w1.normalized()
  q2 = w2.normalized()
  q3 = w3.normalized()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

    Consider $V=\Span\Set{\vv*{v}{1}, \vv*{v}{2}, \vv*{v}{3}}$ where
    \begin{align*}
      \vv*{v}{1} &= \sage{v1} & \vv*{v}{2} &= \sage{v2} & \vv*{v}{3} &= \sage{v3}
    \end{align*}
    \onslide<2->{The vectors $\Set{\vv*{w}{1}, \vv*{w}{2}, \vv*{w}{3}}$ are
      given by}
    \begin{gather*}
      \begin{align*}
        \onslide<3->{\vv*{w}{1} &= \vv*{v}{1} = \sage{w1}} \\
        \onslide<4->{\vv*{w}{2} &= \vv*{v}{2}-\proj_{\vv*{w}{1}}(\vv*{v}{2})
                                  =} \onslide<5->{\sage{v2} - \oldfrac{\sage{w1}\cdot\sage{v2}}{\sage{w1}\cdot\sage{w1}}\sage{w1}
                                  =} \onslide<6->{\sage{w2}} \\
        \onslide<7->{\vv*{w}{3} &= \vv*{v}{3}-\proj_{\vv*{w}{1}}(\vv*{v}{3})-\proj_{\vv*{w}{2}}(\vv*{v}{3}) \\
                                &=} \onslide<8->{\sage{v3} - \oldfrac{\sage{w1}\cdot\sage{v3}}{\sage{w1}\cdot\sage{w1}}\sage{w1} - \oldfrac{\sage{w2}\cdot\sage{v3}}{\sage{w2}\cdot\sage{w2}}\sage{w2} \\
                                &=} \onslide<9->{\sage{16/3}\sage{3/16*w3}}
      \end{align*}
    \end{gather*}
    \onslide<10->{Our orthonormal basis $\Set{\vv*{q}{1}, \vv*{q}{2}, \vv*{q}{3}}$ of $V$ is then}
    \begin{gather*}
    \begin{align*}
      \onslide<11->{\vv*{q}{1} &= \sage{w1.normalized()}} & \onslide<12->{\vv*{q}{2} &= \sage{1/3}\sage{w2}} & \onslide<13->{\vv*{q}{3} &= \sage{1/3}\sage{3*w3.normalized()}}
    \end{align*}
  \end{gather*}

\end{frame}


\subsection{Using Gram-Schmidt to Compute $A=QR$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Computing $A=QR$ with Gram-Schmidt}
    Suppose $A$ has full column rank. \pause
    \begin{description}
    \item[Step 1] Use the Gram-Schmidt algorithm to find an orthonormal basis
      $\Set{\vv*{q}{1}, \vv*{q}{2}, \dotsc, \vv*{q}{d}}$ of $\Col(A)$.\pause
    \item[Step 2] Define $Q=\begin{bmatrix}\vv*{q}{1} & \vv*{q}{2} & \cdots & \vv*{q}{d}\end{bmatrix}$ \pause
    \item[Step 3] Define $R=Q^\intercal A$.
    \end{description}
    \pause This gives a $QR$-factorization $A=QR$.
  \end{block}

\end{frame}


\begin{sagesilent}
  Q = matrix.column([q1, q2, q3])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrices $A$ and $Q$ given by
    \begin{align*}
      A &= \sage{A} & Q &= \sage{Q}
    \end{align*}
    \pause We previously applied Gram-Schmidt to the columns of $A$ to obtain
    the columns of $Q$. \pause To complete the $QR$-factorization, we define
    \begin{gather*}
      \overset{R}{\sage{Q.T*A}}
      =
      \overset{Q^\intercal}{\sage{Q.T}}
      \overset{A}{\sage{A}}
    \end{gather*}

  \end{example}

\end{frame}



\end{document}
